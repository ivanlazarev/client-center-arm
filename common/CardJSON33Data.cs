﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rfidapp
{
    public class CardJSON33Data
    {
        public Byte FORMAT_ID { get; set; }
        public Byte FORMAT_VERSION { get; set; }
        public string CARD_NUMBER { get; set; }
        public UInt16 START_TSTMP { get; set; }
        public UInt16 END_TSTMP { get; set; }
        public UInt32 TRANSACTION_COUNTER { get; set; }
        public UInt32 TRANSACTION_TIME_TSTMP { get; set; }
        public string TRANSACTION_TERMINAL_ID { get; set; }
        public UInt32 STATUS_TIME_TSTMP { get; set; }
    }
}
