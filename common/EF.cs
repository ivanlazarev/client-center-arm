﻿using System;

namespace rfidapp
{
    public class MapEF
    {
        public Byte[] efFileId = new Byte[0];
        public Byte[] efFileSize = new Byte[0];
        public Byte[] efFileBuffer = new Byte[0]; 
    }
    public class EFConstants
    {
        public static Byte[] File1Id = new Byte[] { 0x00, 0x01 };
        public static Byte[] File1Size = new Byte[] { 0x00, 0x10 };
        public static Byte[] File1_34_Size = new Byte[] { 0x00, 0x1C };
        public static Byte[] File2Id = new Byte[] { 0x00, 0x02 };
        public static Byte[] File2Size = new Byte[] { 0x00, 0x1C };
        public static Byte[] File2_34_Size = new Byte[] { 0x00, 0x24 };

        public static Byte[] ControlInfoFileId = new Byte[] { 0x00, 0x01 };
        public static Byte[] ControlInfoFileSize = new Byte[] { 0x00, 0x0B };

        public static Byte[] Key1FileId = new Byte[] { 0x00, 0x02 };
        public static Byte[] Key1FileSize = new Byte[] { 0x00, 0x0B };

        public static Byte[] Key2FileId = new Byte[] { 0x00, 0x03 };
        public static Byte[] Key2FileSize = new Byte[] { 0x00, 0x0B };
    }
}