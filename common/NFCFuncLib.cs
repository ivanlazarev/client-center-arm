﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using rfidapp;

namespace rfidapp
{
    public class NFCFuncLib
    {
        //constants
        private const UInt16 M_SCOPE = 2;
        private const String m_emptyString = "";
        private const String VITAPATTERN = "VITA";
        private const int VITAOFFSET = 5;
        //private class variables
        private static int m_rtv = 0;
        //public class variables

        //********************************************************
        //Function Name: sCardEstablishContext
        //Input Parameter:-------
        //OutPutParameter:-------
        //Description:Perform action after time interval passed 
        //returns:
        //0 - sucess
        //1, 2 - errors
        public static UInt16 sCardEstablishContext(ref IntPtr context, String readerName)
        {
            if (!String.Equals(readerName, "") && !readerName.Equals(null))
            {
                m_rtv = HID.SCardEstablishContext(M_SCOPE, IntPtr.Zero, IntPtr.Zero, out context);
                if (m_rtv.Equals(0))
                    return 0; //SCardEstablishContext Successful 
                else
                {
                    System.Console.WriteLine("> SCardEstablishContext" + "   Failed... " + "   Error Code: " + String.Format("{0:x}", m_rtv));
                    return 1;
                }
            }
            else
            {
                System.Console.WriteLine("No readers available...");
                return 2;
            }
        }

        //public static UInt16 checkReader()
        //{


        //}

        //********************************************************
        //Function Name: getReaderData
        //Input Parameter:-------
        //OutPutParameter:-------
        //Description:Perform action after time interval passed 
        //********************************************************
        public static UInt16 getReaderData(ref IntPtr context, int valueTimeout, uint readerCount, ref HiDWinscard.SCARD_READERSTATE readerState, string readerList)
        {
            if (String.Equals(readerList, ""))// || String.Equals(readerList, null))
            {
                return 3; //No reader
            }
            else
            {
                m_rtv = HID.SCardGetStatusChange(context, valueTimeout, ref readerState, readerCount);
                if (m_rtv.Equals(0))
                {
                    if ((readerState.ATRLength.Equals(0)) || (!m_rtv.Equals(0)))
                    {
                        return 1; //SmartCard Removed;   
                    }
                }
                else
                {
                    return 2;
                }
            }
            return 0; //SmartCard Inserted 1
        }

        //********************************************************
        //Function Name:SCardReleaseContext
        //Input(Parameter) : sender, e
        //OutPutParameter:-------
        //Description:Release the context
        //********************************************************
        public static void SCardReleaseContext(ref IntPtr context)
        {
            m_rtv = HID.SCardReleaseContext(context);
        }

        //********************************************************
        //Function Name: Connect
        //Input(Parameter) : 
        //OutPutParameter:-------
        //Description:Connect to SmartCard
        //********************************************************
        public static UInt16 Connect(ref IntPtr context, String readerName, ref IntPtr hCard, ref IntPtr protocol)
        {
            m_rtv = HID.SCardConnect(context, readerName, HiDWinscard.SCARD_SHARE_SHARED, HiDWinscard.SCARD_PROTOCOL_T1, ref hCard, ref protocol); //Command to connect the card ,protocol T=1
            if (!m_rtv.Equals(0))
            {
                System.Console.WriteLine("> SCardConnect" + "   Failed... " + "   Error Code: " + String.Format("{0:x}", m_rtv));
                return 1;
            }
            return 0;
        }

        //********************************************************
        //Function Name:GetCardType
        //Description:Gives type of the card 
        //********************************************************
        public static UInt16 GetMifareCardType(ref IntPtr hCard,
                                            ref Byte[] sendBuffer,
                                            ref Byte[] receiveBuffer,
                                            ref ushort cardType,
                                            ref CardInfo cardInfo,
                                            ref NFCCardOps.CardOWMRDataStruct cardOWMRDataStruct,
                                            ref NFCCardOps.CardMWMRDataStruct cardMWMRDataStruct

                                                                )
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = Constants.DW_PROTOCOL;
            sioreq.cbPciLength = Constants.CB_PCI_LENGTH;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.dwProtocol = Constants.DW_PROTOCOL;
            rioreq.cbPciLength = Constants.CB_PCI_LENGTH;
            sendBuffer[0] = 0xFF;   //Class Byte
            sendBuffer[1] = 0xB0;   //Instruction Byte
            sendBuffer[2] = 0x0;    //Parameter Byte P1            
            sendBuffer[3] = 0x04;   //Parameter Byte P2
            sendBuffer[4] = 0x10;   //Lc/Le Byte
            int sendbufferlen = 0x5;
            int receivebufferlen = 0x12;
            m_rtv = HID.SCardTransmit(hCard, ref sioreq, sendBuffer, sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (m_rtv.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    Byte datalen = receiveBuffer[0];
                    switch (datalen)
                    {
                        case (byte)Constants.FORMAT_OLD_DATA_LENGTH:
                            cardType = 0;
                            return 0;
                        case (byte)Constants.ESEK_DATA_LENGTH:
                            cardType = 1;
                            return 0;
                        case (byte)Constants.FORMAT_2_5_DATA_LENGTH:
                            NFCCardOps.Process25CardData(receiveBuffer, ref cardInfo, ref cardOWMRDataStruct, ref cardMWMRDataStruct);
                            switch (cardOWMRDataStruct.FORMAT_VERSION)
                            {
                                case (5):
                                    cardType = 2;
                                    break;
                                case (6):
                                    cardType = 3;
                                    break;
                            }
                            return 0;
                        case 24:
                            cardType = 27;
                            return 0;
                        default:
                            return 1;   //wrong format
                    }
                }
                else
                    return 2; //wrong card
            }
            else
                return 3; //transmit failed
        }

        //********************************************************
        //Function Name:ReadCardDataBuffer
        //Description:Gives binary buffer of the card 
        //********************************************************
        public static UInt16 ReadCardDataBuffer(ref IntPtr hCard,
                                                ref Byte[] sendBuffer,
                                                ref Byte[] receiveBuffer,
                                                ref Byte[] cardDataBuffer,
                                                byte maxBlock)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = Constants.DW_PROTOCOL;
            sioreq.cbPciLength = Constants.CB_PCI_LENGTH;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.dwProtocol = Constants.DW_PROTOCOL;
            rioreq.cbPciLength = Constants.CB_PCI_LENGTH;
            byte packet = 0x10;
            byte step = 0x04;
            byte startBlock = 0x04;
            sendBuffer[0] = 0xFF;   //Class Byte
            sendBuffer[1] = 0xB0;   //Instruction Byte
            sendBuffer[2] = 0x00;    //Parameter Byte P1 
            sendBuffer[4] = packet;   //Lc/Le Byte
            int sendbufferlen = 0x05;
            int receivebufferlen = 0x12;
            int count = 0;
            for (byte block = startBlock; block < maxBlock; block += step)
            {
                sendBuffer[3] = block; //Parameter Byte P2
                m_rtv = HID.SCardTransmit(hCard, ref sioreq, sendBuffer, sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
                if (m_rtv.Equals(0))
                {
                    if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                    {
                        for (int i = 0; i < receivebufferlen - 2; i++)
                            cardDataBuffer[i + count * packet] = receiveBuffer[i];
                        count++;
                    }
                    else
                    {
                        System.Console.WriteLine("> SCardTransmit" + "   Failed(SW1 SW2 =" +
                            BitConverter.ToString(receiveBuffer, (receivebufferlen - 2), 1) + " " +
                                BitConverter.ToString(receiveBuffer, (receivebufferlen - 1), 1) + ")\n");
                        return 1; //wrong card
                    }
                }
                else
                {
                    System.Console.WriteLine("> SCardTransmit" + "   Failed... " + "   Error Code: " + String.Format("{0:x}", m_rtv) + "H\n");
                    return 2; //transmit failed
                }
            }
            return 0;
        }

        //********************************************************
        //Function Name: WriteCardDataBuffer
        //Description: Writes binary buffer of the card 
        //********************************************************
        public static UInt16 WriteCardDataBuffer(   ref IntPtr hCard,
                                                    Byte[] cardDataBuffer,
                                                    ref Byte[] sendBuffer,
                                                    ref Byte[] receiveBuffer,
                                                    int maxBlock    )
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = Constants.DW_PROTOCOL;
            sioreq.cbPciLength = Constants.CB_PCI_LENGTH;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = Constants.CB_PCI_LENGTH;
            rioreq.dwProtocol = Constants.DW_PROTOCOL;
            Byte currentBlock = 0x04;
            int sendbufferlen = 0x15;
            int receivebufferlen = 0x12;
            sendBuffer[0] = 0xFF;       //Class Byte
            sendBuffer[1] = 0xD6;       //Instruction Byte
            sendBuffer[2] = 0x00;       //Parameter Byte P1
            sendBuffer[4] = 0x10;       //Lc/Le Byte
            for (int i = 0; i < maxBlock; i++)
            {
                sendBuffer[3] = currentBlock;        //Parameter Byte P2
                for (int k1 = 0; k1 <= 15; k1++)
                    sendBuffer[k1 + 5] = cardDataBuffer[k1 + 16 * i];
                int m_rtv = HID.SCardTransmit(hCard, ref sioreq, sendBuffer, sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
                if (m_rtv.Equals(0))
                {
                    if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                    {
                        System.Console.WriteLine("page №: " + i);
                        currentBlock += 0x04;
                    }
                    else
                    {
                        System.Console.WriteLine("> UPDATE BINARY: Failed");
                        return 1;
                    }
                }
                else
                    return 2;
            }
            return 0;
        }

        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static UInt16 GetUID(ref IntPtr hCard, ref String uid, ref Byte[] sendBuffer, ref Byte[] receiveBuffer)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            uid = String.Empty;
            sendBuffer[0] = 0xFF;  //Class Byte
            sendBuffer[1] = 0xCA;  //Instruction Byte
            sendBuffer[2] = 0x0;   //Parameter Byte P1
            sendBuffer[3] = 0x0;   //Parameter Byte P2
            sendBuffer[4] = 0x0;   //Lc/Le Byte
            int sendbufferlen = 0x5;
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, sendBuffer, sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    StringBuilder hex1 = new StringBuilder((receivebufferlen - 2) * 2);
                    foreach (byte b in receiveBuffer)
                        hex1.AppendFormat("{0:X2}", b);
                    uid = hex1.ToString();
                    uid = uid.Substring(0, ((int)(receivebufferlen - 2)) * 2).ToLower();
                }
                else
                    return 1;
            }
            else
            {
                System.Console.WriteLine("> SCardTransmit" + "   Failed... " + "   Error Code: " + String.Format("{0:x}", retval));
                return 2;
            }
            return 0;
        }

        //********************************************************
        //Function Name: Disconnect
        //Input(Parameter) : sender, e
        //OutPutParameter:-------
        //Description:Disconnect the Smartcard
        //********************************************************
        public static void Disconnect(ref IntPtr hCard)
        {
            HID.SCardDisconnect(hCard, HiDWinscard.SCARD_UNPOWER_CARD); //Command to disconnect the card            
        }
    }
}
