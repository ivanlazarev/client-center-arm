using System;

namespace rfidapp
{
    public class CardInfo
    {
        public string cardId { get; set; }
        public string cardNum { get; set; }
        public UInt64 dateStart { get; set; }
        public UInt64 dateEnd { get; set; }
        public Byte cardType { get; set; }
        public UInt16 tripsLeft { get; set; }
    }

    public class ThrustCardInfoPart1
    {
        public Byte formatId { get; set; }
        public Byte formatVersion { get; set; }
        public String CardNumber { get; set; }
        public UInt64 StartTimestamp { get; set; }
        public UInt64 EndTimestamp { get; set; }
    }

    public class ThrustCardInfoPart2
    {
        public UInt32 TransactionCounter { get; set; }
        public UInt64 TransactionTimestamp { get; set; }
        public String TransactionTerminalID { get; set; }
        public UInt64 StatusTimestamp { get; set; }
        public String MAC { get; set; }
    }

    public class ThrustCardInfo
    {
        public ThrustCardInfoPart1 thrustCardInfoPart1 { get; set; }
        public ThrustCardInfoPart2 thrustCardInfoPart2 { get; set; }
        public String uid { get; set; }

        public ThrustCardInfo()
        {
            thrustCardInfoPart1 = new ThrustCardInfoPart1();
            thrustCardInfoPart2 = new ThrustCardInfoPart2();
        }
    }

    public class Msg
    {
        public string subject { get; set; }
        public Object data { get; set; }
    }

    public class CardJSONData
    {
        public Byte FORMAT { get; set; }
        public Byte FORMAT_VERSION { get; set; }
        public string CARD_NUMBER { get; set; }
    }

    public class Constants
    {
        public const ushort ESEK_DATA_LENGTH = 48;
        public const ushort FORMAT_OLD_DATA_LENGTH = 39;
        public const ushort FORMAT_2_5_DATA_LENGTH = 20;
        public const Byte DW_PROTOCOL = 0x2;
        public const ushort CB_PCI_LENGTH = 8;
        public const Byte MAX_TICKET_BLOCK = 0x10;
        public const Byte MAX_2_5_BLOCK = 0x16;
        public const ushort OWMR_BUFFER_LENGTH = 20;
        public const ushort MWMR_BUFFER_LENGTH = 27;
        public const ushort OWMR_27_BUFFER_LENGTH = 24;
        public const ushort MWMR_27_BUFFER_LENGTH = 27;
    }
}