﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using DamienG.Security.Cryptography;
using Newtonsoft.Json;
using System.IO;

namespace rfidapp
{
    public class NFCCardOps
    {
        private static int discarded;
        public const uint EPOCH = 1262304000;

        //card formats
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CardOWMRDataStruct
        {
            public Byte BLOCK_SIZE;
            public Byte FORMAT_ID;
            public Byte FORMAT_VERSION;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public Byte[] CARD_NUMBER;
            public Byte TICKET_TYPE;
            public UInt16 START_TSTMP;
            public UInt16 END_TSTMP;
            public UInt16 PREPAID_SERVICE_LIMIT;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CardMWMRDataStruct
        {
            public Byte BLOCK_SIZE;
            public UInt16 FIRST_SERVICE_TSTMP;
            public UInt32 TRANSACTION_TSTMP;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public Byte[] TRANSACTION_TERMINAL_ID;
            public UInt16 TRANSACTION_COUNTER;
            public UInt16 ACTUAL_SERVICE_COUNT;
            public Int64 CRC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CardMWMR26DataStruct
        {
            public Byte BLOCK_SIZE;
            public UInt16 FIRST_SERVICE_TSTMP;
            public UInt32 TRANSACTION_TSTMP;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public Byte[] TRANSACTION_TERMINAL_ID;
            public UInt16 TRANSACTION_COUNTER;
            public Int64 CRC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CardOWMR27DataStruct
        {
            public Byte BLOCK_SIZE;
            public Byte FORMAT_ID;
            public Byte FORMAT_VERSION;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public Byte[] CARD_NUMBER;
            public Byte TICKET_TYPE;
            public UInt32 START_TSTMP;
            public UInt32 END_TSTMP;
            public UInt16 PREPAID_SERVICE_LIMIT;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CardMWMR27DataStruct
        {
            public Byte BLOCK_SIZE;
            public UInt32 FIRST_SERVICE_TSTMP;
            public UInt32 TRANSACTION_TSTMP;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public Byte[] TRANSACTION_TERMINAL_ID;
            public UInt16 TRANSACTION_COUNTER;
            public Int64 CRC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CardDataStruct
        {
            public UInt16 BODY_LENGTH;
            public Byte FORMAT;
            public Byte FORMAT_VERSION;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public Byte[] CARD_NUMBER;
            public Byte CardType;
            public UInt32 START_TIME;
            public UInt32 END_TIME;
            public UInt32 LAST_TRANSACTION_TIME;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public Byte[] LAST_TERMINAL_ID;
            public UInt16 TRIPS_LEFT;
            public Int64 CRC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CardDataStructESEK
        {
            public UInt16 BODY_LENGTH;
            public Byte FORMAT;
            public Byte FORMAT_VERSION;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public Byte[] CARD_NUMBER;
            public UInt32 START_TIME;
            public UInt32 END_TIME;
            public UInt32 STATUS_TIME;
            public UInt32 LAST_TRANSACTION_TIME;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public Byte[] LAST_TERMINAL_ID;
            public UInt16 TRANSACTION_COUNTER;
            public Int64 CRC;
        }

        public static Byte[] SerializeMessage<T>(T msg) where T : struct
        {
            int objsize = Marshal.SizeOf(typeof(T));
            Byte[] ret = new Byte[objsize];
            IntPtr buff = Marshal.AllocHGlobal(objsize);
            Marshal.StructureToPtr(msg, buff, true);
            Marshal.Copy(buff, ret, 0, objsize);
            Marshal.FreeHGlobal(buff);
            return ret;
        }

        public static T DeserializeMsg<T>(Byte[] data) where T : struct
        {
            int objsize = Marshal.SizeOf(typeof(T));
            IntPtr buff = Marshal.AllocHGlobal(objsize);
            Marshal.Copy(data, 0, buff, objsize);
            T retStruct = (T)Marshal.PtrToStructure(buff, typeof(T));
            Marshal.FreeHGlobal(buff);
            return retStruct;
        }

        public static byte[] decimalStringToBytes(String val, int exactByteLength)
        {
            if (exactByteLength < 0)
            {
                exactByteLength = val.Length / 2 + (val.Length % 2);
            }
            Byte[] bb = new Byte[exactByteLength];
            for (int i = 0; i < exactByteLength; i++)
            {
                byte high = 10, low = 10; // when parse, numbers greater than 9 will be ignored
                if (i * 2 < val.Length)
                {
                    char c = val[i * 2];
                    high = (byte)((((int)c) - ((int)'0')) & 0x0F);
                }
                if (i * 2 + 1 < val.Length)
                {
                    char c = val[i * 2 + 1];
                    low = (byte)((((int)c) - ((int)'0')) & 0x0F);
                }
                bb[i] = ((byte)((high << 4) | low));
            }
            return bb.ToArray();
        }

        public static String decimalStringFromBytes(byte[] data, int start, int length)
        {
            if (length < 0)
            {
                length = data.Length - start;
            }
            StringBuilder bld = new StringBuilder();
            for (int i = start; i < start + length; i++)
            {
                int b = ((int)data[i]) & 0xFF;
                int high = (b >> 4) & 0x0F;
                int low = (b & 0x0F);
                if (high <= 9)
                {
                    bld.Append((char)(high + (int)'0'));
                }
                if (low <= 9)
                {
                    bld.Append((char)(low + (int)'0'));
                }
            }
            return bld.ToString();
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        public static void ProcessOldCardData(Byte[] cardDataBuffer,
                                                ref CardInfo cardInfo,
                                                ref CardDataStruct cardDataStruct)
        {
            Byte[] cardBuffer = CopyArray(cardDataBuffer, Constants.FORMAT_OLD_DATA_LENGTH, 8);
            object d = DeserializeMsg<CardDataStruct>(cardDataBuffer);
            CardDataStruct ds = (CardDataStruct)d;
            CardOldDataOutput(ds, cardBuffer, ref cardInfo, ref cardDataStruct);
        }

        ////********************************************************
        ////Function Name:ProcessOldCardData
        ////Description:
        ////********************************************************
        public static void ProcessESEKCardData(Byte[] cardDataBuffer,
                                                ref CardInfo cardInfo,
                                                ref CardDataStructESEK cardDataStructESEK)
        {
            Byte[] cardBufferESEK = CopyArray(cardDataBuffer, Constants.ESEK_DATA_LENGTH, 10);
            object dESEK = DeserializeMsg<CardDataStructESEK>(cardDataBuffer);
            CardDataStructESEK dsESEK = (CardDataStructESEK)dESEK;
            CardESEKDataOutput(dsESEK, cardBufferESEK, ref cardInfo, ref cardDataStructESEK);
        }


        //********************************************************
        //Function Name:CopyArray
        //Description:
        //********************************************************
        private static Byte[] CopyArray(Byte[] sourceArray,
                                            int arrayLength,
                                            int diff)
        {
            Byte[] resultArray = new Byte[arrayLength - diff];
            for (int i = 0; i < arrayLength - diff; i++)
                resultArray[i] = sourceArray[i + 2];
            return resultArray;
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        private static void CardOldDataOutput(CardDataStruct ds,
                                                    Byte[] cardBuffer,
                                                    ref CardInfo cardInfo,
                                                    ref CardDataStruct cardDataStruct)
        {
            String number = String.Empty;
            foreach (byte b in ds.CARD_NUMBER)
                number += (Char)b;
            Crc32 crc32proc = new Crc32();
            String hash = String.Empty;
            foreach (byte b in crc32proc.ComputeHash(cardBuffer)) hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            System.Console.WriteLine("BODY_LENGTH: " + ds.BODY_LENGTH);
            System.Console.WriteLine("FORMAT: " + ds.FORMAT);
            System.Console.WriteLine("FORMAT_VERSION: " + ds.FORMAT_VERSION);
            System.Console.WriteLine("CARD_NUMBER: " + number);
            System.Console.WriteLine("CardType: " + ds.CardType);
            System.Console.WriteLine("START_TIME: " + ds.START_TIME);
            System.Console.WriteLine("END_TIME: " + ds.END_TIME);
            System.Console.WriteLine("LAST_TRANSACTION_TIME: " + ds.LAST_TRANSACTION_TIME);
            System.Console.WriteLine("LAST_TERMINAL_ID: " + HexToBytenByteToHex.ToString(ds.LAST_TERMINAL_ID));
            System.Console.WriteLine("TRIPS_LEFT: " + ds.TRIPS_LEFT);
            System.Console.WriteLine("CRC: " + (UInt64)ds.CRC);
            System.Console.WriteLine(HexToBytenByteToHex.ToString(cardBuffer));
            System.Console.WriteLine("crc convert = {0}", f);
            cardInfo.cardNum = number;
            cardInfo.dateStart = ds.START_TIME;
            cardInfo.dateEnd = ds.END_TIME;
            cardInfo.cardType = ds.CardType;
            cardInfo.tripsLeft = ds.TRIPS_LEFT;
            cardDataStruct = ds;
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        private static void CardESEKDataOutput(CardDataStructESEK ds,
                                                    Byte[] cardBuffer,
                                                    ref CardInfo cardInfo,
                                                    ref CardDataStructESEK cardDataStructESEK)
        {
            String number = String.Empty;
            foreach (byte b in ds.CARD_NUMBER)
                number += (Char)b;
            Crc32 crc32proc = new Crc32();
            String hash = String.Empty;
            System.Console.WriteLine("BODY_LENGTH: " + ds.BODY_LENGTH);
            System.Console.WriteLine("FORMAT: " + ds.FORMAT);
            System.Console.WriteLine("FORMAT_VERSION: " + ds.FORMAT_VERSION);
            System.Console.WriteLine("CARD_NUMBER: " + number);
            System.Console.WriteLine("START_TIME: " + ds.START_TIME);
            System.Console.WriteLine("END_TIME: " + ds.END_TIME);
            System.Console.WriteLine("STATUS_TIME: " + ds.STATUS_TIME);
            System.Console.WriteLine("LAST_TRANSACTION_TIME: " + ds.LAST_TRANSACTION_TIME);
            System.Console.WriteLine("LAST_TERMINAL_ID: " + HexToBytenByteToHex.ToString(ds.LAST_TERMINAL_ID));
            System.Console.WriteLine("TRANSACTION_COUNTER: " + ds.TRANSACTION_COUNTER);
            System.Console.WriteLine("CRC: " + (UInt32)ds.CRC);
            foreach (byte b in crc32proc.ComputeHash(cardBuffer)) hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            System.Console.WriteLine("crc checksum = {0}", f);
            cardInfo.cardNum = number;
            cardInfo.dateStart = ds.START_TIME;
            cardInfo.dateEnd = ds.END_TIME;
            cardInfo.cardType = 10;
            cardInfo.tripsLeft = 0;
            cardDataStructESEK = ds;
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        public static void Process25CardData(Byte[] cardDataBuffer,
                                                ref CardInfo cardInfo,
                                                ref CardOWMRDataStruct cardOWMRDataStruct,
                                                ref CardMWMRDataStruct cardMWMRDataStruct)
        {
            Byte[] bufOWMR = new Byte[Constants.OWMR_BUFFER_LENGTH];
            Byte[] bufMWMR = new Byte[Constants.MWMR_BUFFER_LENGTH];
            String cardNumber = String.Empty;
            String terminalID = String.Empty;
            for (int i = 0; i < Constants.OWMR_BUFFER_LENGTH; i++)
                bufOWMR[i] = cardDataBuffer[i];
            object d = DeserializeMsg<CardOWMRDataStruct>(cardDataBuffer);
            cardOWMRDataStruct = (CardOWMRDataStruct)d;
            // OWMR field
            ProcessOWMROutput(cardOWMRDataStruct, ref cardNumber);
            cardInfo.cardNum = cardNumber;
            if (cardOWMRDataStruct.END_TSTMP.Equals(0))
                cardInfo.dateEnd = 0;
            else
                cardInfo.dateEnd = (uint)(cardOWMRDataStruct.END_TSTMP * 3600 * 24) + EPOCH;
            cardInfo.cardType = cardOWMRDataStruct.TICKET_TYPE;
            cardInfo.tripsLeft = cardOWMRDataStruct.PREPAID_SERVICE_LIMIT;
            // MWMR field 1
            for (int i = Constants.OWMR_BUFFER_LENGTH; i < Constants.OWMR_BUFFER_LENGTH + Constants.MWMR_BUFFER_LENGTH; i++)
                bufMWMR[i - Constants.OWMR_BUFFER_LENGTH] = cardDataBuffer[i];
            d = DeserializeMsg<CardMWMRDataStruct>(bufMWMR);
            cardMWMRDataStruct = (CardMWMRDataStruct)d;
            ProcessMWMROutput(cardMWMRDataStruct, ref terminalID);
            // MWMR field 1
            if (cardOWMRDataStruct.START_TSTMP.Equals(0))
                cardInfo.dateStart = 0;
            else
                cardInfo.dateStart = cardMWMRDataStruct.TRANSACTION_TSTMP + EPOCH;
            // MWMR field 2
            for (int i = 48; i < 75; i++)
                bufMWMR[i - 48] = cardDataBuffer[i];
            d = DeserializeMsg<CardMWMRDataStruct>(bufMWMR);
            cardMWMRDataStruct = (CardMWMRDataStruct)d;
            ProcessMWMROutput(cardMWMRDataStruct, ref terminalID);
            // MWMR field 2
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        public static void Process26CardData(Byte[] cardDataBuffer,
                                                ref CardInfo cardInfo,
                                                ref CardOWMRDataStruct cardOWMRDataStruct,
                                                ref CardMWMR26DataStruct cardMWMRDataStruct)
        {
            Byte[] bufOWMR = new Byte[Constants.OWMR_BUFFER_LENGTH];
            Byte[] bufMWMR = new Byte[Constants.MWMR_BUFFER_LENGTH];
            String cardNumber = String.Empty;
            String terminalID = String.Empty;
            for (int i = 0; i < Constants.OWMR_BUFFER_LENGTH; i++)
                bufOWMR[i] = cardDataBuffer[i];
            object d = DeserializeMsg<CardOWMRDataStruct>(cardDataBuffer);
            cardOWMRDataStruct = (CardOWMRDataStruct)d;
            // OWMR field
            ProcessOWMROutput(cardOWMRDataStruct, ref cardNumber);
            cardInfo.cardNum = cardNumber;
            if (cardOWMRDataStruct.END_TSTMP.Equals(0))
                cardInfo.dateEnd = 0;
            else
                cardInfo.dateEnd = (uint)(cardOWMRDataStruct.END_TSTMP * 3600 * 24) + EPOCH;
            cardInfo.cardType = cardOWMRDataStruct.TICKET_TYPE;
            cardInfo.tripsLeft = cardOWMRDataStruct.PREPAID_SERVICE_LIMIT;
            // MWMR field 1
            for (int i = Constants.OWMR_BUFFER_LENGTH; i < Constants.OWMR_BUFFER_LENGTH + Constants.MWMR_BUFFER_LENGTH; i++)
                bufMWMR[i - Constants.OWMR_BUFFER_LENGTH] = cardDataBuffer[i];
            d = DeserializeMsg<CardMWMR26DataStruct>(bufMWMR);
            cardMWMRDataStruct = (CardMWMR26DataStruct)d;
            ProcessMWMR26Output(cardMWMRDataStruct, ref terminalID);
            // MWMR field 1
            if (cardOWMRDataStruct.START_TSTMP.Equals(0))
                cardInfo.dateStart = 0;
            else
                cardInfo.dateStart = cardMWMRDataStruct.TRANSACTION_TSTMP + EPOCH;
            // MWMR field 2
            for (int i = 48; i < 75; i++)
                bufMWMR[i - 48] = cardDataBuffer[i];
            d = DeserializeMsg<CardMWMR26DataStruct>(bufMWMR);
            cardMWMRDataStruct = (CardMWMR26DataStruct)d;
            ProcessMWMR26Output(cardMWMRDataStruct, ref terminalID);
            // MWMR field 2
        }

        //********************************************************
        //Function Name:Process25OWMROutput
        //Description:
        //********************************************************
        private static void ProcessOWMROutput(CardOWMRDataStruct cardOWMRDataStruct,
                                                    ref String cardNumber)
        {
            cardNumber = decimalStringFromBytes(cardOWMRDataStruct.CARD_NUMBER, 0, 10);
            System.Console.WriteLine("OWMR field data:");
            System.Console.WriteLine("BLOCK_SIZE: " + cardOWMRDataStruct.BLOCK_SIZE);
            System.Console.WriteLine("FORMAT_ID: " + cardOWMRDataStruct.FORMAT_ID);
            System.Console.WriteLine("FORMAT_VERSION: " + cardOWMRDataStruct.FORMAT_VERSION);
            System.Console.WriteLine("CARD_NUMBER: " + cardNumber);
            System.Console.WriteLine("TICKET_TYPE: " + cardOWMRDataStruct.TICKET_TYPE);
            System.Console.WriteLine("END_TIME: " + cardOWMRDataStruct.END_TSTMP);
            System.Console.WriteLine("STATUS_TIME: " + cardOWMRDataStruct.TICKET_TYPE);
            System.Console.WriteLine("START_TSTMP: " + cardOWMRDataStruct.START_TSTMP);
            System.Console.WriteLine("END_TSTMP: " + cardOWMRDataStruct.END_TSTMP);
            System.Console.WriteLine("PREPAID_SERVICE_LIMIT: " + cardOWMRDataStruct.PREPAID_SERVICE_LIMIT);
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        private static void ProcessMWMROutput(CardMWMRDataStruct cardMWMRDataStruct,
                                                ref String terminalID)
        {
            System.Console.WriteLine("MWMR field data:");
            foreach (byte b in cardMWMRDataStruct.TRANSACTION_TERMINAL_ID)
                terminalID += (Char)b;
            System.Console.WriteLine("BLOCK_SIZE: " + cardMWMRDataStruct.BLOCK_SIZE);
            System.Console.WriteLine("FIRST_SERVICE_TSTMP: " + cardMWMRDataStruct.FIRST_SERVICE_TSTMP);
            System.Console.WriteLine("TRANSACTION_TSTMP: " + cardMWMRDataStruct.TRANSACTION_TSTMP);
            System.Console.WriteLine("TRANSACTION_TERMINAL_ID: " + terminalID);
            System.Console.WriteLine("TRANSACTION_COUNTER: " + cardMWMRDataStruct.TRANSACTION_COUNTER);
            System.Console.WriteLine("ACTUAL_SERVICE_COUNT: " + cardMWMRDataStruct.ACTUAL_SERVICE_COUNT);
            System.Console.WriteLine("CRC: " + cardMWMRDataStruct.CRC);
            terminalID = String.Empty;
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        private static void ProcessMWMR26Output(CardMWMR26DataStruct cardMWMRDataStruct,
                                                ref String terminalID)
        {
            System.Console.WriteLine("MWMR field data:");
            foreach (byte b in cardMWMRDataStruct.TRANSACTION_TERMINAL_ID)
                terminalID += (Char)b;
            System.Console.WriteLine("BLOCK_SIZE: " + cardMWMRDataStruct.BLOCK_SIZE);
            System.Console.WriteLine("FIRST_SERVICE_TSTMP: " + cardMWMRDataStruct.FIRST_SERVICE_TSTMP);
            System.Console.WriteLine("TRANSACTION_TSTMP: " + cardMWMRDataStruct.TRANSACTION_TSTMP);
            System.Console.WriteLine("TRANSACTION_TERMINAL_ID: " + terminalID);
            System.Console.WriteLine("TRANSACTION_COUNTER: " + cardMWMRDataStruct.TRANSACTION_COUNTER);
            System.Console.WriteLine("CRC: " + cardMWMRDataStruct.CRC);
            terminalID = String.Empty;
        }

        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static void PrepareOldCardDataBuffer(ref Byte[] cardDataBuffer,
                                                        ref CardDataStruct cardDataStruct)
        {
            String hash = String.Empty; //hash for crc32
            int cardCount = 0;
            Crc32 crc32proc = new Crc32();
            cardCount = 31;
            Byte[] cardBufferOld = new Byte[31];
            Byte[] sendCardData = new Byte[31];
            sendCardData = SerializeMessage<CardDataStruct>(cardDataStruct);
            for (int i = 0; i < cardCount; i++)
                cardBufferOld[i] = sendCardData[i + 2];
            foreach (byte b in crc32proc.ComputeHash(cardBufferOld)) hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            cardDataStruct.CRC = f;
            sendCardData = SerializeMessage<CardDataStruct>(cardDataStruct);
            for (int j = 0; j < sendCardData.Length - 1; j++)
                cardDataBuffer[j] = sendCardData[j];
        }

        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static void PrepareESEKCardDataBuffer(ref Byte[] cardDataBuffer,
                                                        ref CardDataStructESEK cardDataStructESEK)
        {
            String hash = String.Empty; //hash for crc32
            int cardCount = 0;
            Crc32 crc32proc = new Crc32();
            cardCount = 38;
            Byte[] cardBufferOld = new Byte[38];
            Byte[] sendCardData = new Byte[38];
            sendCardData = SerializeMessage<CardDataStructESEK>(cardDataStructESEK);
            for (int i = 0; i < cardCount; i++)
                cardBufferOld[i] = sendCardData[i + 2];
            foreach (byte b in crc32proc.ComputeHash(cardBufferOld)) hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            cardDataStructESEK.CRC = f;
            sendCardData = SerializeMessage<CardDataStructESEK>(cardDataStructESEK);
            for (int j = 0; j < sendCardData.Length - 1; j++)
                cardDataBuffer[j] = sendCardData[j];
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void Prepare25CardDataBuffer(ref Byte[] cardDataBuffer,
                                                        CardOWMRDataStruct cardOWMRData,
                                                        CardMWMRDataStruct cardMWMRData,
                                                        String uid,
                                                        Byte[] sault)
        {
            String hash = String.Empty; //hash for crc32
            Byte[] oWMRDataBytes = new Byte[20];
            Byte[] mWMRDataBytes = new Byte[27];
            Byte[] mWMRDataBytesNoCRC = new Byte[19];
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }
            oWMRDataBytes = SerializeMessage<CardOWMRDataStruct>(cardOWMRData); ; //serialize once writed data
            mWMRDataBytes = SerializeMessage<CardMWMRDataStruct>(cardMWMRData); //serialize once writed data with wrong crc
            for (int i = 0; i < mWMRDataBytes.Length - 8; i++)
                mWMRDataBytesNoCRC[i] = mWMRDataBytes[i];
            Byte[] rv = cardUIDBytes.Concat(oWMRDataBytes).Concat(mWMRDataBytesNoCRC).Concat(sault).ToArray();
            foreach (byte b in crc32proc.ComputeHash(rv))
                hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            cardMWMRData.CRC = f;
            mWMRDataBytes = SerializeMessage<CardMWMRDataStruct>(cardMWMRData); //serialize once writed data with wrong crc
            for (int i = 0; i < oWMRDataBytes.Length; i++)
                cardDataBuffer[i] = oWMRDataBytes[i];
            for (int i = 20; i < mWMRDataBytes.Length + 20; i++)
                cardDataBuffer[i] = mWMRDataBytes[i - 20];
            for (int i = 48; i < mWMRDataBytes.Length + 48; i++)
                cardDataBuffer[i] = mWMRDataBytes[i - 48];
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void Prepare26CardDataBuffer(ref Byte[] cardDataBuffer,
                                                        CardOWMRDataStruct cardOWMRData,
                                                        CardMWMR26DataStruct cardMWMRData,
                                                        String uid,
                                                        Byte[] sault)
        {
            String hash = String.Empty; //hash for crc32
            Byte[] oWMRDataBytes = new Byte[20];
            Byte[] mWMRDataBytes = new Byte[25];
            Byte[] mWMRDataBytesNoCRC = new Byte[17];
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }
            oWMRDataBytes = SerializeMessage<CardOWMRDataStruct>(cardOWMRData); ; //serialize once writed data
            mWMRDataBytes = SerializeMessage<CardMWMR26DataStruct>(cardMWMRData); //serialize once writed data with wrong crc
            for (int i = 0; i < mWMRDataBytes.Length - 8; i++)
                mWMRDataBytesNoCRC[i] = mWMRDataBytes[i];
            Byte[] rv = cardUIDBytes.Concat(oWMRDataBytes).Concat(mWMRDataBytesNoCRC).Concat(sault).ToArray();
            foreach (byte b in crc32proc.ComputeHash(rv))
                hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            cardMWMRData.CRC = f;
            mWMRDataBytes = SerializeMessage<CardMWMR26DataStruct>(cardMWMRData); //serialize once writed data with wrong crc
            for (int i = 0; i < oWMRDataBytes.Length; i++)
                cardDataBuffer[i] = oWMRDataBytes[i];
            for (int i = 20; i < mWMRDataBytes.Length + 20; i++)
                cardDataBuffer[i] = mWMRDataBytes[i - 20];
            for (int i = 48; i < mWMRDataBytes.Length + 48; i++)
                cardDataBuffer[i] = mWMRDataBytes[i - 48];
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void Prepare26CardPersonalizationDataBuffer(  ref Byte[] cardDataBuffer,
                                                                    CardOWMRDataStruct cardOWMRData,
                                                                    CardMWMR26DataStruct cardMWMRData,
                                                                    String uid,
                                                                    Byte[] sault,
                                                                    String cardFile )
        {
            String hash = String.Empty; //hash for crc32
            String tickCount = System.Diagnostics.Stopwatch.GetTimestamp().ToString();
            Byte[] bbp = decimalStringToBytes(tickCount, 10);
            Byte[] oWMRDataBytes = new Byte[20];
            Byte[] mWMRDataBytes = new Byte[25];
            Byte[] mWMRDataBytesNoCRC = new Byte[17];
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }
            // read file into a string and deserialize JSON to a type
            CardJSON26Data cardJSON26Data = JsonConvert.DeserializeObject<CardJSON26Data>(File.ReadAllText(cardFile));
            cardOWMRData.BLOCK_SIZE = 0x14;
            cardOWMRData.FORMAT_ID = cardJSON26Data.FORMAT_ID;
            cardOWMRData.FORMAT_VERSION = cardJSON26Data.FORMAT_VERSION;
            cardOWMRData.CARD_NUMBER = bbp;
            cardOWMRData.TICKET_TYPE = cardJSON26Data.TICKET_TYPE;
            cardOWMRData.START_TSTMP = cardJSON26Data.START_TSTMP;
            cardOWMRData.END_TSTMP = cardJSON26Data.END_TSTMP;
            cardOWMRData.PREPAID_SERVICE_LIMIT = cardJSON26Data.PREPAID_SERVICE_LIMIT;
            oWMRDataBytes = SerializeMessage<CardOWMRDataStruct>(cardOWMRData); //serialize once writed data
            cardMWMRData.BLOCK_SIZE = 0x19;
            cardMWMRData.FIRST_SERVICE_TSTMP = cardJSON26Data.FIRST_SERVICE_TSTMP;
            cardMWMRData.TRANSACTION_TSTMP = cardJSON26Data.TRANSACTION_TSTMP;
            cardMWMRData.TRANSACTION_TERMINAL_ID = HexToBytenByteToHex.GetBytes(cardJSON26Data.TRANSACTION_TERMINAL_ID, out discarded);
            cardMWMRData.TRANSACTION_COUNTER = cardJSON26Data.TRANSACTION_COUNTER;
            mWMRDataBytes = SerializeMessage<CardMWMR26DataStruct>(cardMWMRData); //serialize once writed data with wrong crc
            for (int i = 0; i < mWMRDataBytes.Length - 8; i++)
                mWMRDataBytesNoCRC[i] = mWMRDataBytes[i];
            Byte[] rv = cardUIDBytes.Concat(oWMRDataBytes).Concat(mWMRDataBytesNoCRC).Concat(sault).ToArray();
            foreach (byte b in crc32proc.ComputeHash(rv))
                hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            cardMWMRData.CRC = f;
            mWMRDataBytes = SerializeMessage<CardMWMR26DataStruct>(cardMWMRData); //serialize once writed data with wrong crc
            for (int i = 0; i < oWMRDataBytes.Length; i++)
                cardDataBuffer[i] = oWMRDataBytes[i];
            for (int i = 20; i < mWMRDataBytes.Length + 20; i++)
                cardDataBuffer[i] = mWMRDataBytes[i - 20];
            for (int i = 48; i < mWMRDataBytes.Length + 48; i++)
                cardDataBuffer[i] = mWMRDataBytes[i - 48];
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void Prepare27CardPersonalizationDataBuffer(ref Byte[] cardDataBuffer,
                                                                    CardOWMR27DataStruct cardOWMRData,
                                                                    CardMWMR27DataStruct cardMWMRData,
                                                                    String uid,
                                                                    Byte[] sault,
                                                                    String cardFile)
        {
            String hash = String.Empty; //hash for crc32
            String tickCount = System.Diagnostics.Stopwatch.GetTimestamp().ToString();
            Byte[] bbp = decimalStringToBytes(tickCount, 10);
            Byte[] oWMRDataBytes = new Byte[24];
            Byte[] mWMRDataBytes = new Byte[27];
            Byte[] mWMRDataBytesNoCRC = new Byte[19];
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }
            // read file into a string and deserialize JSON to a type
            CardJSON27Data cardJSON27Data = JsonConvert.DeserializeObject<CardJSON27Data>(File.ReadAllText(cardFile));
            cardOWMRData.BLOCK_SIZE = 0x18;
            cardOWMRData.FORMAT_ID = cardJSON27Data.FORMAT_ID;
            cardOWMRData.FORMAT_VERSION = cardJSON27Data.FORMAT_VERSION;
            cardOWMRData.CARD_NUMBER = bbp;
            cardOWMRData.TICKET_TYPE = cardJSON27Data.TICKET_TYPE;
            cardOWMRData.START_TSTMP = cardJSON27Data.START_TSTMP;
            cardOWMRData.END_TSTMP = cardJSON27Data.END_TSTMP;
            cardOWMRData.PREPAID_SERVICE_LIMIT = cardJSON27Data.PREPAID_SERVICE_LIMIT;
            oWMRDataBytes = SerializeMessage<CardOWMR27DataStruct>(cardOWMRData); //serialize once writed data
            cardMWMRData.BLOCK_SIZE = 0x1B;
            cardMWMRData.FIRST_SERVICE_TSTMP = cardJSON27Data.FIRST_SERVICE_TSTMP;
            cardMWMRData.TRANSACTION_TSTMP = cardJSON27Data.TRANSACTION_TSTMP;
            cardMWMRData.TRANSACTION_TERMINAL_ID = HexToBytenByteToHex.GetBytes(cardJSON27Data.TRANSACTION_TERMINAL_ID, out discarded);
            cardMWMRData.TRANSACTION_COUNTER = cardJSON27Data.TRANSACTION_COUNTER;
            mWMRDataBytes = SerializeMessage<CardMWMR27DataStruct>(cardMWMRData); //serialize once writed data with wrong crc
            for (int i = 0; i < mWMRDataBytes.Length - 8; i++)
                mWMRDataBytesNoCRC[i] = mWMRDataBytes[i];
            Byte[] rv = cardUIDBytes.Concat(oWMRDataBytes).Concat(mWMRDataBytesNoCRC).Concat(sault).ToArray();
            foreach (byte b in crc32proc.ComputeHash(rv))
                hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            cardMWMRData.CRC = f;
            mWMRDataBytes = SerializeMessage<CardMWMR27DataStruct>(cardMWMRData); //serialize once writed data with wrong crc
            for (int i = 0; i < oWMRDataBytes.Length; i++)
                cardDataBuffer[i] = oWMRDataBytes[i];
            for (int i = 24; i < mWMRDataBytes.Length + 24; i++)
                cardDataBuffer[i] = mWMRDataBytes[i - 24];
            for (int i = 52; i < mWMRDataBytes.Length + 52; i++)
                cardDataBuffer[i] = mWMRDataBytes[i - 52];
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        public static void Process27CardData(Byte[] cardDataBuffer,
                                                ref CardInfo cardInfo,
                                                ref CardOWMR27DataStruct cardOWMRDataStruct,
                                                ref CardMWMR27DataStruct cardMWMRDataStruct)
        {
            Byte[] bufOWMR = new Byte[Constants.OWMR_27_BUFFER_LENGTH];
            Byte[] bufMWMR = new Byte[27];
            String cardNumber = String.Empty;
            String terminalID = String.Empty;
            for (int i = 0; i < Constants.OWMR_BUFFER_LENGTH; i++)
                bufOWMR[i] = cardDataBuffer[i];
            object d = DeserializeMsg<CardOWMR27DataStruct>(cardDataBuffer);
            cardOWMRDataStruct = (CardOWMR27DataStruct)d;
            // OWMR field
            ProcessOWMR27Output(cardOWMRDataStruct, ref cardNumber);
            cardInfo.cardNum = cardNumber;
            cardInfo.dateStart = cardOWMRDataStruct.START_TSTMP;
            cardInfo.dateEnd = cardOWMRDataStruct.END_TSTMP;
            cardInfo.cardType = cardOWMRDataStruct.TICKET_TYPE;
            cardInfo.tripsLeft = cardOWMRDataStruct.PREPAID_SERVICE_LIMIT;
            // MWMR field 1
            for (int i = Constants.OWMR_27_BUFFER_LENGTH; i < Constants.OWMR_27_BUFFER_LENGTH + Constants.MWMR_27_BUFFER_LENGTH; i++)
                bufMWMR[i - Constants.OWMR_27_BUFFER_LENGTH] = cardDataBuffer[i];
            d = DeserializeMsg<CardMWMR27DataStruct>(bufMWMR);
            cardMWMRDataStruct = (CardMWMR27DataStruct)d;
            ProcessMWMR27Output(cardMWMRDataStruct, ref terminalID);
            // MWMR field 1

            // MWMR field 2
            for (int i = 52; i < 79; i++)
                bufMWMR[i - 52] = cardDataBuffer[i];
            d = DeserializeMsg<CardMWMR27DataStruct>(bufMWMR);
            cardMWMRDataStruct = (CardMWMR27DataStruct)d;
            ProcessMWMR27Output(cardMWMRDataStruct, ref terminalID);
            // MWMR field 2
        }

        //********************************************************
        //Function Name:Process25OWMROutput
        //Description:
        //********************************************************
        private static void ProcessOWMR27Output(CardOWMR27DataStruct cardOWMRDataStruct,
                                                    ref String cardNumber)
        {
            cardNumber = decimalStringFromBytes(cardOWMRDataStruct.CARD_NUMBER, 0, 10);
            System.Console.WriteLine("OWMR field data:");
            System.Console.WriteLine("BLOCK_SIZE: " + cardOWMRDataStruct.BLOCK_SIZE);
            System.Console.WriteLine("FORMAT_ID: " + cardOWMRDataStruct.FORMAT_ID);
            System.Console.WriteLine("FORMAT_VERSION: " + cardOWMRDataStruct.FORMAT_VERSION);
            System.Console.WriteLine("CARD_NUMBER: " + cardNumber);
            System.Console.WriteLine("TICKET_TYPE: " + cardOWMRDataStruct.TICKET_TYPE);
            System.Console.WriteLine("END_TIME: " + cardOWMRDataStruct.END_TSTMP);
            System.Console.WriteLine("STATUS_TIME: " + cardOWMRDataStruct.TICKET_TYPE);
            System.Console.WriteLine("START_TSTMP: " + cardOWMRDataStruct.START_TSTMP);
            System.Console.WriteLine("END_TSTMP: " + cardOWMRDataStruct.END_TSTMP);
            System.Console.WriteLine("PREPAID_SERVICE_LIMIT: " + cardOWMRDataStruct.PREPAID_SERVICE_LIMIT);
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        private static void ProcessMWMR27Output(CardMWMR27DataStruct cardMWMRDataStruct,
                                                ref String terminalID)
        {
            System.Console.WriteLine("MWMR field data:");
            foreach (byte b in cardMWMRDataStruct.TRANSACTION_TERMINAL_ID)
                terminalID += (Char)b;
            System.Console.WriteLine("BLOCK_SIZE: " + cardMWMRDataStruct.BLOCK_SIZE);
            System.Console.WriteLine("FIRST_SERVICE_TSTMP: " + cardMWMRDataStruct.FIRST_SERVICE_TSTMP);
            System.Console.WriteLine("TRANSACTION_TSTMP: " + cardMWMRDataStruct.TRANSACTION_TSTMP);
            System.Console.WriteLine("TRANSACTION_TERMINAL_ID: " + terminalID);
            System.Console.WriteLine("TRANSACTION_COUNTER: " + cardMWMRDataStruct.TRANSACTION_COUNTER);
            System.Console.WriteLine("CRC: " + cardMWMRDataStruct.CRC);
            terminalID = String.Empty;
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void Prepare27CardDataBuffer(ref Byte[] cardDataBuffer,
                                                        CardOWMR27DataStruct cardOWMRData,
                                                        CardMWMR27DataStruct cardMWMRData,
                                                        String uid,
                                                        Byte[] sault)
        {
            String hash = String.Empty; //hash for crc32
            Byte[] oWMRDataBytes = new Byte[24];
            Byte[] mWMRDataBytes = new Byte[27];
            Byte[] mWMRDataBytesNoCRC = new Byte[19];
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }
            oWMRDataBytes = SerializeMessage<CardOWMR27DataStruct>(cardOWMRData); ; //serialize once writed data
            mWMRDataBytes = SerializeMessage<CardMWMR27DataStruct>(cardMWMRData); //serialize once writed data with wrong crc
            for (int i = 0; i < mWMRDataBytes.Length - 8; i++)
                mWMRDataBytesNoCRC[i] = mWMRDataBytes[i];
            Byte[] rv = cardUIDBytes.Concat(oWMRDataBytes).Concat(mWMRDataBytesNoCRC).Concat(sault).ToArray();
            foreach (byte b in crc32proc.ComputeHash(rv))
                hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            cardMWMRData.CRC = f;
            mWMRDataBytes = SerializeMessage<CardMWMR27DataStruct>(cardMWMRData); //serialize once writed data with wrong crc
            for (int i = 0; i < oWMRDataBytes.Length; i++)
                cardDataBuffer[i] = oWMRDataBytes[i];
            for (int i = 24; i < mWMRDataBytes.Length + 24; i++)
                cardDataBuffer[i] = mWMRDataBytes[i - 24];
            for (int i = 52; i < mWMRDataBytes.Length + 52; i++)
                cardDataBuffer[i] = mWMRDataBytes[i - 52];
        }

    }
}
