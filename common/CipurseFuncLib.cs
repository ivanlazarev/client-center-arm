﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace rfidapp
{
    public class CipurseFuncLib
    {
        //constants
        private const UInt16 M_SCOPE = 2;
        private const String m_emptyString = "";
        private const String VITAPATTERN = "VITA";
        private const int VITAOFFSET = 5;
        public const String APP_AID = "ESEK_APP";
        public const String KEYCHAIN_APP_AID = "KEYCHAIN_APP";
        public static readonly Byte[] APP_FILE_ID = new Byte[] { 0x01, 0x03 };
        public static readonly Byte[] KEYCHAIN_FILE_ID = new Byte[] { 0x01, 0x04 };

        //private class variables
        private static int m_rtv = 0;

        //public class variables
        //private static WORD_CONVERT(x) ((x >> 8) | ((x & 0xff) << 8));
        
        private static void succProc(Byte[] buf, int buflen, ref String res)
        {
            String input = String.Empty;
            input = HexToBytenByteToHex.ToString(buf);
            StringBuilder hex1 = new StringBuilder((buflen - 2) * 2);
            foreach (byte b in buf)
                hex1.AppendFormat("{0:X2}", b);
            res = hex1.ToString();
            res = res.Substring(0, ((int)(buflen - 2)) * 2).ToLower();
        }

        private static void errProc(Byte[] buf, int buflen, ref String res)
        {
            String input = String.Empty;
            input = HexToBytenByteToHex.ToString(buf);
            StringBuilder hex1 = new StringBuilder((buflen - 2) * 2);
            foreach (byte b in buf)
                hex1.AppendFormat("{0:X2}", b);
            res = hex1.ToString();
            res = res.Substring(0, 4);
        }

        //********************************************************
        //Function Name:GetCipurseCardType
        //Description:Gives type of the card 
        //********************************************************
        public static UInt16 GetCipurseCardType(ref IntPtr hCard,
                                                    ref Byte[] sendBuffer,
                                                    ref Byte[] receiveBuffer)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = Constants.DW_PROTOCOL;
            sioreq.cbPciLength = Constants.CB_PCI_LENGTH;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.dwProtocol = Constants.DW_PROTOCOL;
            rioreq.cbPciLength = Constants.CB_PCI_LENGTH;
            sendBuffer[0] = 0xFF;   //Class Byte
            sendBuffer[1] = 0xCA;   //Instruction Byte
            sendBuffer[2] = 0x01;    //Parameter Byte P1            
            sendBuffer[3] = 0x00;   //Parameter Byte P2
            sendBuffer[4] = 0x00;   //Lc/Le Byte
            int sendbufferlen = 0x5;
            int receivebufferlen = 0xFF;
            String rtvStr = String.Empty;
            m_rtv = HID.SCardTransmit(hCard, ref sioreq, sendBuffer, sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (m_rtv.Equals(0) && ((receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))))
            {
                rtvStr = Encoding.Default.GetString(receiveBuffer, VITAOFFSET, VITAPATTERN.Length);
                if (!rtvStr.Equals(VITAPATTERN))
                    return 1;
            }
            else
                return 2;
            return 0;
        }

        public static UInt16 GetCipurseUIDCycle(ref IntPtr hCard, ref String uid)
        {
            UInt16 retry = 0;
            String response = String.Empty;
            while (!CipurseFuncLib.GetCipurseUID(ref hCard, ref response).Equals(0))
            {
                if (retry.Equals(5))
                {
                    System.Console.WriteLine("Cipurse card UID error : " + response);
                    return 1;
                }
                retry++;
                System.Threading.Thread.Sleep(100);
            }
            uid = response;
            System.Console.WriteLine("Cipurse card UID : " + uid);
            return 0;            
        }

        //********************************************************
        //Function Name:GetCipurseUID
        //Description:Gives UID of the Cipurse card 
        //********************************************************
        public static UInt16 GetCipurseUID(ref IntPtr hCard, ref String response)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            response = String.Empty;
            int sendbufferlen = 0x05;

            Byte[] sendBuffer = new Byte[] {
                         0x80,  // CLA
                         0x06,  // INS
                         0x00,  //P1
                         0x00,  //P2
                         0x08,  //Le                         
                    };

            Byte[] receiveBuffer = new Byte[255];
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, sendBuffer, sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    succProc(receiveBuffer, receivebufferlen, ref response);
                    if (response == "")
                        response = "no UID received";
                }
                else
                {
                    errProc(receiveBuffer, receivebufferlen, ref response);
                    switch (response)
                    {
                        case "6984":
                            response = response + ": Data is not initialized";
                            break;
                        case "6985":
                            response = response + ": Data is not loaded yet";
                            break;
                        case "6A86":
                            response = response + ": P1 param is wrong";
                            break;
                        default:
                            response = response + ": error code";
                            break;
                    }
                    return 1;
                }
            }
            else
            {
                response = "unknown error";
                return 1;
            }
            return 0;
        }

        public static UInt16 SelectMFCycle(ref IntPtr hCard)
        {
            UInt16 retry = 0;
            String response = String.Empty;
            while (!SelectMF(ref hCard, ref response).Equals(0))
            {
                if (retry.Equals(5))
                {
                    System.Console.WriteLine("MF selection error: " + response);
                    return 1;
                }
                retry++;
                System.Threading.Thread.Sleep(100);
            }
            System.Console.WriteLine("MF selection success: " + response);
            return 0;
        }

        public static UInt16 SelectMF(ref IntPtr hCard, ref String response)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            response = String.Empty;
            int sendbufferlen = 0x05;
            Byte[] sendBuffer = new Byte[] {
                         0x00,  // CLA
                         0xA4,  // INS
                         0x00,  //P1
                         0x00,  //P2                      
                    };
            Byte[] receiveBuffer = new Byte[255];
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, sendBuffer, sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    succProc(receiveBuffer, receivebufferlen, ref response);
                    if (response == "")
                        response = "MF selected";
                }
                else
                {
                    errProc(receiveBuffer, receivebufferlen, ref response);
                    switch (response)
                    {
                        case "":
                            response = response + ": Success";
                            break;
                        case "6283":
                            response = response + ": MF deactivated";
                            break;
                        case "6700":
                            response = response + ": Wrong input data";
                            break;
                        case "6986":
                            response = response + ": No current MF";
                            break;
                        case "6A82":
                            response = response + ": File not found";
                            break;
                        case "6B00":
                            response = response + ": Wrong P1/P2";
                            break;
                        default:
                            response = response + ": error code";
                            break;
                    }
                    return 1;
                }
            }
            else
            {
                response = "unknown";
                return 1;
            }
            return 0;
        }

        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static UInt16 CreateADFUnsecured(ref IntPtr hCard, ref String responce, String appAID, Byte[] fileID, Byte filesCount)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            responce = String.Empty;

            Byte[] aidBytes = Encoding.ASCII.GetBytes(appAID);
            MemoryStream tag84 = new MemoryStream(2 + aidBytes.Length);
            using (BinaryWriter writeTag84 = new BinaryWriter(tag84))
            {
                writeTag84.Write((byte)0x84);
                writeTag84.Write((byte)aidBytes.Length);
                writeTag84.Write(aidBytes);
            }

            MemoryStream fcp = new MemoryStream(2 + tag84.ToArray().Length);
            using (BinaryWriter writeFcp = new BinaryWriter(fcp))
            {
                writeFcp.Write((byte)0x62);
                writeFcp.Write((byte)tag84.ToArray().Length);
                writeFcp.Write(tag84.ToArray());
            }

            //Byte [] adfFileId = new Byte [] {0x01 , 0x03};

            MemoryStream fileAttrs = new MemoryStream(7 + fcp.ToArray().Length);
            using (BinaryWriter writeFileAttrs = new BinaryWriter(fileAttrs))
            {
                writeFileAttrs.Write((byte)0x38);
                writeFileAttrs.Write((byte)0x20);
                writeFileAttrs.Write(fileID);
                writeFileAttrs.Write((byte)filesCount);//(byte)0x02);
                writeFileAttrs.Write((byte)0x00);
                writeFileAttrs.Write((byte)0x00);
                writeFileAttrs.Write(fcp.ToArray());
            }

            MemoryStream cmdData = new MemoryStream(3 + fileAttrs.ToArray().Length);
            using (BinaryWriter writeCmdData = new BinaryWriter(cmdData))
            {
                writeCmdData.Write((byte)0x92);
                writeCmdData.Write((byte)0x00);
                writeCmdData.Write((byte)fileAttrs.ToArray().Length);
                writeCmdData.Write(fileAttrs.ToArray());
            }

            MemoryStream cmd = new MemoryStream(5 + cmdData.ToArray().Length);
            using (BinaryWriter writeCmd = new BinaryWriter(cmd))
            {
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0xE0);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)cmdData.ToArray().Length);
                writeCmd.Write(cmdData.ToArray());
            }

            //String stri = String.Empty;
            int sendbufferlen = (byte)cmd.ToArray().Length;//0x13;

            Byte[] receiveBuffer = new Byte[255];
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, cmd.ToArray(), sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    succProc(receiveBuffer, receivebufferlen, ref responce);
                    if (responce == "")
                        responce = "ADF created";
                }
                else
                {
                    errProc(receiveBuffer, receivebufferlen, ref responce);
                    switch (responce)
                    {
                        case "6700":
                            responce = responce + ": Wrong data size";
                            break;
                        case "6983":
                            responce = responce + ": Key is blocked";
                            break;
                        case "6985":
                            responce = responce + ": Key is not loaded";
                            break;
                    }
                    return 1;
                }
            }
            else
            {
                responce = "unknown";
                return 1;
            }
            return 0;
        }


        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static UInt16 SelectAdf(ref IntPtr hCard, ref String responce, String appAID)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            responce = String.Empty;

            Byte[] aidBytes = Encoding.ASCII.GetBytes(appAID);
            MemoryStream cmd = new MemoryStream(5 + aidBytes.Length);
            using (BinaryWriter writeCmd = new BinaryWriter(cmd))
            {
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0xA4);
                writeCmd.Write((byte)0x04);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)aidBytes.Length);
                writeCmd.Write(aidBytes);
            }

            int sendbufferlen = (byte)cmd.ToArray().Length;//0x13;

            Byte[] receiveBuffer = new Byte[255];
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, cmd.ToArray(), sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    succProc(receiveBuffer, receivebufferlen, ref responce);
                    if (responce == "")
                        responce = "ADF selected";
                }
                else
                {
                    errProc(receiveBuffer, receivebufferlen, ref responce);
                    switch (responce)
                    {
                        case "6700":
                            responce = responce + ": Wrong data size";
                            break;
                        case "6983":
                            responce = responce + ": Key is blocked";
                            break;
                        case "6985":
                            responce = responce + ": Key is not loaded";
                            break;
                    }
                    return 1;
                }
            }
            else
            {
                responce = "unknown";
                return 1;
            }
            return 0;
        }

        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static UInt16 DeleteAdf(ref IntPtr hCard, ref String responce, String appAID)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            responce = String.Empty;

            Byte[] aidBytes = Encoding.ASCII.GetBytes(appAID);
            MemoryStream cmd = new MemoryStream(5 + aidBytes.Length);
            using (BinaryWriter writeCmd = new BinaryWriter(cmd))
            {
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0xE4);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0x00);
            }

            int sendbufferlen = (byte)cmd.ToArray().Length;//0x13;

            Byte[] receiveBuffer = new Byte[255];
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, cmd.ToArray(), sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    succProc(receiveBuffer, receivebufferlen, ref responce);
                    if (responce == "")
                        responce = "ADF deleted";
                }
                else
                {
                    errProc(receiveBuffer, receivebufferlen, ref responce);
                    switch (responce)
                    {
                        case "6700":
                            responce = responce + ": Wrong data size";
                            break;
                        case "6983":
                            responce = responce + ": Key is blocked";
                            break;
                        case "6985":
                            responce = responce + ": Key is not loaded";
                            break;
                    }
                    return 1;
                }
            }
            else
            {
                responce = "unknown";
                return 1;
            }            //responce = "unknown";
            return 0;
        }

        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static UInt16 CreateEfUnsecured(ref IntPtr hCard, ref String responce, MapEF mapEF)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            responce = String.Empty;

            //Byte[] efFileId = new Byte[] { 0x01, 0x00 };
            //Byte[] efFileSize = new Byte[] { 0x10, 0x00 };

            MemoryStream fileAttrs = new MemoryStream( 6 );
            using (BinaryWriter writeFileAttrs = new BinaryWriter(fileAttrs))
            {
                writeFileAttrs.Write((byte)0x11);
                writeFileAttrs.Write((byte)0x00);
                writeFileAttrs.Write(mapEF.efFileId);//  efFileId);
                writeFileAttrs.Write(mapEF.efFileSize);// efFileSize);
            }

            MemoryStream cmdData = new MemoryStream(3 + fileAttrs.ToArray().Length);
            using (BinaryWriter writeCmdData = new BinaryWriter(cmdData))
            {
                writeCmdData.Write((byte)0x92);
                writeCmdData.Write((byte)0x01);
                writeCmdData.Write((byte)fileAttrs.ToArray().Length);
                writeCmdData.Write(fileAttrs.ToArray());
            }

            MemoryStream cmd = new MemoryStream(5 + cmdData.ToArray().Length);
            using (BinaryWriter writeCmd = new BinaryWriter(cmd))
            {
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0xE0);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)cmdData.ToArray().Length);
                writeCmd.Write(cmdData.ToArray());
            }

            //String stri = String.Empty;
            int sendbufferlen = (byte)cmd.ToArray().Length;//0x13;

            Byte[] receiveBuffer = new Byte[255];
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, cmd.ToArray(), sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    succProc(receiveBuffer, receivebufferlen, ref responce);
                    if (responce == "")
                        responce = "EF created";
                }
                else
                {
                    errProc(receiveBuffer, receivebufferlen, ref responce);
                    switch (responce)
                    {
                        case "6700":
                            responce = responce + ": Wrong data size";
                            break;
                        case "6983":
                            responce = responce + ": Key is blocked";
                            break;
                        case "6985":
                            responce = responce + ": Key is not loaded";
                            break;
                    }
                    return 1;
                }
            }
            else
            {
                responce = "unknown";
                return 1;
            }
            return 0;
        }

        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static UInt16 UpdateEfBinary(ref IntPtr hCard, ref String responce, MapEF mapEF)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            responce = String.Empty;

            MemoryStream cmd = new MemoryStream(5 + mapEF.efFileBuffer.Length);
            using (BinaryWriter writeCmd = new BinaryWriter(cmd))
            {
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0xD6);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)mapEF.efFileBuffer.Length);
                writeCmd.Write(mapEF.efFileBuffer);
            }

            int sendbufferlen = (byte)cmd.ToArray().Length;//0x13;

            Byte[] receiveBuffer = new Byte[255];
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, cmd.ToArray(), sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    succProc(receiveBuffer, receivebufferlen, ref responce);
                    if (responce == "")
                        responce = "EF updated";
                }
                else
                {
                    errProc(receiveBuffer, receivebufferlen, ref responce);
                    switch (responce)
                    {
                        case "6700":
                            responce = responce + ": Wrong data size";
                            break;
                        case "6983":
                            responce = responce + ": Key is blocked";
                            break;
                        case "6985":
                            responce = responce + ": Key is not loaded";
                            break;
                    }
                    return 1;
                }
            }
            else
            {
                responce = "unknown";
                return 1;
            }
            return 0;
        }

        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static UInt16 SelectEf(ref IntPtr hCard, ref String responce, MapEF mapEF)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            responce = String.Empty;

            MemoryStream cmd = new MemoryStream(5 + 2);
            using (BinaryWriter writeCmd = new BinaryWriter(cmd))
            {
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0xA4);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0x02);
                writeCmd.Write(mapEF.efFileId);
            }

            int sendbufferlen = (byte)cmd.ToArray().Length;//0x13;

            Byte[] receiveBuffer = new Byte[255];
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, cmd.ToArray(), sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    succProc(receiveBuffer, receivebufferlen, ref responce);
                    if (responce == "")
                        responce = "EF selected";
                }
                else
                {
                    errProc(receiveBuffer, receivebufferlen, ref responce);
                    switch (responce)
                    {
                        case "6700":
                            responce = responce + ": Wrong data size";
                            break;
                        case "6983":
                            responce = responce + ": Key is blocked";
                            break;
                        case "6985":
                            responce = responce + ": Key is not loaded";
                            break;
                    }
                    return 1;
                }
            }
            else
            {
                responce = "unknown";
                return 1;
            }
            return 0;
        }

        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static UInt16 ReadBinaryEf(ref IntPtr hCard, ref String responce, ref Byte[] fileData)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            responce = String.Empty;

            MemoryStream cmd = new MemoryStream(5);
            using (BinaryWriter writeCmd = new BinaryWriter(cmd))
            {
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0xB0);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0x00);
                writeCmd.Write((byte)0xFF);
            }

            int sendbufferlen = (byte)cmd.ToArray().Length;//0x13;

            Byte[] receiveBuffer = new Byte[255];
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, cmd.ToArray(), sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    for (int i = 0; i < fileData.Length; i++)
                        fileData[i] = receiveBuffer[i];
                    succProc(receiveBuffer, receivebufferlen, ref responce);
                    if (responce == "")
                        responce = "EF selected";
                }
                else
                {
                    errProc(receiveBuffer, receivebufferlen, ref responce);
                    switch (responce)
                    {
                        case "6700":
                            responce = responce + ": Wrong data size";
                            break;
                        case "6983":
                            responce = responce + ": Key is blocked";
                            break;
                        case "6985":
                            responce = responce + ": Key is not loaded";
                            break;
                    }
                    return 1;
                }
            }
            else
            {
                responce = "unknown";
                return 1;
            }
            return 0;
        }

        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static UInt16 GetFileAttributes(ref IntPtr hCard, ref String responce, ref ushort fileSize)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            responce = String.Empty;

            String stri = String.Empty;
            int sendbufferlen = 0x05;

            Byte[] sendBuffer = new Byte[] {
                         0x80,  // CLA
                         0xCE,  // INS
                         0x00,  //P1
                         0x00,  //P2
                         0x80
                    };

            Byte[] receiveBuffer = new Byte[255];
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, sendBuffer, sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    fileSize = (ushort)receiveBuffer[5];
                    succProc(receiveBuffer, receivebufferlen, ref responce);
                    if (responce == "")
                        responce = "Transaction performed";
                }
                else
                {
                    errProc(receiveBuffer, receivebufferlen, ref responce);
                    switch (responce)
                    {
                        case "6700":
                            responce = responce + ": Wrong data size";
                            break;
                        case "6983":
                            responce = responce + ": Key is blocked";
                            break;
                        case "6985":
                            responce = responce + ": Key is not loaded";
                            break;
                    }
                    return 1;
                }
            }
            else
            {
                responce = "unknown";
                return 1;
            }
            return 0;
        }

        //********************************************************
        //Function Name:GetUID
        //Description:Gives UID of the card 
        //********************************************************
        public static UInt16 PerformTransaction(ref IntPtr hCard, ref String responce)
        {
            HiDWinscard.SCARD_IO_REQUEST sioreq;
            sioreq.dwProtocol = 0x2;
            sioreq.cbPciLength = 8;
            HiDWinscard.SCARD_IO_REQUEST rioreq;
            rioreq.cbPciLength = 8;
            rioreq.dwProtocol = 0x2;
            responce = String.Empty;

            String stri = String.Empty;
            int sendbufferlen = 0x04;

            Byte[] sendBuffer = new Byte[] {
                         0x80,  // CLA
                         0x7E,  // INS
                         0x00,  //P1
                         0x00,  //P2
                    };

            Byte[] receiveBuffer = new Byte[255];
            int receivebufferlen = 255;
            int retval = HID.SCardTransmit(hCard, ref sioreq, sendBuffer, sendbufferlen, ref rioreq, receiveBuffer, ref receivebufferlen);
            if (retval.Equals(0))
            {
                if (receiveBuffer[receivebufferlen - 2].Equals(0x90) && receiveBuffer[receivebufferlen - 1].Equals(0))
                {
                    succProc(receiveBuffer, receivebufferlen, ref responce);
                    if (responce == "")
                        responce = "Transaction performed";
                }
                else
                {
                    errProc(receiveBuffer, receivebufferlen, ref responce);
                    switch (responce)
                    {
                        case "6700":
                            responce = responce + ": Wrong data size";
                            break;
                        case "6983":
                            responce = responce + ": Key is blocked";
                            break;
                        case "6985":
                            responce = responce + ": Key is not loaded";
                            break;
                    }
                    return 1;
                }
            }
            else
            {
                responce = "unknown";
                return 1;
            }
            return 0;
        }


    }
}
