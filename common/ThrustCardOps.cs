﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using DamienG.Security.Cryptography;
using Newtonsoft.Json;
using System.IO;
using System.Threading;

namespace rfidapp
{
    public class ThrustCardOps
    {
        private static int discarded;
        
        //----------------------  Cipurse format 3-5

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct File1Card35
        {
            public Byte FORMAT_ID;
            public Byte FORMAT_VERSION;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public Byte[] CARD_NUMBER;
            public UInt64 START_TSTMP;
            public UInt64 END_TSTMP;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct File2Card35
        {
            public UInt32 TRANSACTION_COUNTER;
            public UInt64 TRANSACTION_TIME_TSTMP;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public Byte[] TRANSACTION_TERMINAL_ID;
            public UInt64 STATUS_TIME_TSTMP;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public Byte[] MAC;
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void Prepare35CardPersonalizationDataBuffer(  //ref Byte[] cardDataBuffer,
            //File1Card35 file1Data,
            //File2Card35 file2Data,
                                                                        out Byte[] file1DataBytes,
                                                                        out Byte[] file2DataBytes,
                                                                        String uid,
                                                                        Byte[] sault,
                                                                        String cardFile)
        {
            String hash = String.Empty; //hash for crc32
            //System.Threading.Thread.Sleep(500);
            String tickCount = System.Diagnostics.Stopwatch.GetTimestamp().ToString();
            Byte[] bbp = NFCCardOps.decimalStringToBytes(tickCount, 10);
            Byte[] file2DataBytesNoCRC = new Byte[28];
            File1Card35 file1Data = new File1Card35();
            File2Card35 file2Data = new File2Card35();
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }
            // read file into a string and deserialize JSON to a type
            CardJSON34Data cardJSON34Data = JsonConvert.DeserializeObject<CardJSON34Data>(File.ReadAllText(cardFile));
            file1Data.FORMAT_ID = cardJSON34Data.FORMAT_ID;
            file1Data.FORMAT_VERSION = cardJSON34Data.FORMAT_VERSION;
            file1Data.CARD_NUMBER = bbp;
            file1Data.START_TSTMP = cardJSON34Data.START_TSTMP;
            file1Data.END_TSTMP = cardJSON34Data.END_TSTMP;
            file1DataBytes = NFCCardOps.SerializeMessage<File1Card35>(file1Data); //serialize once writed data

            file2Data.TRANSACTION_COUNTER = cardJSON34Data.TRANSACTION_COUNTER;
            file2Data.TRANSACTION_TIME_TSTMP = cardJSON34Data.TRANSACTION_TIME_TSTMP;
            file2Data.TRANSACTION_TERMINAL_ID = HexToBytenByteToHex.GetBytes(cardJSON34Data.TRANSACTION_TERMINAL_ID, out discarded);
            file2Data.STATUS_TIME_TSTMP = cardJSON34Data.STATUS_TIME_TSTMP;
            file2DataBytes = NFCCardOps.SerializeMessage<File2Card35>(file2Data); //serialize once writed data with wrong crc

            for (int i = 0; i < file2DataBytes.Length - 8; i++)
                file2DataBytesNoCRC[i] = file2DataBytes[i];
            Byte[] rv = cardUIDBytes.Concat(file1DataBytes).Concat(file2DataBytesNoCRC).Concat(sault).ToArray();
            foreach (byte b in crc32proc.ComputeHash(rv))
                hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            //file2Data.MAC = f;
            file2DataBytes = NFCCardOps.SerializeMessage<File2Card35>(file2Data); //serialize once writed data with wrong crc
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        public static int ProcessFile1Card35Data( Byte[] fileDataBuffer,
                                                    ref ThrustCardInfoPart1 thrustCardInfoPart1 )
        {
            try
            {
                File1Card35 file1Card35 = new File1Card35();
                object d = NFCCardOps.DeserializeMsg<File1Card35>(fileDataBuffer);
                file1Card35 = (File1Card35)d;
                thrustCardInfoPart1.formatId = file1Card35.FORMAT_ID;
                thrustCardInfoPart1.formatVersion = file1Card35.FORMAT_VERSION;
                thrustCardInfoPart1.CardNumber = NFCCardOps.decimalStringFromBytes(file1Card35.CARD_NUMBER, 0, 16);
                thrustCardInfoPart1.StartTimestamp = file1Card35.START_TSTMP;
                thrustCardInfoPart1.EndTimestamp = file1Card35.END_TSTMP;
                return 0;
            }
            catch
            {
                return 1;
            }
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        public static int ProcessFile2Card35Data(  Byte[] cardDataBuffer,
                                                    ref ThrustCardInfoPart2 thrustCardInfoPart2 )       
        {
            try
            {
                File2Card35 file2Card35 = new File2Card35();
                object d = NFCCardOps.DeserializeMsg<File2Card35>(cardDataBuffer);
                file2Card35 = (File2Card35)d;
                thrustCardInfoPart2.TransactionCounter = file2Card35.TRANSACTION_COUNTER;
                thrustCardInfoPart2.TransactionTimestamp = file2Card35.TRANSACTION_TIME_TSTMP;
                thrustCardInfoPart2.TransactionTerminalID = NFCCardOps.decimalStringFromBytes(file2Card35.TRANSACTION_TERMINAL_ID, 0, 16);
                thrustCardInfoPart2.StatusTimestamp = file2Card35.STATUS_TIME_TSTMP;
                thrustCardInfoPart2.MAC = NFCCardOps.decimalStringFromBytes(file2Card35.MAC, 0, 16);
                return 0;
            }
            catch
            {
                return 1;
            }
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void Prepare34File1Fil2CardDataBuffer(
                                                                ref File1Card35 file1Card35,
                                                                ref File2Card35 file2Card35,
                                                                ref Byte[] file1DataBytes,
                                                                ref Byte[] file2DataBytes,
                                                                String uid,
                                                                Byte[] sault)
        {
            String hash = String.Empty; //hash for crc32
            //System.Threading.Thread.Sleep(500);

            Byte[] file2DataBytesNoCRC = new Byte[28];
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }
            file1DataBytes = NFCCardOps.SerializeMessage<File1Card35>(file1Card35); //serialize once writed data
            file2DataBytes = NFCCardOps.SerializeMessage<File2Card35>(file2Card35); //serialize once writed data with wrong crc
            for (int i = 0; i < file2DataBytes.Length - 8; i++)
                file2DataBytesNoCRC[i] = file2DataBytes[i];
            Byte[] rv = cardUIDBytes.Concat(file1DataBytes).Concat(file2DataBytesNoCRC).Concat(sault).ToArray();
            foreach (byte b in crc32proc.ComputeHash(rv))
                hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            //file2Card35.MAC = f;
            file2DataBytes = NFCCardOps.SerializeMessage<File2Card35>(file2Card35);
        }

        public static UInt16 GetCipurseFormatTypeCycle(ref IntPtr hCard, MapEF mapFile, ref ushort cardType, String appAID)
        {
            UInt16 retry = 0;
            ushort response = 0;
            while (!GetCipurseFormatType(ref hCard, mapFile, ref response, appAID).Equals(0))
            {
                if (retry.Equals(5))
                {
                    System.Console.WriteLine("Cipurse format error: " + response);
                    return 1;
                }
                retry++;
                System.Threading.Thread.Sleep(100);
            }
            cardType = response;
            System.Console.WriteLine("Cipurse format: " + response);
            return 0;
        }

        public static UInt16 GetCipurseFormatType(ref IntPtr hCard, MapEF mapFile, ref ushort formatType, String appAID)
        {
            String response = String.Empty;
            ushort fileSize = 0;
            if (CipurseFuncLib.SelectMFCycle(ref hCard).Equals(0))
            {
                if (CipurseFuncLib.SelectAdf(ref hCard, ref response, appAID).Equals(0))
                {
                    System.Console.WriteLine("Select ADF: " + response);

                    if (CipurseFuncLib.SelectEf(ref hCard, ref response, mapFile).Equals(0))
                    {
                        System.Threading.Thread.Sleep(100);
                        System.Console.WriteLine("EF selected: " + response);
                        if (CipurseFuncLib.GetFileAttributes(ref hCard, ref response, ref fileSize).Equals(0))
                        {
                            switch (fileSize)
                            {
                                case 16:
                                    formatType = 33;
                                    break;
                                case 28:
                                    formatType = 34;
                                    break;
                                default:
                                    formatType = 0;
                                    break;
                            }
                        }
                        else
                        {
                            System.Console.WriteLine("Can't get format: " + response);
                            return 1;
                        }
                    }
                    else
                    {
                        System.Console.WriteLine("EF selection eror: " + response);
                        return 1;
                    }
                }
                else
                {
                    System.Console.WriteLine("No ADF to select: " + response);
                }
            }
            else
            {
                //System.Console.WriteLine("MF selection eror: " + response);
                return 2;
            }
            return 0;
        }

        // ----------------- KeyCard
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ControlInfo
        {
            public Byte FORMAT_ID;
            public Byte FORMAT_VERSION;
            public Byte KEY_COUNT;
            public Int64 MAC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct KeyInfo
        {
            public Byte KEY_TYPE;
            public UInt16 Flags;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public Byte[] KeyBody;
            //public IntPtr KeyBody; 
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void PrepareKeyCardPersonalizationDataBuffer( out Byte[] controlInfoDataBytes,
                                                                    out Byte[] key1DataBytes,
                                                                    out Byte[] key2DataBytes,
                                                                    String uid,
                                                                    Byte[] sault,
                                                                    Byte[] key  )
        {
            String hash = String.Empty; //hash for crc32

            Byte[] controlInfoDataBytesNoCRC = new Byte[3];

            ControlInfo controlInfoData = new ControlInfo();
            KeyInfo key1InfoData = new KeyInfo();
            KeyInfo key2InfoData = new KeyInfo();
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }

            controlInfoData.FORMAT_ID = 4;
            controlInfoData.FORMAT_VERSION = 1;
            controlInfoData.KEY_COUNT = 2;//(Byte)keys.Count;
            controlInfoDataBytes = NFCCardOps.SerializeMessage<ControlInfo>(controlInfoData); //serialize once writed data

            key1InfoData.KEY_TYPE = 1;
            key1InfoData.Flags = 0;
            key1InfoData.KeyBody = key;
            key1DataBytes = NFCCardOps.SerializeMessage<KeyInfo>(key1InfoData);

            key2InfoData.KEY_TYPE = 2;
            key2InfoData.Flags = 0;
            key2InfoData.KeyBody = key;
            key2DataBytes = NFCCardOps.SerializeMessage<KeyInfo>(key2InfoData);

            for (int i = 0; i < controlInfoDataBytes.Length - 8; i++)
                controlInfoDataBytesNoCRC[i] = controlInfoDataBytes[i];

            Byte[] rv = cardUIDBytes.Concat(controlInfoDataBytesNoCRC).Concat(key1DataBytes).Concat(key2DataBytes).Concat(sault).ToArray();

            foreach (byte b in crc32proc.ComputeHash(rv))
            {
                hash += b.ToString("x2").ToLower();
            }

            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            controlInfoData.MAC = f;
            controlInfoDataBytes = NFCCardOps.SerializeMessage<ControlInfo>(controlInfoData); //serialize once writed data with wrong crc
        }
    }
}