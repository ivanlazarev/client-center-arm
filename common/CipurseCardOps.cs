﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using DamienG.Security.Cryptography;
using Newtonsoft.Json;
using System.IO;
using System.Threading;

namespace rfidapp
{
    public class CipurseCardOps
    {
        private static int discarded;

        //----------------------  Cipurse Format 3-3
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct File1
        {
            public Byte FORMAT_ID;
            public Byte FORMAT_VERSION;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public Byte[] CARD_NUMBER;
            public UInt16 START_TSTMP;
            public UInt16 END_TSTMP;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct File2
        {
            public UInt32 TRANSACTION_COUNTER;
            public UInt32 TRANSACTION_TIME_TSTMP;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public Byte[] TRANSACTION_TERMINAL_ID;
            public UInt32 STATUS_TIME_TSTMP;
            public Int64 MAC;
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void Prepare33CardPersonalizationDataBuffer(  out Byte[] file1DataBytes,
                                                                    out Byte[] file2DataBytes,
                                                                    String uid,
                                                                    Byte[] sault,
                                                                    String cardFile )
        {
            String hash = String.Empty; //hash for crc32
            //System.Threading.Thread.Sleep(500);
            String tickCount = System.Diagnostics.Stopwatch.GetTimestamp().ToString();
            Byte[] bbp = NFCCardOps.decimalStringToBytes(tickCount, 10);
            Byte[] file2DataBytesNoCRC = new Byte[20];
            File1 file1Data = new File1();
            File2 file2Data = new File2();
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }
            // read file into a string and deserialize JSON to a type
            CardJSON33Data cardJSON33Data = JsonConvert.DeserializeObject<CardJSON33Data>(File.ReadAllText(cardFile));
            file1Data.FORMAT_ID = cardJSON33Data.FORMAT_ID;
            file1Data.FORMAT_VERSION = cardJSON33Data.FORMAT_VERSION;
            file1Data.CARD_NUMBER = bbp;
            file1Data.START_TSTMP = cardJSON33Data.START_TSTMP;
            file1Data.END_TSTMP = cardJSON33Data.END_TSTMP;
            file1DataBytes = NFCCardOps.SerializeMessage<File1>(file1Data); //serialize once writed data

            file2Data.TRANSACTION_COUNTER = cardJSON33Data.TRANSACTION_COUNTER;
            file2Data.TRANSACTION_TIME_TSTMP = cardJSON33Data.TRANSACTION_TIME_TSTMP;        
            file2Data.TRANSACTION_TERMINAL_ID = HexToBytenByteToHex.GetBytes(cardJSON33Data.TRANSACTION_TERMINAL_ID, out discarded);
            file2Data.STATUS_TIME_TSTMP = cardJSON33Data.STATUS_TIME_TSTMP;
            file2DataBytes = NFCCardOps.SerializeMessage<File2>(file2Data); //serialize once writed data with wrong crc

            for (int i = 0; i < file2DataBytes.Length - 8; i++)
                file2DataBytesNoCRC[i] = file2DataBytes[i];
            Byte[] rv = cardUIDBytes.Concat(file1DataBytes).Concat(file2DataBytesNoCRC).Concat(sault).ToArray();
            foreach (byte b in crc32proc.ComputeHash(rv))
                hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            file2Data.MAC = f;
            file2DataBytes = NFCCardOps.SerializeMessage<File2>(file2Data); //serialize once writed data with wrong crc
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        public static void Process33File1CardData(  Byte[] fileDataBuffer,
                                                    ref CardInfo cardInfo,
                                                    ref File1 file1DataStruct   )
        {
            String cardNumber = String.Empty;
            //File1 file1DataStruct = new File1();
            object d = NFCCardOps.DeserializeMsg<File1>(fileDataBuffer);
            file1DataStruct = (File1)d;
            // OWMR field
            cardNumber = NFCCardOps.decimalStringFromBytes(file1DataStruct.CARD_NUMBER, 0, 10);
            System.Console.WriteLine("File 1 data:");
            System.Console.WriteLine("FORMAT_ID: " + file1DataStruct.FORMAT_ID);
            System.Console.WriteLine("FORMAT_VERSION: " + file1DataStruct.FORMAT_VERSION);
            System.Console.WriteLine("CARD_NUMBER: " + cardNumber);
            System.Console.WriteLine("START_TSTMP: " + file1DataStruct.START_TSTMP);
            System.Console.WriteLine("END_TSTMP: " + file1DataStruct.END_TSTMP);
            cardInfo.cardNum = cardNumber;

            if (file1DataStruct.START_TSTMP.Equals(0))
                cardInfo.dateStart = 0;
            else
                cardInfo.dateStart = (uint)(file1DataStruct.START_TSTMP * 3600 * 24) + NFCCardOps.EPOCH;

            if (file1DataStruct.END_TSTMP.Equals(0))
                cardInfo.dateEnd = 0;
            else
                cardInfo.dateEnd = (uint)(file1DataStruct.END_TSTMP * 3600 * 24) + NFCCardOps.EPOCH;
            cardInfo.cardType = 10;            
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        public static void Process33File2CardData(Byte[] cardDataBuffer,
                                                    ref File2 file2DataStruct)
        {
            String terminalID = String.Empty;
            //File2 file2DataStruct = new File2();
            // MWMR field 1

            object d = NFCCardOps.DeserializeMsg<File2>(cardDataBuffer);
            file2DataStruct = (File2)d;

            System.Console.WriteLine("File 2 data:");
            foreach (byte b in file2DataStruct.TRANSACTION_TERMINAL_ID)
                terminalID += (Char)b;
            System.Console.WriteLine("TRANSACTION_COUNTER: " + file2DataStruct.TRANSACTION_COUNTER);
            System.Console.WriteLine("TRANSACTION_TIME_TSTMP: " + file2DataStruct.TRANSACTION_TIME_TSTMP);
            System.Console.WriteLine("TRANSACTION_TERMINAL_ID: " + terminalID);
            System.Console.WriteLine("STATUS_TIME_TSTMP: " + file2DataStruct.STATUS_TIME_TSTMP);
            System.Console.WriteLine("MAC: " + file2DataStruct.MAC);
            terminalID = String.Empty;
            // MWMR field 2
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void Prepare33File1Fil2CardDataBuffer(   
                                                                ref File1 file1DataStruct,
                                                                ref File2 file2DataStruct,
                                                                ref Byte[] file1DataBytes,
                                                                ref Byte[] file2DataBytes,
                                                                String uid,
                                                                Byte[] sault    )
        {
            String hash = String.Empty; //hash for crc32
            //System.Threading.Thread.Sleep(500);

            Byte[] file2DataBytesNoCRC = new Byte[20];
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }
            file1DataBytes = NFCCardOps.SerializeMessage<File1>(file1DataStruct); //serialize once writed data
            file2DataBytes = NFCCardOps.SerializeMessage<File2>(file2DataStruct); //serialize once writed data with wrong crc
            for (int i = 0; i < file2DataBytes.Length - 8; i++)
                file2DataBytesNoCRC[i] = file2DataBytes[i];
            Byte[] rv = cardUIDBytes.Concat(file1DataBytes).Concat(file2DataBytesNoCRC).Concat(sault).ToArray();
            foreach (byte b in crc32proc.ComputeHash(rv))
                hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            file2DataStruct.MAC = f;
            file2DataBytes = NFCCardOps.SerializeMessage<File2>(file2DataStruct);
        }

        //----------------------  Cipurse format 3-4

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct File1_34
        {
            public Byte FORMAT_ID;
            public Byte FORMAT_VERSION;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public Byte[] CARD_NUMBER;
            public UInt64 START_TSTMP;
            public UInt64 END_TSTMP;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct File2_34
        {
            public UInt32 TRANSACTION_COUNTER;
            public UInt64 TRANSACTION_TIME_TSTMP;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public Byte[] TRANSACTION_TERMINAL_ID;
            public UInt64 STATUS_TIME_TSTMP;
            public Int64 MAC;
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void Prepare34CardPersonalizationDataBuffer(  //ref Byte[] cardDataBuffer,
            //File1_34 file1Data,
            //File2_34 file2Data,
                                                                        out Byte[] file1DataBytes,
                                                                        out Byte[] file2DataBytes,
                                                                        String uid,
                                                                        Byte[] sault,
                                                                        String cardFile)
        {
            String hash = String.Empty; //hash for crc32
            //System.Threading.Thread.Sleep(500);
            String tickCount = System.Diagnostics.Stopwatch.GetTimestamp().ToString();
            Byte[] bbp = NFCCardOps.decimalStringToBytes(tickCount, 10);
            Byte[] file2DataBytesNoCRC = new Byte[28];
            File1_34 file1Data = new File1_34();
            File2_34 file2Data = new File2_34();
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }
            // read file into a string and deserialize JSON to a type
            CardJSON34Data cardJSON34Data = JsonConvert.DeserializeObject<CardJSON34Data>(File.ReadAllText(cardFile));
            file1Data.FORMAT_ID = cardJSON34Data.FORMAT_ID;
            file1Data.FORMAT_VERSION = cardJSON34Data.FORMAT_VERSION;
            file1Data.CARD_NUMBER = bbp;
            file1Data.START_TSTMP = cardJSON34Data.START_TSTMP;
            file1Data.END_TSTMP = cardJSON34Data.END_TSTMP;
            file1DataBytes = NFCCardOps.SerializeMessage<File1_34>(file1Data); //serialize once writed data

            file2Data.TRANSACTION_COUNTER = cardJSON34Data.TRANSACTION_COUNTER;
            file2Data.TRANSACTION_TIME_TSTMP = cardJSON34Data.TRANSACTION_TIME_TSTMP;
            file2Data.TRANSACTION_TERMINAL_ID = HexToBytenByteToHex.GetBytes(cardJSON34Data.TRANSACTION_TERMINAL_ID, out discarded);
            file2Data.STATUS_TIME_TSTMP = cardJSON34Data.STATUS_TIME_TSTMP;
            file2DataBytes = NFCCardOps.SerializeMessage<File2_34>(file2Data); //serialize once writed data with wrong crc

            for (int i = 0; i < file2DataBytes.Length - 8; i++)
                file2DataBytesNoCRC[i] = file2DataBytes[i];
            Byte[] rv = cardUIDBytes.Concat(file1DataBytes).Concat(file2DataBytesNoCRC).Concat(sault).ToArray();
            foreach (byte b in crc32proc.ComputeHash(rv))
                hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            file2Data.MAC = f;
            file2DataBytes = NFCCardOps.SerializeMessage<File2_34>(file2Data); //serialize once writed data with wrong crc
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        public static void Process34File1CardData(Byte[] fileDataBuffer,
                                                    ref CardInfo cardInfo,
                                                    ref File1_34 file1DataStruct)
        {
            String cardNumber = String.Empty;
            object d = NFCCardOps.DeserializeMsg<File1_34>(fileDataBuffer);
            file1DataStruct = (File1_34)d;
            // OWMR field
            cardNumber = NFCCardOps.decimalStringFromBytes(file1DataStruct.CARD_NUMBER, 0, 10);
            System.Console.WriteLine("File 1 data:");
            System.Console.WriteLine("FORMAT_ID: " + file1DataStruct.FORMAT_ID);
            System.Console.WriteLine("FORMAT_VERSION: " + file1DataStruct.FORMAT_VERSION);
            System.Console.WriteLine("CARD_NUMBER: " + cardNumber);
            System.Console.WriteLine("START_TSTMP: " + file1DataStruct.START_TSTMP);
            System.Console.WriteLine("END_TSTMP: " + file1DataStruct.END_TSTMP);
            cardInfo.cardNum = cardNumber;
            cardInfo.dateStart = file1DataStruct.START_TSTMP;//file1DataStruct.START_TSTMP;
            //if (file1DataStruct.START_TSTMP.Equals(0))
            //    cardInfo.dateStart = 0;
            //else
            //    cardInfo.dateStart = (uint)(file1DataStruct.START_TSTMP * 3600 * 24) + NFCCardOps.EPOCH;
            cardInfo.dateEnd = file1DataStruct.END_TSTMP;
            //if (file1DataStruct.END_TSTMP.Equals(0))
            //    0;
            //else
            //    cardInfo.dateEnd = (uint)(file1DataStruct.END_TSTMP * 3600 * 24) + NFCCardOps.EPOCH;
            cardInfo.cardType = 0;
        }

        //********************************************************
        //Function Name:ProcessOldCardData
        //Description:
        //********************************************************
        public static void Process34File2CardData(Byte[] cardDataBuffer,
                                                    ref File2_34 file2DataStruct)
        {
            String terminalID = String.Empty;
            //File2_34 file2DataStruct = new File2_34();
            // MWMR field 1

            object d = NFCCardOps.DeserializeMsg<File2_34>(cardDataBuffer);
            file2DataStruct = (File2_34)d;

            System.Console.WriteLine("File 2 data:");
            foreach (byte b in file2DataStruct.TRANSACTION_TERMINAL_ID)
                terminalID += (Char)b;
            System.Console.WriteLine("TRANSACTION_COUNTER: " + file2DataStruct.TRANSACTION_COUNTER);
            System.Console.WriteLine("TRANSACTION_TIME_TSTMP: " + file2DataStruct.TRANSACTION_TIME_TSTMP);
            System.Console.WriteLine("TRANSACTION_TERMINAL_ID: " + terminalID);
            System.Console.WriteLine("STATUS_TIME_TSTMP: " + file2DataStruct.STATUS_TIME_TSTMP);
            System.Console.WriteLine("MAC: " + file2DataStruct.MAC);
            terminalID = String.Empty;
            // MWMR field 2
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void Prepare34File1Fil2CardDataBuffer(
                                                                ref File1_34 file1DataStruct,
                                                                ref File2_34 file2DataStruct,
                                                                ref Byte[] file1DataBytes,
                                                                ref Byte[] file2DataBytes,
                                                                String uid,
                                                                Byte[] sault)
        {
            String hash = String.Empty; //hash for crc32
            //System.Threading.Thread.Sleep(500);

            Byte[] file2DataBytesNoCRC = new Byte[28];
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }
            file1DataBytes = NFCCardOps.SerializeMessage<File1_34>(file1DataStruct); //serialize once writed data
            file2DataBytes = NFCCardOps.SerializeMessage<File2_34>(file2DataStruct); //serialize once writed data with wrong crc
            for (int i = 0; i < file2DataBytes.Length - 8; i++)
                file2DataBytesNoCRC[i] = file2DataBytes[i];
            Byte[] rv = cardUIDBytes.Concat(file1DataBytes).Concat(file2DataBytesNoCRC).Concat(sault).ToArray();
            foreach (byte b in crc32proc.ComputeHash(rv))
                hash += b.ToString("x2").ToLower();
            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            file2DataStruct.MAC = f;
            file2DataBytes = NFCCardOps.SerializeMessage<File2_34>(file2DataStruct);
        }

        public static UInt16 GetCipurseFormatTypeCycle(ref IntPtr hCard, MapEF mapFile, ref ushort cardType, String appAID)
        {
            UInt16 retry = 0;
            ushort response = 0;
            while (!GetCipurseFormatType(ref hCard, mapFile, ref response, appAID).Equals(0))
            {
                if (retry.Equals(5))
                {
                    System.Console.WriteLine("Cipurse format error: " + response);
                    return 1;
                }
                retry++;
                System.Threading.Thread.Sleep(100);
            }
            cardType = response;
            System.Console.WriteLine("Cipurse format: " + response);
            return 0;
        }

        public static UInt16 GetCipurseFormatType(ref IntPtr hCard, MapEF mapFile, ref ushort formatType, String appAID)
        {
            String response = String.Empty;
            ushort fileSize = 0;
            if (CipurseFuncLib.SelectMFCycle(ref hCard).Equals(0))
            {
                if (CipurseFuncLib.SelectAdf(ref hCard, ref response, appAID).Equals(0))
                {
                    System.Console.WriteLine("Select ADF: " + response);

                    if (CipurseFuncLib.SelectEf(ref hCard, ref response, mapFile).Equals(0))
                    {
                        System.Threading.Thread.Sleep(100);
                        System.Console.WriteLine("EF selected: " + response);
                        if (CipurseFuncLib.GetFileAttributes(ref hCard, ref response, ref fileSize).Equals(0))
                        {
                            switch (fileSize)
                            {
                                case 16:
                                    formatType = 33;
                                    break;
                                case 28:
                                    formatType = 34;
                                    break;
                                default:
                                    formatType = 0;
                                    break;
                            }
                        }
                        else
                        {
                            System.Console.WriteLine("Can't get format: " + response);
                            return 1;
                        }
                    }
                    else
                    {
                        System.Console.WriteLine("EF selection eror: " + response);
                        return 1;
                    }
                }
                else
                {
                    System.Console.WriteLine("No ADF to select: " + response);
                }
            }
            else
            {
                //System.Console.WriteLine("MF selection eror: " + response);
                return 2;
            }
            return 0;
        }

        // ----------------- KeyCard
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ControlInfo
        {
            public Byte FORMAT_ID;
            public Byte FORMAT_VERSION;
            public Byte KEY_COUNT;
            public Int64 MAC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct KeyInfo
        {
            public Byte KEY_TYPE;
            public UInt16 Flags;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public Byte[] KeyBody;
            //public IntPtr KeyBody; 
        }

        //********************************************************
        //Function Name:Prepare25CardDataBuffer
        //Description:Gives UID of the card 
        //********************************************************
        public static void PrepareKeyCardPersonalizationDataBuffer( out Byte[] controlInfoDataBytes,
                                                                    out Byte[] key1DataBytes,
                                                                    out Byte[] key2DataBytes,
                                                                    String uid,
                                                                    Byte[] sault,
                                                                    Byte[] key  )
        {
            String hash = String.Empty; //hash for crc32

            Byte[] controlInfoDataBytesNoCRC = new Byte[3];

            ControlInfo controlInfoData = new ControlInfo();
            KeyInfo key1InfoData = new KeyInfo();
            KeyInfo key2InfoData = new KeyInfo();
            Crc32 crc32proc = new Crc32();
            String[] cardUIDSubs = new String[uid.Length / 2];
            for (int i = 0; i < uid.Length / 2; i++)
                cardUIDSubs[i] = uid.Substring(i * 2, 2);
            Byte[] cardUIDBytes = new Byte[cardUIDSubs.Length];
            for (int i = 0; i < cardUIDSubs.Length; i++)
            {
                try
                {
                    cardUIDBytes[i] = Convert.ToByte(cardUIDSubs[i], 16);
                }
                catch { }
            }

            controlInfoData.FORMAT_ID = 4;
            controlInfoData.FORMAT_VERSION = 1;
            controlInfoData.KEY_COUNT = 2;//(Byte)keys.Count;
            controlInfoDataBytes = NFCCardOps.SerializeMessage<ControlInfo>(controlInfoData); //serialize once writed data

            key1InfoData.KEY_TYPE = 1;
            key1InfoData.Flags = 0;
            key1InfoData.KeyBody = key;
            key1DataBytes = NFCCardOps.SerializeMessage<KeyInfo>(key1InfoData);

            key2InfoData.KEY_TYPE = 2;
            key2InfoData.Flags = 0;
            key2InfoData.KeyBody = key;
            key2DataBytes = NFCCardOps.SerializeMessage<KeyInfo>(key2InfoData);

            for (int i = 0; i < controlInfoDataBytes.Length - 8; i++)
                controlInfoDataBytesNoCRC[i] = controlInfoDataBytes[i];

            Byte[] rv = cardUIDBytes.Concat(controlInfoDataBytesNoCRC).Concat(key1DataBytes).Concat(key2DataBytes).Concat(sault).ToArray();

            foreach (byte b in crc32proc.ComputeHash(rv))
            {
                hash += b.ToString("x2").ToLower();
            }

            uint num = uint.Parse(hash, System.Globalization.NumberStyles.AllowHexSpecifier);
            byte[] intVals = BitConverter.GetBytes(num);
            UInt32 f = BitConverter.ToUInt32(intVals, 0);
            controlInfoData.MAC = f;
            controlInfoDataBytes = NFCCardOps.SerializeMessage<ControlInfo>(controlInfoData); //serialize once writed data with wrong crc
        }
    }
}