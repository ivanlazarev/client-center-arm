﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace armoperator
{
    public partial class WndFIO : Form
    {
        WndStatement __wndESEK;
        private static String __type = "";
        private static String __fio = "";        

        public WndFIO(WndStatement wndESEK, String type, String fio)
        {
            InitializeComponent();
            __wndESEK = wndESEK;
            __type = type;
            __fio = fio;
        }

        private void FIOWnd_Load(object sender, EventArgs e)
        {
            btnOk.Enabled = false;

            tbxEditName.ShortcutsEnabled = true;
            tbxEditFName.ShortcutsEnabled = true;
            tbxEditFamily.ShortcutsEnabled = true;

            try
            {
                if ("" != __fio)
                {
                    String[] partsFIO = __fio.Split(' ');
                    tbxEditName.Text = partsFIO[0];
                    tbxEditFName.Text = partsFIO[1];
                    tbxEditFamily.Text = partsFIO[2].Substring(0, 1);
                }
            }
            catch { }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (!tbxEditName.Text.Equals("") && !tbxEditFName.Text.Equals("") && !tbxEditFamily.Text.Equals(""))
            {
                switch (__type)
                {
                    case WndStatement.MODE_CREATE_STR:
                        __wndESEK.CreatePersonInfo(tbxEditName.Text, tbxEditFName.Text, tbxEditFamily.Text);
                        break;
                    case WndStatement.MODE_CHANGE_STR:
                        __wndESEK.ChangePersonalInfo(tbxEditName.Text, tbxEditFName.Text, tbxEditFamily.Text);
                        break;
                    default:
                        break;
                }
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tbxEditName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void tbxEditFName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void tbxEditFamily_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)'-');
        }

        private void tbxEditName_KeyUp(object sender, KeyEventArgs e)
        {
            if (0 < tbxEditName.Text.Length && !tbxEditFName.Text.Equals("") && !tbxEditFamily.Text.Equals(""))
            {
                btnOk.Enabled = true;
            }
            else
            {
                btnOk.Enabled = false;
            }
        }

        private void tbxEditFName_KeyUp(object sender, KeyEventArgs e)
        {
            if (0 < tbxEditFName.Text.Length && !tbxEditName.Text.Equals("") && !tbxEditFamily.Text.Equals(""))
            {
                btnOk.Enabled = true;
            }
            else
            {
                btnOk.Enabled = false;
            }
        }

        private void tbxEditFamily_KeyUp(object sender, KeyEventArgs e)
        {
            if (0 < tbxEditFamily.Text.Length && !tbxEditName.Text.Equals("") && !tbxEditFName.Text.Equals(""))
            {
                btnOk.Enabled = true;
            }
            else
            {
                btnOk.Enabled = false;
            }
        }

        private void tbxEditName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.A))
                (sender as TextBox).SelectAll();

            if (e.KeyData == (Keys.Control | Keys.X))
                (sender as TextBox).Cut();

            if (e.KeyData == (Keys.Control | Keys.C))
                (sender as TextBox).Copy();

            if (e.KeyData == (Keys.Control | Keys.V))
                (sender as TextBox).Paste();
        }

        private void tbxEditFName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.A))
                (sender as TextBox).SelectAll();

            if (e.KeyData == (Keys.Control | Keys.X))
                (sender as TextBox).Cut();

            if (e.KeyData == (Keys.Control | Keys.C))
                (sender as TextBox).Copy();

            if (e.KeyData == (Keys.Control | Keys.V))
                (sender as TextBox).Paste();
        }

        private void tbxEditFamily_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.A))
                (sender as TextBox).SelectAll();

            if (e.KeyData == (Keys.Control | Keys.X))
                (sender as TextBox).Cut();

            if (e.KeyData == (Keys.Control | Keys.C))
                (sender as TextBox).Copy();

            if (e.KeyData == (Keys.Control | Keys.V))
                (sender as TextBox).Paste();
        }
    }
}
