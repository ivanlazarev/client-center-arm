﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace armoperator
{
    public partial class WndAddBallESEK : Form
    {
        private const String TRANSPORT_BALL = "TRB";

        WndMain __mainWindow;

        private static List<TextBox> __tbxs;

        private static WkrReadCard __wkrReadCard;
        private static WkrReadOperationResults __wkrReadOperationResults;
        private static WkrGetMediasByRFID __wkrGetMediasByRFID;
        private static ResultMediasDataByRFID __resultMediasDataByRFID;
        private static WkrGetAccByAccountId __wkrGetAccByAccountId;
        private static ResultAccountDataByAccountId __resultAccountDataByAccountId;
        private static WkrGetPersonByUUID __wkrGetPersonByUUID;
        private static ResultPersonDataByUUID __resultPersonDataByUUID;
        private static WkrGetCurrencies __wkrGetCurrencies;
        private static ResultGetCurrencies __resultGetCurrencies;
        private static WkrGetBagsByAccId __wkrGetBagsByAccId;
        private static ResultGetBagsByAccId __resultGetBagsByAccId;
        private static WkrGetBagTransactions __wkrGetBagTransactions;
        private static ResultGetBagTransactions __resultGetBagTransactions;
        private static WkrReplenishment __wkrReplenishment;
        private static ResultReplenishment __resultReplenishment;
        private static String __lastRFID;
        private static int __transportCurrencyId;
        private static int __bagId;

        public WndAddBallESEK(WndMain mainWindow)
        {
            InitializeComponent();
            __mainWindow = mainWindow;
        }

        private void WndInfoESEK_Load(object sender, EventArgs e)
        {
            __tbxs = new System.Collections.Generic.List<TextBox>();
            __tbxs.Add(tbxCardRFID);
            __tbxs.Add(tbxCardNumber);
            __tbxs.Add(tbxStatus);
            __tbxs.Add(tbxPerson);
            __tbxs.Add(tbxSNILS);
            __tbxs.Add(tbxCardState);
            __tbxs.Add(tbxStatusREST);
            __tbxs.Add(tbxBallValue);
            __tbxs.Add(tbxReplenishmentResult);

            foreach (var tbx in __tbxs)
            {
                tbx.ReadOnly = true;
                tbx.BackColor = System.Drawing.SystemColors.Control;
            }

            InitializeTmrReadCard();
            InitializeTmrGetMediaByRFID();
            InitializeTmrGetAccByAccountId();
            InitializeTmrGetPersonByUUID();
            InitializeTmrGetCurrencies();
            InitializeTmrGetBagsByAccId();
            InitializeTmrGetBagTransactions();
            InitializeTmrReplenishment();

            __wkrReadCard = new WkrReadCard(WndMain.READER_NAME);
            __wkrReadOperationResults = new WkrReadOperationResults();
            __wkrGetMediasByRFID = new WkrGetMediasByRFID();
            __resultMediasDataByRFID = new ResultMediasDataByRFID();
            __wkrGetAccByAccountId = new WkrGetAccByAccountId();
            __resultAccountDataByAccountId = new ResultAccountDataByAccountId();
            __wkrGetPersonByUUID = new WkrGetPersonByUUID();
            __resultPersonDataByUUID = new ResultPersonDataByUUID();
            __wkrGetCurrencies = new WkrGetCurrencies();
            __resultGetCurrencies = new ResultGetCurrencies();
            __wkrGetBagsByAccId = new WkrGetBagsByAccId();
            __resultGetBagsByAccId = new ResultGetBagsByAccId();
            __wkrGetBagTransactions = new WkrGetBagTransactions();
            __resultGetBagTransactions = new ResultGetBagTransactions();
            __wkrReplenishment = new WkrReplenishment();
            __resultReplenishment = new ResultReplenishment();

            __lastRFID = "";
            __transportCurrencyId = 0;
            __bagId = 0;

            this.lblBallESEK.Text = "1. Гражданин приходит в центр обслуживания клиентов и" +
                                                    " передает карту оператору и называет сумму баллов для зачисления;\n\r\n\r" +
                                                "2. Оператор размещает карту на считывающем NFC устройстве;\n\r\n\r" +
                                                "3. Оператор называет итоговую стоимость, принимает от гражданина" +
                                                    " денежные средства и выдает кассовый чек (кассовый аппарат не входит в оборудование Системы);\n\r\n\r" +
                                                "4. Оператор, c использованием АРМ, выполняет запись купленного количества баллов в Системе;\n\r\n\r" +
                                                "5. Оператор возвращает карту гражданину.";

            btnReplenishment.Enabled = false;

            tmrReadCard.Start();
        }

        private void InitializeTmrReadCard()
        {
            tmrReadCard.Interval = 300;
            tmrReadCard.Tick += new EventHandler(tmrReadCard_Tick);
        }

        private void tmrReadCard_Tick(object Sender, EventArgs e)
        {
            bool readWkrStatus = __wkrReadCard.GetWkrStatus();
            if (!readWkrStatus)
            {
                __wkrReadOperationResults = __wkrReadCard.GetResultREST();
                int readStatus = __wkrReadOperationResults.readStatus;
                switch (readStatus)
                {
                    case 0:
                        try
                        {
                            String rfid = __wkrReadOperationResults.thrustCardInfo.uid;
                            if (null != rfid)
                            {
                                tbxCardRFID.Text = rfid;
                                tbxCardNumber.Text = __wkrReadOperationResults.thrustCardInfo.thrustCardInfoPart1.CardNumber;
                                tbxStatus.Text = "Данные карты считаны.";
                                if (__lastRFID != rfid)
                                {
                                    __transportCurrencyId = 0;
                                    __bagId = 0;
                                    ClearTbxs(__tbxs);

                                    __lastRFID = rfid;
                                    WkrGetMediasByRFIDData wkrGetMediasByRFIDData = new WkrGetMediasByRFIDData();
                                    wkrGetMediasByRFIDData.URL = WndMain.BASE_URL;
                                    wkrGetMediasByRFIDData.api = "/api/cp/v1/medias/";
                                    wkrGetMediasByRFIDData.rfid = rfid;
                                    wkrGetMediasByRFIDData.token = __mainWindow.GetAuthToken();
                                    __wkrGetMediasByRFID.SetWorkerData(wkrGetMediasByRFIDData);

                                    if (!__wkrGetMediasByRFID.GetWkrStatus())
                                    {
                                        __wkrGetMediasByRFID.PerformRestOperation();
                                        tmrGetMediaByRFID.Start();
                                    }
                                    tbxStatusREST.Text = "Запрос данных о карте держателя.";                                                                             
                                }
                            }
                        }
                        catch { }
                        break;
                    case 1:
                        tbxStatus.Text = "Неверный формат карты.";
                        break;
                    case 2:
                        tbxStatus.Text = "Приложите карту ещё раз.";
                        break;
                    case 3:
                        __lastRFID = "";
                        __transportCurrencyId = 0;
                        __bagId = 0;
                        ClearTbxs(__tbxs);
                        btnReplenishment.Enabled = false;
                        tbxStatus.Text = "Положите карту на считыватель.";
                        break;
                    case 4:
                        btnReplenishment.Enabled = false;
                        tbxStatus.Text = "Подключите считыватель.";
                        break;
                    case 5:
                        tbxStatus.Text = "Ошибка модели считывателя.";
                        break;
                    case 6:
                        tbxStatus.Text = "Неизвестная ошибка при чтении карты";
                        break;
                    case 7:
                        tbxStatus.Text = "Ошибка подключения к считывателю";
                        break;
                    default:
                        tbxStatus.Text = "Неизвестная ошибка";
                        break;
                }
                __wkrReadCard.PerformReadOperation();                
            }
        }

        private void InitializeTmrGetMediaByRFID()
        {
            tmrGetMediaByRFID.Interval = 100;
            tmrGetMediaByRFID.Tick += new EventHandler(tmrGetMediaByRFID_Tick);
        }

        private void tmrGetMediaByRFID_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetMediasByRFID.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrGetMediaByRFID.Stop();
                __resultMediasDataByRFID = __wkrGetMediasByRFID.GetResultREST();
                switch (__resultMediasDataByRFID.statusCode)
                {
                    case 200:
                        try
                        {
                            int accountId = __resultMediasDataByRFID.mediasDataByRFID.accountId;
                            String cardState = __resultMediasDataByRFID.mediasDataByRFID.states[0].state;
                            switch (cardState)
                            {
                                case  WndStatement.CARD_REGISTERED_STR:
                                    tbxCardState.Text = "Карта зарегестрирована.";
                                    MessageBox.Show("Карта не активирована. Операция зачисления баллов недоступна.");
                                    break;
                                case WndStatement.CARD_ACTIVATED_STR:
                                    tbxCardState.Text = "Карта активирована.";
                                    break;
                                case WndStatement.CARD_BLOCKED_STR:
                                    MessageBox.Show("Карта заблокирована. Операция зачисления баллов недоступна.");
                                    tbxCardState.Text = "Карта заблокирована.";
                                    break;
                                default:
                                    break;
                            }

                            WkrGetAccByAccountIdData wkrGetAccByAccountIdData = new WkrGetAccByAccountIdData();
                            wkrGetAccByAccountIdData.URL = WndMain.BASE_URL;
                            wkrGetAccByAccountIdData.api = "/api/cp/v1/accounts/";
                            wkrGetAccByAccountIdData.accountId = Convert.ToString(accountId);
                            wkrGetAccByAccountIdData.token = __mainWindow.GetAuthToken();
                            __wkrGetAccByAccountId.SetWorkerData(wkrGetAccByAccountIdData);

                            if (!__wkrGetAccByAccountId.GetWkrStatus())
                            {
                                __wkrGetAccByAccountId.PerformRestOperation();
                                tmrGetAccByAccountId.Start();
                            }
                            tbxStatusREST.Text = "Запрос счета держателя карты.";

                            //отсюда запрос списка валют и кошельков
                            WkrGetCurrenciesData wkrGetCurrenciesData = new WkrGetCurrenciesData();
                            wkrGetCurrenciesData.URL = WndMain.BASE_URL;
                            wkrGetCurrenciesData.api = "/api/cp/v1/applications/";
                            wkrGetCurrenciesData.applicationId = __mainWindow.GetApplicationId().ToString() + "/currencies";
                            wkrGetCurrenciesData.token = __mainWindow.GetAuthToken();
                            __wkrGetCurrencies.SetWorkerData(wkrGetCurrenciesData);

                            if (!__wkrGetCurrencies.GetWkrStatus())
                            {
                                __wkrGetCurrencies.PerformRestOperation();
                                tmrGetCurrencies.Start();
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("У данной карты отсутствует держатель.");
                        break;
                    case 401:
                        MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        WndAuth authWnd = new WndAuth(__mainWindow);
                        authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetAccByAccountId()
        {
            tmrGetAccByAccountId.Interval = 100;
            tmrGetAccByAccountId.Tick += new EventHandler(tmrGetAccByAccountId_Tick);
        }

        private void tmrGetAccByAccountId_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetAccByAccountId.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrGetAccByAccountId.Stop();
                __resultAccountDataByAccountId = __wkrGetAccByAccountId.GetResultREST();
                switch (__resultAccountDataByAccountId.statusCode)
                {
                    case 200:
                        try
                        {
                            String userId = __resultAccountDataByAccountId.accountDataByAccountId.userId;

                            WkrPersonByUUIDWorkerData wkrPersonByUUIDWorkerData = new WkrPersonByUUIDWorkerData();
                            wkrPersonByUUIDWorkerData.URL = WndMain.BASE_URL;
                            wkrPersonByUUIDWorkerData.api = "/api/spd/v1/persons/";
                            wkrPersonByUUIDWorkerData.uuid = userId;
                            wkrPersonByUUIDWorkerData.token = __mainWindow.GetAuthToken();
                            __wkrGetPersonByUUID.SetWorkerData(wkrPersonByUUIDWorkerData);

                            if (!__wkrGetPersonByUUID.GetWkrStatus())
                            {
                                __wkrGetPersonByUUID.PerformRestOperation();
                                tmrGetPersonByUUID.Start();
                            }
                            tbxStatusREST.Text = "Запрос сведений о держателе карты.";
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        WndAuth authWnd = new WndAuth(__mainWindow);
                        authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetPersonByUUID()
        {
            tmrGetPersonByUUID.Interval = 100;
            tmrGetPersonByUUID.Tick += new EventHandler(tmrGetPersonByUUID_Tick);
        }

        private void tmrGetPersonByUUID_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetPersonByUUID.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrGetPersonByUUID.Stop();
                __resultPersonDataByUUID = __wkrGetPersonByUUID.GetResultREST();
                switch (__resultPersonDataByUUID.statusCode)
                {
                    case 200:
                        try
                        {
                            String personInfo = __resultPersonDataByUUID.personDataByUUID.name;
                            String snilsInfo = __resultPersonDataByUUID.personDataByUUID.snils;
                            tbxPerson.Text = personInfo;
                            tbxSNILS.Text = snilsInfo;
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        WndAuth authWnd = new WndAuth(__mainWindow);
                        authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }        

        private void InitializeTmrGetCurrencies()
        {
            tmrGetCurrencies.Interval = 100;
            tmrGetCurrencies.Tick += new EventHandler(tmrGetCurrencies_Tick);
        }

        private void tmrGetCurrencies_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetCurrencies.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrGetCurrencies.Stop();
                __resultGetCurrencies = __wkrGetCurrencies.GetResultREST();
                switch (__resultGetCurrencies.statusCode)
                {
                    case 200:
                        try
                        {
                            foreach (var currency in __resultGetCurrencies.currenciesData)
                            {
                                if (currency.symbol == TRANSPORT_BALL)
                                {
                                    __transportCurrencyId = currency.id;
                                    WkrGetBagsByAccIdData wkrGetBagsByAccIdData = new WkrGetBagsByAccIdData();
                                    wkrGetBagsByAccIdData.api = "/api/cp/v1/accounts/";
                                    wkrGetBagsByAccIdData.accountId = __resultMediasDataByRFID.mediasDataByRFID.accountId.ToString() + "/bags";
                                    wkrGetBagsByAccIdData.URL = WndMain.BASE_URL;
                                    wkrGetBagsByAccIdData.token = __mainWindow.GetAuthToken();
                                    __wkrGetBagsByAccId.SetWorkerData(wkrGetBagsByAccIdData);
                                    if (!__wkrGetBagsByAccId.GetWkrStatus())
                                    {
                                        __wkrGetBagsByAccId.PerformRestOperation();
                                        tmrGetBagsByAccId.Start();
                                    }
                                    break;
                                }
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Приложение не найдено.");
                        break;
                    case 401:
                        MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        WndAuth authWnd = new WndAuth(__mainWindow);
                        authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetBagsByAccId()
        {
            tmrGetBagsByAccId.Interval = 100;
            tmrGetBagsByAccId.Tick += new EventHandler(tmrGetBagsByAccId_Tick);
        }

        private void tmrGetBagsByAccId_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetBagsByAccId.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrGetBagsByAccId.Stop();
                __resultGetBagsByAccId = __wkrGetBagsByAccId.GetResultREST();
                switch (__resultGetBagsByAccId.statusCode)
                {
                    case 200:
                        try
                        {
                            if (0 != __resultGetBagsByAccId.bagsData.Count)
                            {
                                foreach (var bag in __resultGetBagsByAccId.bagsData)
                                {
                                    if (__transportCurrencyId == bag.currencyId)
                                    {
                                        __bagId = bag.id;
                                        WkrGetBagTransactionsData wkrGetBagTransactionsData = new WkrGetBagTransactionsData();
                                        wkrGetBagTransactionsData.URL = WndMain.BASE_URL;
                                        wkrGetBagTransactionsData.api = "/api/cp/v1/accounts/";
                                        wkrGetBagTransactionsData.accountId = __resultMediasDataByRFID.mediasDataByRFID.accountId.ToString() + "/bags/";
                                        wkrGetBagTransactionsData.bagId = __bagId.ToString() + "/transactions";
                                        wkrGetBagTransactionsData.token = __mainWindow.GetAuthToken();
                                        __wkrGetBagTransactions.SetWorkerData(wkrGetBagTransactionsData);
                                        if (!__wkrGetBagTransactions.GetWkrStatus())
                                        {
                                            __wkrGetBagTransactions.PerformRestOperation();
                                            tmrGetBagTransactions.Start();
                                        }
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                tbxStatusREST.Text = "Данные о держателе карты получены";
                                tbxBallValue.Text = "Транспортные баллы на счете отсутствуют";
                            }

                            if ("" == tbxBallValue.Text)
                                tbxBallValue.Text = "Транспортные баллы на счете отсутствуют.";

                            if (WndStatement.CARD_ACTIVATED_STR == __resultMediasDataByRFID.mediasDataByRFID.states[0].state)
                            {
                                btnReplenishment.Enabled = true;
                            }

                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Кошельки отсутствуют.");
                        break;
                    case 401:
                        MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        WndAuth authWnd = new WndAuth(__mainWindow);
                        authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetBagTransactions()
        {
            tmrGetBagTransactions.Interval = 100;
            tmrGetBagTransactions.Tick += new EventHandler(tmrGetBagTransactions_Tick);
        }

        private void tmrGetBagTransactions_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetBagTransactions.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrGetBagTransactions.Stop();
                __resultGetBagTransactions = __wkrGetBagTransactions.GetResultREST();
                switch (__resultGetBagTransactions.statusCode)
                {
                    case 200:
                        try
                        {
                            if (0 != __resultGetBagTransactions.transactionsData.Count)
                            {
                                int ballValue = 0;
                                foreach (var transaction in __resultGetBagTransactions.transactionsData)
                                {
                                    switch (transaction.type)
                                    {
                                        case "replenishment":
                                            ballValue += transaction.value;
                                            break;
                                        case "payment":
                                            ballValue -= transaction.value;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                tbxStatusREST.Text = "Данные о держателе карты получены";
                                tbxBallValue.Text = ballValue.ToString();
                            }
                            else
                            {
                                tbxBallValue.Text = "Транзакции не проводились";
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Транзакции отсутствуют.");
                        break;
                    case 401:
                        MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        WndAuth authWnd = new WndAuth(__mainWindow);
                        authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrReplenishment()
        {
            tmrReplenishment.Interval = 100;
            tmrReplenishment.Tick += new EventHandler(tmrReplenishment_Tick);
        }

        private void tmrReplenishment_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrReplenishment.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrReplenishment.Stop();
                __resultReplenishment = __wkrReplenishment.GetResultREST();
                switch (__resultReplenishment.statusCode)
                {
                    case 200:
                        try
                        {
                            tbxReplenishmentResult.Text = "Баллы успешно зачислены";
                            if (!__wkrGetBagsByAccId.GetWkrStatus())
                            {
                                __wkrGetBagsByAccId.PerformRestOperation();
                                tmrGetBagsByAccId.Start();
                            }
                        }
                        catch { }
                        break;
                    case 400:
                        MessageBox.Show("Неправильный запрос.");
                        break;
                    case 404:
                        MessageBox.Show("Транзакции отсутствуют.");
                        break;
                    case 401:
                        MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        WndAuth authWnd = new WndAuth(__mainWindow);
                        authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        public static DateTime UnixTimestampToDateTime(double unixTime)
        {
            DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            long unixTimeStampInTicks = (long)(unixTime * TimeSpan.TicksPerSecond);
            return new DateTime(unixStart.Ticks + unixTimeStampInTicks);
        }

        private void ClearTbxs(List<TextBox> textBoxes)
        {
            try
            {
                foreach (var textBox in textBoxes)
                    textBox.Text = "";
            }
            catch { }
        }

        private void WndInfoESEK_FormClosing(object sender, FormClosingEventArgs e)
        {
            __lastRFID = "";
            this.Dispose(true);
        }

        private void btnReplenishment_Click(object sender, EventArgs e)
        {
            if (!tbxReplenishmentValue.Text.Length.Equals(0))
            {
                UInt16 value = 0;
                try
                {
                    value = Convert.ToUInt16(tbxReplenishmentValue.Text);
                    if (!value.Equals(0))
                    {
                        WndConfirmAddBall wndConfirmAddBall = new WndConfirmAddBall();
                        wndConfirmAddBall.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, value.ToString());
                        wndConfirmAddBall.ShowDialog();
                        if (DialogResult.OK == wndConfirmAddBall.DialogResult)
                        {
                            try
                            {
                                DateTime baseDate = new DateTime(1970, 01, 01);
                                DateTime timeStampNow;
                                TimeSpan ts;
                                UInt64 totalsecondsTimeStampNow = 0;
                                WkrReplenishmentData wkrReplenishmentData = new WkrReplenishmentData();
                                wkrReplenishmentData.URL = WndMain.BASE_URL;
                                wkrReplenishmentData.api = "/api/cp/v1/replenishments";
                                timeStampNow = DateTime.UtcNow;
                                ts = timeStampNow - baseDate;
                                totalsecondsTimeStampNow = Convert.ToUInt64(ts.TotalSeconds);
                                var replenishmentPayload = new ReplenishmentPayload
                                {
                                    symbol = TRANSPORT_BALL,
                                    rfid = __wkrReadOperationResults.thrustCardInfo.uid,
                                    timestamp = totalsecondsTimeStampNow,
                                    value = (int)value,//Convert.ToInt32(tbxReplenishmentValue.Text),
                                    providerId = __mainWindow.GetProviderId()
                                };
                                wkrReplenishmentData.replenishmentPayload = replenishmentPayload;
                                wkrReplenishmentData.token = __mainWindow.GetAuthToken();
                                __wkrReplenishment.SetWorkerData(wkrReplenishmentData);
                                if (!__wkrReplenishment.GetWkrStatus())
                                {
                                    __wkrReplenishment.PerformRestOperation();
                                    tmrReplenishment.Start();
                                }
                                btnReplenishment.Enabled = false;
                            }
                            catch
                            {
                                MessageBox.Show("Операция зачисления недоступна");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Укажите количество транспортных баллов для зачисления отличное от нуля.");
                    }
                }
                catch
                {
                    MessageBox.Show("Введите количество транспортных баллов для записи в диапазоне от 1 до " + UInt16.MaxValue.ToString());
                }
            }
            else
            {
                MessageBox.Show("Укажите количество транспортных баллов для зачисления.");
            }
        }
        
        private void tbxReplenishmentValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(e.KeyChar == (char)Keys.Back || char.IsDigit(e.KeyChar));
        }
    }
}
