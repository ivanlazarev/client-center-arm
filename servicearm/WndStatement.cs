﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using System.IO;

namespace armoperator
{
    public partial class WndStatement : Form
    {
        public const short SNILS_LENGTH = 11;
        public const String CREATE_USER_STR = "Зарегестрировать пользователя";
        public const String CHANGE_USER_STR = "Изменить данные пользователя";
        public const String MODE_CREATE_STR = "create";
        public const String MODE_CHANGE_STR = "change";
        public const String NO_SPD_USER_STR = "Данные держателя карты отсутствуют.";
        public const String NO_SPD_SNILS_STR = "Данные номера СНИЛС отсутствуют.";
        public const String CARD_REGISTERED_STR = "registered";
        public const String CARD_ACTIVATED_STR = "activated";
        public const String CARD_BLOCKED_STR = "blocked";
        public const String PDF_FILE_NAME = "esekDoc.pdf";
        public const String IMG_FILE_NAME = "photoPic.jpg";

        WndMain __mainWindow;

        static WkrGetPersonInfo __wkrGetPersonInfo;
        static WkrUpdatePersonalInfo __wkrUpdatePersonalInfo;
        static WkrCreatePersonalInfo __wkrCreatePersonalInfo;
        static WkrGetMediasById __wkrGetMediasById;
        static WkrGetAccByUUID __wkrGetAccByUUID;
        static ResultPersonDataBySNILS __resultPersonDataBySNILS;
        static ResultAccountDataByUUID __resultAccountDataByUUID;
        static ResultMediasDataById __resultMediasDataById;
        static AppSettings __appSettings;
        static String __titleCardReason;
        static String __bodyCardReason;
        static bool __isdraw;
        WndCam2 __camView2;
        static bool __isNewStatement;

        public WndStatement(WndMain mainWindow)
        {            
            InitializeComponent();
            __mainWindow = mainWindow;            
        }

        private void btnCancelPersonalInfo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBackPersonalInfo_Click(object sender, EventArgs e)
        {
            this.tabESEK.SelectedIndex = 0;
        }

        private void btnNextPersonalInfo_Click(object sender, EventArgs e)
        {

            WkrGetAccByUUIDData wkrGetAccByUUIDData = new WkrGetAccByUUIDData();
            wkrGetAccByUUIDData.URL = WndMain.BASE_URL;
            wkrGetAccByUUIDData.api = @"/api/cp/v1/accounts?filter={""userId"":""";
            wkrGetAccByUUIDData.uuid = __resultPersonDataBySNILS.personDataBySNILS.uuid + "\"}";
            wkrGetAccByUUIDData.token = __mainWindow.GetAuthToken();
            __wkrGetAccByUUID.SetWorkerData(wkrGetAccByUUIDData);
            tbxCardNum.Text = "";
            tbxCardState.Text = "";
            if (!__wkrGetAccByUUID.GetWkrStatus())
            {
                __wkrGetAccByUUID.PerformRestOperation();
                timer4.Start();
            }
        }

        private void wndESEK_Load(object sender, EventArgs e)
        {
            // Ввод СНИЛС
            btnNextEnterSNILS.Enabled = false;
            this.ActiveControl = tbxEnterSNILS;
            // Ввод СНИЛС
            
            tabESEK.Appearance = TabAppearance.FlatButtons;
            tabESEK.ItemSize = new Size(0, 1);
            tabESEK.SizeMode = TabSizeMode.Fixed;

            InitializeTimer1();
            InitializeTimer2();
            InitializeTimer3();
            InitializeTimer4();
            InitializeTimer5();

            __wkrGetPersonInfo = new WkrGetPersonInfo();
            __wkrUpdatePersonalInfo = new WkrUpdatePersonalInfo();
            __wkrCreatePersonalInfo = new WkrCreatePersonalInfo();
            __wkrGetMediasById = new WkrGetMediasById();
            __wkrGetAccByUUID = new WkrGetAccByUUID();
            __resultPersonDataBySNILS = new ResultPersonDataBySNILS();
            __resultAccountDataByUUID = new ResultAccountDataByUUID();
            __resultMediasDataById = new ResultMediasDataById();
            __appSettings = new AppSettings();
            __titleCardReason = "";
            __bodyCardReason = "";
            __isdraw = false;
            __isNewStatement = false;

            this.lblESEKInfo.Text = "1. Гражданин приходит в центр обслуживания и" +
                                                " обращается к оператору за помощью в оформлении заявления на выпуск ЕСЭК;\n\r\n\r" +
                                                "2. Оператор проверяет наличие у гражданина " +
                                                "списка необходимых для подачи заявления документов;\n\r\n\r" +
                                                "3. Оператор проверяет наличие ранее выданной действующей" +
                                                " социальной карты и в случае ее нахождения отказывает в приеме заявки;\n\r\n\r" +
                                                "4. Оператор выполняет фотографирование гражданина " +
                                                "с помощью оборудования АРМ, заполняет заявление ЕСЭК " +
                                                "в электронной форме  и распечатывает его. Распечатанный " +
                                                "бланк заявления  должен содержать, черно-белую фотографию " +
                                                "заявителя, уникальный номер заявки и отрывную квитанцию;\n\r\n\r" +
                                                "5. Оператор передает распечатанный бланк заявления гражданину " +
                                                "для проверки заполненных  полей: ФИО, дата рождения, СНИЛС, кодовое слово;\n\r\n\r" +
                                                "6. Гражданин подписывает заявление на получение ЕСЭК и передает его оператору;\n\r\n\r" +
                                                "7. Оператор отрывает квитанцию, подписывает ее и передает гражданину;\n\r\n\r" +
                                                "8. Бумажные заявления передаются курьером в офис организации-оператора для дальнейшей обработки.";

            dtpBirthDate.MaxDate = DateTime.Now;
            dtpDocDate.MaxDate = DateTime.Now;

        }

        private void btnCancelEnterSNILS_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNextEnterSNILS_Click(object sender, EventArgs e)
        {
            if (!tbxEnterSNILS.Text.Equals(String.Empty))
            {
                tbxEnterSNILS.Enabled = false;
                btnNextEnterSNILS.Enabled = false;
                //btnCancelEnterSNILS.Enabled = false;
                enterSNILS(tbxEnterSNILS.Text);
            }
            else
            {
                MessageBox.Show("Введите номер СНИЛС");
            }
        }

        private void enterSNILS(String snilsNum)
        {
            try
            {
                GetUUIDWorkerData wkrGetPersonInfoData = new GetUUIDWorkerData();
                wkrGetPersonInfoData.api = "/api/spd/v1/persons/snils/";
                wkrGetPersonInfoData.SNILS = snilsNum;
                wkrGetPersonInfoData.URL = WndMain.BASE_URL;
                wkrGetPersonInfoData.token = __mainWindow.GetAuthToken();
                __wkrGetPersonInfo.SetWorkerData(wkrGetPersonInfoData);
                if (!__wkrGetPersonInfo.GetWkrStatus())
                {
                    __wkrGetPersonInfo.PerformRestOperation();
                    timer1.Start();
                }
            }
            catch { }
        }

        private void btnCancelListESEK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBackListESEK_Click(object sender, EventArgs e)
        {
            this.tabESEK.SelectedIndex = 1;
        }

        private void btnNextListESEK_Click(object sender, EventArgs e)
        {
            if (__isdraw)
            {
                __isdraw = false;
            }

            if (null != imgPhoto.Image)
            {
                imgPhoto.Image = null;
            }

            tbxBirthPlace.Text = "";
            tbxDocSeries.Text = "";
            tbxDocNumber.Text = "";
            tbxDocSupplier.Text = "";
            tbxDocReleaserCode.Text = "";
            dtpBirthDate.Text = DateTime.Now.ToShortDateString();
            dtpDocDate.Text = DateTime.Now.ToShortDateString();
            cbxGender.SelectedIndex = 0;
            cbxNation.SelectedIndex = 0;
            tbxDocType.SelectedIndex = 0;

            this.tabESEK.SelectedIndex = 3;

            if (radioButton1.Checked)
            {
                __titleCardReason = "ВЫПУСК ";
                __bodyCardReason = " выпуска";
                lblStatementType.Text = "Ввод данных для оформления заявления на выпуск ЕСЭК";
            }
            else if (radioButton2.Checked)
            {
                __titleCardReason = "ЗАМЕНУ ";
                __bodyCardReason = " замены";
                lblStatementType.Text = "Ввод данных для оформления заявления на замену ЕСЭК";
            }
            else if (radioButton3.Checked)
            {
                __titleCardReason = "БЛОКИРОВКУ ";
                __bodyCardReason = " блокировки";
                lblStatementType.Text = "Ввод данных для оформления заявления на блокировку ЕСЭК";
            }
            else if (radioButton4.Checked)
            {
                __titleCardReason = "РАЗБЛОКИРОВКУ ";
                __bodyCardReason = " разблокировки";
                lblStatementType.Text = "Ввод данных для оформления заявления на разблокировку ЕСЭК";
            }
            else if (radioButton5.Checked)
            {
                __titleCardReason = "ОТКАЗ ОТ ";
                __bodyCardReason = " отказа от";
                lblStatementType.Text = "Ввод данных для оформления заявления на отказ от ЕСЭК";
            }

            try
            {
                String[] partsFIO = tbxSPDFIO.Text.Split(' ');
                tbxName.Text = partsFIO[0];
                tbxFatherName.Text = partsFIO[1];
                tbxFamily.Text = partsFIO[2].Substring(0, 1);
            }
            catch { }

            tbxSNILS.Text = tbxSPDSNILS.Text;

        }

        private void btnCancelGenStatement_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBackGenStatement_Click(object sender, EventArgs e)
        {
            this.tabESEK.SelectedIndex = 2;
        }

        private void btnPrintPDF_Click(object sender, EventArgs e)
        {
            if (null != imgPhoto.Image)
            {
                StringBuilder message = new StringBuilder("Заполните следующие поля:\n");

                if (tbxFamily.Text.Length.Equals(1) || tbxFamily.Text.Equals(""))
                {
                    message.Append("- Фамилия\n");
                }
                if (tbxBirthPlace.Text.Equals(""))
                {
                    message.Append("- Место рождения\n");
                }
                if (tbxDocSeries.Text.Equals(""))
                {
                    message.Append("- Серия\n");
                }
                if (tbxDocNumber.Text.Equals(""))
                {
                    message.Append("- Номер\n");
                }
                if (tbxDocSupplier.Text.Equals(""))
                {
                    message.Append("- Кем выдан\n");
                }
                if (tbxDocReleaserCode.Text.Equals(""))
                {
                    message.Append("- Код подразделения\n");
                }

                if (!tbxBirthPlace.Text.Equals("") &&
                        !tbxDocSeries.Text.Equals("") &&
                        !tbxDocNumber.Text.Equals("") &&
                        !tbxDocSupplier.Text.Equals("") &&
                        !tbxDocReleaserCode.Text.Equals(""))
                {
                    try
                    {
                        using (Stream stream = new FileStream(WndMain.SETTINGS_FILE_NAME, FileMode.Open))
                        {
                            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(AppSettings));
                            __appSettings = (AppSettings)serializer.Deserialize(stream);
                        }
                        esekPDF();
                    }
                    catch
                    {
                        MessageBox.Show("Отсутствует файл settings.xml. Перезапустите приложение.");
                    }
                }
                else
                {
                    MessageBox.Show(message.ToString(), "Требуется ввести данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    message.Clear();
                }
            }
            else
            {
                MessageBox.Show("Сделайте фото.");
            }
        }

        private void btnCloseGenStatement_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void InitializeTimer1()
        {
            timer1.Interval = 100;
            timer1.Tick += new EventHandler(timer1_Tick);
        }

        private void InitializeTimer2()
        {
            timer2.Interval = 100;
            timer2.Tick += new EventHandler(timer2_Tick);
        }

        private void InitializeTimer3()
        {
            timer3.Interval = 100;
            timer3.Tick += new EventHandler(timer3_Tick);
        }

        private void InitializeTimer4()
        {
            timer4.Interval = 100;
            timer4.Tick += new EventHandler(timer4_Tick);
        }

        private void InitializeTimer5()
        {
            timer5.Interval = 100;
            timer5.Tick += new EventHandler(timer5_Tick);
        }

        private void timer1_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (__wkrGetPersonInfo.GetWkrStatus().Equals(false))
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                timer1.Stop();
                __resultPersonDataBySNILS = __wkrGetPersonInfo.GetResultREST();
                switch (__resultPersonDataBySNILS.statusCode)
                {
                    case 200:
                        tbxFamily.Text = String.Empty;
                        tbxFamily.Text = String.Empty;
                        tbxFatherName.Text = String.Empty;
                        this.tabESEK.SelectedIndex = 1;
                        try
                        {
                            tbxSPDFIO.Text = __resultPersonDataBySNILS.personDataBySNILS.name;
                            tbxSPDSNILS.Text = __resultPersonDataBySNILS.personDataBySNILS.snils;
                            btnNextPersonalInfo.Enabled = true;
                            btnCreatePersonInfo.Text = CHANGE_USER_STR;
                            this.ActiveControl = btnNextPersonalInfo;
                        }
                        catch { }
                        break;
                    case 404:
                        tbxFamily.Text = String.Empty;
                        tbxFamily.Text = String.Empty;
                        tbxFatherName.Text = String.Empty;
                        this.tabESEK.SelectedIndex = 1;
                        tbxSPDFIO.Text = NO_SPD_USER_STR;
                        tbxSPDSNILS.Text = NO_SPD_SNILS_STR;
                        btnNextPersonalInfo.Enabled = true;
                        this.ActiveControl = btnNextPersonalInfo;
                        btnCreatePersonInfo.Text = CREATE_USER_STR;
                        break;
                    case 401:
                        tbxEnterSNILS.Enabled = true;
                        btnNextEnterSNILS.Enabled = true;
                        btnCancelEnterSNILS.Enabled = true;
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и повторите запрос.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и повторите запрос.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        tbxEnterSNILS.Enabled = true;
                        btnNextEnterSNILS.Enabled = true;
                        btnCancelEnterSNILS.Enabled = true;
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        tbxEnterSNILS.Enabled = true;
                        btnNextEnterSNILS.Enabled = true;
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }        
        }

        private void timer2_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (__wkrUpdatePersonalInfo.GetWkrStatus().Equals(false))
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                timer2.Stop();
                __resultPersonDataBySNILS = __wkrUpdatePersonalInfo.GetResultREST();
                switch (__resultPersonDataBySNILS.statusCode)
                {
                    case 200:
                        try
                        {
                            tbxSPDFIO.Text = __resultPersonDataBySNILS.personDataBySNILS.name;
                            tbxSPDSNILS.Text = __resultPersonDataBySNILS.personDataBySNILS.snils;
                        }
                        catch { }
                        break;
                    case 401:
                        tbxEnterSNILS.Enabled = true;
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и повторите запрос.");
                            __mainWindow.Authorize();
                        }
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void timer3_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (__wkrCreatePersonalInfo.GetWkrStatus().Equals(false))
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                timer3.Stop();
                __resultPersonDataBySNILS = __wkrCreatePersonalInfo.GetResultREST();
                switch (__resultPersonDataBySNILS.statusCode)
                {
                    case 200:
                        try
                        {
                            tbxSPDFIO.Text = __resultPersonDataBySNILS.personDataBySNILS.name;
                            tbxSPDSNILS.Text = __resultPersonDataBySNILS.personDataBySNILS.snils;
                            btnNextPersonalInfo.Enabled = true;
                        }
                        catch { }
                        break;
                    case 401:
                        tbxEnterSNILS.Enabled = true;
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и повторите запрос.");
                            __mainWindow.Authorize();
                        }
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void timer4_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (__wkrGetAccByUUID.GetWkrStatus().Equals(false))
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                timer4.Stop();
                __resultAccountDataByUUID = __wkrGetAccByUUID.GetResultREST();
                switch (__resultAccountDataByUUID.statusCode)
                {
                    case 200:
                        try
                        {
                            WkrGetMediasByIdData wkrGetMediasByIdData = new WkrGetMediasByIdData();
                            wkrGetMediasByIdData.URL = WndMain.BASE_URL;
                            wkrGetMediasByIdData.api = "/api/cp/v1/accounts/";
                            wkrGetMediasByIdData.id = __resultAccountDataByUUID.accountDataByUUID[0].id.ToString() + "/medias";
                            wkrGetMediasByIdData.token = __mainWindow.GetAuthToken();
                            __wkrGetMediasById.SetWorkerData(wkrGetMediasByIdData);
                            if (!__wkrGetMediasById.GetWkrStatus())
                            {
                                __wkrGetMediasById.PerformRestOperation();
                                timer5.Start();
                            }
                            this.tabESEK.SelectedIndex = 2;
                        }
                        catch { }
                        break;
                    case 401:
                        tbxEnterSNILS.Enabled = true;
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и повторите запрос.");
                            __mainWindow.Authorize();
                        }
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void timer5_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (__wkrGetMediasById.GetWkrStatus().Equals(false))
            {
                __isNewStatement = false;
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                timer5.Stop();
                __resultMediasDataById = __wkrGetMediasById.GetResultREST();
                switch (__resultMediasDataById.statusCode)
                {
                    case 200:
                        tbxCardNum.Text = "";
                        tbxCardState.Text = "";
                        try
                        {
                            if (0 == __resultMediasDataById.mediasDataById[0].accountId)
                            {
                                MessageBox.Show("У данного пользователя отсутствуют карты.");
                                radioButton1.Enabled = false;
                                radioButton2.Enabled = false;
                                radioButton3.Enabled = false;
                                radioButton4.Enabled = false;
                            }
                            else
                            {
                                tbxCardNum.Text = __resultMediasDataById.mediasDataById[0].rfid;
                                switch (__resultMediasDataById.mediasDataById[0].states[0].state)
                                {
                                    case CARD_REGISTERED_STR:
                                        radioButton1.Checked = true;
                                        radioButton1.Enabled = true;
                                        radioButton2.Enabled = false;
                                        radioButton3.Enabled = false;
                                        radioButton4.Enabled = false;
                                        radioButton5.Enabled = true;
                                        tbxCardState.Text = "Карта зарегестрирована.";
                                        btnNextListESEK.Enabled = true;
                                        __isNewStatement = true;
                                        break;
                                    case CARD_ACTIVATED_STR:
                                        radioButton1.Enabled = false;
                                        radioButton2.Enabled = true;
                                        radioButton2.Checked = true;
                                        radioButton3.Enabled = true;
                                        radioButton4.Enabled = false;
                                        radioButton5.Enabled = true;
                                        tbxCardState.Text = "Карта активирована.";
                                        btnNextListESEK.Enabled = true;
                                        break;
                                    case CARD_BLOCKED_STR:
                                        radioButton1.Enabled = false;
                                        radioButton2.Enabled = true;
                                        radioButton3.Enabled = false;
                                        radioButton4.Enabled = true;
                                        radioButton4.Checked = true;
                                        radioButton5.Enabled = true;
                                        tbxCardState.Text = "Карта заблокирована.";
                                        btnNextListESEK.Enabled = true;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        radioButton1.Enabled = true;
                        radioButton1.Checked = true;
                        radioButton2.Enabled = false;
                        radioButton3.Enabled = false;
                        radioButton4.Enabled = false;
                        radioButton5.Enabled = false;
                        tbxCardNum.Text = "Отсутствует.";
                        tbxCardState.Text = "Карта не зарегестрирована.";
                        btnNextListESEK.Enabled = true;
                        __isNewStatement = true;
                        break;
                    case 401:
                        tbxEnterSNILS.Enabled = true;
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и повторите запрос.");
                            __mainWindow.Authorize();
                        }
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }
        
        public void ChangePersonalInfo(String name, String fname, String family)
        {
            try
            {
                WkrUpdatePersonalInfoData wkrUpdatePersonalInfoData = new WkrUpdatePersonalInfoData();
                wkrUpdatePersonalInfoData.api = "/api/spd/v1/persons/";
                wkrUpdatePersonalInfoData.uuid = __resultPersonDataBySNILS.personDataBySNILS.uuid;
                wkrUpdatePersonalInfoData.URL = WndMain.BASE_URL;
                wkrUpdatePersonalInfoData.token = __mainWindow.GetAuthToken();
                wkrUpdatePersonalInfoData.name = name + " " + fname + " " + family.Substring(0, 1) + ".";
                __wkrUpdatePersonalInfo.SetWorkerData(wkrUpdatePersonalInfoData);
                if (!__wkrUpdatePersonalInfo.GetWkrStatus())
                {
                    __wkrUpdatePersonalInfo.PerformRestOperation();
                    timer2.Start();
                }
            }
            catch { }
        }

        private void btnCreatePersonInfo_Click(object sender, EventArgs e)
        {
            String mode = "";
            switch (btnCreatePersonInfo.Text)
            {
                case CREATE_USER_STR:
                    mode = MODE_CREATE_STR;
                    break;
                case CHANGE_USER_STR:
                    mode = MODE_CHANGE_STR;
                    break;
                default:
                    break;
            }
            String spdFioStr = "";
            if (!tbxSPDFIO.Text.Equals(NO_SPD_USER_STR))
            {
                spdFioStr = tbxSPDFIO.Text;
            }
            WndFIO fioWnd = new WndFIO(this, mode, spdFioStr);
            fioWnd.ShowDialog();
        }

        public void CreatePersonInfo(String name, String fname, String family)
        {
            try
            {
                WkrCreatePersonalInfoData wkrCreatePersonalInfoData = new WkrCreatePersonalInfoData();
                wkrCreatePersonalInfoData.api = "/api/spd/v1/persons";
                wkrCreatePersonalInfoData.snils = tbxEnterSNILS.Text;
                wkrCreatePersonalInfoData.URL = WndMain.BASE_URL;
                wkrCreatePersonalInfoData.token = __mainWindow.GetAuthToken();
                wkrCreatePersonalInfoData.name = name + " " + fname + " " + family.Substring(0, 1) + ".";
                __wkrCreatePersonalInfo.SetWorkerData(wkrCreatePersonalInfoData);
                __wkrCreatePersonalInfo.PerformRestOperation();
                timer3.Start();
            }
            catch { }
        }

        private void esekPDF()
        {
            try
            {
                var esekDoc = new Document();
                BaseFont baseFont = BaseFont.CreateFont(@"C:\Windows\Fonts\times.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 8, iTextSharp.text.Font.NORMAL);
                //String appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                //String esekDocFileName = Path.Combine(appPath, "esekDoc.pdf");

                String blankNum = __appSettings.m_OperatorStation/* tbxOperatorDep.Text*/ + DateTime.Now.ToString("yy") + DateTime.Now.ToString("MM") +
                    DateTime.Now.ToString("dd") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + __mainWindow.GetLblOperatorLoginText();/*__appSettings.m_OperatorCode;*/// tbxOperatorName.Text;


                PdfWriter.GetInstance(esekDoc, new FileStream(blankNum + PDF_FILE_NAME, FileMode.Create));
                esekDoc.Open();
                String text = String.Empty;
                iTextSharp.text.Font headerFont = new iTextSharp.text.Font(baseFont, 11, iTextSharp.text.Font.NORMAL);

                // заявление

                text = "Номер бланка: " + blankNum;

                iTextSharp.text.Font statementFont = new iTextSharp.text.Font(baseFont, 11, iTextSharp.text.Font.BOLD);
                esekDoc.Add(new Paragraph(new Phrase(text, statementFont)) { Alignment = 2 });

                // предмет
                text = "НА "+ __titleCardReason + "СОЦИАЛЬНОЙ ЭЛЕКТРОННОЙ КАРТЫ ЖИТЕЛЯ РЕСПУБЛИКИ МОРДОВИЯ\n\r";
                esekDoc.Add(new Paragraph(new Phrase(text, headerFont)) { Alignment = 1 });

                // таблица
                PdfPTable table = new PdfPTable(10);
                table.WidthPercentage = 100;

                table.AddCell(new PdfPCell(new Phrase("ФАМИЛИЯ ЗАЯВИТЕЛЯ", font)) { Colspan = 3, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(tbxFamily.Text, font)) { Colspan = 5, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });


                if (File.Exists(IMG_FILE_NAME))
                {
                    FileStream fs = new FileStream(IMG_FILE_NAME, FileMode.Open, FileAccess.Read);
                    iTextSharp.text.Image photo = iTextSharp.text.Image.GetInstance(System.Drawing.Image.FromStream(fs), System.Drawing.Imaging.ImageFormat.Jpeg);
                    photo.ScaleAbsolute(90f, 120f);
                    table.AddCell(new PdfPCell(photo, false) { Colspan = 2, Rowspan = 12, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE });
                    fs.Close();
                }
                else
                {
                    table.AddCell(new PdfPCell(new Phrase("фото", font)) { Colspan = 2, Rowspan = 12, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE });
                }

                //if (null != imgPhoto.Image)
                //{
                //    iTextSharp.text.Image photo = iTextSharp.text.Image.GetInstance(imgPhoto.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
                //    photo.ScaleAbsolute(90f, 120f);
                //    table.AddCell(new PdfPCell(photo, false) { Colspan = 2, Rowspan = 12, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE });
                //}
                //else
                //{
                //    table.AddCell(new PdfPCell(new Phrase("фото", font)) { Colspan = 2, Rowspan = 12, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE });
                //}

                table.AddCell(new PdfPCell(new Phrase("ИМЯ ЗАЯВИТЕЛЯ", font)) { Colspan = 3, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(tbxName.Text, font)) { Colspan = 5, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                table.AddCell(new PdfPCell(new Phrase("ОТЧЕСТВО ЗАЯВИТЕЛЯ", font)) { Colspan = 3, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(tbxFatherName.Text, font)) { Colspan = 5, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                table.AddCell(new PdfPCell(new Phrase("ГРАЖДАНСТВО РФ (ДА/НЕТ)", font)) { Colspan = 3, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(cbxNation.Text, font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase("ПОЛ (М/Ж)", font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(cbxGender.Text, font)) { HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                table.AddCell(new PdfPCell(new Phrase("ДАТА РОЖДЕНИЯ", font)) { Colspan = 3, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(dtpBirthDate.Text, font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase("МЕСТО РОЖДЕНИЯ", font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(tbxBirthPlace.Text, font)) { HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                table.AddCell(new PdfPCell(new Phrase("ДОКУМЕНТ, УДОСТОВЕРЯЮЩИЙ ЛИЧНОСТЬ", font)) { Colspan = 3, Rowspan = 5, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase("ВИД ДОКУМЕНТА", font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(tbxDocType.Text, font)) { Colspan = 3, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                table.AddCell(new PdfPCell(new Phrase("СЕРИЯ", font)) { HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(tbxDocSeries.Text, font)) { HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase("НОМЕР", font)) { HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(tbxDocNumber.Text, font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase("КОД ПОДРАЗДЕЛЕНИЯ", font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(tbxDocReleaserCode.Text, font)) { Colspan = 3, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                table.AddCell(new PdfPCell(new Phrase("КЕМ ВЫДАН", font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(tbxDocSupplier.Text, font)) { Colspan = 3, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase("КОГДА ВЫДАН", font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(dtpDocDate.Text, font)) { Colspan = 3, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase("СНИЛС", font)) { Colspan = 3, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                table.AddCell(new PdfPCell(new Phrase(tbxSNILS.Text, font)) { Colspan = 5, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                esekDoc.Add(table);

                text = "        Я, ___________";

                Chunk beginning = new Chunk(text, headerFont);
                Phrase p1 = new Phrase(beginning);
                iTextSharp.text.Font underlineFont = new iTextSharp.text.Font(baseFont, 11, iTextSharp.text.Font.UNDERLINE);
                text = tbxFamily.Text + " " + tbxName.Text + " " + tbxFatherName.Text;
                Chunk f_n_f = new Chunk(text, underlineFont);
                p1.Add(f_n_f);
                text = "______________, проинформирован(а) и согласен(на) с тем, что:";
                Chunk ending = new Chunk(text, headerFont);
                p1.Add(ending);
                esekDoc.Add(p1);

                List list = new List(List.UNORDERED, 10f);
                list.SetListSymbol("\u25AA");

                text = "вышеуказанные персональные данные будут использованы для" + __bodyCardReason +
                        " единой социальной карты жителя Республики Мордовия";

                if (__isNewStatement)
                {
                    text += " и подключения ее к Региональной системе обработки единой социальной электронной карты жителя;";
                }
                else
                {
                    text += ";";
                }

                list.Add(new ListItem(new Phrase(text, headerFont)));

                text = "персональные данные собираются в бумажной и электронной формах для обработки," +
                        " дальнейшей передачи, уточнения (обновления, изменения), накопления," +
                        " уничтожения в установленных случаях сотрудниками оператора, банка-соэмитента (нужное подчеркнуть);";
                list.Add(new ListItem(new Phrase(text, headerFont)));

                text = " персональные данные передаются  в ___";
                Chunk beginning2 = new Chunk(text, headerFont);
                Phrase p2 = new Phrase(beginning2);

                text = __appSettings.m_StationName;
                Chunk operatorNameChunk = new Chunk(text, underlineFont);
                p2.Add(operatorNameChunk);

                text = "___ (наименование оператора), адрес ___";
                Chunk medium2 = new Chunk(text, headerFont);
                p2.Add(medium2);

                text = __appSettings.m_OperatorAddress;
                Chunk operatorAddressChunk = new Chunk(text, underlineFont);
                p2.Add(operatorAddressChunk);

                text = "___ (адрес оператора), - организацию, уполномоченную на обработку, сбор, уточнение (обновление, изменение)," +
                        " обезличивание, блокирование, уничтожение, систематизацию, накопление, распространение " +
                        "(в том числе передачу), использования персональных данных с целью оформления и обслуживания социальной карты.";
                Chunk ending3 = new Chunk(text, headerFont);
                p2.Add(ending3);

                list.Add(new ListItem(p2));

                esekDoc.Add(list);

                text = "        Мое согласие является бессрочным до особого распоряженя, сделанного мною" +
                        " в письменной форме в адрес ___";
                Chunk beginning3 = new Chunk(text, headerFont);
                Phrase p3 = new Phrase(beginning3);

                p3.Add(operatorNameChunk);
                text = "___ (наименование оператора)" +
                        " и представленного в многофункциональный центр.\n\r";
                Chunk ending4 = new Chunk(text, headerFont);
                p3.Add(ending4);

                esekDoc.Add(p3);

                PdfPTable tableBottom = new PdfPTable(5);
                tableBottom.WidthPercentage = 100;

                tableBottom.AddCell(new PdfPCell(new Phrase("ПОДПИСЬ", font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                tableBottom.AddCell(new PdfPCell(new Phrase("ИНИЦИАЛЫ, ФАМИЛИЯ", font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                tableBottom.AddCell(new PdfPCell(new Phrase("ДАТА", font)) { HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                tableBottom.AddCell(new PdfPCell(new Phrase("", font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE, MinimumHeight = 40 });


                String initials = String.Empty;
                if ("" != tbxFamily.Text && "" != tbxName.Text && "" != tbxFatherName.Text)
                {
                    initials = tbxFamily.Text + " " + tbxName.Text.Substring(0, 1) + "." + tbxFatherName.Text.Substring(0, 1) + ".";
                }
                else
                {
                    initials = "";
                }
                tableBottom.AddCell(new PdfPCell(new Phrase(initials, font)) { Colspan = 2, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });
                tableBottom.AddCell(new PdfPCell(new Phrase(DateTime.Now.ToLongDateString(), font)) { HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                esekDoc.Add(tableBottom);

                Phrase separator = new Phrase(new Chunk(new DottedLineSeparator()));
                separator.Add(new Chunk("Линия отрыва", font));
                separator.Add(new Chunk(new DottedLineSeparator()));
                esekDoc.Add(separator);

                // заявление
                text = "Номер бланка: " + blankNum;
                esekDoc.Add(new Paragraph(new Phrase(text, statementFont)) { Alignment = 2 });

                // заявление
                text = "КОРЕШОК БЛАНКА АНКЕТЫ-ЗАЯВКИ \r (Заполняется оператором и передается заявителю)\n\r";
                esekDoc.Add(new Paragraph(new Phrase(text, headerFont)) { Alignment = 1 });

                // таблица
                PdfPTable table2 = new PdfPTable(10);
                table2.WidthPercentage = 100;

                table2.AddCell(new PdfPCell(new Phrase("ПУНКТ ПРИЕМА АНКЕТЫ-ЗАЯВКИ:", font)) { BorderWidth = 0f, Colspan = 3, HorizontalAlignment = 0, VerticalAlignment = Element.ALIGN_MIDDLE });
                table2.AddCell(new PdfPCell(new Phrase(__appSettings.m_OperatorStation  /*tbxOperatorDep.Text*/, font)) { BorderWidthLeft = 0f, BorderWidthTop = 0f, BorderWidthRight = 0f, Colspan = 7, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                table2.AddCell(new PdfPCell(new Phrase("ДАТА ПРИЕМА АНКЕТЫ-ЗАЯВКИ:", font)) { BorderWidth = 0f, Colspan = 3, HorizontalAlignment = 0, VerticalAlignment = Element.ALIGN_MIDDLE });
                table2.AddCell(new PdfPCell(new Phrase(DateTime.Now.ToLongDateString(), font)) { BorderWidthLeft = 0f, BorderWidthTop = 0f, BorderWidthRight = 0f, Colspan = 7, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                table2.AddCell(new PdfPCell(new Phrase("КОД ОПЕРАТОРА:", font)) { BorderWidth = 0f, Colspan = 3, HorizontalAlignment = 0, VerticalAlignment = Element.ALIGN_MIDDLE });
                table2.AddCell(new PdfPCell(new Phrase(/*__appSettings.m_OperatorCode*/ __mainWindow.GetLblOperatorLoginText(), font)) { BorderWidthLeft = 0f, BorderWidthTop = 0f, BorderWidthRight = 0f, Colspan = 7, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                table2.AddCell(new PdfPCell(new Phrase("ПОДПИСЬ ОПЕРАТОРА:", font)) { BorderWidth = 0f, Colspan = 3, HorizontalAlignment = 0, VerticalAlignment = Element.ALIGN_MIDDLE });
                table2.AddCell(new PdfPCell(new Phrase("", font)) { BorderWidthLeft = 0f, BorderWidthTop = 0f, BorderWidthRight = 0f, Colspan = 7, HorizontalAlignment = 1, VerticalAlignment = Element.ALIGN_MIDDLE });

                esekDoc.Add(table2);

                esekDoc.Add(new Paragraph(new Phrase("\n\rПроверить состояние обработки анкеты-заявки можно по телефону 8-800-12345678 или на сайте http://esec.e-mordovia.ru", font)) { Alignment = 0 });


                esekDoc.Close();
                System.Diagnostics.Process.Start(blankNum + PDF_FILE_NAME);
            }
            catch (Exception e)
            {
                String error = e.Message;
                MessageBox.Show("PDF файл открыт. Завершите просмотр файла.");
            }
        }

        private void imgPhoto_Click(object sender, EventArgs e)
        {
            __isdraw = true;
            __camView2 = new WndCam2(this);
            __camView2.ShowDialog();
            if (DialogResult.OK == __camView2.DialogResult)
            {
                if (File.Exists(IMG_FILE_NAME))
                {
                    try
                    {
                        FileStream fs = new FileStream(IMG_FILE_NAME, FileMode.Open, FileAccess.Read);
                        imgPhoto.Image = System.Drawing.Image.FromStream(fs);
                        fs.Close();
                    }
                    catch
                    {
                        MessageBox.Show("Ошибка отображения кадра. Повторите операцию.");
                    }
                }
                else
                {
                    MessageBox.Show("Файл фотографии отсутствует.");
                }
                //setImageBoxPhoto();
            }
            //else
            //{
            //    //MessageBox.Show("Повторите операцию.");
            //}

        }

        private void imgPhoto_Paint(object sender, PaintEventArgs e)
        {
            if (!__isdraw)
            {
                String text = "Нажмите для получения фото.";
                SizeF textSize = e.Graphics.MeasureString(text, Font);
                e.Graphics.DrawString(text, this.Font, Brushes.Black, (this.imgPhoto.Width / 2) - (textSize.Width / 2), (this.imgPhoto.Height / 2) - (textSize.Height / 2));
            }
        }

        private void setImageBoxPhoto()
        {
            ////Graphics g = imgPhoto.CreateGraphics();
            ////g.Clear(Color.White);

            //if (File.Exists(IMG_FILE_NAME))
            //{
            //    try
            //    {
            //        FileStream fs = new FileStream(IMG_FILE_NAME, FileMode.Open, FileAccess.Read);
            //        imgPhoto.Image = System.Drawing.Image.FromStream(fs);
            //        fs.Close();
            //    }
            //    catch
            //    {
            //        MessageBox.Show("Ошибка отображения кадра. Повторите операцию.");
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Файл фотографии отсутствует.");
            //}
        }

        private void tbxEnterSNILS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (SNILS_LENGTH == tbxEnterSNILS.Text.Length)
                {
                    tbxEnterSNILS.Enabled = false;
                    btnNextEnterSNILS.Enabled = false;
                    enterSNILS(tbxEnterSNILS.Text);
                }
            }
        }

        private void tbxEnterSNILS_Click(object sender, EventArgs e)
        {
            ReturnToLastChar(tbxEnterSNILS);
        }

        private void tbxEnterSNILS_KeyUp(object sender, KeyEventArgs e)
        {
            if (SNILS_LENGTH == tbxEnterSNILS.Text.Length)
            {
                if (checkSnils(tbxEnterSNILS.Text))
                {
                    btnNextEnterSNILS.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Введите корректный номер СНИЛС.");
                }
            }
            else
            {
                btnNextEnterSNILS.Enabled = false;
            }
        }

        private void tbxEditFamily_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)'-');
        }

        private void tbxEditName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void tbxEditFName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)'-');
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void tbxBirthPlace_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)'-' || e.KeyChar == (char)'.' || char.IsDigit(e.KeyChar));
        }

        private void tbxDocType_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }

        private void tbxDocSeries_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(e.KeyChar == (char)Keys.Back || char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Space);        
        }

        private void tbxDocNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(e.KeyChar == (char)Keys.Back || char.IsDigit(e.KeyChar));
        }

        private void tbxDocSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)'-' || e.KeyChar == (char)'.' || char.IsDigit(e.KeyChar) || e.KeyChar == (char)',' || e.KeyChar == (char)Keys.Space);        
        }

        private void tbxDocReleaserCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(e.KeyChar == (char)Keys.Back || e.KeyChar == (char)'-' || char.IsDigit(e.KeyChar));        
        }

        private void tbxSNILS_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(e.KeyChar == (char)Keys.Back || char.IsDigit(e.KeyChar) || e.KeyChar == (char)'-' || e.KeyChar == (char)Keys.Space);
        }

        private void ReturnToLastChar(MaskedTextBox _mtbx)
        {
            if (_mtbx.Text.Equals(String.Empty))
            {
                _mtbx.Select(0, 0);
            }
            int pos = _mtbx.SelectionStart;
            if (pos > _mtbx.Text.Length)
            {
                pos = _mtbx.Text.Length;
                _mtbx.Select(pos, 0);
            }
        }

        private bool checkSnils(String snils)
        {
            StringBuilder cSnils = new StringBuilder();
            foreach (char ch in snils)
            {
                if ('0' <= ch && '9' >= ch)
                {
                    cSnils.Append(ch);                
                }
            }

            if (cSnils.Length != 11)
            {
                return false;
            }

            int sum1 = 0;
            foreach (char ch in cSnils.ToString())
            {
                sum1 += Convert.ToInt16(ch - '0');
            }

            if (0 == sum1)
            {
                return false;
            }

            int mul = 9, idx = 0, sum2 = 0;
            while (0 != mul)
            {
                sum2 += Convert.ToInt16(cSnils[idx]-'0') * mul;
                idx++;
                mul--;
            }

            int check = Convert.ToInt16(cSnils[9] - '0')*10 + Convert.ToInt16(cSnils[10] - '0');
            int sum3 = (sum2 % 101)%100;
            if (check == sum3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private void tabESEK_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabESEK.SelectedIndex)
            {
                case 0:
                    this.ActiveControl = tbxEnterSNILS;
                    tbxEnterSNILS.Enabled = true;

                    if (SNILS_LENGTH == tbxEnterSNILS.Text.Length)
                    {
                        btnNextEnterSNILS.Enabled = true;
                    }
                    else
                    {
                        btnNextEnterSNILS.Enabled = false;
                    }
                    break;
                case 1:
                    btnNextListESEK.Enabled = false;
                    break;
                case 2:
                    this.ActiveControl = btnNextListESEK;
                    break;
                case 3:
                    this.ActiveControl = tbxFamily;
                    break;
                default:
                    break;
            }
        }

        private void WndStatement_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose(true);
        }
    }
}
