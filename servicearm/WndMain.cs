﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using rfidapp;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using RestSharp.Serializers;
using System.Net;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace armoperator
{
    public partial class WndMain : Form
    {
        public const String READER_NAME = "ACS ACR1252 Dual Reader PICC 0";
        public const String SETTINGS_FILE_NAME = "settings.xml";
        public const String ROAMING_FOLDER_NAME = "armoperator";

        public static String BASE_URL = "https://cpd.sarov-itc.ru";

        static AppSettings __appSettings = new AppSettings();

        static WkrGetAppData wkrGetAppData = new WkrGetAppData();
        static ResultAppData resultAppData = new ResultAppData();

        static WkrGetAppProviderData wkrGetAppProviderData = new WkrGetAppProviderData();
        static ResultAppProviderData resultAppProviderData = new ResultAppProviderData();

        private String __authToken = "";
        static int __providerId = 0;
        static int __applicationId = 0;

        public bool isAuthWindowActive = false;

        WndStatement snilsWnd;
        WndActivateESEK wndActivateESEK;
        WndUnblockESEK wndBlockActivateESEK;
        WndInfoESEK wndInfoESEK;
        WndAddBallESEK wndAddBallESEK;
        WndTicketESEK wndTicketESEK;
        WndBlockESEK wndBlockESEK;

        public WndMain()
        {
            InitializeComponent();
            String path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            path = Path.Combine(path, ROAMING_FOLDER_NAME);
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch
                {
                    MessageBox.Show("Недостаточно прав у пользователя для работы с программой. Выход из приложения.");
                    this.Close();
                }
            }
            System.IO.Directory.SetCurrentDirectory(path);
        }

        private void Log(String msg, RichTextBox tbxLog)
        {
            tbxLog.AppendText(msg + "\r\n");
            tbxLog.ScrollToCaret();
        }
 
        private void btnRequestESEK_Click(object sender, EventArgs e)
        {
            snilsWnd = new WndStatement(this);
            snilsWnd.ShowDialog();
        }

        private void btnTravelESEK_Click(object sender, EventArgs e)
        {
            wndTicketESEK = new WndTicketESEK(this);
            wndTicketESEK.ShowDialog();
        }

        private void btnOnceTravelESEK_Click(object sender, EventArgs e)
        {
            wndInfoESEK = new WndInfoESEK(this);
            wndInfoESEK.ShowDialog();
        }

        private void btnActivateESEK_Click(object sender, EventArgs e)
        {
            wndActivateESEK = new WndActivateESEK(this);
            wndActivateESEK.ShowDialog();
        }

        private void btnPointsESEK_Click(object sender, EventArgs e)
        {
            wndAddBallESEK = new WndAddBallESEK(this);
            wndAddBallESEK.ShowDialog();
        }

        private void btnBlocking_Click(object sender, EventArgs e)
        {
            wndBlockActivateESEK = new WndUnblockESEK(this);
            wndBlockActivateESEK.ShowDialog();
        }

        private void btnUnblocking_Click(object sender, EventArgs e)
        {
            wndBlockESEK = new WndBlockESEK(this);
            wndBlockESEK.ShowDialog();
        }

        public void Authorize()
        {
            isAuthWindowActive = true;
            using (var authWnd = new WndAuth(this))
            {
                authWnd.authToken = "";
                authWnd.operatorCode = "";
                authWnd.ShowDialog();
                if (DialogResult.OK != authWnd.DialogResult)
                {
                    this.Close();
                }
                else
                {
                    this.__authToken = authWnd.authToken;
                    this.lblOperatorLogin.Text = authWnd.operatorCode;
                }
                isAuthWindowActive = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!File.Exists(SETTINGS_FILE_NAME))
            {
                try
                {
                    using (Stream writer = new FileStream(SETTINGS_FILE_NAME, FileMode.Create))
                    {
                        System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(AppSettings));
                        serializer.Serialize(writer, __appSettings);
                    }
                }
                catch
                {
                    MessageBox.Show("Запись файла конфигурации на диск невозможна");
                }
            }
            else
            {
                try
                {
                    using (Stream stream = new FileStream(SETTINGS_FILE_NAME, FileMode.Open))
                    {
                        System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(AppSettings));
                        __appSettings = (AppSettings)serializer.Deserialize(stream);
                    }
                }
                catch
                {
                    MessageBox.Show("Отсутствует файл settings.xml");
                }
            }

            BASE_URL = __appSettings.m_BaseUrl;           

            Authorize();
            InitializeTmrGetAppData();
            InitializeTmrGetAppProviderData();

            GetAppDataData getAppDataData = new GetAppDataData();
            getAppDataData.URL = BASE_URL;
            getAppDataData.api = @"/api/cp/v1/applications?filter={""title"":{""$eq"":""";
            getAppDataData.app = @"Транспортное""}}";
            getAppDataData.token = __authToken;
            wkrGetAppData.SetWorkerData(getAppDataData);

            if (!wkrGetAppData.GetWkrStatus())
            {
                wkrGetAppData.PerformRestOperation();
                tmrGetAppData.Start();
            }


        }

        public void SetAppSettings(String operatorStation, String operatorAddress, String stationName)
        {
            __appSettings.m_OperatorStation = operatorStation;
            __appSettings.m_OperatorAddress = operatorAddress;
            __appSettings.m_StationName = stationName;
        }

        public void GetAppSettings( AppSettings appSettings,
                                    ref MaskedTextBox tbxOperatorStation,
                                    ref TextBox tbxOperatorAddress,
                                    ref TextBox tbxStationName  )
        {
            tbxOperatorStation.Text = appSettings.m_OperatorStation;
            tbxOperatorAddress.Text = appSettings.m_OperatorAddress;
            tbxStationName.Text = appSettings.m_StationName;
        }

        public AppSettings GetAppSettingsData()
        {
            return __appSettings;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Authorize();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            try
            {
                using (Stream stream = new FileStream(SETTINGS_FILE_NAME, FileMode.Open))
                {
                    System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(AppSettings));
                    __appSettings = (AppSettings)serializer.Deserialize(stream);
                }
            }
            catch
            {
                MessageBox.Show("Отсутствует файл settings.xml");
            }

            WndSettings settingsWnd = new WndSettings(this, __appSettings);
            settingsWnd.ShowDialog();
        }

        public void SetAuthToken(String token)
        {
            __authToken = token;
        }

        public String GetAuthToken()
        {
            return __authToken;
        }
        
        public AppSettings GetAppSettings()
        {
            return __appSettings; 
        }

        private void InitializeTmrGetAppData()
        {
            tmrGetAppData.Interval = 100;
            tmrGetAppData.Tick += new EventHandler(tmrGetAppData_Tick);
        }

        private void tmrGetAppData_Tick(object Sender, EventArgs e)
        {
            if (wkrGetAppData.GetWkrStatus().Equals(false))
            {
                tmrGetAppData.Stop();
                resultAppData = wkrGetAppData.GetResultREST();
                __applicationId = resultAppData.appData[0].id;
                GetAppProviderDataData getAppProviderDataData = new GetAppProviderDataData();
                getAppProviderDataData.URL = BASE_URL;
                getAppProviderDataData.api = "/api/cp/v1/applications/";
                getAppProviderDataData.id = __applicationId + "/providers";
                getAppProviderDataData.token = __authToken;
                wkrGetAppProviderData.SetWorkerData(getAppProviderDataData);
                if (!wkrGetAppProviderData.GetWkrStatus())
                {
                    wkrGetAppProviderData.PerformRestOperation();
                    tmrGetAppProviderData.Start();
                }
            }
        }

        public int GetApplicationId()
        {
            return __applicationId;
        }

        private void InitializeTmrGetAppProviderData()
        {
            tmrGetAppProviderData.Interval = 100;
            tmrGetAppProviderData.Tick += new EventHandler(tmrGetAppProviderData_Tick);
        }

        private void tmrGetAppProviderData_Tick(object Sender, EventArgs e)
        {
            if (wkrGetAppProviderData.GetWkrStatus().Equals(false))
            {
                tmrGetAppProviderData.Stop();
                resultAppProviderData = wkrGetAppProviderData.GetResultREST();

                foreach (var element in resultAppProviderData.appProviderData)
                {
                    Meta meta = new Meta();
                    try
                    {
                        meta = JsonConvert.DeserializeObject<Meta>(element.meta);
                    }
                    catch { }
                    string m = meta.type;
                    if (m == "operator")
                    {
                        __providerId = element.id;
                        break;
                    }
                }
            }
        }

        public int GetProviderId()
        {
            return __providerId;
        }

        public void SetBtnLoginText(String login)
        {
            if ("" != login)
            {
                btnLogin.Text = login;
            }
            else
            {
                btnLogin.Text = "Вход";
            }
        }

        public void SetLblOperatorLoginText(String login)
        {
            lblOperatorLogin.Text = login;
        }

        public String GetLblOperatorLoginText()
        {
            return lblOperatorLogin.Text;
        }

    };

    public class Timeframe
    {
        public object startTimestamp { get; set; }
        public object finishTimestamp { get; set; }
    }
    
    public class ReplenishmentData
    {
        public int value { get; set; }
        public string symbol { get; set; }
    }

    public class AppSettings
    {
        public String m_OperatorStation { get; set; }
        public String m_OperatorAddress { get; set; }
        public String m_StationName { get; set; }
        public String m_BaseUrl { get; set; }

        public AppSettings()
        {
            m_OperatorStation = "000000";
            m_StationName = "Наименование пункта";
            m_OperatorAddress = "Адрес пункта";
            m_BaseUrl = "https://cpd.sarov-itc.ru";
        }
    }
}

