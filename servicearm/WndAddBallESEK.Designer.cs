﻿namespace armoperator
{
    partial class WndAddBallESEK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WndAddBallESEK));
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblBallESEK = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxStatus = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxCardState = new System.Windows.Forms.TextBox();
            this.tbxSNILS = new System.Windows.Forms.TextBox();
            this.tbxPerson = new System.Windows.Forms.TextBox();
            this.tbxCardNumber = new System.Windows.Forms.TextBox();
            this.tbxCardRFID = new System.Windows.Forms.TextBox();
            this.tbxStatusREST = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbxBallValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnReplenishment = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.tbxReplenishmentResult = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tbxReplenishmentValue = new System.Windows.Forms.TextBox();
            this.tmrReadCard = new System.Windows.Forms.Timer(this.components);
            this.tmrGetMediaByRFID = new System.Windows.Forms.Timer(this.components);
            this.tmrGetAccByAccountId = new System.Windows.Forms.Timer(this.components);
            this.tmrGetPersonByUUID = new System.Windows.Forms.Timer(this.components);
            this.tmrGetCurrencies = new System.Windows.Forms.Timer(this.components);
            this.tmrGetBagsByAccId = new System.Windows.Forms.Timer(this.components);
            this.tmrGetBagTransactions = new System.Windows.Forms.Timer(this.components);
            this.tmrReplenishment = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.restResponceStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel6.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 5;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel6.Controls.Add(this.lblBallESEK, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label5, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.tbxStatus, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.label8, 1, 7);
            this.tableLayoutPanel6.Controls.Add(this.label7, 1, 6);
            this.tableLayoutPanel6.Controls.Add(this.label6, 1, 5);
            this.tableLayoutPanel6.Controls.Add(this.label2, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.label1, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardState, 2, 7);
            this.tableLayoutPanel6.Controls.Add(this.tbxSNILS, 2, 6);
            this.tableLayoutPanel6.Controls.Add(this.tbxPerson, 2, 5);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardNumber, 2, 4);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardRFID, 2, 3);
            this.tableLayoutPanel6.Controls.Add(this.tbxStatusREST, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.label9, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.label10, 1, 8);
            this.tableLayoutPanel6.Controls.Add(this.tbxBallValue, 2, 8);
            this.tableLayoutPanel6.Controls.Add(this.label3, 1, 9);
            this.tableLayoutPanel6.Controls.Add(this.btnReplenishment, 3, 9);
            this.tableLayoutPanel6.Controls.Add(this.label11, 1, 10);
            this.tableLayoutPanel6.Controls.Add(this.tbxReplenishmentResult, 2, 10);
            this.tableLayoutPanel6.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button2, 4, 12);
            this.tableLayoutPanel6.Controls.Add(this.tbxReplenishmentValue, 2, 9);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 14;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(895, 571);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // lblBallESEK
            // 
            this.lblBallESEK.AutoSize = true;
            this.lblBallESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBallESEK.Location = new System.Drawing.Point(3, 110);
            this.lblBallESEK.Name = "lblBallESEK";
            this.tableLayoutPanel6.SetRowSpan(this.lblBallESEK, 10);
            this.lblBallESEK.Size = new System.Drawing.Size(352, 401);
            this.lblBallESEK.TabIndex = 7;
            this.lblBallESEK.Text = "lblDataESEK";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(352, 30);
            this.label4.TabIndex = 6;
            this.label4.Text = "Последовательность действий:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(361, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(217, 30);
            this.label5.TabIndex = 17;
            this.label5.Text = "Состояние чтения карты:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxStatus
            // 
            this.tbxStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.SetColumnSpan(this.tbxStatus, 3);
            this.tbxStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxStatus.Location = new System.Drawing.Point(584, 85);
            this.tbxStatus.Name = "tbxStatus";
            this.tbxStatus.Size = new System.Drawing.Size(308, 20);
            this.tbxStatus.TabIndex = 12;
            this.tbxStatus.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(361, 260);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(217, 30);
            this.label8.TabIndex = 20;
            this.label8.Text = "Статус карты:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(361, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(217, 30);
            this.label7.TabIndex = 19;
            this.label7.Text = "Номер СНИЛС:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(361, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(217, 30);
            this.label6.TabIndex = 18;
            this.label6.Text = "Держатель карты:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(361, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 30);
            this.label2.TabIndex = 9;
            this.label2.Text = "Номер карты:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(361, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 30);
            this.label1.TabIndex = 8;
            this.label1.Text = "Серийный номер карты:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardState
            // 
            this.tbxCardState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardState, 3);
            this.tbxCardState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxCardState.Location = new System.Drawing.Point(584, 265);
            this.tbxCardState.Name = "tbxCardState";
            this.tbxCardState.Size = new System.Drawing.Size(308, 20);
            this.tbxCardState.TabIndex = 16;
            this.tbxCardState.TabStop = false;
            // 
            // tbxSNILS
            // 
            this.tbxSNILS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.SetColumnSpan(this.tbxSNILS, 3);
            this.tbxSNILS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxSNILS.Location = new System.Drawing.Point(584, 235);
            this.tbxSNILS.Name = "tbxSNILS";
            this.tbxSNILS.Size = new System.Drawing.Size(308, 20);
            this.tbxSNILS.TabIndex = 14;
            this.tbxSNILS.TabStop = false;
            // 
            // tbxPerson
            // 
            this.tbxPerson.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.SetColumnSpan(this.tbxPerson, 3);
            this.tbxPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxPerson.Location = new System.Drawing.Point(584, 205);
            this.tbxPerson.Name = "tbxPerson";
            this.tbxPerson.Size = new System.Drawing.Size(308, 20);
            this.tbxPerson.TabIndex = 13;
            this.tbxPerson.TabStop = false;
            // 
            // tbxCardNumber
            // 
            this.tbxCardNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardNumber, 3);
            this.tbxCardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxCardNumber.Location = new System.Drawing.Point(584, 175);
            this.tbxCardNumber.Name = "tbxCardNumber";
            this.tbxCardNumber.Size = new System.Drawing.Size(308, 20);
            this.tbxCardNumber.TabIndex = 11;
            this.tbxCardNumber.TabStop = false;
            // 
            // tbxCardRFID
            // 
            this.tbxCardRFID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardRFID, 3);
            this.tbxCardRFID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxCardRFID.Location = new System.Drawing.Point(584, 145);
            this.tbxCardRFID.Name = "tbxCardRFID";
            this.tbxCardRFID.Size = new System.Drawing.Size(308, 20);
            this.tbxCardRFID.TabIndex = 10;
            this.tbxCardRFID.TabStop = false;
            // 
            // tbxStatusREST
            // 
            this.tbxStatusREST.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.SetColumnSpan(this.tbxStatusREST, 3);
            this.tbxStatusREST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxStatusREST.Location = new System.Drawing.Point(584, 115);
            this.tbxStatusREST.Name = "tbxStatusREST";
            this.tbxStatusREST.Size = new System.Drawing.Size(308, 20);
            this.tbxStatusREST.TabIndex = 15;
            this.tbxStatusREST.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(361, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(217, 30);
            this.label9.TabIndex = 21;
            this.label9.Text = "Состояние запроса в систему:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(361, 290);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(217, 30);
            this.label10.TabIndex = 22;
            this.label10.Text = "Количество транспортных баллов:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxBallValue
            // 
            this.tbxBallValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.SetColumnSpan(this.tbxBallValue, 3);
            this.tbxBallValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxBallValue.Location = new System.Drawing.Point(584, 295);
            this.tbxBallValue.Name = "tbxBallValue";
            this.tbxBallValue.Size = new System.Drawing.Size(308, 20);
            this.tbxBallValue.TabIndex = 23;
            this.tbxBallValue.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(361, 320);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(217, 40);
            this.label3.TabIndex = 25;
            this.label3.Text = "Баллы для зачисления :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnReplenishment
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.btnReplenishment, 2);
            this.btnReplenishment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReplenishment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReplenishment.Location = new System.Drawing.Point(718, 323);
            this.btnReplenishment.Name = "btnReplenishment";
            this.btnReplenishment.Size = new System.Drawing.Size(174, 34);
            this.btnReplenishment.TabIndex = 1;
            this.btnReplenishment.Text = "Зачислить баллы";
            this.btnReplenishment.UseVisualStyleBackColor = true;
            this.btnReplenishment.Click += new System.EventHandler(this.btnReplenishment_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(361, 360);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(217, 30);
            this.label11.TabIndex = 27;
            this.label11.Text = "Состояние зачисления:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxReplenishmentResult
            // 
            this.tbxReplenishmentResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.SetColumnSpan(this.tbxReplenishmentResult, 3);
            this.tbxReplenishmentResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxReplenishmentResult.Location = new System.Drawing.Point(584, 365);
            this.tbxReplenishmentResult.Name = "tbxReplenishmentResult";
            this.tbxReplenishmentResult.Size = new System.Drawing.Size(308, 20);
            this.tbxReplenishmentResult.TabIndex = 28;
            this.tbxReplenishmentResult.TabStop = false;
            // 
            // button1
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.button1, 5);
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Image = global::armoperator.Properties.Resources.accessory163;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(889, 74);
            this.button1.TabIndex = 29;
            this.button1.TabStop = false;
            this.button1.Text = "Зачисление баллов на ЕСЕК";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(762, 514);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(130, 34);
            this.button2.TabIndex = 30;
            this.button2.Text = "Закрыть";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // tbxReplenishmentValue
            // 
            this.tbxReplenishmentValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxReplenishmentValue.Location = new System.Drawing.Point(584, 330);
            this.tbxReplenishmentValue.Name = "tbxReplenishmentValue";
            this.tbxReplenishmentValue.Size = new System.Drawing.Size(128, 20);
            this.tbxReplenishmentValue.TabIndex = 31;
            this.tbxReplenishmentValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxReplenishmentValue_KeyPress);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.restResponceStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 549);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(895, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(123, 17);
            this.toolStripStatusLabel1.Text = "Состояние запросов:";
            // 
            // restResponceStatus
            // 
            this.restResponceStatus.Name = "restResponceStatus";
            this.restResponceStatus.Size = new System.Drawing.Size(184, 17);
            this.restResponceStatus.Text = "Активные запросы отсутствуют.";
            // 
            // WndAddBallESEK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(895, 571);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tableLayoutPanel6);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WndAddBallESEK";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Зачисление баллов на ЕСЕК";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WndInfoESEK_FormClosing);
            this.Load += new System.EventHandler(this.WndInfoESEK_Load);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lblBallESEK;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer tmrReadCard;
        private System.Windows.Forms.Timer tmrGetMediaByRFID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxCardRFID;
        private System.Windows.Forms.TextBox tbxCardNumber;
        private System.Windows.Forms.TextBox tbxStatus;
        private System.Windows.Forms.TextBox tbxPerson;
        private System.Windows.Forms.TextBox tbxSNILS;
        private System.Windows.Forms.TextBox tbxStatusREST;
        private System.Windows.Forms.TextBox tbxCardState;
        private System.Windows.Forms.Timer tmrGetAccByAccountId;
        private System.Windows.Forms.Timer tmrGetPersonByUUID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Timer tmrGetCurrencies;
        private System.Windows.Forms.Timer tmrGetBagsByAccId;
        private System.Windows.Forms.Timer tmrGetBagTransactions;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbxBallValue;
        private System.Windows.Forms.Timer tmrReplenishment;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnReplenishment;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbxReplenishmentResult;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripStatusLabel restResponceStatus;
        private System.Windows.Forms.TextBox tbxReplenishmentValue;
    }
}