﻿namespace armoperator
{
    partial class WndInfoESEK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WndInfoESEK));
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDataESEK = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewExemptions = new System.Windows.Forms.DataGridView();
            this.gridExsemptionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridExemptionTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridViewTickets = new System.Windows.Forms.DataGridView();
            this.gridTicketName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridTicketValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridTicketTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnPrintEsekInfo = new System.Windows.Forms.Button();
            this.tbxStatus = new System.Windows.Forms.Label();
            this.tbxStatusREST = new System.Windows.Forms.Label();
            this.tbxCardRFID = new System.Windows.Forms.Label();
            this.tbxCardNumber = new System.Windows.Forms.Label();
            this.tbxPerson = new System.Windows.Forms.Label();
            this.tbxSNILS = new System.Windows.Forms.Label();
            this.tbxCardState = new System.Windows.Forms.Label();
            this.tbxBallValue = new System.Windows.Forms.Label();
            this.tmrReadCard = new System.Windows.Forms.Timer(this.components);
            this.tmrGetMediaByRFID = new System.Windows.Forms.Timer(this.components);
            this.tmrGetAccByAccountId = new System.Windows.Forms.Timer(this.components);
            this.tmrGetPersonByUUID = new System.Windows.Forms.Timer(this.components);
            this.tmrGetExemptions = new System.Windows.Forms.Timer(this.components);
            this.tmrGetCurrencies = new System.Windows.Forms.Timer(this.components);
            this.tmrGetBagsByAccId = new System.Windows.Forms.Timer(this.components);
            this.tmrGetBagTransactions = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.restResponceStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrCheckBags = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewExemptions)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTickets)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel6.Controls.Add(this.lblDataESEK, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label5, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.label8, 1, 7);
            this.tableLayoutPanel6.Controls.Add(this.label7, 1, 6);
            this.tableLayoutPanel6.Controls.Add(this.label6, 1, 5);
            this.tableLayoutPanel6.Controls.Add(this.label2, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.label1, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.label9, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.label10, 1, 8);
            this.tableLayoutPanel6.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button2, 3, 11);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel1, 1, 9);
            this.tableLayoutPanel6.Controls.Add(this.btnPrintEsekInfo, 3, 10);
            this.tableLayoutPanel6.Controls.Add(this.tbxStatus, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.tbxStatusREST, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardRFID, 2, 3);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardNumber, 2, 4);
            this.tableLayoutPanel6.Controls.Add(this.tbxPerson, 2, 5);
            this.tableLayoutPanel6.Controls.Add(this.tbxSNILS, 2, 6);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardState, 2, 7);
            this.tableLayoutPanel6.Controls.Add(this.tbxBallValue, 2, 8);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 13;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1008, 730);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // lblDataESEK
            // 
            this.lblDataESEK.AutoSize = true;
            this.lblDataESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataESEK.Location = new System.Drawing.Point(3, 110);
            this.lblDataESEK.Name = "lblDataESEK";
            this.tableLayoutPanel6.SetRowSpan(this.lblDataESEK, 8);
            this.lblDataESEK.Size = new System.Drawing.Size(397, 520);
            this.lblDataESEK.TabIndex = 7;
            this.lblDataESEK.Text = "lblDataESEK";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(397, 30);
            this.label4.TabIndex = 6;
            this.label4.Text = "Последовательность действий:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(406, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(246, 30);
            this.label5.TabIndex = 17;
            this.label5.Text = "Состояние чтения карты:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(406, 260);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(246, 30);
            this.label8.TabIndex = 20;
            this.label8.Text = "Статус карты:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(406, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(246, 30);
            this.label7.TabIndex = 19;
            this.label7.Text = "Номер СНИЛС:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(406, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(246, 30);
            this.label6.TabIndex = 18;
            this.label6.Text = "Держатель карты:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(406, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(246, 30);
            this.label2.TabIndex = 9;
            this.label2.Text = "Номер карты:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(406, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 30);
            this.label1.TabIndex = 8;
            this.label1.Text = "Серийный номер карты:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(406, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(246, 30);
            this.label9.TabIndex = 21;
            this.label9.Text = "Состояние запроса в систему:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(406, 290);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(246, 30);
            this.label10.TabIndex = 22;
            this.label10.Text = "Количество транспортных баллов:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.button1, 4);
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Image = global::armoperator.Properties.Resources.search99;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(1002, 74);
            this.button1.TabIndex = 25;
            this.button1.TabStop = false;
            this.button1.Text = "Просмотр данных ЕСЕК";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(859, 673);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(146, 34);
            this.button2.TabIndex = 26;
            this.button2.Text = "Закрыть";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel6.SetColumnSpan(this.tableLayoutPanel1, 3);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(406, 323);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(599, 304);
            this.tableLayoutPanel1.TabIndex = 27;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewExemptions);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(293, 298);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Льготы";
            // 
            // dataGridViewExemptions
            // 
            this.dataGridViewExemptions.AllowUserToAddRows = false;
            this.dataGridViewExemptions.AllowUserToDeleteRows = false;
            this.dataGridViewExemptions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewExemptions.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewExemptions.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewExemptions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewExemptions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gridExsemptionName,
            this.gridExemptionTime});
            this.dataGridViewExemptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewExemptions.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewExemptions.Name = "dataGridViewExemptions";
            this.dataGridViewExemptions.ReadOnly = true;
            this.dataGridViewExemptions.RowHeadersVisible = false;
            this.dataGridViewExemptions.Size = new System.Drawing.Size(287, 279);
            this.dataGridViewExemptions.TabIndex = 0;
            this.dataGridViewExemptions.TabStop = false;
            // 
            // gridExsemptionName
            // 
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridExsemptionName.DefaultCellStyle = dataGridViewCellStyle1;
            this.gridExsemptionName.HeaderText = "Наименование";
            this.gridExsemptionName.Name = "gridExsemptionName";
            this.gridExsemptionName.ReadOnly = true;
            // 
            // gridExemptionTime
            // 
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridExemptionTime.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridExemptionTime.HeaderText = "Срок действия";
            this.gridExemptionTime.Name = "gridExemptionTime";
            this.gridExemptionTime.ReadOnly = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridViewTickets);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(302, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(294, 298);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Билеты";
            // 
            // dataGridViewTickets
            // 
            this.dataGridViewTickets.AllowUserToAddRows = false;
            this.dataGridViewTickets.AllowUserToDeleteRows = false;
            this.dataGridViewTickets.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewTickets.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewTickets.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewTickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTickets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gridTicketName,
            this.gridTicketValue,
            this.gridTicketTime});
            this.dataGridViewTickets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTickets.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewTickets.Name = "dataGridViewTickets";
            this.dataGridViewTickets.ReadOnly = true;
            this.dataGridViewTickets.RowHeadersVisible = false;
            this.dataGridViewTickets.Size = new System.Drawing.Size(288, 279);
            this.dataGridViewTickets.TabIndex = 1;
            this.dataGridViewTickets.TabStop = false;
            // 
            // gridTicketName
            // 
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridTicketName.DefaultCellStyle = dataGridViewCellStyle3;
            this.gridTicketName.HeaderText = "Наименование";
            this.gridTicketName.Name = "gridTicketName";
            this.gridTicketName.ReadOnly = true;
            // 
            // gridTicketValue
            // 
            this.gridTicketValue.HeaderText = "Кол-во";
            this.gridTicketValue.Name = "gridTicketValue";
            this.gridTicketValue.ReadOnly = true;
            // 
            // gridTicketTime
            // 
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridTicketTime.DefaultCellStyle = dataGridViewCellStyle4;
            this.gridTicketTime.HeaderText = "Срок действия";
            this.gridTicketTime.Name = "gridTicketTime";
            this.gridTicketTime.ReadOnly = true;
            // 
            // btnPrintEsekInfo
            // 
            this.btnPrintEsekInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrintEsekInfo.Enabled = false;
            this.btnPrintEsekInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintEsekInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintEsekInfo.Image")));
            this.btnPrintEsekInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrintEsekInfo.Location = new System.Drawing.Point(859, 633);
            this.btnPrintEsekInfo.Name = "btnPrintEsekInfo";
            this.btnPrintEsekInfo.Size = new System.Drawing.Size(146, 34);
            this.btnPrintEsekInfo.TabIndex = 28;
            this.btnPrintEsekInfo.Text = "Печать";
            this.btnPrintEsekInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrintEsekInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrintEsekInfo.UseVisualStyleBackColor = true;
            this.btnPrintEsekInfo.Click += new System.EventHandler(this.btnPrintEsekInfo_Click);
            // 
            // tbxStatus
            // 
            this.tbxStatus.AutoSize = true;
            this.tbxStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxStatus, 2);
            this.tbxStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxStatus.Location = new System.Drawing.Point(658, 80);
            this.tbxStatus.Name = "tbxStatus";
            this.tbxStatus.Size = new System.Drawing.Size(347, 30);
            this.tbxStatus.TabIndex = 29;
            this.tbxStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxStatusREST
            // 
            this.tbxStatusREST.AutoSize = true;
            this.tbxStatusREST.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxStatusREST, 2);
            this.tbxStatusREST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxStatusREST.Location = new System.Drawing.Point(658, 110);
            this.tbxStatusREST.Name = "tbxStatusREST";
            this.tbxStatusREST.Size = new System.Drawing.Size(347, 30);
            this.tbxStatusREST.TabIndex = 30;
            this.tbxStatusREST.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardRFID
            // 
            this.tbxCardRFID.AutoSize = true;
            this.tbxCardRFID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardRFID, 2);
            this.tbxCardRFID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxCardRFID.Location = new System.Drawing.Point(658, 140);
            this.tbxCardRFID.Name = "tbxCardRFID";
            this.tbxCardRFID.Size = new System.Drawing.Size(347, 30);
            this.tbxCardRFID.TabIndex = 31;
            this.tbxCardRFID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardNumber
            // 
            this.tbxCardNumber.AutoSize = true;
            this.tbxCardNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardNumber, 2);
            this.tbxCardNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxCardNumber.Location = new System.Drawing.Point(658, 170);
            this.tbxCardNumber.Name = "tbxCardNumber";
            this.tbxCardNumber.Size = new System.Drawing.Size(347, 30);
            this.tbxCardNumber.TabIndex = 32;
            this.tbxCardNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxPerson
            // 
            this.tbxPerson.AutoSize = true;
            this.tbxPerson.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxPerson, 2);
            this.tbxPerson.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxPerson.Location = new System.Drawing.Point(658, 200);
            this.tbxPerson.Name = "tbxPerson";
            this.tbxPerson.Size = new System.Drawing.Size(347, 30);
            this.tbxPerson.TabIndex = 33;
            this.tbxPerson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxSNILS
            // 
            this.tbxSNILS.AutoSize = true;
            this.tbxSNILS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxSNILS, 2);
            this.tbxSNILS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxSNILS.Location = new System.Drawing.Point(658, 230);
            this.tbxSNILS.Name = "tbxSNILS";
            this.tbxSNILS.Size = new System.Drawing.Size(347, 30);
            this.tbxSNILS.TabIndex = 34;
            this.tbxSNILS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardState
            // 
            this.tbxCardState.AutoSize = true;
            this.tbxCardState.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardState, 2);
            this.tbxCardState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxCardState.Location = new System.Drawing.Point(658, 260);
            this.tbxCardState.Name = "tbxCardState";
            this.tbxCardState.Size = new System.Drawing.Size(347, 30);
            this.tbxCardState.TabIndex = 35;
            this.tbxCardState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxBallValue
            // 
            this.tbxBallValue.AutoSize = true;
            this.tbxBallValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxBallValue, 2);
            this.tbxBallValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxBallValue.Location = new System.Drawing.Point(658, 290);
            this.tbxBallValue.Name = "tbxBallValue";
            this.tbxBallValue.Size = new System.Drawing.Size(347, 30);
            this.tbxBallValue.TabIndex = 36;
            this.tbxBallValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.restResponceStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 708);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(123, 17);
            this.toolStripStatusLabel1.Text = "Состояние запросов:";
            // 
            // restResponceStatus
            // 
            this.restResponceStatus.Name = "restResponceStatus";
            this.restResponceStatus.Size = new System.Drawing.Size(184, 17);
            this.restResponceStatus.Text = "Активные запросы отсутствуют.";
            // 
            // tmrCheckBags
            // 
            this.tmrCheckBags.Tick += new System.EventHandler(this.tmrCheckBags_Tick);
            // 
            // WndInfoESEK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.ControlBox = false;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tableLayoutPanel6);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WndInfoESEK";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Просмотр данных ЕСЭК";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WndInfoESEK_FormClosing);
            this.Load += new System.EventHandler(this.WndInfoESEK_Load);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewExemptions)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTickets)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lblDataESEK;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer tmrReadCard;
        private System.Windows.Forms.Timer tmrGetMediaByRFID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer tmrGetAccByAccountId;
        private System.Windows.Forms.Timer tmrGetPersonByUUID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Timer tmrGetExemptions;
        private System.Windows.Forms.Timer tmrGetCurrencies;
        private System.Windows.Forms.Timer tmrGetBagsByAccId;
        private System.Windows.Forms.Timer tmrGetBagTransactions;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripStatusLabel restResponceStatus;
        private System.Windows.Forms.Timer tmrCheckBags;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewExemptions;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridExsemptionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridExemptionTime;
        private System.Windows.Forms.DataGridView dataGridViewTickets;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridTicketName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridTicketValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridTicketTime;
        private System.Windows.Forms.Button btnPrintEsekInfo;
        private System.Windows.Forms.Label tbxStatus;
        private System.Windows.Forms.Label tbxStatusREST;
        private System.Windows.Forms.Label tbxCardRFID;
        private System.Windows.Forms.Label tbxCardNumber;
        private System.Windows.Forms.Label tbxPerson;
        private System.Windows.Forms.Label tbxSNILS;
        private System.Windows.Forms.Label tbxCardState;
        private System.Windows.Forms.Label tbxBallValue;
    }
}