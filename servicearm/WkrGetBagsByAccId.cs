﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;
using Newtonsoft.Json;

namespace armoperator
{
    class WkrGetBagsByAccId
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultGetBagsByAccId m_resultREST = new ResultGetBagsByAccId();
        private WkrGetBagsByAccIdData m_workerData = new WkrGetBagsByAccIdData();

        public WkrGetBagsByAccId()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultGetBagsByAccId)e.Result;
        }

        private ResultGetBagsByAccId RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultGetBagsByAccId resultRest = new ResultGetBagsByAccId();
            List<Bags> bags = new List<Bags>();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<List<String>>(response);
                    Bags bagsData = new Bags();
                    foreach (var element in JSONobj)
                    {
                        bagsData = JsonConvert.DeserializeObject<Bags>(element);
                        bags.Add(bagsData);
                    }
                }
                catch { }
            }
            resultRest.bagsData = bags;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediaData = new RestRequest(m_workerData.api + m_workerData.accountId, Method.GET);
            requestGetMediaData.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetMediaData);
        }

        public ResultGetBagsByAccId GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrGetBagsByAccIdData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class ResultGetBagsByAccId
    {
        public List<Bags> bagsData { get; set; }
        public int statusCode { get; set; }
    }

    public class Bags
    {
        public int accountId { get; set; }
        public int applicationId { get; set; }
        public int currencyId { get; set; }
        public int id { get; set; }
        public Timeframe timeframe { get; set; }
        public int updatedAt { get; set; }
    }

    public class WkrGetBagsByAccIdData
    {
        public String URL { get;set; }
        public String api { get; set; }
        public String accountId { get; set; }
        public String token { get; set; }        
    }
}
