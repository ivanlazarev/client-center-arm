﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;

// TODO add timer to wait all operations done

namespace armoperator
{
    public partial class WndInfoESEK : Form
    {
        public const String TRANSPORT_BALL = "TRB";
        public const String PDF_FILE_NAME = "esekInfo.pdf";

        WndMain __mainWindow;
        private static List<Label> __tbxs;
        private static WkrReadCard __wkrReadCard;
        private static WkrReadOperationResults __wkrReadOperationResults;
        private static WkrGetMediasByRFID __wkrGetMediasByRFID;
        private static ResultMediasDataByRFID __resultMediasDataByRFID;
        private static WkrGetAccByAccountId __wkrGetAccByAccountId;
        private static ResultAccountDataByAccountId __resultAccountDataByAccountId;
        private static WkrGetPersonByUUID __wkrGetPersonByUUID;
        private static ResultPersonDataByUUID __resultPersonDataByUUID;
        List<Exemption> __personExemptions;
        private static WkrGetExemptions __wkrGetExemptions;
        private static ResultGetExemptions __resultGetExemptions;
        private static WkrGetCurrencies __wkrGetCurrencies;
        private static ResultGetCurrencies __resultGetCurrencies;
        private static WkrGetBagsByAccId __wkrGetBagsByAccId;
        private static ResultGetBagsByAccId __resultGetBagsByAccId;
        private static WkrGetBagTransactions __wkrGetBagTransactions;
        private static ResultGetBagTransactions __resultGetBagTransactions;
        private static String __lastRFID;

        private static List<Bags> __bagsForCheck;
        private static int __countBagsForCheck;
        
        public WndInfoESEK(WndMain mainWindow)
        {
            InitializeComponent();
            __mainWindow = mainWindow;
        }

        private void WndInfoESEK_Load(object sender, EventArgs e)
        {
            __tbxs = new System.Collections.Generic.List<Label>();
            __tbxs.Add(tbxCardRFID);
            __tbxs.Add(tbxCardNumber);
            __tbxs.Add(tbxStatus);
            __tbxs.Add(tbxPerson);
            __tbxs.Add(tbxSNILS);
            __tbxs.Add(tbxCardState);
            __tbxs.Add(tbxStatusREST);
            __tbxs.Add(tbxBallValue);

            //foreach (var tbx in __tbxs)
            //{
            //    tbx.ReadOnly = true;
            //    tbx.BackColor = System.Drawing.SystemColors.Control;
            //}
            
            InitializeTmrReadCard();
            InitializeTmrGetMediaByRFID();
            InitializeTmrGetAccByAccountId();
            InitializeTmrGetPersonByUUID();
            InitializeTmrGetExemptions();
            InitializeTmrGetCurrencies();
            InitializeTmrGetBagsByAccId();
            InitializeTmrGetBagTransactions();

            __wkrReadCard = new WkrReadCard(WndMain.READER_NAME);
            __wkrReadOperationResults = new WkrReadOperationResults();
            __wkrGetMediasByRFID = new WkrGetMediasByRFID();
            __resultMediasDataByRFID = new ResultMediasDataByRFID();
            __wkrGetAccByAccountId = new WkrGetAccByAccountId();
            __resultAccountDataByAccountId = new ResultAccountDataByAccountId();
            __wkrGetPersonByUUID = new WkrGetPersonByUUID();
            __resultPersonDataByUUID = new ResultPersonDataByUUID();
            __personExemptions = new List<Exemption>();
            __wkrGetExemptions = new WkrGetExemptions();
            __resultGetExemptions = new ResultGetExemptions();
            __wkrGetCurrencies = new WkrGetCurrencies();
            __resultGetCurrencies = new ResultGetCurrencies();
            __wkrGetBagsByAccId = new WkrGetBagsByAccId();
            __resultGetBagsByAccId = new ResultGetBagsByAccId();
            __wkrGetBagTransactions = new WkrGetBagTransactions();
            __resultGetBagTransactions = new ResultGetBagTransactions();

            __lastRFID = "";
            //__countHistory = "";
            //__transportCurrencyId = 0;
            //__bagId = 0;

            __bagsForCheck = new List<Bags>();
            
            this.lblDataESEK.Text = "1. Гражданин приходит в центр обслуживания клиентов;\n\r\n\r" +
                        "2. Гражданин обращается к оператору с целью получения " +
                            "информации о текущем бальном балансе карты и/или доступных льготах;\n\r\n\r" +
                        "3. Оператор проверяет соответствие фотографии на карте с личностью гражданина;\n\r\n\r" +
                        "4. Оператор размещает карту на считывающем NFC устройстве;\n\r\n\r" +
                        "5. Оператор называет количество баллов доступных на виртуальном " +
                            "бальном счете, сообщает информацию о действующих льготах," +
                            " доступных для держателя карты (при наличии);\n\r\n\r" +
                        "6. Оператор возвращает карту гражданину.";


            tmrReadCard.Start();
        }

        private void InitializeTmrReadCard()
        {
            tmrReadCard.Interval = 300;
            tmrReadCard.Tick += new EventHandler(tmrReadCard_Tick);
        }

        private void tmrReadCard_Tick(object Sender, EventArgs e)
        {
            bool readWkrStatus = __wkrReadCard.GetWkrStatus();
            if (!readWkrStatus)
            {
                __wkrReadOperationResults = __wkrReadCard.GetResultREST();
                int readStatus = __wkrReadOperationResults.readStatus;
                switch (readStatus)
                {
                    case 0:
                        try
                        {
                            String rfid = __wkrReadOperationResults.thrustCardInfo.uid;
                            if (null != rfid)
                            {
                                tbxCardRFID.Text = rfid;
                                tbxCardNumber.Text = __wkrReadOperationResults.thrustCardInfo.thrustCardInfoPart1.CardNumber;
                                tbxStatus.Text = "Данные карты считаны.";

                                if (__lastRFID != rfid)
                                {
                                    ClearTbxs(__tbxs);
                                    __personExemptions.Clear();
                                    if (btnPrintEsekInfo.Enabled)
                                        btnPrintEsekInfo.Enabled = false;
                                    dataGridViewTickets.Rows.Clear();
                                    dataGridViewTickets.Refresh();
                                    dataGridViewExemptions.Rows.Clear();
                                    dataGridViewExemptions.Refresh();

                                    __lastRFID = rfid;
                                    WkrGetMediasByRFIDData wkrGetMediasByRFIDData = new WkrGetMediasByRFIDData();
                                    wkrGetMediasByRFIDData.URL = WndMain.BASE_URL;
                                    wkrGetMediasByRFIDData.api = "/api/cp/v1/medias/";
                                    wkrGetMediasByRFIDData.rfid = rfid;
                                    wkrGetMediasByRFIDData.token = __mainWindow.GetAuthToken();
                                    __wkrGetMediasByRFID.SetWorkerData(wkrGetMediasByRFIDData);

                                    if (!__wkrGetMediasByRFID.GetWkrStatus())
                                    {
                                        __wkrGetMediasByRFID.PerformRestOperation();
                                        tmrGetMediaByRFID.Start();
                                    }
                                    tbxStatusREST.Text = "Запрос данных о карте держателя.";                                                                             
                                }
                            }
                        }
                        catch { }
                        break;
                    case 1:
                        tbxStatus.Text = "Неверный формат карты.";
                        break;
                    case 2:
                        tbxStatus.Text = "Приложите карту ещё раз.";
                        break;
                    case 3:
                        __lastRFID = "";
                        ClearTbxs(__tbxs);
                        __personExemptions.Clear();
                        if (btnPrintEsekInfo.Enabled)
                            btnPrintEsekInfo.Enabled = false;
                        dataGridViewTickets.Rows.Clear();
                        dataGridViewTickets.Refresh();
                        dataGridViewExemptions.Rows.Clear();
                        dataGridViewExemptions.Refresh();
                        tbxStatus.Text = "Положите карту на считыватель.";
                        break;
                    case 4:
                        tbxStatus.Text = "Подключите считыватель.";
                        break;
                    case 5:
                        tbxStatus.Text = "Ошибка модели считывателя.";
                        break;
                    case 6:
                        tbxStatus.Text = "Неизвестная ошибка при чтении карты";
                        break;
                    case 7:
                        tbxStatus.Text = "Ошибка подключения к считывателю";
                        break;
                    default:
                        tbxStatus.Text = "Неизвестная ошибка";
                        break;
                }
                __wkrReadCard.PerformReadOperation();                
            }
        }

        private void InitializeTmrGetMediaByRFID()
        {
            tmrGetMediaByRFID.Interval = 100;
            tmrGetMediaByRFID.Tick += new EventHandler(tmrGetMediaByRFID_Tick);
        }

        private void tmrGetMediaByRFID_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetMediasByRFID.GetWkrStatus())
            {
                tmrGetMediaByRFID.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultMediasDataByRFID = __wkrGetMediasByRFID.GetResultREST();
                switch (__resultMediasDataByRFID.statusCode)
                {
                    case 200:
                        try
                        {
                            int accountId = __resultMediasDataByRFID.mediasDataByRFID.accountId;
                            String cardState = __resultMediasDataByRFID.mediasDataByRFID.states[0].state;
                            switch (cardState)
                            {
                                case  WndStatement.CARD_REGISTERED_STR:
                                    tbxCardState.Text = "Карта зарегестрирована.";
                                    break;
                                case WndStatement.CARD_ACTIVATED_STR:
                                    tbxCardState.Text = "Карта активирована.";
                                    break;
                                case WndStatement.CARD_BLOCKED_STR:
                                    tbxCardState.Text = "Карта заблокирована.";
                                    break;
                                default:
                                    break;
                            }

                            WkrGetAccByAccountIdData wkrGetAccByAccountIdData = new WkrGetAccByAccountIdData();
                            wkrGetAccByAccountIdData.URL = WndMain.BASE_URL;
                            wkrGetAccByAccountIdData.api = "/api/cp/v1/accounts/";
                            wkrGetAccByAccountIdData.accountId = Convert.ToString(accountId);
                            wkrGetAccByAccountIdData.token = __mainWindow.GetAuthToken();
                            __wkrGetAccByAccountId.SetWorkerData(wkrGetAccByAccountIdData);

                            if (!__wkrGetAccByAccountId.GetWkrStatus())
                            {
                                __wkrGetAccByAccountId.PerformRestOperation();
                                tmrGetAccByAccountId.Start();
                            }
                            tbxStatusREST.Text = "Запрос счета держателя карты.";

                            //отсюда запрос списка валют и кошельков
                            WkrGetCurrenciesData wkrGetCurrenciesData = new WkrGetCurrenciesData();
                            wkrGetCurrenciesData.URL = WndMain.BASE_URL;
                            wkrGetCurrenciesData.api = "/api/cp/v1/applications/";
                            wkrGetCurrenciesData.applicationId = __mainWindow.GetApplicationId().ToString() + "/currencies";
                            wkrGetCurrenciesData.token = __mainWindow.GetAuthToken();
                            __wkrGetCurrencies.SetWorkerData(wkrGetCurrenciesData);

                            if (!__wkrGetCurrencies.GetWkrStatus())
                            {
                                __wkrGetCurrencies.PerformRestOperation();
                                tmrGetCurrencies.Start();
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("У данной карты отсутствует держатель.");
                        break;
                    case 401:                        
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetAccByAccountId()
        {
            tmrGetAccByAccountId.Interval = 100;
            tmrGetAccByAccountId.Tick += new EventHandler(tmrGetAccByAccountId_Tick);
        }

        private void tmrGetAccByAccountId_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetAccByAccountId.GetWkrStatus())
            {
                tmrGetAccByAccountId.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultAccountDataByAccountId = __wkrGetAccByAccountId.GetResultREST();
                switch (__resultAccountDataByAccountId.statusCode)
                {
                    case 200:
                        try
                        {
                            String userId = __resultAccountDataByAccountId.accountDataByAccountId.userId;

                            WkrPersonByUUIDWorkerData wkrPersonByUUIDWorkerData = new WkrPersonByUUIDWorkerData();
                            wkrPersonByUUIDWorkerData.URL = WndMain.BASE_URL;
                            wkrPersonByUUIDWorkerData.api = "/api/spd/v1/persons/";
                            wkrPersonByUUIDWorkerData.uuid = userId;
                            wkrPersonByUUIDWorkerData.token = __mainWindow.GetAuthToken();
                            __wkrGetPersonByUUID.SetWorkerData(wkrPersonByUUIDWorkerData);

                            if (!__wkrGetPersonByUUID.GetWkrStatus())
                            {
                                __wkrGetPersonByUUID.PerformRestOperation();
                                tmrGetPersonByUUID.Start();
                            }
                            tbxStatusREST.Text = "Запрос сведений о держателе карты.";
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetPersonByUUID()
        {
            tmrGetPersonByUUID.Interval = 100;
            tmrGetPersonByUUID.Tick += new EventHandler(tmrGetPersonByUUID_Tick);
        }

        private void tmrGetPersonByUUID_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetPersonByUUID.GetWkrStatus())
            {
                tmrGetPersonByUUID.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultPersonDataByUUID = __wkrGetPersonByUUID.GetResultREST();
                switch (__resultPersonDataByUUID.statusCode)
                {
                    case 200:
                        try
                        {
                            String personInfo = __resultPersonDataByUUID.personDataByUUID.name;
                            String snilsInfo = __resultPersonDataByUUID.personDataByUUID.snils;
                            try
                            {
                                if (0!=__resultPersonDataByUUID.personDataByUUID.exemptions.Count)
                                {
                                    if (0!=__resultPersonDataByUUID.personDataByUUID.exemptions[0].Count)
                                    {
                                        foreach (var element in __resultPersonDataByUUID.personDataByUUID.exemptions[0])
                                        {
                                            Exemption ext = new Exemption();
                                            ext = JsonConvert.DeserializeObject<Exemption>(element[0]);
                                            __personExemptions.Add(ext);
                                        }
                                        GetExemptionsWorkerData getExemptionsWorkerData = new GetExemptionsWorkerData();
                                        getExemptionsWorkerData.URL = WndMain.BASE_URL;
                                        getExemptionsWorkerData.api = "/api/spd/v1/exemptions";
                                        getExemptionsWorkerData.token = __mainWindow.GetAuthToken();
                                        __wkrGetExemptions.SetWorkerData(getExemptionsWorkerData);
                                        if (!__wkrGetExemptions.GetWkrStatus())
                                        {
                                            __wkrGetExemptions.PerformRestOperation();
                                            tmrGetExemptions.Start();
                                        }
                                        tbxStatusREST.Text = "Запрос сведений о льготах и транспортных баллах.";
                                    }
                                    else
                                    {
                                        tbxStatusREST.Text = "Данные о держателе карты получены. Сведения о льготах отсутствуют.";
                                    }
                                }
                                else
                                {
                                    //rtbxLogShowDataESEK.AppendText("Сведения о льготах отсутствуют.\n\r");
                                }
                            }
                            catch { }

                            tbxPerson.Text = personInfo;
                            tbxSNILS.Text = snilsInfo;
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }        

        private void InitializeTmrGetExemptions()
        {
            tmrGetExemptions.Interval = 100;
            tmrGetExemptions.Tick += new EventHandler(tmrGetExemptions_Tick);
        }

        private void tmrGetExemptions_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetExemptions.GetWkrStatus())
            {
                tmrGetExemptions.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultGetExemptions = __wkrGetExemptions.GetResultREST();
                switch (__resultGetExemptions.statusCode)
                {
                    case 200:
                        List<String> personExemptionsDataFromRest = new List<String>();
                        try
                        {
                            dynamic exemptions = __resultGetExemptions.exemptionsData[0].exemptions;                            
                            foreach (var personExemption in __personExemptions)
                            {
                                foreach (var exemption in exemptions)
                                {
                                    if (personExemption.ouid == exemption.Key)
                                    {
                                        int putCycle = 0;
                                        foreach (var value in exemption.Value)
                                        {
                                            if ((putCycle%2) != 0)
                                            {
                                                personExemptionsDataFromRest.Add(value.Value);
                                            }
                                            putCycle++;
                                        }
                                    }
                                }
                            }

                            int printCycle = 0;
                            foreach (var personExemption in __personExemptions)
                            {
                                if (null == personExemption.timeframe.startTimestamp)
                                {
                                    dataGridViewExemptions.Rows.Add(new object[] { personExemptionsDataFromRest[printCycle],
                                                                                    "бессрочный"});
                                }
                                else
                                {
                                    dataGridViewExemptions.Rows.Add(new object[] { personExemptionsDataFromRest[printCycle],
                                                                                    "по " + UnixTimestampToDateTime(Convert.ToDouble(personExemption.timeframe.finishTimestamp)).AddDays(-1).ToLongDateString()});
                                }
                                printCycle++;
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetCurrencies()
        {
            tmrGetCurrencies.Interval = 100;
            tmrGetCurrencies.Tick += new EventHandler(tmrGetCurrencies_Tick);
        }

        private void tmrGetCurrencies_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetCurrencies.GetWkrStatus())
            {
                tmrGetCurrencies.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultGetCurrencies = __wkrGetCurrencies.GetResultREST();
                switch (__resultGetCurrencies.statusCode)
                {
                    case 200:
                        try
                        {
                            WkrGetBagsByAccIdData wkrGetBagsByAccIdData = new WkrGetBagsByAccIdData();
                            wkrGetBagsByAccIdData.api = "/api/cp/v1/accounts/";
                            wkrGetBagsByAccIdData.accountId = __resultMediasDataByRFID.mediasDataByRFID.accountId.ToString() + "/bags";
                            wkrGetBagsByAccIdData.URL = WndMain.BASE_URL;
                            wkrGetBagsByAccIdData.token = __mainWindow.GetAuthToken();
                            __wkrGetBagsByAccId.SetWorkerData(wkrGetBagsByAccIdData);
                            if (!__wkrGetBagsByAccId.GetWkrStatus())
                            {
                                __wkrGetBagsByAccId.PerformRestOperation();
                                tmrGetBagsByAccId.Start();
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Приложение не найдено.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetBagsByAccId()
        {
            tmrGetBagsByAccId.Interval = 100;
            tmrGetBagsByAccId.Tick += new EventHandler(tmrGetBagsByAccId_Tick);
        }

        private void tmrGetBagsByAccId_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetBagsByAccId.GetWkrStatus())
            {
                tmrGetBagsByAccId.Stop();
                __bagsForCheck.Clear();
                __countBagsForCheck = 0;
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultGetBagsByAccId = __wkrGetBagsByAccId.GetResultREST();
                switch (__resultGetBagsByAccId.statusCode)
                {
                    case 200:
                        try
                        {
                            if (0 != __resultGetBagsByAccId.bagsData.Count)
                            {
                                foreach (var bag in __resultGetBagsByAccId.bagsData)
                                {
                                    int bagCurrencyId = bag.currencyId;
                                    foreach (var currency in __resultGetCurrencies.currenciesData)
                                    {
                                        if (bagCurrencyId==currency.id && currency.countable)
                                        {
                                            __bagsForCheck.Add(bag);
                                            __countBagsForCheck++;
                                        }
                                        else if (bagCurrencyId==currency.id)
                                        {
                                            if (null == bag.timeframe.finishTimestamp)
                                            {
                                                dataGridViewTickets.Rows.Add(new object[] { currency.title,
                                                                                            "-",
                                                                                            "бессрочный"});
                                            }
                                            else
                                            {
                                                dataGridViewTickets.Rows.Add(new object[] { currency.title,
                                                                                    "-",
                                                                                    "по " + UnixTimestampToDateTime(Convert.ToDouble(bag.timeframe.finishTimestamp)).AddDays(-1).ToLongDateString()});
                                                
                                            }
                                        }
                                    }
                                }

                                if (0 != __countBagsForCheck)
                                {
                                    tmrCheckBags.Start();
                                }
                                else
                                {
                                    if ("" == tbxBallValue.Text)
                                        tbxBallValue.Text = "Транспортные баллы на счете отсутствуют.";
                                    if (!btnPrintEsekInfo.Enabled)
                                        btnPrintEsekInfo.Enabled = true;
                                }
                            }
                            else
                            {
                                if (!btnPrintEsekInfo.Enabled)
                                    btnPrintEsekInfo.Enabled = true;
                                tbxStatusREST.Text = "Данные о держателе карты получены.";
                                tbxBallValue.Text = "Транспортные баллы на счете отсутствуют.";
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        if (!btnPrintEsekInfo.Enabled)
                            btnPrintEsekInfo.Enabled = true;
                        MessageBox.Show("Кошельки отсутствуют.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetBagTransactions()
        {
            tmrGetBagTransactions.Interval = 100;
            tmrGetBagTransactions.Tick += new EventHandler(tmrGetBagTransactions_Tick);
        }

        private void tmrGetBagTransactions_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetBagTransactions.GetWkrStatus())
            {
                tmrGetBagTransactions.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultGetBagTransactions = __wkrGetBagTransactions.GetResultREST();
                switch (__resultGetBagTransactions.statusCode)
                {
                    case 200:
                        try
                        {
                            if (0 != __resultGetBagTransactions.transactionsData.Count)
                            {
                                int ballValue = 0;
                                foreach (var transaction in __resultGetBagTransactions.transactionsData)
                                {
                                    switch (transaction.type)
                                    {
                                        case "replenishment":
                                            ballValue += transaction.value;
                                            break;
                                        case "payment":
                                            ballValue -= transaction.value;
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                String curencyTitle = "";
                                String currencySymbol = "";
                                foreach (var currency in __resultGetCurrencies.currenciesData)
                                {
                                    if (__resultGetBagTransactions.transactionsData[0].currencyId == currency.id)
                                    {
                                        curencyTitle = currency.title;
                                        currencySymbol = currency.symbol;
                                        break;
                                    }
                                }

                                if (currencySymbol == TRANSPORT_BALL)
                                {
                                    tbxStatusREST.Text = "Данные о держателе карты получены.";
                                    tbxBallValue.Text = ballValue.ToString();
                                }
                                else
                                {                                    
                                    tbxStatusREST.Text = "Данные о держателе карты получены.";

                                    if (null == __bagsForCheck[__countBagsForCheck].timeframe.finishTimestamp)
                                    {
                                        dataGridViewTickets.Rows.Add(new object[] { curencyTitle,
                                                                                    ballValue.ToString(),
                                                                                    "бессрочный"});
                                    }
                                    else
                                    {
                                        dataGridViewTickets.Rows.Add(new object[] { curencyTitle,
                                                                                    ballValue.ToString(),
                                                                                    "по " + UnixTimestampToDateTime(Convert.ToDouble(__bagsForCheck[__countBagsForCheck].timeframe.finishTimestamp)).AddDays(-1).ToLongDateString()});
                                    }                                    
                                }
                            }
                            else
                            {
                                tbxBallValue.Text = "Транзакции не проводились.";
                            }
                            //rtbxLogShowDataESEK.Refresh();
                        }
                        catch { MessageBox.Show("Ошибка при проверке транзакций кошелька"); }
                        break;
                    case 404:
                        MessageBox.Show("Транзакции отсутствуют.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }           

        public static DateTime UnixTimestampToDateTime(double unixTime)
        {
            DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            long unixTimeStampInTicks = (long)(unixTime * TimeSpan.TicksPerSecond);
            return new DateTime(unixStart.Ticks + unixTimeStampInTicks);
        }

        private void ClearTbxs(List<Label> textBoxes)
        {
            try
            {
                foreach (var textBox in textBoxes)
                    textBox.Text = "";
            }
            catch { }
        }

        private void WndInfoESEK_FormClosing(object sender, FormClosingEventArgs e)
        {
            __lastRFID = "";
            this.Dispose(true);
        }

        private void tmrCheckBags_Tick(object sender, EventArgs e)
        {
            if (0 != __countBagsForCheck)
            {
                bool getBagTransactionsWkrStatus = __wkrGetBagTransactions.GetWkrStatus();
                if (!getBagTransactionsWkrStatus && !tmrGetBagTransactions.Enabled)
                {
                    try
                    {
                        WkrGetBagTransactionsData wkrGetBagTransactionsData = new WkrGetBagTransactionsData();
                        wkrGetBagTransactionsData.URL = WndMain.BASE_URL;
                        wkrGetBagTransactionsData.api = "/api/cp/v1/accounts/";
                        wkrGetBagTransactionsData.accountId = __bagsForCheck[__countBagsForCheck - 1].accountId.ToString() + "/bags/";
                        wkrGetBagTransactionsData.bagId = __bagsForCheck[__countBagsForCheck - 1].id.ToString() + "/transactions";
                        wkrGetBagTransactionsData.token = __mainWindow.GetAuthToken();
                        __wkrGetBagTransactions.SetWorkerData(wkrGetBagTransactionsData);
                        __wkrGetBagTransactions.PerformRestOperation();
                        tmrGetBagTransactions.Start();
                        __countBagsForCheck--;
                        //__countHistory += __countBagsForCheck.ToString() + ",";
                    }
                    catch { MessageBox.Show("Ошибка проверки кошелька"); }
                }
            }
            else
            {
                if (""==tbxBallValue.Text)
                    tbxBallValue.Text = "Транспортные баллы на счете отсутствуют.";
                if (!btnPrintEsekInfo.Enabled)
                    btnPrintEsekInfo.Enabled = true;
                tmrCheckBags.Stop();
            }  
        }

        private void makeEsekInfo()
        {
            try
            {
                var esekInfo = new Document();
                BaseFont baseFont = BaseFont.CreateFont(@"C:\Windows\Fonts\times.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 8, iTextSharp.text.Font.NORMAL);

                String pdfFileName = DateTime.UtcNow.Ticks.ToString() + PDF_FILE_NAME;

                PdfWriter.GetInstance(esekInfo, new FileStream(pdfFileName, FileMode.Create));
                esekInfo.Open();
                String text = String.Empty;
                iTextSharp.text.Font headerFont = new iTextSharp.text.Font(baseFont, 11, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font boldFont = new iTextSharp.text.Font(baseFont, 11, iTextSharp.text.Font.BOLD);


                AppSettings appSettings = new AppSettings();
                appSettings = __mainWindow.GetAppSettingsData();
                // предмет

                text = "Оператор: " + __mainWindow.GetLblOperatorLoginText() + "\n";
                esekInfo.Add(new Paragraph(new Phrase(text, headerFont)) { Alignment = 2 });

                text = "Пункт обслуживания клиентов №" + appSettings.m_OperatorStation + "\n";
                text += "Адрес пункта обслуживания: " + appSettings.m_OperatorAddress + "\n";
                text += "Информация о карте ЕСЭК\n";
                text += "По состоянию на: " + DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToShortDateString() + "\n\r";
                esekInfo.Add(new Paragraph(new Phrase(text, boldFont)) { Alignment = 1 });

                text = "Серийный номер карты: " + tbxCardRFID.Text  + "\n\r";
                text += "Номер карты: " + tbxCardNumber.Text + "\n\r";
                text += "Держатель карты: " + tbxPerson.Text + "\n\r";
                text += "Номер СНИЛС: " + tbxSNILS.Text + "\n\r";
                text += "Статус карты: " + tbxCardState.Text + "\n\r";
                text += "Количество транспортных баллов: " + tbxBallValue.Text + "\n\r";
                esekInfo.Add(new Paragraph(new Phrase(text, headerFont)) { Alignment = 0 });

                text = "Сведения о льготах: \n\r";
                esekInfo.Add(new Paragraph(new Phrase(text, boldFont)) { Alignment = 0 });

                if (0 != dataGridViewExemptions.Rows.Count)
                {
                    //Creating iTextSharp Table from the DataTable data
                    PdfPTable pdfExemptions = new PdfPTable(dataGridViewExemptions.ColumnCount);
                    pdfExemptions.DefaultCell.Padding = 3;
                    pdfExemptions.WidthPercentage = 100;
                    pdfExemptions.HorizontalAlignment = Element.ALIGN_LEFT;
                    pdfExemptions.DefaultCell.BorderWidth = 1;

                    //Adding Header row
                    foreach (DataGridViewColumn column in dataGridViewExemptions.Columns)
                    {
                        PdfPCell cellHeader = new PdfPCell(new Phrase(column.HeaderText, headerFont));
                        //cell.BackgroundColor = new iTextSharp.text.Color(240, 240, 240);
                        pdfExemptions.AddCell(cellHeader);
                    }

                    //Adding DataRow
                    foreach (DataGridViewRow row in dataGridViewExemptions.Rows)
                    {
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            PdfPCell cellRow = new PdfPCell(new Phrase(cell.Value.ToString(), headerFont));
                            pdfExemptions.AddCell(cellRow);
                        }
                    }

                    esekInfo.Add(pdfExemptions);
                }
                else
                {
                    text = "Льготы не зарегестрированы.\n";
                    esekInfo.Add(new Paragraph(new Phrase(text, headerFont)) { Alignment = 0 });
                }

                text = "\nСведения о билетах: \n\r";
                esekInfo.Add(new Paragraph(new Phrase(text, boldFont)) { Alignment = 0 });

                if (0 != dataGridViewTickets.Rows.Count)
                {
                    //Creating iTextSharp Table from the DataTable data
                    PdfPTable pdfTickets = new PdfPTable(dataGridViewTickets.ColumnCount);
                    pdfTickets.DefaultCell.Padding = 3;
                    pdfTickets.WidthPercentage = 100;
                    pdfTickets.HorizontalAlignment = Element.ALIGN_LEFT;
                    pdfTickets.DefaultCell.BorderWidth = 1;

                    //Adding Header row
                    foreach (DataGridViewColumn column in dataGridViewTickets.Columns)
                    {
                        PdfPCell cellHeader = new PdfPCell(new Phrase(column.HeaderText, headerFont));
                        //cell.BackgroundColor = new iTextSharp.text.Color(240, 240, 240);
                        pdfTickets.AddCell(cellHeader);
                    }

                    //Adding DataRow
                    foreach (DataGridViewRow row in dataGridViewTickets.Rows)
                    {
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            PdfPCell cellRow = new PdfPCell(new Phrase(cell.Value.ToString(), headerFont));
                            pdfTickets.AddCell(cellRow);
                        }
                    }

                    esekInfo.Add(pdfTickets);
                }
                else
                {
                    text = "Действительные проездные билеты и разовые поездки на счете отсутствуют.\n\r";
                    esekInfo.Add(new Paragraph(new Phrase(text, headerFont)) { Alignment = 0 });
                }

                // сохранение и открытие
                esekInfo.Close();
                System.Diagnostics.Process.Start(pdfFileName);
            }
            catch (Exception e)
            {
                String error = e.Message;
                MessageBox.Show("PDF файл открыт. Завершите просмотр файла.");
            }            
        }

        private void btnPrintEsekInfo_Click(object sender, EventArgs e)
        {
            makeEsekInfo();
        }
    };
}
