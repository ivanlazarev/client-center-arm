﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;
using Newtonsoft.Json;

namespace armoperator
{
    class WkrActivateESEK
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultActivateESEK m_resultREST = new ResultActivateESEK();
        private WkrActivateESEKData m_workerData = new WkrActivateESEKData();

        public WkrActivateESEK()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultActivateESEK)e.Result;
        }

        private ResultActivateESEK RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultActivateESEK resultRest = new ResultActivateESEK();
            List<CardState> сardState = new List<CardState>();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<List<String>>(response);
                    CardState transactionsData = new CardState();
                    foreach (var element in JSONobj)
                    {
                        transactionsData = JsonConvert.DeserializeObject<CardState>(element);
                        сardState.Add(transactionsData);
                    }
                }
                catch { }
            }
            resultRest.transactionsData = сardState;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediaData = new RestRequest(m_workerData.api + m_workerData.accountId + m_workerData.mediaId, Method.POST);
            requestGetMediaData.AddHeader("Auth-Access-Token", m_workerData.token);
            requestGetMediaData.AddJsonBody("activated");
            RESTWkr.RunWorkerAsync(requestGetMediaData);
        }

        public ResultActivateESEK GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrActivateESEKData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class ResultActivateESEK
    {
        public List<CardState> transactionsData { get; set; }
        public int statusCode { get; set; }
    }

    public class CardState
    {
        public string state { get; set; }
        public int timestamp { get; set; }
    }

    public class WkrActivateESEKData
    {
        public String URL { get;set; }
        public String api { get; set; }
        public String accountId { get; set; }
        public String mediaId { get; set; }
        public String token { get; set; }        
    }
}
