﻿namespace armoperator
{
    partial class WndBlockESEK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WndBlockESEK));
            this.tabESEK = new System.Windows.Forms.TabControl();
            this.tabEnterSNILS = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNextEnterSNILS = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxEnterSNILS = new System.Windows.Forms.MaskedTextBox();
            this.lblESEKInfo = new System.Windows.Forms.Label();
            this.btnCancelEnterSNILS = new System.Windows.Forms.Button();
            this.tabEditPersInfo = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbxSPDFIO = new System.Windows.Forms.TextBox();
            this.tbxSPDSNILS = new System.Windows.Forms.TextBox();
            this.btnNextPersonalInfo = new System.Windows.Forms.Button();
            this.btnBackPersonalInfo = new System.Windows.Forms.Button();
            this.btnCancelPersonalInfo = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.tabListESEK = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.btnCancelListESEK = new System.Windows.Forms.Button();
            this.btnBackListESEK = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tbxCardNum = new System.Windows.Forms.TextBox();
            this.tbxCardState = new System.Windows.Forms.TextBox();
            this.btnBlockESEK = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.timer5 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.restResponceStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrBlockESEK = new System.Windows.Forms.Timer(this.components);
            this.tabESEK.SuspendLayout();
            this.tabEnterSNILS.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabEditPersInfo.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tabListESEK.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabESEK
            // 
            this.tabESEK.Controls.Add(this.tabEnterSNILS);
            this.tabESEK.Controls.Add(this.tabEditPersInfo);
            this.tabESEK.Controls.Add(this.tabListESEK);
            this.tabESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabESEK.Location = new System.Drawing.Point(3, 3);
            this.tabESEK.Name = "tabESEK";
            this.tabESEK.SelectedIndex = 0;
            this.tabESEK.Size = new System.Drawing.Size(778, 536);
            this.tabESEK.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabESEK.TabIndex = 0;
            this.tabESEK.TabStop = false;
            this.tabESEK.SelectedIndexChanged += new System.EventHandler(this.tabESEK_SelectedIndexChanged);
            // 
            // tabEnterSNILS
            // 
            this.tabEnterSNILS.Controls.Add(this.tableLayoutPanel3);
            this.tabEnterSNILS.Location = new System.Drawing.Point(4, 22);
            this.tabEnterSNILS.Name = "tabEnterSNILS";
            this.tabEnterSNILS.Padding = new System.Windows.Forms.Padding(3);
            this.tabEnterSNILS.Size = new System.Drawing.Size(770, 510);
            this.tabEnterSNILS.TabIndex = 0;
            this.tabEnterSNILS.Text = "Ввод СНИЛС";
            this.tabEnterSNILS.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnNextEnterSNILS, 3, 5);
            this.tableLayoutPanel3.Controls.Add(this.label16, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.label2, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.tbxEnterSNILS, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblESEKInfo, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnCancelEnterSNILS, 2, 5);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(764, 504);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label1, 4);
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(758, 60);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ввод номера СНИЛС";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNextEnterSNILS
            // 
            this.btnNextEnterSNILS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextEnterSNILS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNextEnterSNILS.Location = new System.Drawing.Point(576, 467);
            this.btnNextEnterSNILS.Name = "btnNextEnterSNILS";
            this.btnNextEnterSNILS.Size = new System.Drawing.Size(185, 34);
            this.btnNextEnterSNILS.TabIndex = 2;
            this.btnNextEnterSNILS.Text = "Далее";
            this.btnNextEnterSNILS.UseVisualStyleBackColor = true;
            this.btnNextEnterSNILS.Click += new System.EventHandler(this.btnNextEnterSNILS_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(3, 464);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(185, 40);
            this.label16.TabIndex = 9;
            this.label16.Text = "Шаг 1";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(385, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 50);
            this.label2.TabIndex = 7;
            this.label2.Text = "Номер СНИЛС:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbxEnterSNILS
            // 
            this.tbxEnterSNILS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxEnterSNILS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxEnterSNILS.Location = new System.Drawing.Point(576, 72);
            this.tbxEnterSNILS.Mask = "00000000000";
            this.tbxEnterSNILS.Name = "tbxEnterSNILS";
            this.tbxEnterSNILS.Size = new System.Drawing.Size(185, 26);
            this.tbxEnterSNILS.TabIndex = 0;
            this.tbxEnterSNILS.Click += new System.EventHandler(this.tbxEnterSNILS_Click);
            this.tbxEnterSNILS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxEnterSNILS_KeyPress);
            this.tbxEnterSNILS.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbxEnterSNILS_KeyUp);
            // 
            // lblESEKInfo
            // 
            this.lblESEKInfo.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.lblESEKInfo, 2);
            this.lblESEKInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblESEKInfo.Location = new System.Drawing.Point(3, 60);
            this.lblESEKInfo.Name = "lblESEKInfo";
            this.tableLayoutPanel3.SetRowSpan(this.lblESEKInfo, 4);
            this.lblESEKInfo.Size = new System.Drawing.Size(376, 404);
            this.lblESEKInfo.TabIndex = 10;
            this.lblESEKInfo.Text = "label4";
            // 
            // btnCancelEnterSNILS
            // 
            this.btnCancelEnterSNILS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancelEnterSNILS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelEnterSNILS.Location = new System.Drawing.Point(385, 467);
            this.btnCancelEnterSNILS.Name = "btnCancelEnterSNILS";
            this.btnCancelEnterSNILS.Size = new System.Drawing.Size(185, 34);
            this.btnCancelEnterSNILS.TabIndex = 1;
            this.btnCancelEnterSNILS.Text = "Отмена";
            this.btnCancelEnterSNILS.UseVisualStyleBackColor = true;
            this.btnCancelEnterSNILS.Click += new System.EventHandler(this.btnCancelEnterSNILS_Click);
            // 
            // tabEditPersInfo
            // 
            this.tabEditPersInfo.Controls.Add(this.tableLayoutPanel4);
            this.tabEditPersInfo.Location = new System.Drawing.Point(4, 22);
            this.tabEditPersInfo.Name = "tabEditPersInfo";
            this.tabEditPersInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabEditPersInfo.Size = new System.Drawing.Size(770, 510);
            this.tabEditPersInfo.TabIndex = 1;
            this.tabEditPersInfo.Text = "Персональные данные";
            this.tabEditPersInfo.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tbxSPDFIO, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.tbxSPDSNILS, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.btnNextPersonalInfo, 4, 10);
            this.tableLayoutPanel4.Controls.Add(this.btnBackPersonalInfo, 3, 10);
            this.tableLayoutPanel4.Controls.Add(this.btnCancelPersonalInfo, 2, 10);
            this.tableLayoutPanel4.Controls.Add(this.label17, 0, 10);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 11;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(764, 504);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label3, 5);
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(758, 50);
            this.label3.TabIndex = 6;
            this.label3.Text = "Учетные данные";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label7, 2);
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(3, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(298, 40);
            this.label7.TabIndex = 13;
            this.label7.Text = "СНИЛС";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label9, 2);
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(3, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(298, 40);
            this.label9.TabIndex = 15;
            this.label9.Text = "Держатель карты:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxSPDFIO
            // 
            this.tbxSPDFIO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.SetColumnSpan(this.tbxSPDFIO, 3);
            this.tbxSPDFIO.Enabled = false;
            this.tbxSPDFIO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxSPDFIO.Location = new System.Drawing.Point(307, 57);
            this.tbxSPDFIO.Name = "tbxSPDFIO";
            this.tbxSPDFIO.Size = new System.Drawing.Size(454, 26);
            this.tbxSPDFIO.TabIndex = 18;
            this.tbxSPDFIO.TabStop = false;
            // 
            // tbxSPDSNILS
            // 
            this.tbxSPDSNILS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.SetColumnSpan(this.tbxSPDSNILS, 3);
            this.tbxSPDSNILS.Enabled = false;
            this.tbxSPDSNILS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxSPDSNILS.Location = new System.Drawing.Point(307, 97);
            this.tbxSPDSNILS.Name = "tbxSPDSNILS";
            this.tbxSPDSNILS.Size = new System.Drawing.Size(454, 26);
            this.tbxSPDSNILS.TabIndex = 19;
            this.tbxSPDSNILS.TabStop = false;
            // 
            // btnNextPersonalInfo
            // 
            this.btnNextPersonalInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextPersonalInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNextPersonalInfo.Location = new System.Drawing.Point(611, 467);
            this.btnNextPersonalInfo.Name = "btnNextPersonalInfo";
            this.btnNextPersonalInfo.Size = new System.Drawing.Size(150, 34);
            this.btnNextPersonalInfo.TabIndex = 3;
            this.btnNextPersonalInfo.Text = "Далее";
            this.btnNextPersonalInfo.UseVisualStyleBackColor = true;
            this.btnNextPersonalInfo.Click += new System.EventHandler(this.btnNextPersonalInfo_Click);
            // 
            // btnBackPersonalInfo
            // 
            this.btnBackPersonalInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBackPersonalInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackPersonalInfo.Location = new System.Drawing.Point(459, 467);
            this.btnBackPersonalInfo.Name = "btnBackPersonalInfo";
            this.btnBackPersonalInfo.Size = new System.Drawing.Size(146, 34);
            this.btnBackPersonalInfo.TabIndex = 2;
            this.btnBackPersonalInfo.Text = "Назад";
            this.btnBackPersonalInfo.UseVisualStyleBackColor = true;
            this.btnBackPersonalInfo.Click += new System.EventHandler(this.btnBackPersonalInfo_Click);
            // 
            // btnCancelPersonalInfo
            // 
            this.btnCancelPersonalInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancelPersonalInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelPersonalInfo.Location = new System.Drawing.Point(307, 467);
            this.btnCancelPersonalInfo.Name = "btnCancelPersonalInfo";
            this.btnCancelPersonalInfo.Size = new System.Drawing.Size(146, 34);
            this.btnCancelPersonalInfo.TabIndex = 1;
            this.btnCancelPersonalInfo.Text = "Отмена";
            this.btnCancelPersonalInfo.UseVisualStyleBackColor = true;
            this.btnCancelPersonalInfo.Click += new System.EventHandler(this.btnCancelPersonalInfo_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(3, 464);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(146, 40);
            this.label17.TabIndex = 25;
            this.label17.Text = "Шаг 2";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabListESEK
            // 
            this.tabListESEK.Controls.Add(this.tableLayoutPanel2);
            this.tabListESEK.Location = new System.Drawing.Point(4, 22);
            this.tabListESEK.Name = "tabListESEK";
            this.tabListESEK.Padding = new System.Windows.Forms.Padding(3);
            this.tabListESEK.Size = new System.Drawing.Size(770, 510);
            this.tabListESEK.TabIndex = 2;
            this.tabListESEK.Text = "Перечень ЕСЭК";
            this.tabListESEK.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.18182F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.27273F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.18182F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.18182F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.18182F));
            this.tableLayoutPanel2.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCancelListESEK, 4, 5);
            this.tableLayoutPanel2.Controls.Add(this.btnBackListESEK, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.label19, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label21, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label22, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbxCardNum, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbxCardState, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnBlockESEK, 3, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(764, 504);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label11, 5);
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(758, 60);
            this.label11.TabIndex = 1;
            this.label11.Text = "Карта ЕСЭК";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCancelListESEK
            // 
            this.btnCancelListESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancelListESEK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelListESEK.Location = new System.Drawing.Point(625, 467);
            this.btnCancelListESEK.Name = "btnCancelListESEK";
            this.btnCancelListESEK.Size = new System.Drawing.Size(136, 34);
            this.btnCancelListESEK.TabIndex = 4;
            this.btnCancelListESEK.Text = "Закрыть";
            this.btnCancelListESEK.UseVisualStyleBackColor = true;
            this.btnCancelListESEK.Click += new System.EventHandler(this.btnCancelListESEK_Click);
            // 
            // btnBackListESEK
            // 
            this.btnBackListESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBackListESEK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackListESEK.Location = new System.Drawing.Point(487, 467);
            this.btnBackListESEK.Name = "btnBackListESEK";
            this.btnBackListESEK.Size = new System.Drawing.Size(132, 34);
            this.btnBackListESEK.TabIndex = 5;
            this.btnBackListESEK.Text = "Назад";
            this.btnBackListESEK.UseVisualStyleBackColor = true;
            this.btnBackListESEK.Click += new System.EventHandler(this.btnBackListESEK_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(3, 464);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(132, 40);
            this.label19.TabIndex = 26;
            this.label19.Text = "Шаг 3";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(3, 60);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(132, 40);
            this.label21.TabIndex = 27;
            this.label21.Text = "Номер карты";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(3, 100);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(132, 40);
            this.label22.TabIndex = 28;
            this.label22.Text = "Состояние";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardNum
            // 
            this.tbxCardNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.tbxCardNum, 2);
            this.tbxCardNum.Enabled = false;
            this.tbxCardNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxCardNum.Location = new System.Drawing.Point(141, 67);
            this.tbxCardNum.Name = "tbxCardNum";
            this.tbxCardNum.Size = new System.Drawing.Size(340, 26);
            this.tbxCardNum.TabIndex = 29;
            // 
            // tbxCardState
            // 
            this.tbxCardState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.tbxCardState, 2);
            this.tbxCardState.Enabled = false;
            this.tbxCardState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxCardState.Location = new System.Drawing.Point(141, 107);
            this.tbxCardState.Name = "tbxCardState";
            this.tbxCardState.Size = new System.Drawing.Size(340, 26);
            this.tbxCardState.TabIndex = 30;
            // 
            // btnBlockESEK
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.btnBlockESEK, 2);
            this.btnBlockESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBlockESEK.Enabled = false;
            this.btnBlockESEK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBlockESEK.Location = new System.Drawing.Point(487, 103);
            this.btnBlockESEK.Name = "btnBlockESEK";
            this.btnBlockESEK.Size = new System.Drawing.Size(274, 34);
            this.btnBlockESEK.TabIndex = 31;
            this.btnBlockESEK.Text = "Заблокировать";
            this.btnBlockESEK.UseVisualStyleBackColor = true;
            this.btnBlockESEK.Click += new System.EventHandler(this.btnBlockESEK_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tabESEK, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.statusStrip1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 562);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.restResponceStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 542);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 20);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(123, 15);
            this.toolStripStatusLabel1.Text = "Состояние запросов:";
            // 
            // restResponceStatus
            // 
            this.restResponceStatus.Name = "restResponceStatus";
            this.restResponceStatus.Size = new System.Drawing.Size(184, 15);
            this.restResponceStatus.Text = "Активные запросы отсутствуют.";
            // 
            // WndBlockESEK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WndBlockESEK";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Блокирование ЕСЭК";
            this.Load += new System.EventHandler(this.wndESEK_Load);
            this.tabESEK.ResumeLayout(false);
            this.tabEnterSNILS.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabEditPersInfo.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tabListESEK.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabESEK;
        private System.Windows.Forms.TabPage tabEnterSNILS;
        private System.Windows.Forms.TabPage tabEditPersInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TabPage tabListESEK;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbxSPDFIO;
        private System.Windows.Forms.TextBox tbxSPDSNILS;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCancelListESEK;
        private System.Windows.Forms.Button btnBackListESEK;
        private System.Windows.Forms.Button btnNextEnterSNILS;
        private System.Windows.Forms.Button btnCancelEnterSNILS;
        private System.Windows.Forms.Button btnNextPersonalInfo;
        private System.Windows.Forms.Button btnBackPersonalInfo;
        private System.Windows.Forms.Button btnCancelPersonalInfo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Timer timer4;
        private System.Windows.Forms.Timer timer5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbxCardNum;
        private System.Windows.Forms.TextBox tbxCardState;
        private System.Windows.Forms.MaskedTextBox tbxEnterSNILS;
        private System.Windows.Forms.Label lblESEKInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel restResponceStatus;
        private System.Windows.Forms.Button btnBlockESEK;
        private System.Windows.Forms.Timer tmrBlockESEK;
    }
}