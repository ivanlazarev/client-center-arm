﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;

namespace armoperator
{
    class WkrGetPersonInfo
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultPersonDataBySNILS m_resultREST = new ResultPersonDataBySNILS();
        private GetUUIDWorkerData m_workerData = new GetUUIDWorkerData();

        public WkrGetPersonInfo()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultPersonDataBySNILS)e.Result;
        }

        private ResultPersonDataBySNILS RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultPersonDataBySNILS resultRest = new ResultPersonDataBySNILS();
            PersonDataBySNILS personDataBySNILS = new PersonDataBySNILS();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<PersonDataBySNILS>(response);
                    personDataBySNILS.exemptions = JSONobj.exemptions;
                    personDataBySNILS.id = JSONobj.id;
                    personDataBySNILS.name = JSONobj.name;
                    personDataBySNILS.snils = JSONobj.snils;
                    personDataBySNILS.updatedAt = JSONobj.updatedAt;
                    personDataBySNILS.uuid = JSONobj.uuid;
                }
                catch { }
            }
            resultRest.personDataBySNILS = personDataBySNILS;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediaData = new RestRequest(m_workerData.api + m_workerData.SNILS, Method.GET);
            requestGetMediaData.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetMediaData);
        }

        public ResultPersonDataBySNILS GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(GetUUIDWorkerData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class ResultPersonDataBySNILS
    {
        public PersonDataBySNILS personDataBySNILS { get; set; }
        public int statusCode { get; set; }
    }

    public class PersonDataBySNILS
    {
        public List<List<Exemption>> exemptions { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string snils { get; set; }
        public int updatedAt { get; set; }
        public string uuid { get; set; }
    }

    //public class Extemptions
    //{
    //    public string exemptionOuid { get; set; }
    //    public Timeframe timeframe { get; set; }
    //}

    public class GetUUIDWorkerData
    {
        public String URL { get;set; }
        public String api { get; set; }
        public String SNILS { get;set; }
        public String token { get; set; }        
    }
}
