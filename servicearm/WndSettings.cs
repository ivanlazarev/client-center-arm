﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;

namespace armoperator
{
    public partial class WndSettings : Form
    {
        WndMain __mainWindow;

        public WndSettings(WndMain mainWindow, AppSettings appSettings)
        {
            InitializeComponent();
            __mainWindow = mainWindow;
            __mainWindow.GetAppSettings(appSettings, ref tbxOperatorStation, ref tbxOperatorAddress, ref tbxStationName);
        }

        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            __mainWindow.SetAppSettings(tbxOperatorStation.Text, tbxOperatorAddress.Text, tbxStationName.Text);
            try
            {
                using (Stream writer = new FileStream(WndMain.SETTINGS_FILE_NAME, FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(AppSettings));
                    serializer.Serialize(writer, __mainWindow.GetAppSettingsData());//appsettings);
                }
                this.Close();
            }
            catch
            {
                MessageBox.Show("файл settings.xml недоступен для записи.");
            }

        }
    }
}
