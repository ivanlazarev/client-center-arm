﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;

namespace armoperator
{
    class WkrGetMediasByRFID
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultMediasDataByRFID m_resultREST = new ResultMediasDataByRFID();
        private WkrGetMediasByRFIDData m_workerData = new WkrGetMediasByRFIDData();

        public WkrGetMediasByRFID()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultMediasDataByRFID)e.Result;
        }

        private ResultMediasDataByRFID RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultMediasDataByRFID resultRest = new ResultMediasDataByRFID();
            MediasDataByRFID mediasDataByRFID = new MediasDataByRFID();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<MediasDataByRFID>(response);
                    mediasDataByRFID.accountId = JSONobj.accountId;
                    mediasDataByRFID.id = JSONobj.id;
                    mediasDataByRFID.meta = JSONobj.meta;
                    mediasDataByRFID.rfid = JSONobj.rfid;
                    mediasDataByRFID.states = JSONobj.states;
                    mediasDataByRFID.updatedAt = JSONobj.updatedAt;
                    mediasDataByRFID.validityPeriod = JSONobj.validityPeriod;
                }
                catch { }
            }
            resultRest.mediasDataByRFID = mediasDataByRFID;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediasByRFID = new RestRequest(m_workerData.api + m_workerData.rfid, Method.GET);
            requestGetMediasByRFID.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetMediasByRFID);
        }

        public ResultMediasDataByRFID GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrGetMediasByRFIDData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class State
    {
        public string state { get; set; }
        public int timestamp { get; set; }
    }

    public class ValidityPeriod
    {
        public int finishTimestamp { get; set; }
        public int startTimestamp { get; set; }
    }

    public class MediasDataByRFID
    {
        public int accountId { get; set; }
        public int id { get; set; }
        public object meta { get; set; }
        public string rfid { get; set; }
        public List<State> states { get; set; }
        public int updatedAt { get; set; }
        public ValidityPeriod validityPeriod { get; set; }
    }

    public class ResultMediasDataByRFID
    {
        public MediasDataByRFID mediasDataByRFID { get; set; }
        public int statusCode { get; set; }
    }

    public class WkrGetMediasByRFIDData
    {
        public String URL { get; set; }
        public String api { get; set; }
        public String rfid { get; set; }
        public String token { get; set; }
    }

}
