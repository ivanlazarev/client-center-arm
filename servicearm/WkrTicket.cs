﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;
using Newtonsoft.Json;

namespace armoperator
{
    class WkrTicket
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultTicket m_resultREST = new ResultTicket();
        private WkrTicketData m_workerData = new WkrTicketData();

        public WkrTicket()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultTicket)e.Result;
        }

        private ResultTicket RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultTicket resultRest = new ResultTicket();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    resultRest.ticket = deserMediaData.Deserialize<Ticket>(response);
                }
                catch { }
            }
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediaData = new RestRequest(m_workerData.api, Method.POST);
            requestGetMediaData.AddHeader("Auth-Access-Token", m_workerData.token);
            requestGetMediaData.AddJsonBody(m_workerData.ticketPayload);
            RESTWkr.RunWorkerAsync(requestGetMediaData);
        }

        public ResultTicket GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrTicketData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class ResultTicket
    {
        public Ticket ticket { get; set; }
        public int statusCode { get; set; }
    }

    public class Ticket
    {
        public int accountId { get; set; }
        public int applicationId { get; set; }
        public int bagId { get; set; }
        public int credentialId { get; set; }
        public int currencyId { get; set; }
        public int id { get; set; }
        public int mediaId { get; set; }
        public int providerId { get; set; }
        public int serviceId { get; set; }
        public Source source { get; set; }
        public string status { get; set; }
        public int timestamp { get; set; }
        public int transactionCounter { get; set; }
        public string type { get; set; }
        public int updatedAt { get; set; }
        public int value { get; set; }
    }

    public class TicketPayload
    {
        public string rfid { get; set; }
        public UInt64 timestamp { get; set; }
        public string symbol { get; set; }
        public int value { get; set; }
        public Timeframe timeframe { get; set; }

        public Source source { get; set; }
        public int providerId { get; set; }
    }

    public class WkrTicketData
    {
        public String URL { get;set; }
        public String api { get; set; }
        public TicketPayload ticketPayload { get; set; }
        public String token { get; set; }        
    }
}
