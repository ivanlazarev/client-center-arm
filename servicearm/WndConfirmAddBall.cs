﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace armoperator
{
    public partial class WndConfirmAddBall : Form
    {
        public WndConfirmAddBall()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        public void SetTextBoxes(  String person,
                                    String cardNum,
                                    String ticketType   )
        {
            tbxPerson.Text = person;
            tbxCardNum.Text = cardNum;
            tbxTicketType.Text = ticketType;
        }
    }
}
