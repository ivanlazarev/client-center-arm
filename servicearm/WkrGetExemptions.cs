﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;

namespace armoperator
{
    class WkrGetExemptions
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultGetExemptions m_resultREST = new ResultGetExemptions();
        private GetExemptionsWorkerData m_workerData = new GetExemptionsWorkerData();

        public WkrGetExemptions()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultGetExemptions)e.Result;
        }

        private ResultGetExemptions RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultGetExemptions resultRest = new ResultGetExemptions();
            List<ExemptionsData> desList = new List<ExemptionsData>();
            ExemptionsData exemptionsData = new ExemptionsData();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<List<ExemptionsData>>(response);
                    exemptionsData.description = JSONobj[0].description;
                    exemptionsData.exemptions = JSONobj[0].exemptions;
                    exemptionsData.id = JSONobj[0].id;
                    exemptionsData.meta = JSONobj[0].meta;
                    exemptionsData.source = JSONobj[0].source;
                    exemptionsData.updatedAt = JSONobj[0].updatedAt;
                }
                catch { }
            }
            desList.Add(exemptionsData);
            resultRest.exemptionsData = desList;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediaData = new RestRequest(m_workerData.api , Method.GET);
            requestGetMediaData.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetMediaData);
        }

        public ResultGetExemptions GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(GetExemptionsWorkerData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class ResultGetExemptions
    {
        public List<ExemptionsData> exemptionsData { get; set; }
        public int statusCode { get; set; }
    }

    public class ExemptionsData
    {
        public string description { get; set; }
        public Object exemptions { get; set; }
        public int id { get; set; }
        public ExemptionsMeta meta { get; set; }
        public string source { get; set; }
        public int updatedAt { get; set; }
    }

    public class Exemptions
    {
        public string region { get; set; }
        public string title { get; set; }
    }

    public class ExemptionsMeta
    {
        public string file { get; set; }
        public string version { get; set; }
    }
    
    public class GetExemptionsWorkerData
    {
        public String URL { get;set; }
        public String api { get; set; }
        public String token { get; set; }        
    }
}
