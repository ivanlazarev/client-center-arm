﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace armoperator
{
    class WkrGetBagTransactions
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultGetBagTransactions m_resultREST = new ResultGetBagTransactions();
        private WkrGetBagTransactionsData m_workerData = new WkrGetBagTransactionsData();

        public WkrGetBagTransactions()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultGetBagTransactions)e.Result;
        }

        private ResultGetBagTransactions RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultGetBagTransactions resultRest = new ResultGetBagTransactions();
            List<Transactions> bagTransactions = new List<Transactions>();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<List<String>>(response);
                    Transactions transactionsData = new Transactions();
                    foreach (var element in JSONobj)
                    {
                        transactionsData = JsonConvert.DeserializeObject<Transactions>(element);
                        bagTransactions.Add(transactionsData);
                    }
                }
                catch { MessageBox.Show("Ошибка десериализации транзакции."); }
            }
            resultRest.transactionsData = bagTransactions;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediaData = new RestRequest(m_workerData.api + m_workerData.accountId + m_workerData.bagId, Method.GET);
            requestGetMediaData.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetMediaData);
        }

        public ResultGetBagTransactions GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrGetBagTransactionsData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class ResultGetBagTransactions
    {
        public List<Transactions> transactionsData { get; set; }
        public int statusCode { get; set; }
    }

    public class Transactions
    {
        public int accountId { get; set; }
        public int applicationId { get; set; }
        public int bagId { get; set; }
        public int credentialId { get; set; }
        public int currencyId { get; set; }
        public int id { get; set; }
        public int mediaId { get; set; }
        public int providerId { get; set; }
        public int serviceId { get; set; }
        public Source source { get; set; }
        public string status { get; set; }
        public int timestamp { get; set; }
        public int transactionCounter { get; set; }
        public string type { get; set; }
        public int updatedAt { get; set; }
        public int value { get; set; }
    }

    public class Source
    {
        public string order { get; set; }
        public string terminal { get; set; }
    }

    public class WkrGetBagTransactionsData
    {
        public String URL { get;set; }
        public String api { get; set; }
        public String accountId { get; set; }
        public String bagId { get; set; }
        public String token { get; set; }        
    }
}
