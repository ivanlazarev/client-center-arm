﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;
using Newtonsoft.Json;

namespace armoperator
{
    class WkrGetCurrencies
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultGetCurrencies m_resultREST = new ResultGetCurrencies();
        private WkrGetCurrenciesData m_workerData = new WkrGetCurrenciesData();

        public WkrGetCurrencies()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultGetCurrencies)e.Result;
        }

        private ResultGetCurrencies RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultGetCurrencies resultRest = new ResultGetCurrencies();
            List<Currencies> currencies = new List<Currencies>();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<List<String>>(response);
                    Currencies currenciesData = new Currencies();
                    foreach (var element in JSONobj)
                    {
                        currenciesData = JsonConvert.DeserializeObject<Currencies>(element);
                        currencies.Add(currenciesData);
                    }
                }
                catch { }
            }
            resultRest.currenciesData = currencies;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediaData = new RestRequest(m_workerData.api + m_workerData.applicationId , Method.GET);
            requestGetMediaData.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetMediaData);
        }

        public ResultGetCurrencies GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrGetCurrenciesData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class ResultGetCurrencies
    {
        public List<Currencies> currenciesData { get; set; }
        public int statusCode { get; set; }
    }

    public class Currencies
    {
        public int applicationId { get; set; }
        public bool countable { get; set; }
        public bool @default { get; set; }
        public string description { get; set; }
        public string exemption { get; set; }
        public int id { get; set; }
        public object meta { get; set; }
        public string symbol { get; set; }
        public string title { get; set; }
        public int updatedAt { get; set; }
    }

    public class WkrGetCurrenciesData
    {
        public String URL { get;set; }
        public String api { get; set; }
        public String applicationId { get; set; }
        public String token { get; set; }        
    }
}
