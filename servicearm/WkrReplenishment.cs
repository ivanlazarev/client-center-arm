﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;
using Newtonsoft.Json;

namespace armoperator
{
    class WkrReplenishment
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultReplenishment m_resultREST = new ResultReplenishment();
        private WkrReplenishmentData m_workerData = new WkrReplenishmentData();

        public WkrReplenishment()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultReplenishment)e.Result;
        }

        private ResultReplenishment RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultReplenishment resultRest = new ResultReplenishment();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    resultRest.replenishment = deserMediaData.Deserialize<Replenishment>(response);
                }
                catch { }
            }
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediaData = new RestRequest(m_workerData.api, Method.POST);
            requestGetMediaData.AddHeader("Auth-Access-Token", m_workerData.token);
            requestGetMediaData.AddJsonBody(m_workerData.replenishmentPayload);
            RESTWkr.RunWorkerAsync(requestGetMediaData);
        }

        public ResultReplenishment GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrReplenishmentData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class ResultReplenishment
    {
        public Replenishment replenishment { get; set; }
        public int statusCode { get; set; }
    }

    public class Replenishment
    {
        public int accountId { get; set; }
        public int applicationId { get; set; }
        public int bagId { get; set; }
        public int credentialId { get; set; }
        public int currencyId { get; set; }
        public int id { get; set; }
        public int mediaId { get; set; }
        public int providerId { get; set; }
        public int serviceId { get; set; }
        public Source source { get; set; }
        public string status { get; set; }
        public int timestamp { get; set; }
        public int transactionCounter { get; set; }
        public string type { get; set; }
        public int updatedAt { get; set; }
        public int value { get; set; }
    }

    public class ReplenishmentPayload
    {
        public string symbol { get; set; }
        public string rfid { get; set; }
        public UInt64 timestamp { get; set; }
        public int value { get; set; }
        public int providerId { get; set; }
    }

    public class WkrReplenishmentData
    {
        public String URL { get;set; }
        public String api { get; set; }
        public ReplenishmentPayload replenishmentPayload { get; set; }
        public String token { get; set; }        
    }
}
