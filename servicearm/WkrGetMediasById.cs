﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;

namespace armoperator
{
    class WkrGetMediasById
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultMediasDataById m_resultREST = new ResultMediasDataById();
        private WkrGetMediasByIdData m_workerData = new WkrGetMediasByIdData();

        public WkrGetMediasById()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultMediasDataById)e.Result;
        }

        private ResultMediasDataById RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultMediasDataById resultRest = new ResultMediasDataById();
            List<MediasDataById> desList = new List<MediasDataById>();
            MediasDataById mediasDataById = new MediasDataById();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<List<MediasDataById>>(response);
                    mediasDataById.accountId = JSONobj[0].accountId;
                    mediasDataById.id = JSONobj[0].id;
                    mediasDataById.meta = JSONobj[0].meta;
                    mediasDataById.rfid = JSONobj[0].rfid;
                    mediasDataById.states = JSONobj[0].states;
                    mediasDataById.updatedAt = JSONobj[0].updatedAt;
                    mediasDataById.validityPeriod = JSONobj[0].validityPeriod;
                }
                catch { }
            }
            desList.Add(mediasDataById);
            resultRest.mediasDataById = desList;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediasById = new RestRequest(m_workerData.api + m_workerData.id, Method.GET);

            requestGetMediasById.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetMediasById);
        }

        public ResultMediasDataById GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrGetMediasByIdData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    //public class State
    //{
    //    public string state { get; set; }
    //    public int timestamp { get; set; }
    //}

    //public class ValidityPeriod
    //{
    //    public int finishTimestamp { get; set; }
    //    public int startTimestamp { get; set; }
    //}

    public class MediasDataById
    {
        public int accountId { get; set; }
        public int id { get; set; }
        public object meta { get; set; }
        public string rfid { get; set; }
        public List<State> states { get; set; }
        public int updatedAt { get; set; }
        public ValidityPeriod validityPeriod { get; set; }
    }

    public class ResultMediasDataById
    {
        public List<MediasDataById> mediasDataById { get; set; }
        public int statusCode { get; set; }
    }

    public class WkrGetMediasByIdData
    {
        public String URL { get; set; }
        public String api { get; set; }
        public String id { get; set; }
        public String token { get; set; }
    }

}
