﻿namespace armoperator
{
    partial class WndStatement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WndStatement));
            this.tabESEK = new System.Windows.Forms.TabControl();
            this.tabEnterSNILS = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNextEnterSNILS = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxEnterSNILS = new System.Windows.Forms.MaskedTextBox();
            this.lblESEKInfo = new System.Windows.Forms.Label();
            this.btnCancelEnterSNILS = new System.Windows.Forms.Button();
            this.tabEditPersInfo = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbxSPDFIO = new System.Windows.Forms.TextBox();
            this.tbxSPDSNILS = new System.Windows.Forms.TextBox();
            this.btnNextPersonalInfo = new System.Windows.Forms.Button();
            this.btnBackPersonalInfo = new System.Windows.Forms.Button();
            this.btnCancelPersonalInfo = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.btnCreatePersonInfo = new System.Windows.Forms.Button();
            this.tabListESEK = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.btnCancelListESEK = new System.Windows.Forms.Button();
            this.btnBackListESEK = new System.Windows.Forms.Button();
            this.btnNextListESEK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tbxCardNum = new System.Windows.Forms.TextBox();
            this.tbxCardState = new System.Windows.Forms.TextBox();
            this.tabDataStatement = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.imgPhoto = new System.Windows.Forms.PictureBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.cbxNation = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbxGender = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.dtpBirthDate = new System.Windows.Forms.DateTimePicker();
            this.tbxDocSupplier = new System.Windows.Forms.TextBox();
            this.dtpDocDate = new System.Windows.Forms.DateTimePicker();
            this.tbxDocReleaserCode = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tbxDocSeries = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbxDocNumber = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tbxBirthPlace = new System.Windows.Forms.TextBox();
            this.lblStatementType = new System.Windows.Forms.Label();
            this.btnCloseGenStatement = new System.Windows.Forms.Button();
            this.btnBackGenStatement = new System.Windows.Forms.Button();
            this.btnPrintPDF = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.tbxFamily = new System.Windows.Forms.TextBox();
            this.tbxDocType = new System.Windows.Forms.ComboBox();
            this.tbxSNILS = new System.Windows.Forms.Label();
            this.tbxName = new System.Windows.Forms.Label();
            this.tbxFatherName = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.timer5 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.restResponceStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabESEK.SuspendLayout();
            this.tabEnterSNILS.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabEditPersInfo.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tabListESEK.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabDataStatement.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPhoto)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabESEK
            // 
            this.tabESEK.Controls.Add(this.tabEnterSNILS);
            this.tabESEK.Controls.Add(this.tabEditPersInfo);
            this.tabESEK.Controls.Add(this.tabListESEK);
            this.tabESEK.Controls.Add(this.tabDataStatement);
            this.tabESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabESEK.Location = new System.Drawing.Point(3, 3);
            this.tabESEK.Name = "tabESEK";
            this.tabESEK.SelectedIndex = 0;
            this.tabESEK.Size = new System.Drawing.Size(878, 586);
            this.tabESEK.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabESEK.TabIndex = 0;
            this.tabESEK.TabStop = false;
            this.tabESEK.SelectedIndexChanged += new System.EventHandler(this.tabESEK_SelectedIndexChanged);
            // 
            // tabEnterSNILS
            // 
            this.tabEnterSNILS.Controls.Add(this.tableLayoutPanel3);
            this.tabEnterSNILS.Location = new System.Drawing.Point(4, 22);
            this.tabEnterSNILS.Name = "tabEnterSNILS";
            this.tabEnterSNILS.Padding = new System.Windows.Forms.Padding(3);
            this.tabEnterSNILS.Size = new System.Drawing.Size(870, 560);
            this.tabEnterSNILS.TabIndex = 0;
            this.tabEnterSNILS.Text = "Ввод СНИЛС";
            this.tabEnterSNILS.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnNextEnterSNILS, 3, 5);
            this.tableLayoutPanel3.Controls.Add(this.label16, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.label2, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.tbxEnterSNILS, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblESEKInfo, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnCancelEnterSNILS, 2, 5);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(864, 554);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label1, 4);
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(858, 60);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ввод номера СНИЛС";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNextEnterSNILS
            // 
            this.btnNextEnterSNILS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextEnterSNILS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNextEnterSNILS.Location = new System.Drawing.Point(651, 517);
            this.btnNextEnterSNILS.Name = "btnNextEnterSNILS";
            this.btnNextEnterSNILS.Size = new System.Drawing.Size(210, 34);
            this.btnNextEnterSNILS.TabIndex = 2;
            this.btnNextEnterSNILS.Text = "Далее";
            this.btnNextEnterSNILS.UseVisualStyleBackColor = true;
            this.btnNextEnterSNILS.Click += new System.EventHandler(this.btnNextEnterSNILS_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(3, 514);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(210, 40);
            this.label16.TabIndex = 9;
            this.label16.Text = "Шаг 1";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(435, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 50);
            this.label2.TabIndex = 7;
            this.label2.Text = "Номер СНИЛС:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbxEnterSNILS
            // 
            this.tbxEnterSNILS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxEnterSNILS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxEnterSNILS.Location = new System.Drawing.Point(651, 72);
            this.tbxEnterSNILS.Mask = "00000000000";
            this.tbxEnterSNILS.Name = "tbxEnterSNILS";
            this.tbxEnterSNILS.Size = new System.Drawing.Size(210, 26);
            this.tbxEnterSNILS.TabIndex = 0;
            this.tbxEnterSNILS.Click += new System.EventHandler(this.tbxEnterSNILS_Click);
            this.tbxEnterSNILS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxEnterSNILS_KeyPress);
            this.tbxEnterSNILS.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbxEnterSNILS_KeyUp);
            // 
            // lblESEKInfo
            // 
            this.lblESEKInfo.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.lblESEKInfo, 2);
            this.lblESEKInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblESEKInfo.Location = new System.Drawing.Point(3, 60);
            this.lblESEKInfo.Name = "lblESEKInfo";
            this.tableLayoutPanel3.SetRowSpan(this.lblESEKInfo, 4);
            this.lblESEKInfo.Size = new System.Drawing.Size(426, 454);
            this.lblESEKInfo.TabIndex = 10;
            this.lblESEKInfo.Text = "label4";
            // 
            // btnCancelEnterSNILS
            // 
            this.btnCancelEnterSNILS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancelEnterSNILS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelEnterSNILS.Location = new System.Drawing.Point(435, 517);
            this.btnCancelEnterSNILS.Name = "btnCancelEnterSNILS";
            this.btnCancelEnterSNILS.Size = new System.Drawing.Size(210, 34);
            this.btnCancelEnterSNILS.TabIndex = 1;
            this.btnCancelEnterSNILS.Text = "Отмена";
            this.btnCancelEnterSNILS.UseVisualStyleBackColor = true;
            this.btnCancelEnterSNILS.Click += new System.EventHandler(this.btnCancelEnterSNILS_Click);
            // 
            // tabEditPersInfo
            // 
            this.tabEditPersInfo.Controls.Add(this.tableLayoutPanel4);
            this.tabEditPersInfo.Location = new System.Drawing.Point(4, 22);
            this.tabEditPersInfo.Name = "tabEditPersInfo";
            this.tabEditPersInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabEditPersInfo.Size = new System.Drawing.Size(870, 560);
            this.tabEditPersInfo.TabIndex = 1;
            this.tabEditPersInfo.Text = "Персональные данные";
            this.tabEditPersInfo.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tbxSPDFIO, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.tbxSPDSNILS, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.btnNextPersonalInfo, 4, 10);
            this.tableLayoutPanel4.Controls.Add(this.btnBackPersonalInfo, 3, 10);
            this.tableLayoutPanel4.Controls.Add(this.btnCancelPersonalInfo, 2, 10);
            this.tableLayoutPanel4.Controls.Add(this.label17, 0, 10);
            this.tableLayoutPanel4.Controls.Add(this.btnCreatePersonInfo, 0, 3);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 11;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(864, 554);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label3, 5);
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(858, 50);
            this.label3.TabIndex = 6;
            this.label3.Text = "Учетные данные";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label7, 2);
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(3, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(338, 40);
            this.label7.TabIndex = 13;
            this.label7.Text = "СНИЛС";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label9, 2);
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(3, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(338, 40);
            this.label9.TabIndex = 15;
            this.label9.Text = "Держатель карты:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxSPDFIO
            // 
            this.tbxSPDFIO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.SetColumnSpan(this.tbxSPDFIO, 3);
            this.tbxSPDFIO.Enabled = false;
            this.tbxSPDFIO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxSPDFIO.Location = new System.Drawing.Point(347, 57);
            this.tbxSPDFIO.Name = "tbxSPDFIO";
            this.tbxSPDFIO.Size = new System.Drawing.Size(514, 26);
            this.tbxSPDFIO.TabIndex = 18;
            this.tbxSPDFIO.TabStop = false;
            // 
            // tbxSPDSNILS
            // 
            this.tbxSPDSNILS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.SetColumnSpan(this.tbxSPDSNILS, 3);
            this.tbxSPDSNILS.Enabled = false;
            this.tbxSPDSNILS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxSPDSNILS.Location = new System.Drawing.Point(347, 97);
            this.tbxSPDSNILS.Name = "tbxSPDSNILS";
            this.tbxSPDSNILS.Size = new System.Drawing.Size(514, 26);
            this.tbxSPDSNILS.TabIndex = 19;
            this.tbxSPDSNILS.TabStop = false;
            // 
            // btnNextPersonalInfo
            // 
            this.btnNextPersonalInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextPersonalInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNextPersonalInfo.Location = new System.Drawing.Point(691, 517);
            this.btnNextPersonalInfo.Name = "btnNextPersonalInfo";
            this.btnNextPersonalInfo.Size = new System.Drawing.Size(170, 34);
            this.btnNextPersonalInfo.TabIndex = 0;
            this.btnNextPersonalInfo.Text = "Далее";
            this.btnNextPersonalInfo.UseVisualStyleBackColor = true;
            this.btnNextPersonalInfo.Click += new System.EventHandler(this.btnNextPersonalInfo_Click);
            // 
            // btnBackPersonalInfo
            // 
            this.btnBackPersonalInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBackPersonalInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackPersonalInfo.Location = new System.Drawing.Point(519, 517);
            this.btnBackPersonalInfo.Name = "btnBackPersonalInfo";
            this.btnBackPersonalInfo.Size = new System.Drawing.Size(166, 34);
            this.btnBackPersonalInfo.TabIndex = 2;
            this.btnBackPersonalInfo.Text = "Назад";
            this.btnBackPersonalInfo.UseVisualStyleBackColor = true;
            this.btnBackPersonalInfo.Click += new System.EventHandler(this.btnBackPersonalInfo_Click);
            // 
            // btnCancelPersonalInfo
            // 
            this.btnCancelPersonalInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancelPersonalInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelPersonalInfo.Location = new System.Drawing.Point(347, 517);
            this.btnCancelPersonalInfo.Name = "btnCancelPersonalInfo";
            this.btnCancelPersonalInfo.Size = new System.Drawing.Size(166, 34);
            this.btnCancelPersonalInfo.TabIndex = 1;
            this.btnCancelPersonalInfo.Text = "Отмена";
            this.btnCancelPersonalInfo.UseVisualStyleBackColor = true;
            this.btnCancelPersonalInfo.Click += new System.EventHandler(this.btnCancelPersonalInfo_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(3, 514);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(166, 40);
            this.label17.TabIndex = 25;
            this.label17.Text = "Шаг 2";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCreatePersonInfo
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.btnCreatePersonInfo, 2);
            this.btnCreatePersonInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCreatePersonInfo.Enabled = false;
            this.btnCreatePersonInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreatePersonInfo.Location = new System.Drawing.Point(3, 133);
            this.btnCreatePersonInfo.Name = "btnCreatePersonInfo";
            this.btnCreatePersonInfo.Size = new System.Drawing.Size(338, 44);
            this.btnCreatePersonInfo.TabIndex = 0;
            this.btnCreatePersonInfo.TabStop = false;
            this.btnCreatePersonInfo.Text = "Зарегестрировать/изменить";
            this.btnCreatePersonInfo.UseVisualStyleBackColor = true;
            this.btnCreatePersonInfo.Visible = false;
            this.btnCreatePersonInfo.Click += new System.EventHandler(this.btnCreatePersonInfo_Click);
            // 
            // tabListESEK
            // 
            this.tabListESEK.Controls.Add(this.tableLayoutPanel2);
            this.tabListESEK.Location = new System.Drawing.Point(4, 22);
            this.tabListESEK.Name = "tabListESEK";
            this.tabListESEK.Padding = new System.Windows.Forms.Padding(3);
            this.tabListESEK.Size = new System.Drawing.Size(870, 560);
            this.tabListESEK.TabIndex = 2;
            this.tabListESEK.Text = "Перечень ЕСЭК";
            this.tabListESEK.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.18182F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.27273F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.18182F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.18182F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.18182F));
            this.tableLayoutPanel2.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCancelListESEK, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.btnBackListESEK, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.btnNextListESEK, 4, 5);
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.label19, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label21, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label22, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbxCardNum, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbxCardState, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(864, 554);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label11, 5);
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(858, 60);
            this.label11.TabIndex = 1;
            this.label11.Text = "Карта ЕСЭК";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCancelListESEK
            // 
            this.btnCancelListESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancelListESEK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelListESEK.Location = new System.Drawing.Point(395, 517);
            this.btnCancelListESEK.Name = "btnCancelListESEK";
            this.btnCancelListESEK.Size = new System.Drawing.Size(151, 34);
            this.btnCancelListESEK.TabIndex = 4;
            this.btnCancelListESEK.Text = "Отменить";
            this.btnCancelListESEK.UseVisualStyleBackColor = true;
            this.btnCancelListESEK.Click += new System.EventHandler(this.btnCancelListESEK_Click);
            // 
            // btnBackListESEK
            // 
            this.btnBackListESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBackListESEK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackListESEK.Location = new System.Drawing.Point(552, 517);
            this.btnBackListESEK.Name = "btnBackListESEK";
            this.btnBackListESEK.Size = new System.Drawing.Size(151, 34);
            this.btnBackListESEK.TabIndex = 5;
            this.btnBackListESEK.Text = "Назад";
            this.btnBackListESEK.UseVisualStyleBackColor = true;
            this.btnBackListESEK.Click += new System.EventHandler(this.btnBackListESEK_Click);
            // 
            // btnNextListESEK
            // 
            this.btnNextListESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextListESEK.Enabled = false;
            this.btnNextListESEK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNextListESEK.Location = new System.Drawing.Point(709, 517);
            this.btnNextListESEK.Name = "btnNextListESEK";
            this.btnNextListESEK.Size = new System.Drawing.Size(152, 34);
            this.btnNextListESEK.TabIndex = 6;
            this.btnNextListESEK.Text = "Далее";
            this.btnNextListESEK.UseVisualStyleBackColor = true;
            this.btnNextListESEK.Click += new System.EventHandler(this.btnNextListESEK_Click);
            // 
            // groupBox1
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.groupBox1, 2);
            this.groupBox1.Controls.Add(this.radioButton5);
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(552, 63);
            this.groupBox1.Name = "groupBox1";
            this.tableLayoutPanel2.SetRowSpan(this.groupBox1, 3);
            this.groupBox1.Size = new System.Drawing.Size(309, 139);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выберите действие";
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(7, 113);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(218, 21);
            this.radioButton5.TabIndex = 4;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Заявление на отказ от ЕСЭК";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(7, 89);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(261, 21);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Заявление на разблокировку ЕСЭК";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(6, 65);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(238, 21);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Заявление на блокировку ЕСЭК";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 42);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(209, 21);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Заявление на замену ЕСЭК";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(208, 21);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Заявление на выпуск ЕСЭК";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(3, 514);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(151, 40);
            this.label19.TabIndex = 26;
            this.label19.Text = "Шаг 3";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(3, 60);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(151, 40);
            this.label21.TabIndex = 27;
            this.label21.Text = "Номер карты";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(3, 100);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(151, 40);
            this.label22.TabIndex = 28;
            this.label22.Text = "Состояние";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardNum
            // 
            this.tbxCardNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.tbxCardNum, 2);
            this.tbxCardNum.Enabled = false;
            this.tbxCardNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxCardNum.Location = new System.Drawing.Point(160, 67);
            this.tbxCardNum.Name = "tbxCardNum";
            this.tbxCardNum.Size = new System.Drawing.Size(386, 26);
            this.tbxCardNum.TabIndex = 29;
            // 
            // tbxCardState
            // 
            this.tbxCardState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.tbxCardState, 2);
            this.tbxCardState.Enabled = false;
            this.tbxCardState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxCardState.Location = new System.Drawing.Point(160, 107);
            this.tbxCardState.Name = "tbxCardState";
            this.tbxCardState.Size = new System.Drawing.Size(386, 26);
            this.tbxCardState.TabIndex = 30;
            // 
            // tabDataStatement
            // 
            this.tabDataStatement.Controls.Add(this.tableLayoutPanel5);
            this.tabDataStatement.Location = new System.Drawing.Point(4, 22);
            this.tabDataStatement.Name = "tabDataStatement";
            this.tabDataStatement.Size = new System.Drawing.Size(870, 560);
            this.tabDataStatement.TabIndex = 7;
            this.tabDataStatement.Text = "Ввод данных";
            this.tabDataStatement.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.Controls.Add(this.imgPhoto, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.label31, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this.label30, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.label29, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.label28, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.label32, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.cbxNation, 1, 5);
            this.tableLayoutPanel5.Controls.Add(this.label13, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this.cbxGender, 1, 6);
            this.tableLayoutPanel5.Controls.Add(this.label14, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this.label23, 0, 9);
            this.tableLayoutPanel5.Controls.Add(this.label25, 0, 10);
            this.tableLayoutPanel5.Controls.Add(this.label33, 0, 13);
            this.tableLayoutPanel5.Controls.Add(this.label34, 2, 14);
            this.tableLayoutPanel5.Controls.Add(this.label35, 0, 14);
            this.tableLayoutPanel5.Controls.Add(this.dtpBirthDate, 1, 7);
            this.tableLayoutPanel5.Controls.Add(this.tbxDocSupplier, 1, 13);
            this.tableLayoutPanel5.Controls.Add(this.dtpDocDate, 1, 14);
            this.tableLayoutPanel5.Controls.Add(this.tbxDocReleaserCode, 3, 14);
            this.tableLayoutPanel5.Controls.Add(this.label18, 0, 8);
            this.tableLayoutPanel5.Controls.Add(this.label26, 0, 11);
            this.tableLayoutPanel5.Controls.Add(this.tbxDocSeries, 1, 11);
            this.tableLayoutPanel5.Controls.Add(this.label27, 0, 12);
            this.tableLayoutPanel5.Controls.Add(this.tbxDocNumber, 1, 12);
            this.tableLayoutPanel5.Controls.Add(this.label39, 0, 15);
            this.tableLayoutPanel5.Controls.Add(this.tbxBirthPlace, 1, 8);
            this.tableLayoutPanel5.Controls.Add(this.lblStatementType, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.btnCloseGenStatement, 3, 17);
            this.tableLayoutPanel5.Controls.Add(this.btnBackGenStatement, 2, 17);
            this.tableLayoutPanel5.Controls.Add(this.btnPrintPDF, 1, 17);
            this.tableLayoutPanel5.Controls.Add(this.label20, 0, 17);
            this.tableLayoutPanel5.Controls.Add(this.tbxFamily, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.tbxDocType, 1, 10);
            this.tableLayoutPanel5.Controls.Add(this.tbxSNILS, 1, 15);
            this.tableLayoutPanel5.Controls.Add(this.tbxName, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.tbxFatherName, 1, 4);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 18;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(870, 560);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // imgPhoto
            // 
            this.imgPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel5.SetColumnSpan(this.imgPhoto, 2);
            this.imgPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgPhoto.Location = new System.Drawing.Point(437, 53);
            this.imgPhoto.Name = "imgPhoto";
            this.tableLayoutPanel5.SetRowSpan(this.imgPhoto, 12);
            this.imgPhoto.Size = new System.Drawing.Size(430, 354);
            this.imgPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgPhoto.TabIndex = 4;
            this.imgPhoto.TabStop = false;
            this.imgPhoto.Click += new System.EventHandler(this.imgPhoto_Click);
            this.imgPhoto.Paint += new System.Windows.Forms.PaintEventHandler(this.imgPhoto_Paint);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Location = new System.Drawing.Point(3, 170);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(211, 30);
            this.label31.TabIndex = 11;
            this.label31.Text = "Гражданство РФ (ДА/НЕТ):";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Location = new System.Drawing.Point(3, 140);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(211, 30);
            this.label30.TabIndex = 10;
            this.label30.Text = "Отчество:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Location = new System.Drawing.Point(3, 110);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(211, 30);
            this.label29.TabIndex = 9;
            this.label29.Text = "Имя:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Location = new System.Drawing.Point(3, 80);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(211, 30);
            this.label28.TabIndex = 8;
            this.label28.Text = "Фамилия:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel5.SetColumnSpan(this.label32, 2);
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(3, 50);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(428, 30);
            this.label32.TabIndex = 12;
            this.label32.Text = "Личные данные:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxNation
            // 
            this.cbxNation.AllowDrop = true;
            this.cbxNation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxNation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxNation.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbxNation.FormattingEnabled = true;
            this.cbxNation.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbxNation.Items.AddRange(new object[] {
            "ДА",
            "НЕТ"});
            this.cbxNation.Location = new System.Drawing.Point(220, 174);
            this.cbxNation.Name = "cbxNation";
            this.cbxNation.Size = new System.Drawing.Size(211, 21);
            this.cbxNation.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Location = new System.Drawing.Point(3, 200);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(211, 30);
            this.label13.TabIndex = 21;
            this.label13.Text = "Пол:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxGender
            // 
            this.cbxGender.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxGender.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbxGender.FormattingEnabled = true;
            this.cbxGender.Items.AddRange(new object[] {
            "М",
            "Ж"});
            this.cbxGender.Location = new System.Drawing.Point(220, 204);
            this.cbxGender.Name = "cbxGender";
            this.cbxGender.Size = new System.Drawing.Size(211, 21);
            this.cbxGender.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Location = new System.Drawing.Point(3, 230);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(211, 30);
            this.label14.TabIndex = 23;
            this.label14.Text = "Дата рождения:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel5.SetColumnSpan(this.label23, 2);
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(3, 290);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(428, 30);
            this.label23.TabIndex = 25;
            this.label23.Text = "Документ, удостоверяющий личность:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Location = new System.Drawing.Point(3, 320);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(211, 30);
            this.label25.TabIndex = 26;
            this.label25.Text = "Вид документа:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Location = new System.Drawing.Point(3, 410);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(211, 30);
            this.label33.TabIndex = 29;
            this.label33.Text = "Кем выдан:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Location = new System.Drawing.Point(437, 440);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(211, 30);
            this.label34.TabIndex = 30;
            this.label34.Text = "Код подразделения:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label35.Location = new System.Drawing.Point(3, 440);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(211, 30);
            this.label35.TabIndex = 31;
            this.label35.Text = "Когда выдан:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtpBirthDate
            // 
            this.dtpBirthDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpBirthDate.Location = new System.Drawing.Point(220, 235);
            this.dtpBirthDate.Name = "dtpBirthDate";
            this.dtpBirthDate.Size = new System.Drawing.Size(211, 20);
            this.dtpBirthDate.TabIndex = 3;
            this.dtpBirthDate.Value = new System.DateTime(2016, 3, 3, 0, 0, 0, 0);
            // 
            // tbxDocSupplier
            // 
            this.tbxDocSupplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.SetColumnSpan(this.tbxDocSupplier, 3);
            this.tbxDocSupplier.Location = new System.Drawing.Point(220, 415);
            this.tbxDocSupplier.Name = "tbxDocSupplier";
            this.tbxDocSupplier.Size = new System.Drawing.Size(647, 20);
            this.tbxDocSupplier.TabIndex = 8;
            this.tbxDocSupplier.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxDocSupplier_KeyPress);
            // 
            // dtpDocDate
            // 
            this.dtpDocDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpDocDate.Location = new System.Drawing.Point(220, 445);
            this.dtpDocDate.Name = "dtpDocDate";
            this.dtpDocDate.Size = new System.Drawing.Size(211, 20);
            this.dtpDocDate.TabIndex = 9;
            // 
            // tbxDocReleaserCode
            // 
            this.tbxDocReleaserCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxDocReleaserCode.Location = new System.Drawing.Point(654, 445);
            this.tbxDocReleaserCode.Name = "tbxDocReleaserCode";
            this.tbxDocReleaserCode.Size = new System.Drawing.Size(213, 20);
            this.tbxDocReleaserCode.TabIndex = 10;
            this.tbxDocReleaserCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxDocReleaserCode_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Location = new System.Drawing.Point(3, 260);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(211, 30);
            this.label18.TabIndex = 24;
            this.label18.Text = "Место рождения:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Location = new System.Drawing.Point(3, 350);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(211, 30);
            this.label26.TabIndex = 27;
            this.label26.Text = "Серия:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxDocSeries
            // 
            this.tbxDocSeries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxDocSeries.Location = new System.Drawing.Point(220, 355);
            this.tbxDocSeries.Name = "tbxDocSeries";
            this.tbxDocSeries.Size = new System.Drawing.Size(211, 20);
            this.tbxDocSeries.TabIndex = 6;
            this.tbxDocSeries.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxDocSeries_KeyPress);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Location = new System.Drawing.Point(3, 380);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(211, 30);
            this.label27.TabIndex = 28;
            this.label27.Text = "Номер:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxDocNumber
            // 
            this.tbxDocNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxDocNumber.Location = new System.Drawing.Point(220, 385);
            this.tbxDocNumber.Name = "tbxDocNumber";
            this.tbxDocNumber.Size = new System.Drawing.Size(211, 20);
            this.tbxDocNumber.TabIndex = 7;
            this.tbxDocNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxDocNumber_KeyPress);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label39.Location = new System.Drawing.Point(3, 470);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(211, 30);
            this.label39.TabIndex = 45;
            this.label39.Text = "Номер CНИЛС:";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxBirthPlace
            // 
            this.tbxBirthPlace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxBirthPlace.Location = new System.Drawing.Point(220, 265);
            this.tbxBirthPlace.Name = "tbxBirthPlace";
            this.tbxBirthPlace.Size = new System.Drawing.Size(211, 20);
            this.tbxBirthPlace.TabIndex = 4;
            this.tbxBirthPlace.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxBirthPlace_KeyPress);
            // 
            // lblStatementType
            // 
            this.lblStatementType.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.lblStatementType, 4);
            this.lblStatementType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatementType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStatementType.Location = new System.Drawing.Point(3, 0);
            this.lblStatementType.Name = "lblStatementType";
            this.lblStatementType.Size = new System.Drawing.Size(864, 50);
            this.lblStatementType.TabIndex = 60;
            this.lblStatementType.Text = "Ввод данных для оформления заявления";
            this.lblStatementType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCloseGenStatement
            // 
            this.btnCloseGenStatement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCloseGenStatement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseGenStatement.Location = new System.Drawing.Point(654, 518);
            this.btnCloseGenStatement.Name = "btnCloseGenStatement";
            this.btnCloseGenStatement.Size = new System.Drawing.Size(213, 39);
            this.btnCloseGenStatement.TabIndex = 13;
            this.btnCloseGenStatement.Text = "Закрыть";
            this.btnCloseGenStatement.UseVisualStyleBackColor = true;
            this.btnCloseGenStatement.Click += new System.EventHandler(this.btnCloseGenStatement_Click);
            // 
            // btnBackGenStatement
            // 
            this.btnBackGenStatement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBackGenStatement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackGenStatement.Location = new System.Drawing.Point(437, 518);
            this.btnBackGenStatement.Name = "btnBackGenStatement";
            this.btnBackGenStatement.Size = new System.Drawing.Size(211, 39);
            this.btnBackGenStatement.TabIndex = 12;
            this.btnBackGenStatement.Text = "Назад";
            this.btnBackGenStatement.UseVisualStyleBackColor = true;
            this.btnBackGenStatement.Click += new System.EventHandler(this.btnBackGenStatement_Click);
            // 
            // btnPrintPDF
            // 
            this.btnPrintPDF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrintPDF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintPDF.Image = global::armoperator.Properties.Resources.paper6;
            this.btnPrintPDF.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrintPDF.Location = new System.Drawing.Point(220, 518);
            this.btnPrintPDF.Name = "btnPrintPDF";
            this.btnPrintPDF.Size = new System.Drawing.Size(211, 39);
            this.btnPrintPDF.TabIndex = 11;
            this.btnPrintPDF.Text = "Печать";
            this.btnPrintPDF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrintPDF.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrintPDF.UseVisualStyleBackColor = true;
            this.btnPrintPDF.Click += new System.EventHandler(this.btnPrintPDF_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(3, 515);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(211, 45);
            this.label20.TabIndex = 63;
            this.label20.Text = "Шаг 4";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxFamily
            // 
            this.tbxFamily.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxFamily.Location = new System.Drawing.Point(220, 85);
            this.tbxFamily.Name = "tbxFamily";
            this.tbxFamily.Size = new System.Drawing.Size(211, 20);
            this.tbxFamily.TabIndex = 0;
            this.tbxFamily.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // tbxDocType
            // 
            this.tbxDocType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxDocType.FormattingEnabled = true;
            this.tbxDocType.Items.AddRange(new object[] {
            "паспорт"});
            this.tbxDocType.Location = new System.Drawing.Point(220, 324);
            this.tbxDocType.Name = "tbxDocType";
            this.tbxDocType.Size = new System.Drawing.Size(211, 21);
            this.tbxDocType.TabIndex = 5;
            // 
            // tbxSNILS
            // 
            this.tbxSNILS.AutoSize = true;
            this.tbxSNILS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbxSNILS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxSNILS.Location = new System.Drawing.Point(220, 470);
            this.tbxSNILS.Name = "tbxSNILS";
            this.tbxSNILS.Size = new System.Drawing.Size(211, 30);
            this.tbxSNILS.TabIndex = 67;
            this.tbxSNILS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxName
            // 
            this.tbxName.AutoSize = true;
            this.tbxName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbxName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxName.Location = new System.Drawing.Point(220, 110);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(211, 30);
            this.tbxName.TabIndex = 68;
            this.tbxName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxFatherName
            // 
            this.tbxFatherName.AutoSize = true;
            this.tbxFatherName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbxFatherName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxFatherName.Location = new System.Drawing.Point(220, 140);
            this.tbxFatherName.Name = "tbxFatherName";
            this.tbxFatherName.Size = new System.Drawing.Size(211, 30);
            this.tbxFatherName.TabIndex = 69;
            this.tbxFatherName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tabESEK, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.statusStrip1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(884, 612);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.restResponceStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 592);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(884, 20);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(123, 15);
            this.toolStripStatusLabel1.Text = "Состояние запросов:";
            // 
            // restResponceStatus
            // 
            this.restResponceStatus.Name = "restResponceStatus";
            this.restResponceStatus.Size = new System.Drawing.Size(184, 15);
            this.restResponceStatus.Text = "Активные запросы отсутствуют.";
            // 
            // WndStatement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 612);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WndStatement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Прием заявки на получение ЕСЭК";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WndStatement_FormClosing);
            this.Load += new System.EventHandler(this.wndESEK_Load);
            this.tabESEK.ResumeLayout(false);
            this.tabEnterSNILS.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabEditPersInfo.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tabListESEK.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabDataStatement.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPhoto)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabESEK;
        private System.Windows.Forms.TabPage tabEnterSNILS;
        private System.Windows.Forms.TabPage tabEditPersInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TabPage tabListESEK;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbxSPDFIO;
        private System.Windows.Forms.TextBox tbxSPDSNILS;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCancelListESEK;
        private System.Windows.Forms.Button btnBackListESEK;
        private System.Windows.Forms.Button btnNextListESEK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TabPage tabDataStatement;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.PictureBox imgPhoto;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox cbxNation;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbxGender;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DateTimePicker dtpBirthDate;
        private System.Windows.Forms.TextBox tbxDocSupplier;
        private System.Windows.Forms.DateTimePicker dtpDocDate;
        private System.Windows.Forms.TextBox tbxDocReleaserCode;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbxDocSeries;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbxDocNumber;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbxBirthPlace;
        private System.Windows.Forms.Button btnCloseGenStatement;
        private System.Windows.Forms.Button btnBackGenStatement;
        private System.Windows.Forms.Label lblStatementType;
        private System.Windows.Forms.Button btnPrintPDF;
        private System.Windows.Forms.Button btnNextEnterSNILS;
        private System.Windows.Forms.Button btnCancelEnterSNILS;
        private System.Windows.Forms.Button btnNextPersonalInfo;
        private System.Windows.Forms.Button btnBackPersonalInfo;
        private System.Windows.Forms.Button btnCancelPersonalInfo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Button btnCreatePersonInfo;
        private System.Windows.Forms.Timer timer4;
        private System.Windows.Forms.Timer timer5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbxCardNum;
        private System.Windows.Forms.TextBox tbxCardState;
        private System.Windows.Forms.TextBox tbxFamily;
        private System.Windows.Forms.MaskedTextBox tbxEnterSNILS;
        private System.Windows.Forms.Label lblESEKInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ComboBox tbxDocType;
        private System.Windows.Forms.ToolStripStatusLabel restResponceStatus;
        private System.Windows.Forms.Label tbxSNILS;
        private System.Windows.Forms.Label tbxName;
        private System.Windows.Forms.Label tbxFatherName;
        private System.Windows.Forms.RadioButton radioButton5;
    }
}