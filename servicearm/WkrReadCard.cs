﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using rfidapp;

namespace armoperator
{
    class WkrReadCard
    {
        private const int READER_TIMEOUT = 100;
        private const uint READER_COUNT = 1;

        private BackgroundWorker __wkrReadCard = new BackgroundWorker();
        private WkrReadOperationResults __readResult = new WkrReadOperationResults();

        private static IntPtr __hContext;
        private static String __readerName = "";
        private static HiDWinscard.SCARD_READERSTATE __readerState;
        private static String __readerList;
        private static IntPtr __hCard;
        private static IntPtr __Protocol;

        public WkrReadCard(String readerName)
        {
            __readerName = readerName;
            __wkrReadCard.WorkerReportsProgress = true;
            __wkrReadCard.WorkerSupportsCancellation = true;
            __wkrReadCard.DoWork += new System.ComponentModel.DoWorkEventHandler(this.wkrReadCard_DoWork);
            __wkrReadCard.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.wkrReadCard_RunWorkerCompleted);
        }

        private void wkrReadCard_DoWork(object sender, DoWorkEventArgs e)
        {
            if (__wkrReadCard.CancellationPending)
            {
                e.Cancel = true;
            }
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = ReadOperation(worker, e);
        }

        private void wkrReadCard_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            __readResult = (WkrReadOperationResults)e.Result;
        }

        private WkrReadOperationResults ReadOperation(BackgroundWorker worker, DoWorkEventArgs e)
        {
            WkrReadOperationResults wkrReadOperationResults = new WkrReadOperationResults();
            ThrustCardInfo thrustCardInfo = new ThrustCardInfo();
            int sCardEstablishContextRes = NFCFuncLib.sCardEstablishContext(ref __hContext, __readerName);
            if (sCardEstablishContextRes.Equals(0))
            {
                __readerState.RdrName = __readerName;
                __readerState.RdrCurrState = HiDWinscard.SCARD_STATE_UNAWARE;
                __readerState.RdrEventState = 0;
                __readerState.UserData = "Mifare Card";
                int getReaderDataRes = NFCFuncLib.getReaderData(ref __hContext, READER_TIMEOUT, READER_COUNT, ref __readerState, __readerList);
                switch (getReaderDataRes)
                {
                    case 0:
                        int connectRes = NFCFuncLib.Connect(ref __hContext, __readerName, ref __hCard, ref __Protocol);
                        if (connectRes.Equals(0))
                        {
                            int readThrustCardRes = readThrustCard(__hCard, out thrustCardInfo);
                            if (readThrustCardRes.Equals(0))
                            {
                                wkrReadOperationResults.thrustCardInfo = thrustCardInfo;
                                wkrReadOperationResults.readStatus = 0;//"Карта успешно считана";
                            }
                            else
                            {
                                wkrReadOperationResults.readStatus = 1; //"Ошибка формата карты";
                            }
                        }
                        else
                        {
                            wkrReadOperationResults.readStatus = 2; //"Ошибка подключения к карте.";
                        }
                        break;
                    case 1:
                        NFCFuncLib.Disconnect(ref __hCard);
                        wkrReadOperationResults.readStatus = 3; //"Положите карту на считыватель.";
                        break;
                    case 2:
                        wkrReadOperationResults.readStatus = 4; //"Считыватель не подключен.";
                        break;
                    case 3:
                        wkrReadOperationResults.readStatus = 5; //"Модель считывателя не определена.";
                        break;
                    default:
                        wkrReadOperationResults.readStatus = 6; //Неизвестная ошибка
                        break;
                }
                NFCFuncLib.SCardReleaseContext(ref __hContext);
            }
            else
            {
                wkrReadOperationResults.readStatus = 7; // "Ошибка подключения к считывателю"
            }
            return wkrReadOperationResults;
        }

        public void PerformReadOperation()
        {
            __wkrReadCard.RunWorkerAsync();
        }

        public WkrReadOperationResults GetResultREST()
        {
            return __readResult;
        }

        public bool GetWkrStatus()
        {
            if (__wkrReadCard.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private int readThrustCard(IntPtr _hCard, out ThrustCardInfo _thrustCardInfo)
        {
            int retryVal = 5;
            String uid = "";
            _thrustCardInfo = new ThrustCardInfo();

            int trustres = ThrustFuncLib.GetThrustUIDCycle(ref _hCard, ref uid, retryVal);
            if (!trustres.Equals(0))
            {
                return 1;
            }

            uid = uid.ToUpper();

            int selectMFRes = ThrustFuncLib.SelectMFCycle(ref _hCard, retryVal);
            if (!selectMFRes.Equals(0))
            {
                return 2;
            }

            int selectADFRes = ThrustFuncLib.SelectADFCycle(ref _hCard, CipurseFuncLib.APP_AID, retryVal);
            if (!selectADFRes.Equals(0))
            {
                return 3;
            }

            // Select File #1
            int selectEf1Res = ThrustFuncLib.SelectEFCycle(ref _hCard, EFConstants.File1Id, 5);
            if (!selectEf1Res.Equals(0))
            {
                return 4;
            }

            Byte[] file1_35_BinaryData = new Byte[34];
            int readBinaryEF1Res = ThrustFuncLib.ReadBinaryEFCycle(ref _hCard, ref file1_35_BinaryData, 5);
            if (!readBinaryEF1Res.Equals(0))
            {
                return 5;
            }

            ThrustCardInfoPart1 thrustCardInfoPart1 = new ThrustCardInfoPart1();
            int processFile1res = ThrustCardOps.ProcessFile1Card35Data(file1_35_BinaryData, ref thrustCardInfoPart1);
            if (!processFile1res.Equals(0))
            {
                return 6;
            }

            // Select File #2
            int selectEf2Res = ThrustFuncLib.SelectEFCycle(ref _hCard, EFConstants.File2Id, 5);
            if (!selectEf2Res.Equals(0))
            {
                return 7;
            }

            Byte[] file2_35_BinaryData = new Byte[52];
            int readBinaryEF2Res = ThrustFuncLib.ReadBinaryEFCycle(ref _hCard, ref file2_35_BinaryData, 5);
            if (!readBinaryEF2Res.Equals(0))
            {
                return 8;
            }

            ThrustCardInfoPart2 thrustCardInfoPart2 = new ThrustCardInfoPart2();
            int processFile2res = ThrustCardOps.ProcessFile2Card35Data(file2_35_BinaryData, ref thrustCardInfoPart2);
            if (!processFile2res.Equals(0))
            {
                return 9;
            }

            _thrustCardInfo.thrustCardInfoPart1 = thrustCardInfoPart1;
            _thrustCardInfo.thrustCardInfoPart2 = thrustCardInfoPart2;
            _thrustCardInfo.uid = uid;
            return 0;
        }
    }

    class WkrReadOperationResults
    {
        public ThrustCardInfo thrustCardInfo { get; set; }
        public int readStatus { get; set; }
        public WkrReadOperationResults()
        {
            thrustCardInfo = new ThrustCardInfo();
        }
    }
}
