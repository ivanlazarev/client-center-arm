﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using System.IO;

namespace armoperator
{
    public partial class WndBlockESEK : Form
    {
        public const short SNILS_LENGTH = 11;
        public const String CREATE_USER_STR = "Зарегестрировать пользователя";
        public const String CHANGE_USER_STR = "Изменить данные пользователя";
        public const String MODE_CREATE_STR = "create";
        public const String MODE_CHANGE_STR = "change";
        public const String NO_SPD_USER_STR = "Данные держателя карты отсутствуют.";
        public const String NO_SPD_SNILS_STR = "Данные номера СНИЛС отсутствуют.";
        public const String CARD_REGISTERED_STR = "registered";
        public const String CARD_ACTIVATED_STR = "activated";
        public const String CARD_BLOCKED_STR = "blocked";
        public const String PDF_FILE_NAME = "esekDoc.pdf";
        public const String IMG_FILE_NAME = "photoPic.jpg";

        WndMain __mainWindow;

        static WkrGetPersonInfo __wkrGetPersonInfo;
        static WkrUpdatePersonalInfo __wkrUpdatePersonalInfo;
        static WkrCreatePersonalInfo __wkrCreatePersonalInfo;
        static WkrGetMediasById __wkrGetMediasById;
        static WkrGetAccByUUID __wkrGetAccByUUID;
        static ResultPersonDataBySNILS __resultPersonDataBySNILS;

        private static WkrBlockESEK __wkrBlockESEK;
        private static ResultBlockESEK __resultBlockESEK;

        static ResultAccountDataByUUID __resultAccountDataByUUID;
        static ResultMediasDataById __resultMediasDataById;
        static AppSettings __appSettings;

        public WndBlockESEK(WndMain mainWindow)
        {            
            InitializeComponent();
            __mainWindow = mainWindow;            
        }

        private void btnCancelPersonalInfo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBackPersonalInfo_Click(object sender, EventArgs e)
        {
            this.tabESEK.SelectedIndex = 0;
        }

        private void btnNextPersonalInfo_Click(object sender, EventArgs e)
        {

            WkrGetAccByUUIDData wkrGetAccByUUIDData = new WkrGetAccByUUIDData();
            wkrGetAccByUUIDData.URL = WndMain.BASE_URL;
            wkrGetAccByUUIDData.api = @"/api/cp/v1/accounts?filter={""userId"":""";
            wkrGetAccByUUIDData.uuid = __resultPersonDataBySNILS.personDataBySNILS.uuid + "\"}";
            wkrGetAccByUUIDData.token = __mainWindow.GetAuthToken();
            __wkrGetAccByUUID.SetWorkerData(wkrGetAccByUUIDData);
            tbxCardNum.Text = "";
            tbxCardState.Text = "";
            if (!__wkrGetAccByUUID.GetWkrStatus())
            {
                __wkrGetAccByUUID.PerformRestOperation();
                timer4.Start();
            }
        }

        private void wndESEK_Load(object sender, EventArgs e)
        {
            // Ввод СНИЛС
            btnNextEnterSNILS.Enabled = false;
            this.ActiveControl = tbxEnterSNILS;
            // Ввод СНИЛС
            
            tabESEK.Appearance = TabAppearance.FlatButtons;
            tabESEK.ItemSize = new Size(0, 1);
            tabESEK.SizeMode = TabSizeMode.Fixed;

            InitializeTimer1();
            InitializeTimer2();
            InitializeTimer3();
            InitializeTimer4();
            InitializeTimer5();
            InitializeTmrBlockESEK();

            __wkrGetPersonInfo = new WkrGetPersonInfo();
            __wkrUpdatePersonalInfo = new WkrUpdatePersonalInfo();
            __wkrCreatePersonalInfo = new WkrCreatePersonalInfo();
            __wkrGetMediasById = new WkrGetMediasById();
            __wkrGetAccByUUID = new WkrGetAccByUUID();
            __resultPersonDataBySNILS = new ResultPersonDataBySNILS();
            __resultAccountDataByUUID = new ResultAccountDataByUUID();

            __wkrBlockESEK = new WkrBlockESEK();
            __resultBlockESEK = new ResultBlockESEK();

            __resultMediasDataById = new ResultMediasDataById();
            __appSettings = new AppSettings();

            this.lblESEKInfo.Text = "1. Для временной блокировки ЕСЭК по телефону гражданин называет оператору ФИО," +
                                        "номер СНИЛС и кодовое слово, указанное в заявлении на выдачу;\n\r" + 
                                        "2. Специалист пункта обслуживания выполняет проверку предоставленных" + 
                                        "данных и в случае их совпадения временно блокирует ЕСЭК и разъясняет"+
                                        "ее держателю возможные дальнейшие действия.";


        }

        private void btnCancelEnterSNILS_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNextEnterSNILS_Click(object sender, EventArgs e)
        {
            if (!tbxEnterSNILS.Text.Equals(String.Empty))
            {
                tbxEnterSNILS.Enabled = false;
                btnNextEnterSNILS.Enabled = false;
                //btnCancelEnterSNILS.Enabled = false;
                enterSNILS(tbxEnterSNILS.Text);
            }
            else
            {
                MessageBox.Show("Введите номер СНИЛС");
            }
        }

        private void enterSNILS(String snilsNum)
        {
            try
            {
                GetUUIDWorkerData wkrGetPersonInfoData = new GetUUIDWorkerData();
                wkrGetPersonInfoData.api = "/api/spd/v1/persons/snils/";
                wkrGetPersonInfoData.SNILS = snilsNum;
                wkrGetPersonInfoData.URL = WndMain.BASE_URL;
                wkrGetPersonInfoData.token = __mainWindow.GetAuthToken();
                __wkrGetPersonInfo.SetWorkerData(wkrGetPersonInfoData);
                if (!__wkrGetPersonInfo.GetWkrStatus())
                {
                    __wkrGetPersonInfo.PerformRestOperation();
                    timer1.Start();
                }
            }
            catch { }
        }

        private void btnCancelListESEK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBackListESEK_Click(object sender, EventArgs e)
        {
            btnBlockESEK.Enabled = false;
            this.tabESEK.SelectedIndex = 1;
        }
        
        private void btnCancelGenStatement_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBackGenStatement_Click(object sender, EventArgs e)
        {
            this.tabESEK.SelectedIndex = 2;
        }

        private void btnCloseGenStatement_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void InitializeTimer1()
        {
            timer1.Interval = 100;
            timer1.Tick += new EventHandler(timer1_Tick);
        }

        private void InitializeTimer2()
        {
            timer2.Interval = 100;
            timer2.Tick += new EventHandler(timer2_Tick);
        }

        private void InitializeTimer3()
        {
            timer3.Interval = 100;
            timer3.Tick += new EventHandler(timer3_Tick);
        }

        private void InitializeTimer4()
        {
            timer4.Interval = 100;
            timer4.Tick += new EventHandler(timer4_Tick);
        }

        private void InitializeTimer5()
        {
            timer5.Interval = 100;
            timer5.Tick += new EventHandler(timer5_Tick);
        }

        private void timer1_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (__wkrGetPersonInfo.GetWkrStatus().Equals(false))
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                timer1.Stop();
                __resultPersonDataBySNILS = __wkrGetPersonInfo.GetResultREST();
                switch (__resultPersonDataBySNILS.statusCode)
                {
                    case 200:
                        this.tabESEK.SelectedIndex = 1;
                        try
                        {
                            tbxSPDFIO.Text = __resultPersonDataBySNILS.personDataBySNILS.name;
                            tbxSPDSNILS.Text = __resultPersonDataBySNILS.personDataBySNILS.snils;
                            btnNextPersonalInfo.Enabled = true;
                            this.ActiveControl = btnNextPersonalInfo;
                        }
                        catch { }
                        break;
                    case 404:
                        this.tabESEK.SelectedIndex = 1;
                        tbxSPDFIO.Text = NO_SPD_USER_STR;
                        tbxSPDSNILS.Text = NO_SPD_SNILS_STR;
                        btnNextPersonalInfo.Enabled = false;
                        break;
                    case 401:
                        tbxEnterSNILS.Enabled = true;
                        btnNextEnterSNILS.Enabled = true;
                        btnCancelEnterSNILS.Enabled = true;
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и повторите запрос.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и повторите запрос.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        tbxEnterSNILS.Enabled = true;
                        btnNextEnterSNILS.Enabled = true;
                        btnCancelEnterSNILS.Enabled = true;
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        tbxEnterSNILS.Enabled = true;
                        btnNextEnterSNILS.Enabled = true;
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }        
        }

        private void timer2_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (__wkrUpdatePersonalInfo.GetWkrStatus().Equals(false))
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                timer2.Stop();
                __resultPersonDataBySNILS = __wkrUpdatePersonalInfo.GetResultREST();
                switch (__resultPersonDataBySNILS.statusCode)
                {
                    case 200:
                        try
                        {
                            tbxSPDFIO.Text = __resultPersonDataBySNILS.personDataBySNILS.name;
                            tbxSPDSNILS.Text = __resultPersonDataBySNILS.personDataBySNILS.snils;
                        }
                        catch { }
                        break;
                    case 401:
                        tbxEnterSNILS.Enabled = true;
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и повторите запрос.");
                            __mainWindow.Authorize();
                        }
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void timer3_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (__wkrCreatePersonalInfo.GetWkrStatus().Equals(false))
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                timer3.Stop();
                __resultPersonDataBySNILS = __wkrCreatePersonalInfo.GetResultREST();
                switch (__resultPersonDataBySNILS.statusCode)
                {
                    case 200:
                        try
                        {
                            tbxSPDFIO.Text = __resultPersonDataBySNILS.personDataBySNILS.name;
                            tbxSPDSNILS.Text = __resultPersonDataBySNILS.personDataBySNILS.snils;
                            btnNextPersonalInfo.Enabled = true;
                        }
                        catch { }
                        break;
                    case 401:
                        tbxEnterSNILS.Enabled = true;
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и повторите запрос.");
                            __mainWindow.Authorize();
                        }
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void timer4_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (__wkrGetAccByUUID.GetWkrStatus().Equals(false))
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                timer4.Stop();
                __resultAccountDataByUUID = __wkrGetAccByUUID.GetResultREST();
                switch (__resultAccountDataByUUID.statusCode)
                {
                    case 200:
                        try
                        {
                            WkrGetMediasByIdData wkrGetMediasByIdData = new WkrGetMediasByIdData();
                            wkrGetMediasByIdData.URL = WndMain.BASE_URL;
                            wkrGetMediasByIdData.api = "/api/cp/v1/accounts/";
                            wkrGetMediasByIdData.id = __resultAccountDataByUUID.accountDataByUUID[0].id.ToString() + "/medias";
                            wkrGetMediasByIdData.token = __mainWindow.GetAuthToken();
                            __wkrGetMediasById.SetWorkerData(wkrGetMediasByIdData);
                            if (!__wkrGetMediasById.GetWkrStatus())
                            {
                                __wkrGetMediasById.PerformRestOperation();
                                timer5.Start();
                            }
                            this.tabESEK.SelectedIndex = 2;
                        }
                        catch { }
                        break;
                    case 401:
                        tbxEnterSNILS.Enabled = true;
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и повторите запрос.");
                            __mainWindow.Authorize();
                        }
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void timer5_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (__wkrGetMediasById.GetWkrStatus().Equals(false))
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                timer5.Stop();
                __resultMediasDataById = __wkrGetMediasById.GetResultREST();
                switch (__resultMediasDataById.statusCode)
                {
                    case 200:
                        tbxCardNum.Text = "";
                        tbxCardState.Text = "";
                        try
                        {
                            if (0 == __resultMediasDataById.mediasDataById[0].accountId)
                            {
                                MessageBox.Show("У данного пользователя отсутствуют карты.");
                            }
                            else
                            {
                                tbxCardNum.Text = __resultMediasDataById.mediasDataById[0].rfid;
                                switch (__resultMediasDataById.mediasDataById[0].states[0].state)
                                {
                                    case CARD_REGISTERED_STR:
                                        tbxCardState.Text = "Карта зарегестрирована.";
                                        break;
                                    case CARD_ACTIVATED_STR:
                                        tbxCardState.Text = "Карта активирована.";
                                        btnBlockESEK.Enabled = true;
                                        break;
                                    case CARD_BLOCKED_STR:
                                        tbxCardState.Text = "Карта заблокирована.";
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        tbxCardNum.Text = "Отсутствует.";
                        tbxCardState.Text = "Карта не зарегестрирована.";
                        break;
                    case 401:
                        tbxEnterSNILS.Enabled = true;
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и повторите запрос.");
                            __mainWindow.Authorize();
                        }
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }



 
        private void tbxEnterSNILS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (SNILS_LENGTH == tbxEnterSNILS.Text.Length)
                {
                    tbxEnterSNILS.Enabled = false;
                    btnNextEnterSNILS.Enabled = false;
                    enterSNILS(tbxEnterSNILS.Text);
                }
            }
        }

        private void tbxEnterSNILS_Click(object sender, EventArgs e)
        {
            ReturnToLastChar(tbxEnterSNILS);
        }

        private void tbxEnterSNILS_KeyUp(object sender, KeyEventArgs e)
        {
            if (SNILS_LENGTH == tbxEnterSNILS.Text.Length)
            {
                if (checkSnils(tbxEnterSNILS.Text))
                {
                    btnNextEnterSNILS.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Введите корректный номер СНИЛС.");
                }
            }
            else
            {
                btnNextEnterSNILS.Enabled = false;
            }
        }

        private void tbxEditFamily_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)'-');
        }

        private void tbxEditName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void tbxEditFName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)'-');
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void tbxBirthPlace_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)'-' || e.KeyChar == (char)'.' || char.IsDigit(e.KeyChar));
        }

        private void tbxDocType_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }

        private void tbxDocSeries_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(e.KeyChar == (char)Keys.Back || char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Space);        
        }

        private void tbxDocNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(e.KeyChar == (char)Keys.Back || char.IsDigit(e.KeyChar));
        }

        private void tbxDocSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)'-' || e.KeyChar == (char)'.' || char.IsDigit(e.KeyChar) || e.KeyChar == (char)',' || e.KeyChar == (char)Keys.Space);        
        }

        private void tbxDocReleaserCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(e.KeyChar == (char)Keys.Back || e.KeyChar == (char)'-' || char.IsDigit(e.KeyChar));        
        }

        private void tbxSNILS_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(e.KeyChar == (char)Keys.Back || char.IsDigit(e.KeyChar) || e.KeyChar == (char)'-' || e.KeyChar == (char)Keys.Space);
        }

        private void ReturnToLastChar(MaskedTextBox _mtbx)
        {
            if (_mtbx.Text.Equals(String.Empty))
            {
                _mtbx.Select(0, 0);
            }
            int pos = _mtbx.SelectionStart;
            if (pos > _mtbx.Text.Length)
            {
                pos = _mtbx.Text.Length;
                _mtbx.Select(pos, 0);
            }
        }

        private bool checkSnils(String snils)
        {
            StringBuilder cSnils = new StringBuilder();
            foreach (char ch in snils)
            {
                if ('0' <= ch && '9' >= ch)
                {
                    cSnils.Append(ch);                
                }
            }

            if (cSnils.Length != 11)
            {
                return false;
            }

            int sum1 = 0;
            foreach (char ch in cSnils.ToString())
            {
                sum1 += Convert.ToInt16(ch - '0');
            }

            if (0 == sum1)
            {
                return false;
            }

            int mul = 9, idx = 0, sum2 = 0;
            while (0 != mul)
            {
                sum2 += Convert.ToInt16(cSnils[idx]-'0') * mul;
                idx++;
                mul--;
            }

            int check = Convert.ToInt16(cSnils[9] - '0')*10 + Convert.ToInt16(cSnils[10] - '0');
            int sum3 = (sum2 % 101)%100;
            if (check == sum3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void tabESEK_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabESEK.SelectedIndex)
            {
                case 0:
                    this.ActiveControl = tbxEnterSNILS;
                    tbxEnterSNILS.Enabled = true;

                    if (SNILS_LENGTH == tbxEnterSNILS.Text.Length)
                    {
                        btnNextEnterSNILS.Enabled = true;
                    }
                    else
                    {
                        btnNextEnterSNILS.Enabled = false;
                    }
                    break;
                case 1:
                    break;
                case 2:
                    break;
                default:
                    break;
            }
        }

        private void WndUnblocking_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose(true);
        }

        private void btnBlockESEK_Click(object sender, EventArgs e)
        {
            WkrBlockESEKData wkrBlockESEKData = new WkrBlockESEKData();
            wkrBlockESEKData.URL = WndMain.BASE_URL;
            wkrBlockESEKData.api = "/api/cp/v1/accounts/";
            wkrBlockESEKData.accountId = __resultMediasDataById.mediasDataById[0].accountId.ToString() + "/medias/";
            wkrBlockESEKData.mediaId = __resultMediasDataById.mediasDataById[0].id.ToString() + "/states";
            wkrBlockESEKData.token = __mainWindow.GetAuthToken();
            __wkrBlockESEK.SetWorkerData(wkrBlockESEKData);

            if (!__wkrBlockESEK.GetWkrStatus())
            {
                __wkrBlockESEK.PerformRestOperation();
                tmrBlockESEK.Start();
            }
        }

        private void InitializeTmrBlockESEK()
        {
            tmrBlockESEK.Interval = 100;
            tmrBlockESEK.Tick += new EventHandler(tmrBlockESEK_Tick);
        }

        private void tmrBlockESEK_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrBlockESEK.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrBlockESEK.Stop();
                __resultBlockESEK = __wkrBlockESEK.GetResultREST();
                switch (__resultBlockESEK.statusCode)
                {
                    case 200:
                        try
                        {
                            if (0 != __resultBlockESEK.transactionsData.Count)
                            {
                                if ("blocked" == __resultBlockESEK.transactionsData[0].state)
                                {
                                    tbxCardState.Text = "Карта заблокирована";
                                    btnBlockESEK.Enabled = false;
                                    if (!__wkrGetAccByUUID.GetWkrStatus())
                                    {
                                        __wkrGetAccByUUID.PerformRestOperation();
                                        timer4.Start();
                                    }
                                }
                                else
                                {
                                    btnBlockESEK.Enabled = true;
                                    tbxCardState.Text = "Ошибка в процессиноговом центре";
                                }
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:                        
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и повторите запрос.");
                            __mainWindow.Authorize();
                        }
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }
    }
}
