﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;

namespace armoperator
{
    class WkrGetAccByUUID
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultAccountDataByUUID m_resultREST = new ResultAccountDataByUUID();
        private WkrGetAccByUUIDData m_workerData = new WkrGetAccByUUIDData();

        public WkrGetAccByUUID()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultAccountDataByUUID)e.Result;
        }

        private ResultAccountDataByUUID RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultAccountDataByUUID resultRest = new ResultAccountDataByUUID();
            List<AccountDataByUUID> desList = new List<AccountDataByUUID>();
            AccountDataByUUID accountDataByUUID = new AccountDataByUUID();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                var JSONobj = deserMediaData.Deserialize<List<AccountDataByUUID>>(response);
                try
                {
                    accountDataByUUID.id = JSONobj[0].id;
                    accountDataByUUID.meta = JSONobj[0].meta;
                    accountDataByUUID.number = JSONobj[0].number;
                    accountDataByUUID.updatedAt = JSONobj[0].updatedAt; ;
                    accountDataByUUID.userId = JSONobj[0].userId;
                }
                catch { };
            }
            desList.Add(accountDataByUUID);
            resultRest.accountDataByUUID = desList;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediaData = new RestRequest(m_workerData.api + m_workerData.uuid, Method.GET);
            
            requestGetMediaData.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetMediaData);
        }

        public ResultAccountDataByUUID GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrGetAccByUUIDData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class ResultAccountDataByUUID
    {
        public List<AccountDataByUUID> accountDataByUUID { get; set; }
        public int statusCode { get; set; }
    }

    public class AccountDataByUUID
    {
        public int id { get; set; }
        public object meta { get; set; }
        public string number { get; set; }
        public int updatedAt { get; set; }
        public string userId { get; set; }
    }

    public class WkrGetAccByUUIDData
    {
        public String URL { get; set; }
        public String api { get; set; }
        public String uuid { get; set; }
        public String token { get; set; }
    }
}
