﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;

namespace armoperator
{
    class WkrGetAccByAccountId
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultAccountDataByAccountId m_resultREST = new ResultAccountDataByAccountId();
        private WkrGetAccByAccountIdData m_workerData = new WkrGetAccByAccountIdData();

        public WkrGetAccByAccountId()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultAccountDataByAccountId)e.Result;
        }

        private ResultAccountDataByAccountId RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultAccountDataByAccountId resultRest = new ResultAccountDataByAccountId();
            AccountDataByAccountId accountDataByAccountId = new AccountDataByAccountId();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                var JSONobj = deserMediaData.Deserialize<AccountDataByAccountId>(response);
                try
                {
                    accountDataByAccountId.id = JSONobj.id;
                    accountDataByAccountId.meta = JSONobj.meta;
                    accountDataByAccountId.number = JSONobj.number;
                    accountDataByAccountId.updatedAt = JSONobj.updatedAt; ;
                    accountDataByAccountId.userId = JSONobj.userId;
                }
                catch { };
            }
            resultRest.accountDataByAccountId = accountDataByAccountId;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediaData = new RestRequest(m_workerData.api + m_workerData.accountId, Method.GET);            
            requestGetMediaData.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetMediaData);
        }

        public ResultAccountDataByAccountId GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrGetAccByAccountIdData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class ResultAccountDataByAccountId
    {
        public AccountDataByAccountId accountDataByAccountId { get; set; }
        public int statusCode { get; set; }
    }

    public class AccountDataByAccountId
    {
        public int id { get; set; }
        public object meta { get; set; }
        public string number { get; set; }
        public int updatedAt { get; set; }
        public string userId { get; set; }
    }

    public class WkrGetAccByAccountIdData
    {
        public String URL { get; set; }
        public String api { get; set; }
        public String accountId { get; set; }
        public String token { get; set; }
    }
}
