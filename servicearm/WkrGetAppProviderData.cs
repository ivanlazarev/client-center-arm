﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;
using Newtonsoft.Json;

namespace armoperator
{
    class WkrGetAppProviderData
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultAppProviderData m_resultREST = new ResultAppProviderData();
        private GetAppProviderDataData m_workerData = new GetAppProviderDataData();

        public WkrGetAppProviderData()
        {
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultAppProviderData)e.Result;
        }

        private ResultAppProviderData RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultAppProviderData resultRest = new ResultAppProviderData();
            List<AppProviderData> desList = new List<AppProviderData>();
            
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<List<AppProviderData>>(response);
                    foreach (var element in JSONobj)
                    {
                        AppProviderData appData = new AppProviderData();
                        appData.description = element.description;
                        appData.id = element.id;
                        appData.meta = element.meta;
                        appData.title = element.title;
                        appData.updatedAt = element.updatedAt;
                        desList.Add(appData);
                    }
                }
                catch { }
            }
            resultRest.appProviderData = desList;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetAppProviderData = new RestRequest(m_workerData.api + m_workerData.id, Method.GET);
            requestGetAppProviderData.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetAppProviderData);
        }

        public ResultAppProviderData GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(GetAppProviderDataData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class Meta
    {
        public string type { get; set; }
    }

    public class AppProviderData
    {
        public int applicationId { get; set; }
        public string description { get; set; }
        public int id { get; set; }
        public string meta { get; set; }
        public string title { get; set; }
        public int updatedAt { get; set; }
    }

    public class ResultAppProviderData
    {
        public List<AppProviderData> appProviderData { get; set; }
        public int statusCode { get; set; }
    }

    public class GetAppProviderDataData
    {
        public String URL { get; set; }
        public String api { get; set; }
        public String id { get; set; }
        public String token { get; set; }
    }

}
