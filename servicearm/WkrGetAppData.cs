﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;

namespace armoperator
{
    class WkrGetAppData
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultAppData m_resultREST = new ResultAppData();
        private GetAppDataData m_workerData = new GetAppDataData();

        public WkrGetAppData()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultAppData)e.Result;
        }

        private ResultAppData RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultAppData resultRest = new ResultAppData();
            List<AppData> desList = new List<AppData>();
            AppData appData = new AppData();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<List<AppData>>(response);
                    appData.description = JSONobj[0].description;
                    appData.id = JSONobj[0].id;
                    appData.meta = JSONobj[0].meta;
                    appData.title = JSONobj[0].title;
                    appData.updatedAt = JSONobj[0].updatedAt;
                }
                catch { }
            }
            desList.Add(appData);
            resultRest.appData = desList;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetAppData = new RestRequest(m_workerData.api + m_workerData.app, Method.GET);

            requestGetAppData.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetAppData);
        }

        public ResultAppData GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(GetAppDataData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class AppData
    {
        public string description { get; set; }
        public int id { get; set; }
        public object meta { get; set; }
        public string title { get; set; }
        public int updatedAt { get; set; }
    }

    public class ResultAppData
    {
        public List<AppData> appData { get; set; }
        public int statusCode { get; set; }
    }

    public class GetAppDataData
    {
        public String URL { get; set; }
        public String api { get; set; }
        public String app { get; set; }
        public String token { get; set; }
    }

}
