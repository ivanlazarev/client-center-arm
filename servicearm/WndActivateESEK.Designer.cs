﻿namespace armoperator
{
    partial class WndActivateESEK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WndActivateESEK));
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblESEKActivation = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnActivateESEK = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tbxStatus = new System.Windows.Forms.Label();
            this.tbxStatusREST = new System.Windows.Forms.Label();
            this.tbxCardRFID = new System.Windows.Forms.Label();
            this.tbxCardNumber = new System.Windows.Forms.Label();
            this.tbxPerson = new System.Windows.Forms.Label();
            this.tbxSNILS = new System.Windows.Forms.Label();
            this.tbxCardState = new System.Windows.Forms.Label();
            this.tmrReadCard = new System.Windows.Forms.Timer(this.components);
            this.tmrGetMediaByRFID = new System.Windows.Forms.Timer(this.components);
            this.tmrGetAccByAccountId = new System.Windows.Forms.Timer(this.components);
            this.tmrGetPersonByUUID = new System.Windows.Forms.Timer(this.components);
            this.tmrActivateESEK = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusRestResponce = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.restResponceStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel6.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 5;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel6.Controls.Add(this.lblESEKActivation, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label5, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.label8, 2, 7);
            this.tableLayoutPanel6.Controls.Add(this.label7, 2, 6);
            this.tableLayoutPanel6.Controls.Add(this.label6, 2, 5);
            this.tableLayoutPanel6.Controls.Add(this.label2, 2, 4);
            this.tableLayoutPanel6.Controls.Add(this.label1, 2, 3);
            this.tableLayoutPanel6.Controls.Add(this.label9, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.btnActivateESEK, 3, 8);
            this.tableLayoutPanel6.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button2, 4, 10);
            this.tableLayoutPanel6.Controls.Add(this.tbxStatus, 3, 1);
            this.tableLayoutPanel6.Controls.Add(this.tbxStatusREST, 3, 2);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardRFID, 3, 3);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardNumber, 3, 4);
            this.tableLayoutPanel6.Controls.Add(this.tbxPerson, 3, 5);
            this.tableLayoutPanel6.Controls.Add(this.tbxSNILS, 3, 6);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardState, 3, 7);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 12;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(784, 562);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // lblESEKActivation
            // 
            this.lblESEKActivation.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.lblESEKActivation, 2);
            this.lblESEKActivation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblESEKActivation.Location = new System.Drawing.Point(3, 110);
            this.lblESEKActivation.Name = "lblESEKActivation";
            this.tableLayoutPanel6.SetRowSpan(this.lblESEKActivation, 9);
            this.lblESEKActivation.Size = new System.Drawing.Size(307, 432);
            this.lblESEKActivation.TabIndex = 7;
            this.lblESEKActivation.Text = "lblDataESEK";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label4, 2);
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(307, 30);
            this.label4.TabIndex = 6;
            this.label4.Text = "Последовательность действий:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(316, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 30);
            this.label5.TabIndex = 17;
            this.label5.Text = "Состояние чтения карты:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(316, 260);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(150, 30);
            this.label8.TabIndex = 20;
            this.label8.Text = "Статус карты:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(316, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(150, 30);
            this.label7.TabIndex = 19;
            this.label7.Text = "Номер СНИЛС:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(316, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 30);
            this.label6.TabIndex = 18;
            this.label6.Text = "Держатель карты:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(316, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 30);
            this.label2.TabIndex = 9;
            this.label2.Text = "Номер карты:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(316, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 30);
            this.label1.TabIndex = 8;
            this.label1.Text = "Серийный номер карты:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(316, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(150, 30);
            this.label9.TabIndex = 21;
            this.label9.Text = "Состояние запроса в систему:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnActivateESEK
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.btnActivateESEK, 2);
            this.btnActivateESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnActivateESEK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActivateESEK.Location = new System.Drawing.Point(472, 293);
            this.btnActivateESEK.Name = "btnActivateESEK";
            this.btnActivateESEK.Size = new System.Drawing.Size(309, 34);
            this.btnActivateESEK.TabIndex = 22;
            this.btnActivateESEK.Text = "Активировать";
            this.btnActivateESEK.UseVisualStyleBackColor = true;
            this.btnActivateESEK.Click += new System.EventHandler(this.btnActivateESEK_Click);
            // 
            // button1
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.button1, 5);
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Image = global::armoperator.Properties.Resources.credit_card111;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(778, 74);
            this.button1.TabIndex = 23;
            this.button1.TabStop = false;
            this.button1.Text = "Активация и выдача ЕСЕК";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(668, 505);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 34);
            this.button2.TabIndex = 24;
            this.button2.Text = "Закрыть";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // tbxStatus
            // 
            this.tbxStatus.AutoSize = true;
            this.tbxStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxStatus, 2);
            this.tbxStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxStatus.Location = new System.Drawing.Point(472, 80);
            this.tbxStatus.Name = "tbxStatus";
            this.tbxStatus.Size = new System.Drawing.Size(309, 30);
            this.tbxStatus.TabIndex = 25;
            this.tbxStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxStatusREST
            // 
            this.tbxStatusREST.AutoSize = true;
            this.tbxStatusREST.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxStatusREST, 2);
            this.tbxStatusREST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxStatusREST.Location = new System.Drawing.Point(472, 110);
            this.tbxStatusREST.Name = "tbxStatusREST";
            this.tbxStatusREST.Size = new System.Drawing.Size(309, 30);
            this.tbxStatusREST.TabIndex = 26;
            this.tbxStatusREST.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardRFID
            // 
            this.tbxCardRFID.AutoSize = true;
            this.tbxCardRFID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardRFID, 2);
            this.tbxCardRFID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxCardRFID.Location = new System.Drawing.Point(472, 140);
            this.tbxCardRFID.Name = "tbxCardRFID";
            this.tbxCardRFID.Size = new System.Drawing.Size(309, 30);
            this.tbxCardRFID.TabIndex = 27;
            this.tbxCardRFID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardNumber
            // 
            this.tbxCardNumber.AutoSize = true;
            this.tbxCardNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardNumber, 2);
            this.tbxCardNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxCardNumber.Location = new System.Drawing.Point(472, 170);
            this.tbxCardNumber.Name = "tbxCardNumber";
            this.tbxCardNumber.Size = new System.Drawing.Size(309, 30);
            this.tbxCardNumber.TabIndex = 28;
            this.tbxCardNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxPerson
            // 
            this.tbxPerson.AutoSize = true;
            this.tbxPerson.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxPerson, 2);
            this.tbxPerson.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxPerson.Location = new System.Drawing.Point(472, 200);
            this.tbxPerson.Name = "tbxPerson";
            this.tbxPerson.Size = new System.Drawing.Size(309, 30);
            this.tbxPerson.TabIndex = 29;
            this.tbxPerson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxSNILS
            // 
            this.tbxSNILS.AutoSize = true;
            this.tbxSNILS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxSNILS, 2);
            this.tbxSNILS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxSNILS.Location = new System.Drawing.Point(472, 230);
            this.tbxSNILS.Name = "tbxSNILS";
            this.tbxSNILS.Size = new System.Drawing.Size(309, 30);
            this.tbxSNILS.TabIndex = 30;
            this.tbxSNILS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardState
            // 
            this.tbxCardState.AutoSize = true;
            this.tbxCardState.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardState, 2);
            this.tbxCardState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxCardState.Location = new System.Drawing.Point(472, 260);
            this.tbxCardState.Name = "tbxCardState";
            this.tbxCardState.Size = new System.Drawing.Size(309, 30);
            this.tbxCardState.TabIndex = 31;
            this.tbxCardState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusRestResponce,
            this.toolStripStatusLabel1,
            this.restResponceStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 540);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusRestResponce
            // 
            this.statusRestResponce.Name = "statusRestResponce";
            this.statusRestResponce.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(123, 17);
            this.toolStripStatusLabel1.Text = "Состояние запросов:";
            // 
            // restResponceStatus
            // 
            this.restResponceStatus.Name = "restResponceStatus";
            this.restResponceStatus.Size = new System.Drawing.Size(184, 17);
            this.restResponceStatus.Text = "Активные запросы отсутствуют.";
            // 
            // WndActivateESEK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.ControlBox = false;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tableLayoutPanel6);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WndActivateESEK";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Активация и выдача ЕСЭК";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WndInfoESEK_FormClosing);
            this.Load += new System.EventHandler(this.WndInfoESEK_Load);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lblESEKActivation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer tmrReadCard;
        private System.Windows.Forms.Timer tmrGetMediaByRFID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer tmrGetAccByAccountId;
        private System.Windows.Forms.Timer tmrGetPersonByUUID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnActivateESEK;
        private System.Windows.Forms.Timer tmrActivateESEK;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusRestResponce;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripStatusLabel restResponceStatus;
        private System.Windows.Forms.Label tbxStatus;
        private System.Windows.Forms.Label tbxStatusREST;
        private System.Windows.Forms.Label tbxCardRFID;
        private System.Windows.Forms.Label tbxCardNumber;
        private System.Windows.Forms.Label tbxPerson;
        private System.Windows.Forms.Label tbxSNILS;
        private System.Windows.Forms.Label tbxCardState;
    }
}