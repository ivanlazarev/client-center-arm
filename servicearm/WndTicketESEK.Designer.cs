﻿namespace armoperator
{
    partial class WndTicketESEK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WndTicketESEK));
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTravelESEK = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.btnTravelTicket = new System.Windows.Forms.Button();
            this.btnTravelBusTicket = new System.Windows.Forms.Button();
            this.btnTravelTrolleybusTicket = new System.Windows.Forms.Button();
            this.btnTravePenslTicket = new System.Windows.Forms.Button();
            this.btnTraveBusPenslTicket = new System.Windows.Forms.Button();
            this.btnTraveTrolleybusPenslTicket = new System.Windows.Forms.Button();
            this.btnTraveStudTicket = new System.Windows.Forms.Button();
            this.btnTraveBusStudTicket = new System.Windows.Forms.Button();
            this.btnTraveTrolleybusStudTicket = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxMonth = new System.Windows.Forms.ComboBox();
            this.cbxYear = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewTickets = new System.Windows.Forms.DataGridView();
            this.gridTicketName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridTicketValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridTicketTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbxStatus = new System.Windows.Forms.Label();
            this.tbxStatusREST = new System.Windows.Forms.Label();
            this.tbxCardRFID = new System.Windows.Forms.Label();
            this.tbxCardNumber = new System.Windows.Forms.Label();
            this.tbxPerson = new System.Windows.Forms.Label();
            this.tbxSNILS = new System.Windows.Forms.Label();
            this.tbxCardState = new System.Windows.Forms.Label();
            this.tbxTicketResult = new System.Windows.Forms.Label();
            this.tmrReadCard = new System.Windows.Forms.Timer(this.components);
            this.tmrGetMediaByRFID = new System.Windows.Forms.Timer(this.components);
            this.tmrGetAccByAccountId = new System.Windows.Forms.Timer(this.components);
            this.tmrGetPersonByUUID = new System.Windows.Forms.Timer(this.components);
            this.tmrTicket = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.restResponceStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrCheckBags = new System.Windows.Forms.Timer(this.components);
            this.tmrGetCurrencies = new System.Windows.Forms.Timer(this.components);
            this.tmrGetBagsByAccId = new System.Windows.Forms.Timer(this.components);
            this.tmrGetBagTransactions = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTickets)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel6.Controls.Add(this.lblTravelESEK, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label5, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.label8, 1, 7);
            this.tableLayoutPanel6.Controls.Add(this.label7, 1, 6);
            this.tableLayoutPanel6.Controls.Add(this.label6, 1, 5);
            this.tableLayoutPanel6.Controls.Add(this.label2, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.label1, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.label9, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.panel1, 1, 9);
            this.tableLayoutPanel6.Controls.Add(this.label13, 1, 11);
            this.tableLayoutPanel6.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button2, 3, 12);
            this.tableLayoutPanel6.Controls.Add(this.groupBox1, 1, 8);
            this.tableLayoutPanel6.Controls.Add(this.tbxStatus, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.tbxStatusREST, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardRFID, 2, 3);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardNumber, 2, 4);
            this.tableLayoutPanel6.Controls.Add(this.tbxPerson, 2, 5);
            this.tableLayoutPanel6.Controls.Add(this.tbxSNILS, 2, 6);
            this.tableLayoutPanel6.Controls.Add(this.tbxCardState, 2, 7);
            this.tableLayoutPanel6.Controls.Add(this.tbxTicketResult, 2, 11);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 14;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1008, 730);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // lblTravelESEK
            // 
            this.lblTravelESEK.AutoSize = true;
            this.lblTravelESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTravelESEK.Location = new System.Drawing.Point(3, 110);
            this.lblTravelESEK.Name = "lblTravelESEK";
            this.tableLayoutPanel6.SetRowSpan(this.lblTravelESEK, 10);
            this.lblTravelESEK.Size = new System.Drawing.Size(397, 560);
            this.lblTravelESEK.TabIndex = 7;
            this.lblTravelESEK.Text = "lblDataESEK";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(397, 30);
            this.label4.TabIndex = 6;
            this.label4.Text = "Последовательность действий:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(406, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(195, 30);
            this.label5.TabIndex = 17;
            this.label5.Text = "Состояние чтения карты:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(406, 260);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(195, 30);
            this.label8.TabIndex = 20;
            this.label8.Text = "Статус карты:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(406, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(195, 30);
            this.label7.TabIndex = 19;
            this.label7.Text = "Номер СНИЛС:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(406, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(195, 30);
            this.label6.TabIndex = 18;
            this.label6.Text = "Держатель карты:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(406, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 30);
            this.label2.TabIndex = 9;
            this.label2.Text = "Номер карты:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(406, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 30);
            this.label1.TabIndex = 8;
            this.label1.Text = "Серийный номер карты:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(406, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(195, 30);
            this.label9.TabIndex = 21;
            this.label9.Text = "Состояние запроса в систему:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.panel1, 3);
            this.panel1.Controls.Add(this.tableLayoutPanel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(406, 453);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel6.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(599, 184);
            this.panel1.TabIndex = 22;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.27273F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.12121F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.24242F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.12121F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.24242F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel10, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.cbxMonth, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.cbxYear, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.label10, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(599, 184);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel10, 5);
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Controls.Add(this.btnTravelTicket, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.btnTravelBusTicket, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.btnTravelTrolleybusTicket, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.btnTravePenslTicket, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.btnTraveBusPenslTicket, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.btnTraveTrolleybusPenslTicket, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.btnTraveStudTicket, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.btnTraveBusStudTicket, 2, 1);
            this.tableLayoutPanel10.Controls.Add(this.btnTraveTrolleybusStudTicket, 2, 2);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 43);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(593, 138);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // btnTravelTicket
            // 
            this.btnTravelTicket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTravelTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTravelTicket.Location = new System.Drawing.Point(3, 3);
            this.btnTravelTicket.Name = "btnTravelTicket";
            this.btnTravelTicket.Size = new System.Drawing.Size(191, 40);
            this.btnTravelTicket.TabIndex = 5;
            this.btnTravelTicket.Text = "Проездной (единый)";
            this.btnTravelTicket.UseVisualStyleBackColor = true;
            this.btnTravelTicket.Click += new System.EventHandler(this.btnTravelTicket_Click);
            // 
            // btnTravelBusTicket
            // 
            this.btnTravelBusTicket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTravelBusTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTravelBusTicket.Location = new System.Drawing.Point(3, 49);
            this.btnTravelBusTicket.Name = "btnTravelBusTicket";
            this.btnTravelBusTicket.Size = new System.Drawing.Size(191, 40);
            this.btnTravelBusTicket.TabIndex = 6;
            this.btnTravelBusTicket.Text = "Проездной (автобус)";
            this.btnTravelBusTicket.UseVisualStyleBackColor = true;
            this.btnTravelBusTicket.Click += new System.EventHandler(this.btnTravelBusTicket_Click);
            // 
            // btnTravelTrolleybusTicket
            // 
            this.btnTravelTrolleybusTicket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTravelTrolleybusTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTravelTrolleybusTicket.Location = new System.Drawing.Point(3, 95);
            this.btnTravelTrolleybusTicket.Name = "btnTravelTrolleybusTicket";
            this.btnTravelTrolleybusTicket.Size = new System.Drawing.Size(191, 40);
            this.btnTravelTrolleybusTicket.TabIndex = 7;
            this.btnTravelTrolleybusTicket.Text = "Проездной (троллейбус)";
            this.btnTravelTrolleybusTicket.UseVisualStyleBackColor = true;
            this.btnTravelTrolleybusTicket.Click += new System.EventHandler(this.btnTravelTrolleybusTicket_Click);
            // 
            // btnTravePenslTicket
            // 
            this.btnTravePenslTicket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTravePenslTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTravePenslTicket.Location = new System.Drawing.Point(200, 3);
            this.btnTravePenslTicket.Name = "btnTravePenslTicket";
            this.btnTravePenslTicket.Size = new System.Drawing.Size(191, 40);
            this.btnTravePenslTicket.TabIndex = 8;
            this.btnTravePenslTicket.Text = "Единый пенсионный";
            this.btnTravePenslTicket.UseVisualStyleBackColor = true;
            this.btnTravePenslTicket.Click += new System.EventHandler(this.btnTravePenslTicket_Click);
            // 
            // btnTraveBusPenslTicket
            // 
            this.btnTraveBusPenslTicket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTraveBusPenslTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTraveBusPenslTicket.Location = new System.Drawing.Point(200, 49);
            this.btnTraveBusPenslTicket.Name = "btnTraveBusPenslTicket";
            this.btnTraveBusPenslTicket.Size = new System.Drawing.Size(191, 40);
            this.btnTraveBusPenslTicket.TabIndex = 9;
            this.btnTraveBusPenslTicket.Text = "Пенсионный (автобус)";
            this.btnTraveBusPenslTicket.UseVisualStyleBackColor = true;
            this.btnTraveBusPenslTicket.Click += new System.EventHandler(this.btnTraveBusPenslTicket_Click);
            // 
            // btnTraveTrolleybusPenslTicket
            // 
            this.btnTraveTrolleybusPenslTicket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTraveTrolleybusPenslTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTraveTrolleybusPenslTicket.Location = new System.Drawing.Point(200, 95);
            this.btnTraveTrolleybusPenslTicket.Name = "btnTraveTrolleybusPenslTicket";
            this.btnTraveTrolleybusPenslTicket.Size = new System.Drawing.Size(191, 40);
            this.btnTraveTrolleybusPenslTicket.TabIndex = 10;
            this.btnTraveTrolleybusPenslTicket.Text = "Пенсионный (троллейбус)";
            this.btnTraveTrolleybusPenslTicket.UseVisualStyleBackColor = true;
            this.btnTraveTrolleybusPenslTicket.Click += new System.EventHandler(this.btnTraveTrolleybusPenslTicket_Click);
            // 
            // btnTraveStudTicket
            // 
            this.btnTraveStudTicket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTraveStudTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTraveStudTicket.Location = new System.Drawing.Point(397, 3);
            this.btnTraveStudTicket.Name = "btnTraveStudTicket";
            this.btnTraveStudTicket.Size = new System.Drawing.Size(193, 40);
            this.btnTraveStudTicket.TabIndex = 11;
            this.btnTraveStudTicket.Text = "Единый студенческий";
            this.btnTraveStudTicket.UseVisualStyleBackColor = true;
            this.btnTraveStudTicket.Click += new System.EventHandler(this.btnTraveStudTicket_Click);
            // 
            // btnTraveBusStudTicket
            // 
            this.btnTraveBusStudTicket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTraveBusStudTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTraveBusStudTicket.Location = new System.Drawing.Point(397, 49);
            this.btnTraveBusStudTicket.Name = "btnTraveBusStudTicket";
            this.btnTraveBusStudTicket.Size = new System.Drawing.Size(193, 40);
            this.btnTraveBusStudTicket.TabIndex = 12;
            this.btnTraveBusStudTicket.Text = "Студенческий (автобус)";
            this.btnTraveBusStudTicket.UseVisualStyleBackColor = true;
            this.btnTraveBusStudTicket.Click += new System.EventHandler(this.btnTraveBusStudTicket_Click);
            // 
            // btnTraveTrolleybusStudTicket
            // 
            this.btnTraveTrolleybusStudTicket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTraveTrolleybusStudTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTraveTrolleybusStudTicket.Location = new System.Drawing.Point(397, 95);
            this.btnTraveTrolleybusStudTicket.Name = "btnTraveTrolleybusStudTicket";
            this.btnTraveTrolleybusStudTicket.Size = new System.Drawing.Size(193, 40);
            this.btnTraveTrolleybusStudTicket.TabIndex = 13;
            this.btnTraveTrolleybusStudTicket.Text = "Студенческий (троллейбус)";
            this.btnTraveTrolleybusStudTicket.UseVisualStyleBackColor = true;
            this.btnTraveTrolleybusStudTicket.Click += new System.EventHandler(this.btnTraveTrolleybusStudTicket_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(166, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 40);
            this.label3.TabIndex = 2;
            this.label3.Text = "Месяц:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxMonth
            // 
            this.cbxMonth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMonth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbxMonth.FormattingEnabled = true;
            this.cbxMonth.Items.AddRange(new object[] {
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"});
            this.cbxMonth.Location = new System.Drawing.Point(238, 6);
            this.cbxMonth.Name = "cbxMonth";
            this.cbxMonth.Size = new System.Drawing.Size(139, 28);
            this.cbxMonth.TabIndex = 0;
            this.cbxMonth.SelectedIndexChanged += new System.EventHandler(this.cbxMonth_SelectedIndexChanged);
            // 
            // cbxYear
            // 
            this.cbxYear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxYear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbxYear.FormattingEnabled = true;
            this.cbxYear.Items.AddRange(new object[] {
            "2016",
            "2017",
            "2018"});
            this.cbxYear.Location = new System.Drawing.Point(455, 6);
            this.cbxYear.Name = "cbxYear";
            this.cbxYear.Size = new System.Drawing.Size(141, 28);
            this.cbxYear.TabIndex = 1;
            this.cbxYear.SelectedIndexChanged += new System.EventHandler(this.cbxYear_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(383, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 40);
            this.label10.TabIndex = 5;
            this.label10.Text = "Год:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(157, 40);
            this.label11.TabIndex = 6;
            this.label11.Text = "Период действия билета (для проездных):";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Location = new System.Drawing.Point(406, 640);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(195, 30);
            this.label13.TabIndex = 23;
            this.label13.Text = "Статус записи билета :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.button1, 4);
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Image = global::armoperator.Properties.Resources.daily_calendar1;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(1002, 74);
            this.button1.TabIndex = 25;
            this.button1.TabStop = false;
            this.button1.Text = "Запись проездных билетов на ЕСЭК";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(859, 673);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(146, 34);
            this.button2.TabIndex = 14;
            this.button2.Text = "Закрыть";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.groupBox1, 3);
            this.groupBox1.Controls.Add(this.dataGridViewTickets);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(406, 293);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(599, 154);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Билеты";
            // 
            // dataGridViewTickets
            // 
            this.dataGridViewTickets.AllowUserToAddRows = false;
            this.dataGridViewTickets.AllowUserToDeleteRows = false;
            this.dataGridViewTickets.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewTickets.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewTickets.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewTickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTickets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gridTicketName,
            this.gridTicketValue,
            this.gridTicketTime});
            this.dataGridViewTickets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTickets.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewTickets.Name = "dataGridViewTickets";
            this.dataGridViewTickets.ReadOnly = true;
            this.dataGridViewTickets.RowHeadersVisible = false;
            this.dataGridViewTickets.Size = new System.Drawing.Size(593, 135);
            this.dataGridViewTickets.TabIndex = 2;
            this.dataGridViewTickets.TabStop = false;
            // 
            // gridTicketName
            // 
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridTicketName.DefaultCellStyle = dataGridViewCellStyle1;
            this.gridTicketName.HeaderText = "Наименование";
            this.gridTicketName.Name = "gridTicketName";
            this.gridTicketName.ReadOnly = true;
            // 
            // gridTicketValue
            // 
            this.gridTicketValue.HeaderText = "Кол-во";
            this.gridTicketValue.Name = "gridTicketValue";
            this.gridTicketValue.ReadOnly = true;
            // 
            // gridTicketTime
            // 
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridTicketTime.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridTicketTime.HeaderText = "Срок действия";
            this.gridTicketTime.Name = "gridTicketTime";
            this.gridTicketTime.ReadOnly = true;
            // 
            // tbxStatus
            // 
            this.tbxStatus.AutoSize = true;
            this.tbxStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxStatus, 2);
            this.tbxStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxStatus.Location = new System.Drawing.Point(607, 80);
            this.tbxStatus.Name = "tbxStatus";
            this.tbxStatus.Size = new System.Drawing.Size(398, 30);
            this.tbxStatus.TabIndex = 28;
            this.tbxStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxStatusREST
            // 
            this.tbxStatusREST.AutoSize = true;
            this.tbxStatusREST.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxStatusREST, 2);
            this.tbxStatusREST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxStatusREST.Location = new System.Drawing.Point(607, 110);
            this.tbxStatusREST.Name = "tbxStatusREST";
            this.tbxStatusREST.Size = new System.Drawing.Size(398, 30);
            this.tbxStatusREST.TabIndex = 29;
            this.tbxStatusREST.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardRFID
            // 
            this.tbxCardRFID.AutoSize = true;
            this.tbxCardRFID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardRFID, 2);
            this.tbxCardRFID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxCardRFID.Location = new System.Drawing.Point(607, 140);
            this.tbxCardRFID.Name = "tbxCardRFID";
            this.tbxCardRFID.Size = new System.Drawing.Size(398, 30);
            this.tbxCardRFID.TabIndex = 30;
            this.tbxCardRFID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardNumber
            // 
            this.tbxCardNumber.AutoSize = true;
            this.tbxCardNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardNumber, 2);
            this.tbxCardNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxCardNumber.Location = new System.Drawing.Point(607, 170);
            this.tbxCardNumber.Name = "tbxCardNumber";
            this.tbxCardNumber.Size = new System.Drawing.Size(398, 30);
            this.tbxCardNumber.TabIndex = 31;
            this.tbxCardNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxPerson
            // 
            this.tbxPerson.AutoSize = true;
            this.tbxPerson.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxPerson, 2);
            this.tbxPerson.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxPerson.Location = new System.Drawing.Point(607, 200);
            this.tbxPerson.Name = "tbxPerson";
            this.tbxPerson.Size = new System.Drawing.Size(398, 30);
            this.tbxPerson.TabIndex = 32;
            this.tbxPerson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxSNILS
            // 
            this.tbxSNILS.AutoSize = true;
            this.tbxSNILS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxSNILS, 2);
            this.tbxSNILS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxSNILS.Location = new System.Drawing.Point(607, 230);
            this.tbxSNILS.Name = "tbxSNILS";
            this.tbxSNILS.Size = new System.Drawing.Size(398, 30);
            this.tbxSNILS.TabIndex = 33;
            this.tbxSNILS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxCardState
            // 
            this.tbxCardState.AutoSize = true;
            this.tbxCardState.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxCardState, 2);
            this.tbxCardState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxCardState.Location = new System.Drawing.Point(607, 260);
            this.tbxCardState.Name = "tbxCardState";
            this.tbxCardState.Size = new System.Drawing.Size(398, 30);
            this.tbxCardState.TabIndex = 34;
            this.tbxCardState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxTicketResult
            // 
            this.tbxTicketResult.AutoSize = true;
            this.tbxTicketResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel6.SetColumnSpan(this.tbxTicketResult, 2);
            this.tbxTicketResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxTicketResult.Location = new System.Drawing.Point(607, 640);
            this.tbxTicketResult.Name = "tbxTicketResult";
            this.tbxTicketResult.Size = new System.Drawing.Size(398, 30);
            this.tbxTicketResult.TabIndex = 35;
            this.tbxTicketResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.restResponceStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 708);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(123, 17);
            this.toolStripStatusLabel1.Text = "Состояние запросов:";
            // 
            // restResponceStatus
            // 
            this.restResponceStatus.Name = "restResponceStatus";
            this.restResponceStatus.Size = new System.Drawing.Size(184, 17);
            this.restResponceStatus.Text = "Активные запросы отсутствуют.";
            // 
            // tmrCheckBags
            // 
            this.tmrCheckBags.Tick += new System.EventHandler(this.tmrCheckBags_Tick);
            // 
            // WndTicketESEK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.ControlBox = false;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tableLayoutPanel6);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WndTicketESEK";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Запись проездных билетов на ЕСЭК";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WndInfoESEK_FormClosing);
            this.Load += new System.EventHandler(this.WndInfoESEK_Load);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTickets)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lblTravelESEK;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer tmrReadCard;
        private System.Windows.Forms.Timer tmrGetMediaByRFID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer tmrGetAccByAccountId;
        private System.Windows.Forms.Timer tmrGetPersonByUUID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Button btnTravelTicket;
        private System.Windows.Forms.Button btnTravelBusTicket;
        private System.Windows.Forms.Button btnTravelTrolleybusTicket;
        private System.Windows.Forms.Button btnTravePenslTicket;
        private System.Windows.Forms.Button btnTraveBusPenslTicket;
        private System.Windows.Forms.Button btnTraveTrolleybusPenslTicket;
        private System.Windows.Forms.Button btnTraveStudTicket;
        private System.Windows.Forms.Button btnTraveBusStudTicket;
        private System.Windows.Forms.Button btnTraveTrolleybusStudTicket;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxMonth;
        private System.Windows.Forms.ComboBox cbxYear;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Timer tmrTicket;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripStatusLabel restResponceStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewTickets;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridTicketName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridTicketValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridTicketTime;
        private System.Windows.Forms.Timer tmrCheckBags;
        private System.Windows.Forms.Timer tmrGetCurrencies;
        private System.Windows.Forms.Timer tmrGetBagsByAccId;
        private System.Windows.Forms.Timer tmrGetBagTransactions;
        private System.Windows.Forms.Label tbxStatus;
        private System.Windows.Forms.Label tbxStatusREST;
        private System.Windows.Forms.Label tbxCardRFID;
        private System.Windows.Forms.Label tbxCardNumber;
        private System.Windows.Forms.Label tbxPerson;
        private System.Windows.Forms.Label tbxSNILS;
        private System.Windows.Forms.Label tbxCardState;
        private System.Windows.Forms.Label tbxTicketResult;
    }
}