﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;

namespace armoperator
{
    class WkrCreatePersonalInfo
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultPersonDataBySNILS m_resultREST = new ResultPersonDataBySNILS();
        private WkrCreatePersonalInfoData m_workerData = new WkrCreatePersonalInfoData();

        public WkrCreatePersonalInfo()
        {
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultPersonDataBySNILS)e.Result;
        }

        private ResultPersonDataBySNILS RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultPersonDataBySNILS resultRest = new ResultPersonDataBySNILS();
            PersonDataBySNILS personDataBySNILS = new PersonDataBySNILS();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<PersonDataBySNILS>(response);
                    personDataBySNILS.exemptions = JSONobj.exemptions;
                    personDataBySNILS.id = JSONobj.id;
                    personDataBySNILS.name = JSONobj.name;
                    personDataBySNILS.snils = JSONobj.snils;
                    personDataBySNILS.updatedAt = JSONobj.updatedAt;
                    personDataBySNILS.uuid = JSONobj.uuid;
                }
                catch { }
            }
            resultRest.personDataBySNILS = personDataBySNILS;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestCreatePersInfo = new RestRequest(m_workerData.api, Method.POST);
            requestCreatePersInfo.AddHeader("Auth-Access-Token", m_workerData.token);
            requestCreatePersInfo.AddJsonBody(new
            {
                snils = m_workerData.snils,
                name = m_workerData.name
            });
            RESTWkr.RunWorkerAsync(requestCreatePersInfo);
        }

        public ResultPersonDataBySNILS GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrCreatePersonalInfoData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class WkrCreatePersonalInfoData
    {
        public String URL { get; set; }
        public String api { get; set; }
        public String snils { get; set; }
        public String name { get; set; }
        public String token { get; set; }
    }
}
