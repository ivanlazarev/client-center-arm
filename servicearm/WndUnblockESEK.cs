﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace armoperator
{
    public partial class WndUnblockESEK : Form
    {
        WndMain __mainWindow;

        private static List<Label> __tbxs;
        private static WkrReadCard __wkrReadCard;
        private static WkrReadOperationResults __wkrReadOperationResults;
        private static WkrGetMediasByRFID __wkrGetMediasByRFID;
        private static ResultMediasDataByRFID __resultMediasDataByRFID;
        private static WkrGetAccByAccountId __wkrGetAccByAccountId;
        private static ResultAccountDataByAccountId __resultAccountDataByAccountId;
        private static WkrGetPersonByUUID __wkrGetPersonByUUID;
        private static ResultPersonDataByUUID __resultPersonDataByUUID;
        private static WkrActivateESEK __wkrActivateESEK;
        private static ResultActivateESEK __resultActivateESEK;
        private static WkrBlockESEK __wkrBlockESEK;
        private static ResultBlockESEK __resultBlockESEK;
        private static String __lastRFID;

        public WndUnblockESEK(WndMain mainWindow)
        {
            InitializeComponent();
            __mainWindow = mainWindow;
        }

        private void WndInfoESEK_Load(object sender, EventArgs e)
        {
            __tbxs = new System.Collections.Generic.List<Label>();
            __tbxs.Add(tbxCardRFID);
            __tbxs.Add(tbxCardNumber);
            __tbxs.Add(tbxStatus);
            __tbxs.Add(tbxPerson);
            __tbxs.Add(tbxSNILS);
            __tbxs.Add(tbxCardState);
            __tbxs.Add(tbxStatusREST);
            __tbxs.Add(tbxChangeStatusTimestamp);

            //foreach (var tbx in __tbxs)
            //{
            //    tbx.ReadOnly = true;
            //    tbx.BackColor = System.Drawing.SystemColors.Control;
            //}

            InitializeTmrReadCard();
            InitializeTmrGetMediaByRFID();
            InitializeTmrGetAccByAccountId();
            InitializeTmrGetPersonByUUID();
            InitializeTmrActivateESEK();
            InitializeTmrBlockESEK();

            __wkrReadCard = new WkrReadCard(WndMain.READER_NAME);
            __wkrReadOperationResults = new WkrReadOperationResults();
            __wkrGetMediasByRFID = new WkrGetMediasByRFID();
            __resultMediasDataByRFID = new ResultMediasDataByRFID();
            __wkrGetAccByAccountId = new WkrGetAccByAccountId();
            __resultAccountDataByAccountId = new ResultAccountDataByAccountId();
            __wkrGetPersonByUUID = new WkrGetPersonByUUID();
            __resultPersonDataByUUID = new ResultPersonDataByUUID();
            __wkrActivateESEK = new WkrActivateESEK();
            __resultActivateESEK = new ResultActivateESEK();
            __wkrBlockESEK = new WkrBlockESEK();
            __resultBlockESEK = new ResultBlockESEK();

            __lastRFID = "";

            this.lblBlockESEK.Text = "1. Для снятия ранее заблокированной ЕСЭК гражданин приходит в пункт обслуживания;\n\r"+
                                        "2. Гражданин обращается к специалисту пункта обслуживания с целью разблокировки ЕСЭК"+
                                        "и предъявляет ЕСЭК и документ, удостоверяющий личность;\n\r"+
                                        "3. Cпециалист пункта обслуживания размещает ЕСЭК на считывающем устройстве;\n\r"+
                                        "4. Cпециалист пункта обслуживания проверяет в Системе наличие предъявленной"+
                                        "5. ЕСЭК и в случае ее нахождения разблокирует ее;\n\r"+
                                        "6. Cпециалист пункта обслуживания сообщает о результате операции гражданину.";

            btnActivateESEK.Enabled = false;
            //btnBlockESEK.Enabled = false;
            tmrReadCard.Start();
        }

        private void InitializeTmrReadCard()
        {
            tmrReadCard.Interval = 300;
            tmrReadCard.Tick += new EventHandler(tmrReadCard_Tick);
        }

        private void tmrReadCard_Tick(object Sender, EventArgs e)
        {
            bool readWkrStatus = __wkrReadCard.GetWkrStatus();
            if (!readWkrStatus)
            {
                __wkrReadOperationResults = __wkrReadCard.GetResultREST();
                int readStatus = __wkrReadOperationResults.readStatus;
                switch (readStatus)
                {
                    case 0:
                        try
                        {
                            String rfid = __wkrReadOperationResults.thrustCardInfo.uid;
                            if (null != rfid)
                            {
                                tbxCardRFID.Text = rfid;
                                tbxCardNumber.Text = __wkrReadOperationResults.thrustCardInfo.thrustCardInfoPart1.CardNumber;
                                tbxStatus.Text = "Данные карты считаны.";

                                if (__lastRFID != rfid)
                                {
                                    btnActivateESEK.Enabled = false;
                                    //btnBlockESEK.Enabled = false;
                                    ClearTbxs(__tbxs);    

                                    __lastRFID = rfid;
                                    WkrGetMediasByRFIDData wkrGetMediasByRFIDData = new WkrGetMediasByRFIDData();
                                    wkrGetMediasByRFIDData.URL = WndMain.BASE_URL;
                                    wkrGetMediasByRFIDData.api = "/api/cp/v1/medias/";
                                    wkrGetMediasByRFIDData.rfid = rfid;
                                    wkrGetMediasByRFIDData.token = __mainWindow.GetAuthToken();
                                    __wkrGetMediasByRFID.SetWorkerData(wkrGetMediasByRFIDData);

                                    if (!__wkrGetMediasByRFID.GetWkrStatus())
                                    {
                                        __wkrGetMediasByRFID.PerformRestOperation();
                                        tmrGetMediaByRFID.Start();
                                    }
                                    tbxStatusREST.Text = "Запрос данных о карте держателя.";                                                                             
                                }
                            }
                        }
                        catch { }
                        break;
                    case 1:
                        tbxStatus.Text = "Неверный формат карты.";
                        break;
                    case 2:
                        tbxStatus.Text = "Приложите карту ещё раз.";
                        break;
                    case 3:
                        __lastRFID = "";
                        btnActivateESEK.Enabled = false;
                        //btnBlockESEK.Enabled = false;
                        ClearTbxs(__tbxs);                      
                        tbxStatus.Text = "Положите карту на считыватель.";
                        break;
                    case 4:
                        btnActivateESEK.Enabled = false;
                        //btnBlockESEK.Enabled = false;
                        tbxStatus.Text = "Подключите считыватель.";
                        break;
                    case 5:
                        tbxStatus.Text = "Ошибка модели считывателя.";
                        break;
                    case 6:
                        tbxStatus.Text = "Неизвестная ошибка при чтении карты";
                        break;
                    case 7:
                        tbxStatus.Text = "Ошибка подключения к считывателю";
                        break;
                    default:
                        btnActivateESEK.Enabled = false;
                        //btnBlockESEK.Enabled = false;
                        tbxStatus.Text = "Неизвестная ошибка";
                        break;
                }
                __wkrReadCard.PerformReadOperation();                
            }
        }

        private void InitializeTmrGetMediaByRFID()
        {
            tmrGetMediaByRFID.Interval = 100;
            tmrGetMediaByRFID.Tick += new EventHandler(tmrGetMediaByRFID_Tick);
        }

        private void tmrGetMediaByRFID_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetMediasByRFID.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrGetMediaByRFID.Stop();
                __resultMediasDataByRFID = __wkrGetMediasByRFID.GetResultREST();
                switch (__resultMediasDataByRFID.statusCode)
                {
                    case 200:
                        try
                        {
                            int accountId = __resultMediasDataByRFID.mediasDataByRFID.accountId;
                            String cardState = __resultMediasDataByRFID.mediasDataByRFID.states[0].state;
                            int statusChangeTime = __resultMediasDataByRFID.mediasDataByRFID.states[0].timestamp;
                            tbxChangeStatusTimestamp.Text = WndInfoESEK.UnixTimestampToDateTime(Convert.ToDouble(statusChangeTime)).ToLongDateString();
                            switch (cardState)
                            {
                                case  WndStatement.CARD_REGISTERED_STR:
                                    btnActivateESEK.Enabled = false;
                                    //btnBlockESEK.Enabled = false;
                                    tbxCardState.Text = "Карта зарегестрирована.";
                                    break;
                                case WndStatement.CARD_ACTIVATED_STR:
                                    btnActivateESEK.Enabled = false;
                                    //btnBlockESEK.Enabled = true;
                                    tbxCardState.Text = "Карта активирована.";
                                    break;
                                case WndStatement.CARD_BLOCKED_STR:
                                    btnActivateESEK.Enabled = true;
                                    //btnBlockESEK.Enabled = false;
                                    tbxCardState.Text = "Карта заблокирована.";
                                    break;
                                default:
                                    btnActivateESEK.Enabled = false;
                                    //btnBlockESEK.Enabled = false;
                                    tbxCardState.Text = "Статус неизвестен.";
                                    tbxChangeStatusTimestamp.Text = "";
                                    break;
                            }

                            WkrGetAccByAccountIdData wkrGetAccByAccountIdData = new WkrGetAccByAccountIdData();
                            wkrGetAccByAccountIdData.URL = WndMain.BASE_URL;
                            wkrGetAccByAccountIdData.api = "/api/cp/v1/accounts/";
                            wkrGetAccByAccountIdData.accountId = Convert.ToString(accountId);
                            wkrGetAccByAccountIdData.token = __mainWindow.GetAuthToken();
                            __wkrGetAccByAccountId.SetWorkerData(wkrGetAccByAccountIdData);

                            if (!__wkrGetAccByAccountId.GetWkrStatus())
                            {
                                __wkrGetAccByAccountId.PerformRestOperation();
                                tmrGetAccByAccountId.Start();
                            }
                            tbxStatusREST.Text = "Запрос счета держателя карты.";
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("У данной карты отсутствует держатель.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetAccByAccountId()
        {
            tmrGetAccByAccountId.Interval = 100;
            tmrGetAccByAccountId.Tick += new EventHandler(tmrGetAccByAccountId_Tick);
        }

        private void tmrGetAccByAccountId_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetAccByAccountId.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrGetAccByAccountId.Stop();
                __resultAccountDataByAccountId = __wkrGetAccByAccountId.GetResultREST();
                switch (__resultAccountDataByAccountId.statusCode)
                {
                    case 200:
                        try
                        {
                            String userId = __resultAccountDataByAccountId.accountDataByAccountId.userId;

                            WkrPersonByUUIDWorkerData wkrPersonByUUIDWorkerData = new WkrPersonByUUIDWorkerData();
                            wkrPersonByUUIDWorkerData.URL = WndMain.BASE_URL;
                            wkrPersonByUUIDWorkerData.api = "/api/spd/v1/persons/";
                            wkrPersonByUUIDWorkerData.uuid = userId;
                            wkrPersonByUUIDWorkerData.token = __mainWindow.GetAuthToken();
                            __wkrGetPersonByUUID.SetWorkerData(wkrPersonByUUIDWorkerData);

                            if (!__wkrGetPersonByUUID.GetWkrStatus())
                            {
                                __wkrGetPersonByUUID.PerformRestOperation();
                                tmrGetPersonByUUID.Start();
                            }
                            tbxStatusREST.Text = "Запрос сведений о держателе карты.";
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetPersonByUUID()
        {
            tmrGetPersonByUUID.Interval = 100;
            tmrGetPersonByUUID.Tick += new EventHandler(tmrGetPersonByUUID_Tick);
        }

        private void tmrGetPersonByUUID_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetPersonByUUID.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrGetPersonByUUID.Stop();
                __resultPersonDataByUUID = __wkrGetPersonByUUID.GetResultREST();
                switch (__resultPersonDataByUUID.statusCode)
                {
                    case 200:
                        try
                        {
                            String personInfo = __resultPersonDataByUUID.personDataByUUID.name;
                            String snilsInfo = __resultPersonDataByUUID.personDataByUUID.snils;
                            tbxPerson.Text = personInfo;
                            tbxSNILS.Text = snilsInfo;
                            tbxStatusREST.Text = "Данные о держателе карты получены.";
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrActivateESEK()
        {
            tmrActivateESEK.Interval = 100;
            tmrActivateESEK.Tick += new EventHandler(tmrActivateESEK_Tick);
        }

        private void tmrActivateESEK_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrActivateESEK.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrActivateESEK.Stop();
                __resultActivateESEK = __wkrActivateESEK.GetResultREST();
                switch (__resultActivateESEK.statusCode)
                {
                    case 200:
                        try
                        {
                            if (0 != __resultActivateESEK.transactionsData.Count)
                            {
                                if ("activated" == __resultActivateESEK.transactionsData[0].state)
                                {
                                    tbxStatusREST.Text = "Операция выполнена успешно";
                                    tbxCardState.Text = "Карта разблокирована";
                                    if (!__wkrGetMediasByRFID.GetWkrStatus())
                                    {
                                        __wkrGetMediasByRFID.PerformRestOperation();
                                        tmrGetMediaByRFID.Start();
                                    }
                                    tbxStatusREST.Text = "Запрос данных о карте держателя.";   
                                }
                                else
                                {
                                    tbxStatusREST.Text = "Ошибка в процессиноговом центре";
                                }
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrBlockESEK()
        {
            tmrBlockESEK.Interval = 100;
            tmrBlockESEK.Tick += new EventHandler(tmrBlockESEK_Tick);
        }

        private void tmrBlockESEK_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrBlockESEK.GetWkrStatus())
            {
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                tmrBlockESEK.Stop();
                __resultBlockESEK = __wkrBlockESEK.GetResultREST();
                switch (__resultBlockESEK.statusCode)
                {
                    case 200:
                        try
                        {
                            if (0 != __resultBlockESEK.transactionsData.Count)
                            {
                                if ("blocked" == __resultBlockESEK.transactionsData[0].state)
                                {
                                    tbxStatusREST.Text = "Операция выполнена успешно";
                                    tbxCardState.Text = "Карта заблокирована";
                                    if (!__wkrGetMediasByRFID.GetWkrStatus())
                                    {
                                        __wkrGetMediasByRFID.PerformRestOperation();
                                        tmrGetMediaByRFID.Start();
                                    }
                                    tbxStatusREST.Text = "Запрос данных о карте держателя.";   
                                }
                                else
                                {
                                    tbxStatusREST.Text = "Ошибка в процессиноговом центре";
                                }
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void ClearTbxs(List<Label> textBoxes)
        {
            try
            {
                foreach (var textBox in textBoxes)
                    textBox.Text = "";
            }
            catch { }
        }

        private void WndInfoESEK_FormClosing(object sender, FormClosingEventArgs e)
        {
            __lastRFID = "";
            this.Dispose(true);
        }

        private void btnActivateESEK_Click(object sender, EventArgs e)
        {
            btnActivateESEK.Enabled = false;
            WkrActivateESEKData wkrActivateESEKData = new WkrActivateESEKData();
            wkrActivateESEKData.URL = WndMain.BASE_URL;
            wkrActivateESEKData.api = "/api/cp/v1/accounts/";
            wkrActivateESEKData.accountId = __resultMediasDataByRFID.mediasDataByRFID.accountId.ToString() + "/medias/";
            wkrActivateESEKData.mediaId = __resultMediasDataByRFID.mediasDataByRFID.id.ToString() + "/states";
            wkrActivateESEKData.token = __mainWindow.GetAuthToken();
            __wkrActivateESEK.SetWorkerData(wkrActivateESEKData);

            if (!__wkrActivateESEK.GetWkrStatus())
            {
                __wkrActivateESEK.PerformRestOperation();
                tmrActivateESEK.Start();
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //btnBlockESEK.Enabled = false;
            WkrBlockESEKData wkrBlockESEKData = new WkrBlockESEKData();
            wkrBlockESEKData.URL = WndMain.BASE_URL;
            wkrBlockESEKData.api = "/api/cp/v1/accounts/";
            wkrBlockESEKData.accountId = __resultMediasDataByRFID.mediasDataByRFID.accountId.ToString() + "/medias/";
            wkrBlockESEKData.mediaId = __resultMediasDataByRFID.mediasDataByRFID.id.ToString() + "/states";
            wkrBlockESEKData.token = __mainWindow.GetAuthToken();
            __wkrBlockESEK.SetWorkerData(wkrBlockESEKData);

            if (!__wkrBlockESEK.GetWkrStatus())
            {
                __wkrBlockESEK.PerformRestOperation();
                tmrBlockESEK.Start();
            }
        }
    }
}
