﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RestSharp;
using RestSharp.Deserializers;
using System.Net;

namespace armoperator
{
    class WkrGetPersonByUUID
    {
        private BackgroundWorker RESTWkr = new BackgroundWorker();
        private ResultPersonDataByUUID m_resultREST = new ResultPersonDataByUUID();
        private WkrPersonByUUIDWorkerData m_workerData = new WkrPersonByUUIDWorkerData();

        public WkrGetPersonByUUID()
        {            
            RESTWkr.WorkerReportsProgress = true;
            RESTWkr.WorkerSupportsCancellation = true;
            RESTWkr.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RESTWkr_DoWork);
            RESTWkr.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RESTWkr_RunWorkerCompleted);
        }        

        private void RESTWkr_DoWork(object sender, DoWorkEventArgs e)
        {
            RestRequest wkrRequest = (RestRequest)e.Argument;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RESTWkrOperation(worker, e, wkrRequest);
        }

        private void RESTWkr_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_resultREST = (ResultPersonDataByUUID)e.Result;
        }

        private ResultPersonDataByUUID RESTWkrOperation(BackgroundWorker worker, DoWorkEventArgs e, RestRequest request)
        {
            ResultPersonDataByUUID resultRest = new ResultPersonDataByUUID();
            PersonDataByUUID personDataByUUID = new PersonDataByUUID();
            var client = new RestClient(m_workerData.URL);
            var response = client.Execute(request) as RestResponse;
            HttpStatusCode statusCode = response.StatusCode;
            resultRest.statusCode = (int)statusCode;
            JsonDeserializer deserMediaData = new JsonDeserializer();
            if (resultRest.statusCode.Equals(200))
            {
                try
                {
                    var JSONobj = deserMediaData.Deserialize<PersonDataByUUID>(response);
                    personDataByUUID.exemptions = JSONobj.exemptions;
                    personDataByUUID.id = JSONobj.id;
                    personDataByUUID.name = JSONobj.name;
                    personDataByUUID.snils = JSONobj.snils;
                    personDataByUUID.updatedAt = JSONobj.updatedAt;
                    personDataByUUID.uuid = JSONobj.uuid;
                }
                catch { }
            }
            resultRest.personDataByUUID = personDataByUUID;
            return resultRest;
        }

        public void PerformRestOperation()
        {
            var requestGetMediaData = new RestRequest(m_workerData.api + m_workerData.uuid, Method.GET);
            requestGetMediaData.AddHeader("Auth-Access-Token", m_workerData.token);
            RESTWkr.RunWorkerAsync(requestGetMediaData);
        }

        public void CancelRestOperation()
        {
            RESTWkr.CancelAsync();
        }

        public ResultPersonDataByUUID GetResultREST()
        {
            return m_resultREST;
        }

        public bool GetWkrStatus()
        {
            if (RESTWkr.IsBusy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetWorkerData(WkrPersonByUUIDWorkerData _workerData)
        {
            m_workerData = _workerData;
        }
    }

    public class ResultPersonDataByUUID
    {
        public PersonDataByUUID personDataByUUID { get; set; }
        public int statusCode { get; set; }
    }

    public class PersonDataByUUID
    {
        public List<List<List<String>>> exemptions { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string snils { get; set; }
        public int updatedAt { get; set; }
        public string uuid { get; set; }
    }

    public class Exemption
    {
        public String ouid { get; set; }
        public Timeframe timeframe { get; set; }
    }

    public class WkrPersonByUUIDWorkerData
    {
        public String URL { get;set; }
        public String api { get; set; }
        public String uuid { get;set; }
        public String token { get; set; }        
    }
}
