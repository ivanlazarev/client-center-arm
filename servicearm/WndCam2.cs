﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.Util;

namespace armoperator
{
    public partial class WndCam2 : Form
    {
        WndStatement __wndStatement;
        private Capture __capture = null;
        private bool __captureInProgress;

        public WndCam2(WndStatement wndStatement)
        {
            InitializeComponent();
            __wndStatement = wndStatement;
            captureButton.Enabled = false;

            //try
            //{
            //CvInvoke.UseOpenCL = false;
                try
                {
                    __capture = new Capture();
                    __capture.ImageGrabbed += ProcessFrame;
                }
                catch (NullReferenceException excpt)
                {
                    MessageBox.Show(excpt.Message);
                    this.Close();
                }
            //}
            //catch (Exception e)
            //{
            //    String ex = e.Message;
            //    MessageBox.Show("Отсутствует файл OpenCL.dll.");
            //    this.Close();
            //}
        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            try
            {
                Mat frame = new Mat();
                __capture.Retrieve(frame, 0);
                int width = frame.Bitmap.Width;
                int height = frame.Bitmap.Height;
                Bitmap resImage = frame.Bitmap.Clone(new Rectangle((int)((width - (height * 0.75)) / 2),
                                                                0,
                                                                (int)(height * 0.75),
                                                                height),
                                                                System.Drawing.Imaging.PixelFormat.DontCare);
                pictureBox1.Image = resImage;
                System.Threading.Thread.Sleep(100);
                if (null != frame)
                    frame.Dispose();
                if (null != resImage)
                    resImage.Dispose();
            }
            catch
            {
                MessageBox.Show("Ошибка драйвера камеры.");
            }

        }

        private void captureButton_Click(object sender, EventArgs e)
        {
            if (__capture != null)
            {
                if (__captureInProgress)
                {
                    __capture.Pause();
                    int count = 10;
                    while (0 != count)
                    {
                        try
                        {
                            pictureBox1.Image.Save(WndStatement.IMG_FILE_NAME, ImageFormat.Jpeg);
                            System.Threading.Thread.Sleep(300);
                            this.DialogResult = DialogResult.OK;
                            break;
                        }
                        catch
                        {
                            count--;
                        }
                    }

                    if (0 == count)
                    {
                        MessageBox.Show("Ошибка драйвера камеры. Повторите операцию.");
                    }           
                    this.Close();
                }
                else
                {
                    //start the capture
                    //captureButton.Text = "Stop";
                    //_capture.Start();
                }
                __captureInProgress = !__captureInProgress;
            }
        }

        private void ReleaseData()
        {
            if (__capture != null)
                __capture.Dispose();
        }

        private void WndCam2_FormClosing(object sender, FormClosingEventArgs e)
        {
            ReleaseData();
        }

        private void WndCam2_Load(object sender, EventArgs e)
        {
            timer1.Start();
            if (__capture != null)
            {
                if (!__captureInProgress)
                {
                    __capture.Start();
                }
            }
            __captureInProgress = !__captureInProgress;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {            
            if (null != pictureBox1.Image)
            {
                timer1.Stop();
                captureButton.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void WndCam2_FormClosed(object sender, FormClosedEventArgs e)
        {
            ReleaseData();
        }
    }
}
