﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Security.Cryptography;

namespace armoperator
{
    public partial class WndTicketESEK : Form
    {
        WndMain __mainWindow;

        private const int MINIMAL_YEAR = 2016;
        private const String SYMB_COMMON_PENS = "C-PRP";
        private const String SYMB_BUS_PENS = "B-PRP";
        private const String SYMB_TROLL_PENS = "T-PRP";
        private const String SYMB_COMMON_STUD = "C-PRS";
        private const String SYMB_BUS_STUD = "B-PRS";
        private const String SYMB_TROLL_STUD = "T-PRS";

        private static List<Label> __tbxs;
        private static List<Button> __ticketButtons;
        private static List<Button> __ticketCommonButtons;
        private static List<Button> __ticketPensButtons;
        private static List<Button> __ticketStudButtons;

        private static WkrReadCard __wkrReadCard;
        private static WkrReadOperationResults __wkrReadOperationResults;
        private static WkrGetMediasByRFID __wkrGetMediasByRFID;
        private static ResultMediasDataByRFID __resultMediasDataByRFID;
        private static WkrGetAccByAccountId __wkrGetAccByAccountId;
        private static ResultAccountDataByAccountId __resultAccountDataByAccountId;
        private static WkrGetPersonByUUID __wkrGetPersonByUUID;
        private static ResultPersonDataByUUID __resultPersonDataByUUID;
        List<Exemption> __personExemptions;
        private static WkrActivateESEK __wkrActivateESEK;
        private static ResultActivateESEK __resultActivateESEK;
        private static WkrTicket __wkrTicket;
        private static ResultTicket __resultTicket;

        private static WkrGetCurrencies __wkrGetCurrencies;
        private static ResultGetCurrencies __resultGetCurrencies;
        private static WkrGetBagsByAccId __wkrGetBagsByAccId;
        private static ResultGetBagsByAccId __resultGetBagsByAccId;
        private static WkrGetBagTransactions __wkrGetBagTransactions;
        private static ResultGetBagTransactions __resultGetBagTransactions;

        private static String __lastRFID;
        
        private static WndConfirmTicket wndConfirmTicket;

        private static List<Bags> __bagsForCheck;
        private static int __countBagsForCheck;

        private bool isCardActivated = false;

        public WndTicketESEK(WndMain mainWindow)
        {
            InitializeComponent();
            __mainWindow = mainWindow;
        }

        private void WndInfoESEK_Load(object sender, EventArgs e)
        {
            __tbxs = new System.Collections.Generic.List<Label>();
            __tbxs.Add(tbxCardRFID);
            __tbxs.Add(tbxCardNumber);
            __tbxs.Add(tbxStatus);
            __tbxs.Add(tbxPerson);
            __tbxs.Add(tbxSNILS);
            __tbxs.Add(tbxCardState);
            __tbxs.Add(tbxStatusREST);
            __tbxs.Add(tbxTicketResult);

            //foreach (var tbx in __tbxs)
            //{
            //    tbx.ReadOnly = true;
            //    tbx.BackColor = System.Drawing.SystemColors.Control;
            //}
            __ticketButtons = new System.Collections.Generic.List<Button>();
            __ticketButtons.Add(btnTraveBusPenslTicket);
            __ticketButtons.Add(btnTraveBusStudTicket);
            __ticketButtons.Add(btnTravelBusTicket);
            __ticketButtons.Add(btnTravelTicket);
            __ticketButtons.Add(btnTravelTrolleybusTicket);
            __ticketButtons.Add(btnTravePenslTicket);
            __ticketButtons.Add(btnTraveStudTicket);
            __ticketButtons.Add(btnTraveTrolleybusPenslTicket);
            __ticketButtons.Add(btnTraveTrolleybusStudTicket);

            __ticketCommonButtons = new System.Collections.Generic.List<Button>();
            __ticketCommonButtons.Add(btnTravelTicket);
            __ticketCommonButtons.Add(btnTravelBusTicket);
            __ticketCommonButtons.Add(btnTravelTrolleybusTicket);

            __ticketPensButtons = new System.Collections.Generic.List<Button>();
            __ticketPensButtons.Add(btnTravePenslTicket);
            __ticketPensButtons.Add(btnTraveBusPenslTicket);
            __ticketPensButtons.Add(btnTraveTrolleybusPenslTicket);

            __ticketStudButtons = new System.Collections.Generic.List<Button>();
            __ticketStudButtons.Add(btnTraveStudTicket);
            __ticketStudButtons.Add(btnTraveBusStudTicket);
            __ticketStudButtons.Add(btnTraveTrolleybusStudTicket);

            InitializeTmrReadCard();
            InitializeTmrGetMediaByRFID();
            InitializeTmrGetAccByAccountId();
            InitializeTmrGetPersonByUUID();
            InitializeTmrTicket();
            InitializeTmrGetCurrencies();
            InitializeTmrGetBagsByAccId();
            InitializeTmrGetBagTransactions();

            __wkrReadCard = new WkrReadCard(WndMain.READER_NAME);
            __wkrReadOperationResults = new WkrReadOperationResults();
            __wkrGetMediasByRFID = new WkrGetMediasByRFID();
            __resultMediasDataByRFID = new ResultMediasDataByRFID();
            __wkrGetAccByAccountId = new WkrGetAccByAccountId();
            __resultAccountDataByAccountId = new ResultAccountDataByAccountId();
            __wkrGetPersonByUUID = new WkrGetPersonByUUID();
            __resultPersonDataByUUID = new ResultPersonDataByUUID();
            __personExemptions = new List<Exemption>();
            __wkrActivateESEK = new WkrActivateESEK();
            __resultActivateESEK = new ResultActivateESEK();
            __wkrTicket = new WkrTicket();
            __resultTicket = new ResultTicket();

            __wkrGetCurrencies = new WkrGetCurrencies();
            __resultGetCurrencies = new ResultGetCurrencies();
            __wkrGetBagsByAccId = new WkrGetBagsByAccId();
            __resultGetBagsByAccId = new ResultGetBagsByAccId();
            __wkrGetBagTransactions = new WkrGetBagTransactions();
            __resultGetBagTransactions = new ResultGetBagTransactions();

            __lastRFID = "";

            __bagsForCheck = new List<Bags>();
            __countBagsForCheck = 0;

            this.lblTravelESEK.Text = "1. Гражданин приходит в центр обслуживания клиентов, передает карту оператору;\n\r\n\r" +
                                                  "2. Оператор проверяет соответствие фотографии на карте с личностью гражданина;\n\r\n\r" +
                                                  "3. Оператор размещает карту на считывающем NFC устройстве;\n\r\n\r" +
                                                  "4. Оператор проверяет возможность выдачи (покупки) льготного" +
                                                  " проездного билета на основании предъявленных документов;\n\r\n\r" +
                                                  "5. Оператор называет итоговую стоимость  проездного билета, принимает" +
                                                  " от гражданина денежные средства и выдает кассовый чек (кассовый аппарат" +
                                                  " не входит в оборудование Системы);\n\r\n\r" +
                                                  "6. Оператор, с использованием АРМ, выполняет запись о покупке " +
                                                  "месячного проездного билета в Систему;\n\r\n\r" +
                                                  "7. Оператор возвращает карту гражданину.";
            SetDateComboBoxes();

            //cbxSingleTicketValue.SelectedIndex = 0;

            DisableButtons(__ticketButtons);

            tmrReadCard.Start();
        }

        private void EnableButtons(List<Button> buttons)
        {
            foreach (var button in buttons)
            {
                button.Enabled = true;
            }
        }

        private void DisableButtons(List<Button> buttons)
        {
            foreach (var button in buttons)
            {
                button.Enabled = false;
            }
        }

        private void InitializeTmrReadCard()
        {
            tmrReadCard.Interval = 300;
            tmrReadCard.Tick += new EventHandler(tmrReadCard_Tick);
        }

        private void tmrReadCard_Tick(object Sender, EventArgs e)
        {
            bool readWkrStatus = __wkrReadCard.GetWkrStatus();
            if (!readWkrStatus)
            {
                __wkrReadOperationResults = __wkrReadCard.GetResultREST();
                int readStatus = __wkrReadOperationResults.readStatus;
                switch (readStatus)
                {
                    case 0:
                        try
                        {
                            String rfid = __wkrReadOperationResults.thrustCardInfo.uid;
                            if (null != rfid)
                            {
                                tbxCardRFID.Text = rfid;
                                tbxCardNumber.Text = __wkrReadOperationResults.thrustCardInfo.thrustCardInfoPart1.CardNumber;
                                tbxStatus.Text = "Данные карты считаны.";
                                if (__lastRFID != rfid)
                                {
                                    ClearTbxs(__tbxs);
                                    __personExemptions.Clear();
                                    DisableButtons(__ticketButtons);
                                    dataGridViewTickets.Rows.Clear();
                                    dataGridViewTickets.Refresh();

                                    __lastRFID = rfid;
                                    WkrGetMediasByRFIDData wkrGetMediasByRFIDData = new WkrGetMediasByRFIDData();
                                    wkrGetMediasByRFIDData.URL = WndMain.BASE_URL;
                                    wkrGetMediasByRFIDData.api = "/api/cp/v1/medias/";
                                    wkrGetMediasByRFIDData.rfid = rfid;
                                    wkrGetMediasByRFIDData.token = __mainWindow.GetAuthToken();
                                    __wkrGetMediasByRFID.SetWorkerData(wkrGetMediasByRFIDData);

                                    if (!__wkrGetMediasByRFID.GetWkrStatus())
                                    {
                                        __wkrGetMediasByRFID.PerformRestOperation();
                                        tmrGetMediaByRFID.Start();
                                    }
                                    tbxStatusREST.Text = "Запрос данных о карте держателя.";                                                                             
                                }
                            }
                        }
                        catch { }
                        break;
                    case 1:
                        tbxStatus.Text = "Неверный формат карты.";
                        break;
                    case 2:
                        tbxStatus.Text = "Приложите карту ещё раз.";
                        break;
                    case 3:
                        __lastRFID = "";

                        ClearTbxs(__tbxs);
                        __personExemptions.Clear();
                        DisableButtons(__ticketButtons);
                        dataGridViewTickets.Rows.Clear();
                        dataGridViewTickets.Refresh();

                        tbxStatus.Text = "Положите карту на считыватель.";
                        break;
                    case 4:
                        DisableButtons(__ticketButtons);
                        tbxStatus.Text = "Подключите считыватель.";
                        break;
                    case 5:
                        tbxStatus.Text = "Ошибка модели считывателя.";
                        break;
                    case 6:
                        tbxStatus.Text = "Неизвестная ошибка при чтении карты";
                        break;
                    case 7:
                        tbxStatus.Text = "Ошибка подключения к считывателю";
                        break;
                    default:
                        DisableButtons(__ticketButtons);
                        tbxStatus.Text = "Неизвестная ошибка";
                        break;
                }
                __wkrReadCard.PerformReadOperation();                
            }
        }

        private void InitializeTmrGetMediaByRFID()
        {
            tmrGetMediaByRFID.Interval = 100;
            tmrGetMediaByRFID.Tick += new EventHandler(tmrGetMediaByRFID_Tick);
        }

        private void tmrGetMediaByRFID_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetMediasByRFID.GetWkrStatus())
            {
                tmrGetMediaByRFID.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultMediasDataByRFID = __wkrGetMediasByRFID.GetResultREST();
                switch (__resultMediasDataByRFID.statusCode)
                {
                    case 200:
                        try
                        {
                            int accountId = __resultMediasDataByRFID.mediasDataByRFID.accountId;
                            String cardState = __resultMediasDataByRFID.mediasDataByRFID.states[0].state;
                            switch (cardState)
                            {
                                case  WndStatement.CARD_REGISTERED_STR:
                                    MessageBox.Show("Карта не активирована. Операция записи билетов недоступна.");
                                    tbxCardState.Text = "Карта зарегестрирована.";
                                    isCardActivated = false;
                                    break;
                                case WndStatement.CARD_ACTIVATED_STR:
                                    tbxCardState.Text = "Карта активирована.";
                                    EnableButtons(__ticketCommonButtons);
                                    isCardActivated = true;
                                    break;
                                case WndStatement.CARD_BLOCKED_STR:
                                    MessageBox.Show("Карта заблокирована. Операция записи билетов недоступна.");
                                    tbxCardState.Text = "Карта заблокирована.";
                                    isCardActivated = false;
                                    break;
                                default:
                                    break;
                            }

                            WkrGetAccByAccountIdData wkrGetAccByAccountIdData = new WkrGetAccByAccountIdData();
                            wkrGetAccByAccountIdData.URL = WndMain.BASE_URL;
                            wkrGetAccByAccountIdData.api = "/api/cp/v1/accounts/";
                            wkrGetAccByAccountIdData.accountId = Convert.ToString(accountId);
                            wkrGetAccByAccountIdData.token = __mainWindow.GetAuthToken();
                            __wkrGetAccByAccountId.SetWorkerData(wkrGetAccByAccountIdData);

                            if (!__wkrGetAccByAccountId.GetWkrStatus())
                            {
                                __wkrGetAccByAccountId.PerformRestOperation();
                                tmrGetAccByAccountId.Start();
                            }
                            tbxStatusREST.Text = "Запрос счета держателя карты.";
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("У данной карты отсутствует держатель.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetAccByAccountId()
        {
            tmrGetAccByAccountId.Interval = 100;
            tmrGetAccByAccountId.Tick += new EventHandler(tmrGetAccByAccountId_Tick);
        }

        private void tmrGetAccByAccountId_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetAccByAccountId.GetWkrStatus())
            {
                tmrGetAccByAccountId.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultAccountDataByAccountId = __wkrGetAccByAccountId.GetResultREST();
                switch (__resultAccountDataByAccountId.statusCode)
                {
                    case 200:
                        try
                        {
                            String userId = __resultAccountDataByAccountId.accountDataByAccountId.userId;

                            WkrPersonByUUIDWorkerData wkrPersonByUUIDWorkerData = new WkrPersonByUUIDWorkerData();
                            wkrPersonByUUIDWorkerData.URL = WndMain.BASE_URL;
                            wkrPersonByUUIDWorkerData.api = "/api/spd/v1/persons/";
                            wkrPersonByUUIDWorkerData.uuid = userId;
                            wkrPersonByUUIDWorkerData.token = __mainWindow.GetAuthToken();
                            __wkrGetPersonByUUID.SetWorkerData(wkrPersonByUUIDWorkerData);

                            if (!__wkrGetPersonByUUID.GetWkrStatus())
                            {
                                __wkrGetPersonByUUID.PerformRestOperation();
                                tmrGetPersonByUUID.Start();
                            }
                            tbxStatusREST.Text = "Запрос сведений о держателе карты.";
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetPersonByUUID()
        {
            tmrGetPersonByUUID.Interval = 100;
            tmrGetPersonByUUID.Tick += new EventHandler(tmrGetPersonByUUID_Tick);
        }

        private void tmrGetPersonByUUID_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetPersonByUUID.GetWkrStatus())
            {
                tmrGetPersonByUUID.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultPersonDataByUUID = __wkrGetPersonByUUID.GetResultREST();
                switch (__resultPersonDataByUUID.statusCode)
                {
                    case 200:
                        try
                        {
                            String personInfo = __resultPersonDataByUUID.personDataByUUID.name;
                            String snilsInfo = __resultPersonDataByUUID.personDataByUUID.snils;

                            try
                            {
                                if (0 != __resultPersonDataByUUID.personDataByUUID.exemptions.Count)
                                {
                                    if (0 != __resultPersonDataByUUID.personDataByUUID.exemptions[0].Count)
                                    {
                                        foreach (var element in __resultPersonDataByUUID.personDataByUUID.exemptions[0])
                                        {
                                            Exemption ext = new Exemption();
                                            ext = JsonConvert.DeserializeObject<Exemption>(element[0]);
                                            __personExemptions.Add(ext);
                                        }
                                    }
                                    else
                                    {
                                        //tbxStatusREST.Text = "Данные о держателе карты получены. Сведения о льготах отсутствуют.";
                                    }
                                }
                                else
                                {
                                    //rtbxLogShowDataESEK.AppendText("Сведения о льготах отсутствуют.\n\r");
                                }
                            }
                            catch { }

                            tbxPerson.Text = personInfo;
                            tbxSNILS.Text = snilsInfo;
                            tbxStatusREST.Text = "Данные о держателе карты получены.";

                            //отсюда запрос списка валют и кошельков
                            WkrGetCurrenciesData wkrGetCurrenciesData = new WkrGetCurrenciesData();
                            wkrGetCurrenciesData.URL = WndMain.BASE_URL;
                            wkrGetCurrenciesData.api = "/api/cp/v1/applications/";
                            wkrGetCurrenciesData.applicationId = __mainWindow.GetApplicationId().ToString() + "/currencies";
                            wkrGetCurrenciesData.token = __mainWindow.GetAuthToken();
                            __wkrGetCurrencies.SetWorkerData(wkrGetCurrenciesData);

                            if (!__wkrGetCurrencies.GetWkrStatus())
                            {
                                __wkrGetCurrencies.PerformRestOperation();
                                tmrGetCurrencies.Start();
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetCurrencies()
        {
            tmrGetCurrencies.Interval = 100;
            tmrGetCurrencies.Tick += new EventHandler(tmrGetCurrencies_Tick);
        }

        private void tmrGetCurrencies_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetCurrencies.GetWkrStatus())
            {
                tmrGetCurrencies.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultGetCurrencies = __wkrGetCurrencies.GetResultREST();
                switch (__resultGetCurrencies.statusCode)
                {
                    case 200:
                        try
                        {
                            if (0 != __personExemptions.Count)
                            {
                                foreach (var exemption in __personExemptions)
                                {
                                    foreach (var currency in __resultGetCurrencies.currenciesData)
                                    {
                                        if (exemption.ouid == currency.exemption)
                                        {
                                            switch (currency.symbol)
                                            {
                                                case SYMB_COMMON_PENS:
                                                case SYMB_BUS_PENS:
                                                case SYMB_TROLL_PENS:
                                                    //enable pens buttons
                                                    if (isCardActivated)
                                                        EnableButtons(__ticketPensButtons);
                                                    break;
                                                case SYMB_COMMON_STUD:
                                                case SYMB_BUS_STUD:
                                                case SYMB_TROLL_STUD:
                                                    //enable stud buttons
                                                    if (isCardActivated)
                                                        EnableButtons(__ticketStudButtons);
                                                    break;
                                                default:
                                                    //disable pens and stud buttons
                                                    DisableButtons(__ticketPensButtons);
                                                    DisableButtons(__ticketStudButtons);
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }

                            WkrGetBagsByAccIdData wkrGetBagsByAccIdData = new WkrGetBagsByAccIdData();
                            wkrGetBagsByAccIdData.api = "/api/cp/v1/accounts/";
                            wkrGetBagsByAccIdData.accountId = __resultMediasDataByRFID.mediasDataByRFID.accountId.ToString() + "/bags";
                            wkrGetBagsByAccIdData.URL = WndMain.BASE_URL;
                            wkrGetBagsByAccIdData.token = __mainWindow.GetAuthToken();
                            __wkrGetBagsByAccId.SetWorkerData(wkrGetBagsByAccIdData);
                            if (!__wkrGetBagsByAccId.GetWkrStatus())
                            {
                                __wkrGetBagsByAccId.PerformRestOperation();
                                tmrGetBagsByAccId.Start();
                            }
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Приложение не найдено.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetBagsByAccId()
        {
            tmrGetBagsByAccId.Interval = 100;
            tmrGetBagsByAccId.Tick += new EventHandler(tmrGetBagsByAccId_Tick);
        }

        private void tmrGetBagsByAccId_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetBagsByAccId.GetWkrStatus())
            {
                tmrGetBagsByAccId.Stop();
                __bagsForCheck.Clear();
                __countBagsForCheck = 0;
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultGetBagsByAccId = __wkrGetBagsByAccId.GetResultREST();
                switch (__resultGetBagsByAccId.statusCode)
                {
                    case 200:
                        try
                        {
                            if (0 != __resultGetBagsByAccId.bagsData.Count)
                            {
                                foreach (var bag in __resultGetBagsByAccId.bagsData)
                                {
                                    int bagCurrencyId = bag.currencyId;
                                    foreach (var currency in __resultGetCurrencies.currenciesData)
                                    {
                                        if (bagCurrencyId == currency.id && currency.countable)
                                        {
                                            __bagsForCheck.Add(bag);
                                            __countBagsForCheck++;
                                        }
                                        else if (bagCurrencyId == currency.id)
                                        {
                                            if (null == bag.timeframe.finishTimestamp)
                                            {
                                                dataGridViewTickets.Rows.Add(new object[] { currency.title,
                                                                                            "-",
                                                                                            "бессрочный"});
                                            }
                                            else
                                            {
                                                dataGridViewTickets.Rows.Add(new object[] { currency.title,
                                                                                    "-",
                                                                                    "по " + WndInfoESEK.UnixTimestampToDateTime(Convert.ToDouble(bag.timeframe.finishTimestamp)).AddDays(-1).ToLongDateString()});

                                            }
                                            //dataGridViewTickets.Rows.Add(new object[] { currency.title, 
                                            //                                        "-",
                                            //                                        "по " + WndInfoESEK.UnixTimestampToDateTime(Convert.ToDouble(bag.timeframe.finishTimestamp)).AddDays(-1).ToLongDateString()});
                                        }
                                    }
                                }

                                if (0 != __countBagsForCheck)
                                {
                                    tmrCheckBags.Start();
                                }
                            }
                            else
                            {
                                tbxStatusREST.Text = "Данные о держателе карты получены.";
                            }
                        }
                        catch { MessageBox.Show("Ошибка при получении данных о кошельках."); }
                        break;
                    case 404:
                        MessageBox.Show("Кошельки отсутствуют.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }

        private void InitializeTmrGetBagTransactions()
        {
            tmrGetBagTransactions.Interval = 100;
            tmrGetBagTransactions.Tick += new EventHandler(tmrGetBagTransactions_Tick);
        }

        private void tmrGetBagTransactions_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrGetBagTransactions.GetWkrStatus())
            {
                tmrGetBagTransactions.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultGetBagTransactions = __wkrGetBagTransactions.GetResultREST();
                switch (__resultGetBagTransactions.statusCode)
                {
                    case 200:
                        try
                        {
                            if (0 != __resultGetBagTransactions.transactionsData.Count)
                            {
                                int ballValue = 0;
                                foreach (var transaction in __resultGetBagTransactions.transactionsData)
                                {
                                    switch (transaction.type)
                                    {
                                        case "replenishment":
                                            ballValue += transaction.value;
                                            break;
                                        case "payment":
                                            ballValue -= transaction.value;
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                String curencyTitle = "";
                                String currencySymbol = "";
                                foreach (var currency in __resultGetCurrencies.currenciesData)
                                {
                                    if (__resultGetBagTransactions.transactionsData[0].currencyId == currency.id)
                                    {
                                        curencyTitle = currency.title;
                                        currencySymbol = currency.symbol;
                                        break;
                                    }
                                }
                                if (currencySymbol != WndInfoESEK.TRANSPORT_BALL)
                                {
                                    tbxStatusREST.Text = "Данные о держателе карты получены.";
                                    if (null == __bagsForCheck[__countBagsForCheck].timeframe.finishTimestamp)
                                    {
                                        dataGridViewTickets.Rows.Add(new object[] { curencyTitle,
                                                                                    ballValue.ToString(),
                                                                                    "бессрочный"});
                                    }
                                    else
                                    {
                                        dataGridViewTickets.Rows.Add(new object[] { curencyTitle,
                                                                                    ballValue.ToString(),
                                                                                    "по " + WndInfoESEK.UnixTimestampToDateTime(Convert.ToDouble(__bagsForCheck[__countBagsForCheck].timeframe.finishTimestamp)).AddDays(-1).ToLongDateString()});
                                    }
                                    //dataGridViewTickets.Rows.Add(new object[] { curencyTitle, 
                                    //                                                ballValue.ToString(),
                                    //                                                "по " + WndInfoESEK.UnixTimestampToDateTime(Convert.ToDouble(__bagsForCheck[__countBagsForCheck].timeframe.finishTimestamp)).AddDays(-1).ToLongDateString()});
                                }
                            }
                            else
                            {
                                //tbxBallValue.Text = "Транзакции не проводились.";
                            }
                        }
                        catch { MessageBox.Show("Ошибка при проверке транзакций кошелька"); }
                        break;
                    case 404:
                        MessageBox.Show("Транзакции отсутствуют.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Ошибка связи с сервером.");
                        break;
                }
            }
        }  

        private void InitializeTmrTicket()
        {
            tmrTicket.Interval = 100;
            tmrTicket.Tick += new EventHandler(tmrTicket_Tick);
        }

        private void tmrTicket_Tick(object Sender, EventArgs e)
        {
            WndActivateESEK.StatusLabelBlink(restResponceStatus, WndActivateESEK.REST_RESPONSE_SENT);
            if (!__wkrTicket.GetWkrStatus())
            {
                tmrTicket.Stop();
                WndActivateESEK.StatusLabelFinish(restResponceStatus, WndActivateESEK.REST_RESPONSE_GET);
                __resultTicket = __wkrTicket.GetResultREST();
                switch (__resultTicket.statusCode)
                {
                    case 200:
                        try
                        {
                            tbxTicketResult.Text = "Билет успешно записан";
                            //dataGridViewTickets.Rows.Clear();
                            //dataGridViewTickets.Refresh();
                            //if (!__wkrGetMediasByRFID.GetWkrStatus())
                            //{
                            //    __wkrGetMediasByRFID.PerformRestOperation();
                            //    tmrGetMediaByRFID.Start();                                
                            //}
                            //tbxStatusREST.Text = "Данные о держателе карты получены.";
                        }
                        catch { }
                        break;
                    case 404:
                        MessageBox.Show("Аккаунт отсутствует.");
                        break;
                    case 401:
                        if (!__mainWindow.isAuthWindowActive)
                        {
                            MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                            __mainWindow.Authorize();
                        }
                        //MessageBox.Show("Авторизуйтесь и приложите карту заново.");
                        //WndAuth authWnd = new WndAuth(__mainWindow);
                        //authWnd.ShowDialog();
                        break;
                    case 409:
                        MessageBox.Show("Такой билет уже записан.");
                        //EnableButtons(__ticketButtons);
                        break;
                    case 500:
                        MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                        break;
                    default:
                        MessageBox.Show("Конфликт с сервером. Такой билет записать нельзя.");
                        //EnableButtons(__ticketButtons);
                        break;
                }

                dataGridViewTickets.Rows.Clear();
                dataGridViewTickets.Refresh();
                if (!__wkrGetMediasByRFID.GetWkrStatus())
                {
                    __wkrGetMediasByRFID.PerformRestOperation();
                    tmrGetMediaByRFID.Start();
                }
                tbxStatusREST.Text = "Данные о держателе карты получены.";

            }
        }  

        private void SendTicket(String symbol, bool isSingle)
        {
            try
            {
                DateTime baseDate = new DateTime(1970, 01, 01);
                DateTime date1, date2, timeStampNow;
                TimeSpan ts;
                UInt64 totalsecondsStart = 0, totalsecondsEnd=0, totalsecondsTimeStampNow = 0;

                WkrTicketData wkrTicketData = new WkrTicketData();
                wkrTicketData.URL = WndMain.BASE_URL;
                wkrTicketData.api = "/api/cp/v1/replenishments";

                if (!isSingle)
                {
                    date1 = new DateTime(cbxYear.SelectedIndex + MINIMAL_YEAR, cbxMonth.SelectedIndex + 1, 1);
                    ts = date1 - baseDate;                    
                    totalsecondsStart = Convert.ToUInt64(ts.TotalSeconds);
                    date2 = date1.AddMonths(1);
                    //date2 = date2.AddDays(-1);
                    ts = date2 - baseDate;
                    totalsecondsEnd = Convert.ToUInt64(ts.TotalSeconds);
                }
                else
                {
                    date1 = DateTime.UtcNow;
                    DateTime today = new DateTime(date1.Year, date1.Month, date1.Day);
                    ts = today - baseDate;
                    //ts = date1 - baseDate;
                    totalsecondsStart = Convert.ToUInt64(ts.TotalSeconds);
                    date2 = today.AddMonths(6);
                    //date2 = date1.AddMonths(6);
                    ts = date2 - baseDate;
                    totalsecondsEnd = Convert.ToUInt64(ts.TotalSeconds);
                }

                timeStampNow = DateTime.UtcNow;
                ts = timeStampNow - baseDate;
                totalsecondsTimeStampNow = Convert.ToUInt64(ts.TotalSeconds);
                
                var ticketPayload = new TicketPayload
                {
                    symbol = symbol,
                    rfid = __wkrReadOperationResults.thrustCardInfo.uid,
                    timestamp = totalsecondsTimeStampNow,
                    value = 1,//Convert.ToInt16(cbxSingleTicketValue.Text),
                    providerId = __mainWindow.GetProviderId(),
                    timeframe = new Timeframe
                    {
                        startTimestamp = totalsecondsStart,
                        finishTimestamp = totalsecondsEnd
                    },
                    source = new Source
                    {
                        order = Guid.NewGuid().ToString(),
                        terminal = "1"
                    }
                };

                wkrTicketData.ticketPayload = ticketPayload;
                wkrTicketData.token = __mainWindow.GetAuthToken();
                __wkrTicket.SetWorkerData(wkrTicketData);
                if (!__wkrTicket.GetWkrStatus())
                {
                    __wkrTicket.PerformRestOperation();
                    tmrTicket.Start();
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
            DisableButtons(__ticketButtons);
        }

        private void btnTravelTicket_Click(object sender, EventArgs e)
        {
            wndConfirmTicket = new WndConfirmTicket();
            wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, "Проездной (единый)", cbxMonth.Text + " " + cbxYear.Text);
            wndConfirmTicket.ShowDialog();
            if (DialogResult.OK == wndConfirmTicket.DialogResult)
            {
                SendTicket("C-PR", false);
            }
        }

        private void btnTravelBusTicket_Click(object sender, EventArgs e)
        {
            wndConfirmTicket = new WndConfirmTicket();
            wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, "Проездной (автобус)", cbxMonth.Text + " " + cbxYear.Text);
            wndConfirmTicket.ShowDialog();
            if (DialogResult.OK == wndConfirmTicket.DialogResult)
            {
                SendTicket("B-PR", false);
            }
        }

        private void btnTravelTrolleybusTicket_Click(object sender, EventArgs e)
        {
            wndConfirmTicket = new WndConfirmTicket();
            wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, "Проездной (троллейбус)", cbxMonth.Text + " " + cbxYear.Text);
            wndConfirmTicket.ShowDialog();
            if (DialogResult.OK == wndConfirmTicket.DialogResult)
            {
                SendTicket("T-PR", false);
            }
        }

        private void btnTravePenslTicket_Click(object sender, EventArgs e)
        {
            wndConfirmTicket = new WndConfirmTicket();
            wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, "Единый пенсионный", cbxMonth.Text + " " + cbxYear.Text);
            wndConfirmTicket.ShowDialog();
            if (DialogResult.OK == wndConfirmTicket.DialogResult)
            {
                SendTicket(SYMB_COMMON_PENS, false);
            }
        }

        private void btnTraveBusPenslTicket_Click(object sender, EventArgs e)
        {
            wndConfirmTicket = new WndConfirmTicket();
            wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, "Пенсионный (автобус)", cbxMonth.Text + " " + cbxYear.Text);
            wndConfirmTicket.ShowDialog();
            if (DialogResult.OK == wndConfirmTicket.DialogResult)
            {
                SendTicket(SYMB_BUS_PENS, false);
            }
        }

        private void btnTraveTrolleybusPenslTicket_Click(object sender, EventArgs e)
        {
            wndConfirmTicket = new WndConfirmTicket();
            wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, "Пенсионный (троллейбус)", cbxMonth.Text + " " + cbxYear.Text);
            wndConfirmTicket.ShowDialog();
            if (DialogResult.OK == wndConfirmTicket.DialogResult)
            {
                SendTicket(SYMB_TROLL_PENS, false);
            }
        }

        private void btnTraveStudTicket_Click(object sender, EventArgs e)
        {
            wndConfirmTicket = new WndConfirmTicket();
            wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, "Единый студенческий", cbxMonth.Text + " " + cbxYear.Text);
            wndConfirmTicket.ShowDialog();
            if (DialogResult.OK == wndConfirmTicket.DialogResult)
            {
                SendTicket(SYMB_COMMON_STUD, false);
            }
        }

        private void btnTraveBusStudTicket_Click(object sender, EventArgs e)
        {
            wndConfirmTicket = new WndConfirmTicket();
            wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, "Студенческий (автобус)", cbxMonth.Text + " " + cbxYear.Text);
            wndConfirmTicket.ShowDialog();
            if (DialogResult.OK == wndConfirmTicket.DialogResult)
            {
                SendTicket(SYMB_BUS_STUD, false);
            }
        }

        private void btnTraveTrolleybusStudTicket_Click(object sender, EventArgs e)
        {
            wndConfirmTicket = new WndConfirmTicket();
            wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, "Студенческий (троллейбус)", cbxMonth.Text + " " + cbxYear.Text);
            wndConfirmTicket.ShowDialog();
            if (DialogResult.OK == wndConfirmTicket.DialogResult)
            {
                SendTicket(SYMB_TROLL_STUD, false);
            }
        }

        private void btnSingleTicket_Click(object sender, EventArgs e)
        {
            //wndConfirmTicket = new WndConfirmTicket();
            //wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, cbxSingleTicketValue.Text + " разовых поездок (единый)", "6 месяцев");
            //wndConfirmTicket.ShowDialog();
            //if (DialogResult.OK == wndConfirmTicket.DialogResult)
            //{
            //    SendTicket("C-PFTT", true);
            //}
        }

        private void btnSingleTicketBus_Click(object sender, EventArgs e)
        {
            //wndConfirmTicket = new WndConfirmTicket();
            //wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, cbxSingleTicketValue.Text + " разовых поездок (автобус)", "6 месяцев");
            //wndConfirmTicket.ShowDialog();
            //if (DialogResult.OK == wndConfirmTicket.DialogResult)
            //{
            //    SendTicket("B-PFTT", true);
            //}
        }

        private void btnSingleTicketTrolleybus_Click(object sender, EventArgs e)
        {
            //wndConfirmTicket = new WndConfirmTicket();
            //wndConfirmTicket.SetTextBoxes(tbxPerson.Text, tbxCardNumber.Text, cbxSingleTicketValue.Text + " разовых поездок (троллейбус)", "6 месяцев");
            //wndConfirmTicket.ShowDialog();
            //if (DialogResult.OK == wndConfirmTicket.DialogResult)
            //{
            //    SendTicket("T-PFTT", true);
            //}
        }

        private void ClearTbxs(List<Label> textBoxes)
        {
            try
            {
                foreach (var textBox in textBoxes)
                    textBox.Text = "";
            }
            catch { }
        }

        private void WndInfoESEK_FormClosing(object sender, FormClosingEventArgs e)
        {
            __lastRFID = "";
            this.Dispose(true);
        }

        private void cbxMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnDateUnderCurrent();
        }

        private void cbxYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnDateUnderCurrent();
        }

        private void SetDateComboBoxes()
        {
            cbxMonth.SelectedIndex = DateTime.Now.Month - 1;
            cbxYear.SelectedIndex = DateTime.Now.Year - MINIMAL_YEAR;
        }

        private void OnDateUnderCurrent()
        {
            if (cbxYear.SelectedIndex <= DateTime.Now.Year - MINIMAL_YEAR)
            {
                if (cbxMonth.SelectedIndex < DateTime.Now.Month - 1)
                {
                    MessageBox.Show("Выбранная дата меньше текущей");
                    SetDateComboBoxes();
                }
            }
        }

        private void tmrCheckBags_Tick(object sender, EventArgs e)
        {
            if (0 != __countBagsForCheck)
            {
                bool getBagTransactionsWkrStatus = __wkrGetBagTransactions.GetWkrStatus();
                if (!getBagTransactionsWkrStatus && !tmrGetBagTransactions.Enabled)
                {
                    try
                    {
                        WkrGetBagTransactionsData wkrGetBagTransactionsData = new WkrGetBagTransactionsData();
                        wkrGetBagTransactionsData.URL = WndMain.BASE_URL;
                        wkrGetBagTransactionsData.api = "/api/cp/v1/accounts/";
                        wkrGetBagTransactionsData.accountId = __bagsForCheck[__countBagsForCheck - 1].accountId.ToString() + "/bags/";
                        wkrGetBagTransactionsData.bagId = __bagsForCheck[__countBagsForCheck - 1].id.ToString() + "/transactions";
                        wkrGetBagTransactionsData.token = __mainWindow.GetAuthToken();
                        __wkrGetBagTransactions.SetWorkerData(wkrGetBagTransactionsData);
                        __wkrGetBagTransactions.PerformRestOperation();
                        tmrGetBagTransactions.Start();
                        __countBagsForCheck--;
                        //__countHistory += __countBagsForCheck.ToString() + ",";
                    }
                    catch { MessageBox.Show("Ошибка проверки кошелька"); }
                }
            }
            else
            {
                tmrCheckBags.Stop();
            }
        }
    }
}
