﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Net;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using Newtonsoft.Json;

namespace armoperator
{
    public partial class WndAuth : Form
    {
        private const string API_AUTH = "/api/auth/v1/login";
        public string authToken { get; set; }
        public string operatorCode { get; set; }
        private WndMain __mainWindow;

        public WndAuth(WndMain mainWindow)
        {            
            InitializeComponent();
            __mainWindow = mainWindow;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            await DoAuthAsync();
        }

        private async Task DoAuthAsync()
        {
            this.button1.Enabled = false;
            tbxOperatorCode.Enabled = false;
            tbxPass.Enabled = false;
            await Task.Run(() =>
                {
                    var request = new RestRequest(API_AUTH, Method.POST);
                    request.RequestFormat = DataFormat.Json;
                    var client = new RestClient(WndMain.BASE_URL);
                    client.Authenticator = new HttpBasicAuthenticator(tbxOperatorCode.Text, tbxPass.Text);
                    var response = client.Execute(request) as RestResponse;
                    HttpStatusCode statusCode = response.StatusCode;
                    
                    switch ((int)statusCode)
                    {
                        case 200:
                            try
                            {
                                var deserMediaData = new JsonDeserializer();
                                var authData = deserMediaData.Deserialize<Auth>(response);
                                if ("" != authData.key)
                                {
                                    this.authToken = authData.key;
                                    this.operatorCode = tbxOperatorCode.Text;
                                    this.DialogResult = DialogResult.OK;
                                    this.Close();
                                }
                                else
                                {
                                    MessageBox.Show("Неправильный ответ сервера.");
                                }
                            }
                            catch { }
                            break;
                        case 401:
                            MessageBox.Show("Ошибка авторизации.");
                            break;
                        case 500:
                            MessageBox.Show("Превышено время ожидания. Внутренняя ошибка сервера.");
                            break;
                        default:
                            MessageBox.Show("Неизвестная ошибка авторизации.");
                            break;
                    }  
                });
            this.button1.Enabled = true;
            tbxOperatorCode.Enabled = true;
            tbxPass.Enabled = true;
        }

        private void WndAuth_Load(object sender, EventArgs e)
        {
            this.ActiveControl = tbxOperatorCode;
            this.authToken = "";
            this.operatorCode = "";
        }

        private void WndAuth_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose(true);
        }

        private void tbxPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if ("" == this.authToken)
            {
                DialogResult dialogResult = MessageBox.Show("Невозможна работа с \"АРМ пункта обслуживания\" без авторизации. Приложение будет закрыто.",
                                            "Отказ от авторизации",
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Warning);
                switch (dialogResult)
                {
                    case DialogResult.Yes:
                        __mainWindow.Close();
                        break;
                    case DialogResult.No:
                        break;
                }
            }
        }

        private void tbxOperatorCode_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }

    public class Auth
    {
        public Credential credential { get; set; }
        public string key { get; set; }
        public int expirationTimestamp { get; set; }
    }

    public class Credential
    {
        public int id { get; set; }
        public string login { get; set; }
        public List<int> roleIds { get; set; }
        public object meta { get; set; }
    }
}

