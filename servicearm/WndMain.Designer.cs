﻿namespace armoperator
{
    partial class WndMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WndMain));
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnOnceTravelESEK = new System.Windows.Forms.Button();
            this.btnBlocking = new System.Windows.Forms.Button();
            this.btnTravelESEK = new System.Windows.Forms.Button();
            this.btnRequestESEK = new System.Windows.Forms.Button();
            this.btnPageActivateESEK = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnUnblocking = new System.Windows.Forms.Button();
            this.lblOperatorLogin = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.readWkr = new System.ComponentModel.BackgroundWorker();
            this.RESTWkr = new System.ComponentModel.BackgroundWorker();
            this.tmrGetAppData = new System.Windows.Forms.Timer(this.components);
            this.tmrGetAppProviderData = new System.Windows.Forms.Timer(this.components);
            this.tmrAuth = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.Controls.Add(this.btnLogin, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnSettings, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblOperatorLogin, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(884, 562);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // btnLogin
            // 
            this.btnLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnLogin.Image = global::armoperator.Properties.Resources.direction__1_;
            this.btnLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogin.Location = new System.Drawing.Point(767, 3);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(114, 34);
            this.btnLogin.TabIndex = 6;
            this.btnLogin.Text = "Выход";
            this.btnLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnSettings.Image")));
            this.btnSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSettings.Location = new System.Drawing.Point(647, 3);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(114, 34);
            this.btnSettings.TabIndex = 7;
            this.btnSettings.Text = "Настройки";
            this.btnSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel1, 5);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btnOnceTravelESEK, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnBlocking, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnTravelESEK, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnRequestESEK, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnPageActivateESEK, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.button4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnUnblocking, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 43);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(878, 516);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnOnceTravelESEK
            // 
            this.btnOnceTravelESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOnceTravelESEK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOnceTravelESEK.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnOnceTravelESEK.Image = ((System.Drawing.Image)(resources.GetObject("btnOnceTravelESEK.Image")));
            this.btnOnceTravelESEK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOnceTravelESEK.Location = new System.Drawing.Point(3, 390);
            this.btnOnceTravelESEK.Name = "btnOnceTravelESEK";
            this.btnOnceTravelESEK.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnOnceTravelESEK.Size = new System.Drawing.Size(433, 123);
            this.btnOnceTravelESEK.TabIndex = 2;
            this.btnOnceTravelESEK.Text = "Просмотр данных ЕСЭК";
            this.btnOnceTravelESEK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOnceTravelESEK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOnceTravelESEK.UseVisualStyleBackColor = true;
            this.btnOnceTravelESEK.Click += new System.EventHandler(this.btnOnceTravelESEK_Click);
            // 
            // btnBlocking
            // 
            this.btnBlocking.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBlocking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBlocking.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnBlocking.Image = global::armoperator.Properties.Resources.locked__1_;
            this.btnBlocking.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBlocking.Location = new System.Drawing.Point(442, 261);
            this.btnBlocking.Name = "btnBlocking";
            this.btnBlocking.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnBlocking.Size = new System.Drawing.Size(433, 123);
            this.btnBlocking.TabIndex = 5;
            this.btnBlocking.Text = "Разблокирование ЕСЭК";
            this.btnBlocking.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBlocking.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBlocking.UseVisualStyleBackColor = true;
            this.btnBlocking.Click += new System.EventHandler(this.btnBlocking_Click);
            // 
            // btnTravelESEK
            // 
            this.btnTravelESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTravelESEK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTravelESEK.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnTravelESEK.Image = ((System.Drawing.Image)(resources.GetObject("btnTravelESEK.Image")));
            this.btnTravelESEK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTravelESEK.Location = new System.Drawing.Point(3, 261);
            this.btnTravelESEK.Name = "btnTravelESEK";
            this.btnTravelESEK.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnTravelESEK.Size = new System.Drawing.Size(433, 123);
            this.btnTravelESEK.TabIndex = 1;
            this.btnTravelESEK.Text = "Запись проездного билета на ЕСЭК";
            this.btnTravelESEK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTravelESEK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTravelESEK.UseVisualStyleBackColor = true;
            this.btnTravelESEK.Click += new System.EventHandler(this.btnTravelESEK_Click);
            // 
            // btnRequestESEK
            // 
            this.btnRequestESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRequestESEK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRequestESEK.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnRequestESEK.Image = ((System.Drawing.Image)(resources.GetObject("btnRequestESEK.Image")));
            this.btnRequestESEK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRequestESEK.Location = new System.Drawing.Point(3, 132);
            this.btnRequestESEK.Name = "btnRequestESEK";
            this.btnRequestESEK.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnRequestESEK.Size = new System.Drawing.Size(433, 123);
            this.btnRequestESEK.TabIndex = 0;
            this.btnRequestESEK.Text = "Формирование заявлений по ЕСЭК";
            this.btnRequestESEK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRequestESEK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRequestESEK.UseVisualStyleBackColor = true;
            this.btnRequestESEK.Click += new System.EventHandler(this.btnRequestESEK_Click);
            // 
            // btnPageActivateESEK
            // 
            this.btnPageActivateESEK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPageActivateESEK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPageActivateESEK.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPageActivateESEK.Image = ((System.Drawing.Image)(resources.GetObject("btnPageActivateESEK.Image")));
            this.btnPageActivateESEK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPageActivateESEK.Location = new System.Drawing.Point(442, 132);
            this.btnPageActivateESEK.Name = "btnPageActivateESEK";
            this.btnPageActivateESEK.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnPageActivateESEK.Size = new System.Drawing.Size(433, 123);
            this.btnPageActivateESEK.TabIndex = 3;
            this.btnPageActivateESEK.Text = "Активация и выдача ЕСЭК";
            this.btnPageActivateESEK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPageActivateESEK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPageActivateESEK.UseVisualStyleBackColor = true;
            this.btnPageActivateESEK.Click += new System.EventHandler(this.btnActivateESEK_Click);
            // 
            // button4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.button4, 2);
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.Location = new System.Drawing.Point(3, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(872, 123);
            this.button4.TabIndex = 7;
            this.button4.TabStop = false;
            this.button4.Text = "Выбор сценария использования";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = true;
            // 
            // btnUnblocking
            // 
            this.btnUnblocking.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUnblocking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnblocking.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnUnblocking.Image = global::armoperator.Properties.Resources.security;
            this.btnUnblocking.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUnblocking.Location = new System.Drawing.Point(442, 390);
            this.btnUnblocking.Name = "btnUnblocking";
            this.btnUnblocking.Size = new System.Drawing.Size(433, 123);
            this.btnUnblocking.TabIndex = 8;
            this.btnUnblocking.Text = "Блокирование ЕСЭК";
            this.btnUnblocking.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUnblocking.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUnblocking.UseVisualStyleBackColor = true;
            this.btnUnblocking.Click += new System.EventHandler(this.btnUnblocking_Click);
            // 
            // lblOperatorLogin
            // 
            this.lblOperatorLogin.AutoSize = true;
            this.lblOperatorLogin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblOperatorLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOperatorLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblOperatorLogin.Location = new System.Drawing.Point(577, 0);
            this.lblOperatorLogin.Name = "lblOperatorLogin";
            this.lblOperatorLogin.Size = new System.Drawing.Size(64, 40);
            this.lblOperatorLogin.TabIndex = 9;
            this.lblOperatorLogin.Text = "000000";
            this.lblOperatorLogin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(492, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 40);
            this.label2.TabIndex = 10;
            this.label2.Text = "Оператор:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // WndMain
            // 
            this.ClientSize = new System.Drawing.Size(884, 562);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WndMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "\"АРМ пункта обслуживания\"";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnLogin;
        private System.ComponentModel.BackgroundWorker readWkr;
        private System.ComponentModel.BackgroundWorker RESTWkr;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Timer tmrGetAppData;
        private System.Windows.Forms.Timer tmrGetAppProviderData;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnOnceTravelESEK;
        private System.Windows.Forms.Button btnBlocking;
        private System.Windows.Forms.Button btnTravelESEK;
        private System.Windows.Forms.Button btnRequestESEK;
        private System.Windows.Forms.Button btnPageActivateESEK;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Timer tmrAuth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblOperatorLogin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUnblocking;

    }
}

